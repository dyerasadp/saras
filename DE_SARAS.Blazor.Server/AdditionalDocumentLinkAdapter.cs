﻿using DevExpress.ExpressApp.Blazor.Components;
using DevExpress.ExpressApp.Blazor.Components.Models;
using DevExpress.ExpressApp.Blazor.Editors.Adapters;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Utils;
using Microsoft.AspNetCore.Components;
using DevExpress.ExpressApp.Blazor.Editors;
using DevExpress.ExpressApp.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Microsoft.CodeAnalysis;
using DevExpress.Data.Browsing;
using DevExpress.Xpo;

namespace DE_SARAS.Blazor.Server
{

    public class InputModel : ComponentModelBase
    {
        public string Value
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
        public bool ReadOnly
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }
        public void SetValueFromUI(string value)
        {
            SetPropertyValue(value, notify: false, nameof(Value));
            ValueChanged?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler ValueChanged;
    }

    [PropertyEditor(typeof(string), false)]
    public class CustomStringPropertyEditor : BlazorPropertyEditorBase
    {
        public CustomStringPropertyEditor(Type objectType, IModelMemberViewItem model) : base(objectType, model) { }


        protected override IComponentAdapter CreateComponentAdapter()
        {
            return new InputAdapter(new InputModel());
        }
    }

    public class InputAdapter : ComponentAdapterBase
    {
        public InputAdapter(InputModel componentModel)
        {
            ComponentModel = componentModel ?? throw new ArgumentNullException(nameof(componentModel));
            ComponentModel.ValueChanged += ComponentModel_ValueChanged;
        }
        public InputModel ComponentModel { get; }

        public override void SetAllowEdit(bool allowEdit)
        {
            ComponentModel.ReadOnly = !allowEdit;
        }
        public override object GetValue()
        {
            string originalValue = ComponentModel.Value;

            // Convert the saved string value into a link representation
            string linkValue = ConvertToLink(originalValue);

            return linkValue;
        }

        // Method to convert a string to a link representation
        private string ConvertToLink(string originalValue)
        {
            // Assuming the original string is a URL, format it as an HTML link
            if (Uri.TryCreate(originalValue, UriKind.Absolute, out Uri uriResult))
            {
                return $"<a href=\"{uriResult.AbsoluteUri}\">{originalValue}</a>";
            }

            // If it's not a URL or doesn't fit a link format, return the original string
            return originalValue;
        }
        public override void SetValue(object value)
        {
            ComponentModel.Value = (string)value;
        }

        protected override RenderFragment CreateComponent()
        {
            return ComponentModelObserver.Create(ComponentModel, AdditionalDocumentRenderer.Create(ComponentModel));
        }
        private void ComponentModel_ValueChanged(object sender, EventArgs e) => RaiseValueChanged();
        public override void SetAllowNull(bool allowNull) { /* ...*/ }
        public override void SetDisplayFormat(string displayFormat) { /* ...*/ }
        public override void SetEditMask(string editMask) { /* ...*/ }
        public override void SetEditMaskType(EditMaskType editMaskType) { /* ...*/ }
        public override void SetErrorIcon(ImageInfo errorIcon) { /* ...*/ }
        public override void SetErrorMessage(string errorMessage) { /* ...*/ }
        public override void SetIsPassword(bool isPassword) { /* ...*/ }
        public override void SetMaxLength(int maxLength) { /* ...*/ }
        public override void SetNullText(string nullText) { /* ...*/ }

    }

    public partial class AdditionalDocumentLinkAdapter
    {
    }

}
