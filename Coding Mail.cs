#region Email
ApplicationMailSetup _locApplicationMailSetup = _globFunc.GetApplicationMailSetup(_currSession, _localCompany);
if(_locApplicationMailSetup !=  null)
{
    _locSubjectMail = "Pemberitahuan Persetujuan Permintaan Pembelian " + _locDocNo;
    _locBody = _globFunc.BackgroundBody(_localCompany, _locEmployee, _locDocNo, _locApprovalLevel, "Mohon persetujuannya", null);                                 
    _globFunc.SetAndSendMail(_locApplicationMailSetup.SmtpHost, _locApplicationMailSetup.SmtpPort, _locApplicationMailSetup.MailFrom, _locApplicationMailSetup.MailFromPassword,
        _locMailTo, _locSubjectMail, _locBody);
}
#endregion Email

#region Email

public ApplicationMailSetup GetApplicationMailSetup(Session currentSession, Company _locCompany)
{
    ApplicationMailSetup result = null;
    try
    {
        ApplicationMailSetup _locApplicationMailSetup = currentSession.FindObject<ApplicationMailSetup>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Company", _locCompany),
                                                        new BinaryOperator("Active", true)));
        if(_locApplicationMailSetup != null)
        {
            result = _locApplicationMailSetup;
        }
    }
    catch (Exception ex)
    {
        Tracing.Tracer.LogError(" BusinessObject = GlobalFunction " + ex.ToString());
    }
    return result;
}

public string BackgroundBody(Company _locCompany, Employee _locEmployee, string _locCode, ApprovalLevel _locApprovalLevel, string _locMessageBody, string _locRemarks)
{
    string body = string.Empty;
    string _locStringAppLevel = string.Empty;
    string _locStringStatus = string.Empty;
    string _locStringMessageBody = string.Empty;
    string _locStringEmployee = string.Empty;
    string _locStringRemarks = string.Empty;

    //string Status = _locPurchRequisitionApprovalXPO2.ApprovalStatus.ToString();
    #region ReaderMailTemplate
    if (_locCompany != null)
    {
        if(_locCompany.Name != null && _locCompany.Name != "")
        {
            if (_locCompany.Name == "Merpati Wahana Raya")
            {
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/MailTemplate/EmailTemplate1.html")))
                {
                    body = reader.ReadToEnd();
                }
            }
            if (_locCompany.Name == "Elmas Viana Djaja")
            {
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/MailTemplate/EmailTemplate2.html")))
                {
                    body = reader.ReadToEnd();
                }
            }
        }
    }            
    #endregion ReaderMailTemplate

    if(body != null && body != "")
    {
        if (_locCode != null && _locCode != "") { body = body.Replace("{Code}", _locCode); }

        #region Employee
        if(_locEmployee != null)
        {
            if (_locEmployee.Name != null) { _locStringEmployee = _locEmployee.Name; }
            body = body.Replace("{Employee}", _locEmployee.Name);
        }
        #endregion Employee

        #region ApprovalLevel

        if (_locApprovalLevel == ApprovalLevel.Level1) { _locStringAppLevel = "Level 1"; }
        if (_locApprovalLevel == ApprovalLevel.Level2) { _locStringAppLevel = "Level 2"; }
        if (_locApprovalLevel == ApprovalLevel.Level3) { _locStringAppLevel = "Level 3"; }
        if (_locApprovalLevel == ApprovalLevel.None) { _locStringAppLevel = "Not Available"; }
        if (_locApprovalLevel == ApprovalLevel.Level6) { _locStringAppLevel = "Cancel"; }
        if (_locApprovalLevel != null) { body = body.Replace("{Level}", _locStringAppLevel); }

        #endregion ApprovalLevel

        #region Remark
        if(_locRemarks != null && _locRemarks != "") { _locStringRemarks = _locRemarks; }
        body = body.Replace("{Remarks}", _locStringRemarks);
        #endregion Remark

        #region MessageBody
        if (_locMessageBody != null && _locMessageBody != "") { _locStringMessageBody = _locMessageBody; }
        body = body.Replace("{Message}", _locStringMessageBody);
        #endregion MessageBody
    }


    return body;
}

public void SetAndSendMail(string _locSmtpHost, int _locSmtpPort, string _locMailFrom, string _locMailFromPassword, string _locMailTo, string _locSubjectMail, string _locMessageBody)
{
    try
    {
        if (_locMailTo != null)
        {
            foreach (string _locRowMailTo in _locMailTo.Replace(" ", "").Split(';'))
            {
                if (!string.IsNullOrEmpty(_locRowMailTo))
                {
                    SmtpClient client = new SmtpClient(_locSmtpHost, _locSmtpPort);
                    client.TargetName = "None";
                    client.UseDefaultCredentials = false;
                    client.Credentials = new System.Net.NetworkCredential(_locMailFrom, _locMailFromPassword);
                    MailAddress from = new MailAddress(_locMailFrom, String.Empty, System.Text.Encoding.UTF8);
                    MailAddress to = new MailAddress(_locRowMailTo);
                    MailMessage message = new MailMessage(from, to);
                    message.Body = _locMessageBody;
                    message.IsBodyHtml = true;
                    message.BodyEncoding = System.Text.Encoding.UTF8;
                    message.Subject = _locSubjectMail;
                    message.SubjectEncoding = System.Text.Encoding.UTF8;
                    client.Send(message);
                }
            }
        }
    }
    catch (Exception ex)
    {
        Tracing.Tracer.LogError(" BusinessObject = GlobalFunction " + ex.ToString());
    }
}

#endregion Email