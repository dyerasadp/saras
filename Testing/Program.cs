﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp;
using DE_SARAS.Module.BusinessObjects;
using System.Timers;
using System.Reflection;

namespace Testing
{
    internal class Program
    {

        public static System.Timers.Timer aTimer;

        static void Main(string[] args)
        {
            DevExpress.ExpressApp.FrameworkSettings.DefaultSettingsCompatibilityMode = DevExpress.ExpressApp.FrameworkSettingsCompatibilityMode.v22_2;
            LongProcessInterval();
            Console.ReadLine();
        }

        private static void LongProcessInterval()
        {
            //aTimer = new System.Timers.Timer(14400000);
            aTimer = new System.Timers.Timer(1);
            aTimer.Interval = 10000;
            aTimer.Elapsed += new ElapsedEventHandler(RunThis);
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private static void RunThis(object source, ElapsedEventArgs e)
        {
            DateTime now = DateTime.Now;
            Console.WriteLine("System auto check every 10 second");
            XpoTypesInfoHelper.GetXpoTypeInfoSource();
            XafTypesInfo.Instance.RegisterEntity(typeof(VesselDocument));

            #region connection
            //XPObjectSpaceProvider osProvider = new XPObjectSpaceProvider(
            //@"XpoProvider=MSSqlServer;Data Source=localhost;User ID=sa;Password=coolvank@#2020;Initial Catalog=DISTRIBUTION_DB;Persist Security Info=true", null);
            XPObjectSpaceProvider osProvider = new XPObjectSpaceProvider(
            @"XpoProvider=MySql;Server=localhost;User ID=admin;Password=admin123;Database=devex_saras;Persist Security Info= true;Charset=utf8", null);
            #endregion connection

            if (osProvider != null)
            {

                IObjectSpace objectSpace = osProvider.CreateObjectSpace();
                foreach (VesselDocument _locVesselDocument in objectSpace.GetObjects<VesselDocument>())
                {
                    TimeSpan difference = _locVesselDocument.ExpiryDate - DateTime.Today;
                    if (_locVesselDocument.Active == true && difference.TotalDays < _locVesselDocument.Reminder)
                    {
                        Console.WriteLine("Data Found");
                    }
                }
                objectSpace.CommitChanges();
                objectSpace.Refresh();
                objectSpace.Dispose();

            }
            osProvider.Dispose();
        }
    }
}
