﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.Security;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [ImageName("BO_User")]
    [NavigationItem("UserAccess")]

    public class UserAccess : BaseObject, ISecurityUser, IAuthenticationStandardUser, IAuthenticationActiveDirectoryUser,
        ISecurityUserWithRoles, IPermissionPolicyUser, ICanInitialize
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        // Use CodeRush to create XPO classes and properties with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/118557

        #region Initialization

        private string _code;
        private Employee _employee;
        private Company _company;

        #endregion Initialization

        public UserAccess(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [ImmediatePostData()]
        [Association("Employee-UserAcesses")]
        public Employee Employee
        {
            get { return _employee; }
            set
            {
                SetPropertyValue(nameof(Employee), ref _employee, value);
                if (this._employee != null)
                {
                    if (this._employee.Company != null) { this.Company = this._employee.Company; }
                    //if (this._employee.Workplace != null) { this.Workplace = this._employee.Workplace; }
                }
                else
                {
                    this.Company = null;
                    //this.Workplace = null;
                }
            }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }



        #region OriginalSystemUser

        #region ISecurityUser Members
        private bool isActive = true;
        public bool IsActive
        {
            get { return isActive; }
            set { SetPropertyValue(nameof(IsActive), ref isActive, value); }
        }

        private string userName = String.Empty;
        [RuleRequiredField("EmployeeUserNameRequired", DefaultContexts.Save)]
        [RuleUniqueValue("EmployeeUserNameIsUnique", DefaultContexts.Save, "The login with the entered user name was already registered within the system.")]
        public string UserName
        {
            get { return userName; }
            set { SetPropertyValue(nameof(UserName), ref userName, value); }
        }
        #endregion ISecurityUser Members

        #region IAuthenticationStandardUser Members
        private bool changePasswordOnFirstLogon;
        public bool ChangePasswordOnFirstLogon
        {
            get { return changePasswordOnFirstLogon; }
            set
            {
                SetPropertyValue(nameof(ChangePasswordOnFirstLogon), ref changePasswordOnFirstLogon, value);
            }
        }

        private string storedPassword;
        [Browsable(false), Size(SizeAttribute.Unlimited), Persistent, SecurityBrowsable]
        protected string StoredPassword
        {
            get { return storedPassword; }
            set { storedPassword = value; }
        }
        public bool ComparePassword(string password)
        {
            return PasswordCryptographer.VerifyHashedPasswordDelegate(this.storedPassword, password);
        }
        public void SetPassword(string password)
        {
            this.storedPassword = PasswordCryptographer.HashPasswordDelegate(password);
            OnChanged(nameof(StoredPassword));
        }
        #endregion

        #region ISecurityUserWithRoles Members
        IList<ISecurityRole> ISecurityUserWithRoles.Roles
        {
            get
            {
                IList<ISecurityRole> result = new List<ISecurityRole>();
                foreach (UserAccessRole role in UserAccessRoles)
                {
                    result.Add(role);
                }
                return result;
            }
        }
        #endregion

        [Association("UserAccesses-UserAccessRoles")]
        [RuleRequiredField("UserAccessRoleIsRequired", DefaultContexts.Save,
        TargetCriteria = "IsActive",
        CustomMessageTemplate = "An active User Access must have at least one role assigned")]
        public XPCollection<UserAccessRole> UserAccessRoles
        {
            get
            {
                return GetCollection<UserAccessRole>(nameof(UserAccessRoles));
            }
        }

        #region IPermissionPolicyUser Members
        IEnumerable<IPermissionPolicyRole> IPermissionPolicyUser.Roles
        {
            get { return UserAccessRoles.OfType<IPermissionPolicyRole>(); }
        }
        #endregion

        #region ICanInitialize Members
        void ICanInitialize.Initialize(IObjectSpace objectSpace, SecurityStrategyComplex security)
        {
            UserAccessRole newUserRole = (UserAccessRole)objectSpace.FirstOrDefault<UserAccessRole>(role => role.Name == security.NewUserRoleName);
            if (newUserRole == null)
            {
                newUserRole = objectSpace.CreateObject<UserAccessRole>();
                newUserRole.Name = security.NewUserRoleName;
                newUserRole.IsAdministrative = true;
                newUserRole.UserAccesses.Add(this);
            }
        }
        #endregion

        #endregion OriginalSystemUser

        #endregion Field

    }
}