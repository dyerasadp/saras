﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Voyage Control")]
    [RuleCombinationOfPropertiesIsUnique("VesselRemainsRuleUnique", DefaultContexts.Save, "Code")]


    public class VesselRemains : BaseObject
    {
        #region Initialization

        private string _code;
        private Vessel _vessel;
        private double _qtyBunkerLeft;
        private double _qtyCargoLeft;
        private GlobalFunction _globFunc;

        #endregion #region Initialization

        public VesselRemains(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselRemains);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        //[RuleCombinationOfPropertiesIsUnique("VesselRuleUnique", DefaultContexts.Save, "Vessel")]
        // the code above can't be applied on object type data
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        public double QtyBunkerLeft
        {
            get { return _qtyBunkerLeft; }
            set { SetPropertyValue(nameof(QtyBunkerLeft), ref _qtyBunkerLeft, value); }
        }

        public double QtyCargoLeft
        {
            get { return _qtyCargoLeft; }
            set { SetPropertyValue(nameof(QtyCargoLeft), ref _qtyCargoLeft, value); }
        }

        #endregion Field
    }
}