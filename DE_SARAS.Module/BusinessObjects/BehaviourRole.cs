﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Category")]
    [NavigationItem("Behaviour Setting")]
    [RuleCombinationOfPropertiesIsUnique("BehaviourRoleRuleUnique", DefaultContexts.Save, "Code")]

    public class BehaviourRole : BaseObject
    {
        #region Initialization

        private string _code;
        private BehaviourCategory _category;
        private double _bobot;
        private double _bobotUser;
        private double _bobotAtasan;
        private double _ytdtarget;
        private KpiYear _yearBehaviour;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private bool _active;
        private string _localUserAccess;
        private GlobalFunction _globFunc;


        #endregion Initialization
        public BehaviourRole(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public BehaviourCategory Category
        {
            get { return _category; }
            set { SetPropertyValue(nameof(Category), ref _category, value); }
        }

        [XafDisplayName("YTD Target %")]
        public double Ytdtarget
        {
            get { return _ytdtarget; }
            set { SetPropertyValue(nameof(Ytdtarget), ref _ytdtarget, value); }
        }

        [XafDisplayName("Bobot %")]
        public double Bobot
        {
            get { return _bobot; }
            set { SetPropertyValue(nameof(Bobot), ref _bobot, value); }
        }

        [XafDisplayName("Bobot User %")]
        public double BobotUser
        {
            get { return _bobotUser; }
            set { SetPropertyValue(nameof(BobotUser), ref _bobotUser, value); }
        }

        [XafDisplayName("Bobot Atasan %")]
        public double BobotAtasan
        {
            get { return _bobotAtasan; }
            set { SetPropertyValue(nameof(BobotAtasan), ref _bobotAtasan, value); }
        }


        [XafDisplayName("Year")]
        public KpiYear YearBehaviour
        {
            get { return _yearBehaviour; }
            set { SetPropertyValue(nameof(YearBehaviour), ref _yearBehaviour, value); }
        }
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Departement")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Departement")]
        [Appearance("OrganizationDimension3Close", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("-")]
        [Appearance("OrganizationDimension4Close", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }


        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field

        #region Manny

        [Association("BehaviourRole-BehaviourRoleLines")]
        public XPCollection<BehaviourRoleLine> BehaviourRoleLines
        {
            get { return GetCollection<BehaviourRoleLine>(nameof(BehaviourRoleLines)); }
        }


        #endregion Manny

    }
}