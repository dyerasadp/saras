﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Numbering Setting")]

    public class NumberingLine : BaseObject
    {
        #region Initialization

        private int _no;
        private string _name;
        private CustomProcess.ObjectList _objectList;
        private string _prefix;
        private string _formatNumber;
        private string _suffix;
        private KpiYear _suffix2;
        private QuotationPeriode _quotationPeriod;
        private int _lastValue;
        private int _incrementValue;
        private int _minValue;
        private string _formatedValue;
        private Company _company;
        private bool _active;
        private bool _default;
        private bool _selection;
        private bool _sign;
        private CustomProcess.NumberingType _numberingType;
        private NumberingHeader _numberingHeader;

        #endregion Initialization
        public NumberingLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Override
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Override

        #region Field


        public int No
        {
            get { return _no; }
            set { SetPropertyValue(nameof(No), ref _no, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public CustomProcess.ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue(nameof(ObjectList), ref _objectList, value); }
        }

        public string Prefix
        {
            get { return _prefix; }
            set { SetPropertyValue(nameof(Prefix), ref _prefix, value); }
        }

        public string FormatNumber
        {
            get { return _formatNumber; }
            set { SetPropertyValue(nameof(FormatNumber), ref _formatNumber, value); }
        }

        public string Suffix
        {
            get { return _suffix; }
            set { SetPropertyValue(nameof(Suffix), ref _suffix, value); }
        }

        public KpiYear Suffix2
        {
            get { return _suffix2; }
            set { SetPropertyValue(nameof(Suffix2), ref _suffix2, value); }
        }

        public QuotationPeriode QuotationPeriode
        {
            get { return _quotationPeriod; }
            set { SetPropertyValue(nameof(QuotationPeriode), ref _quotationPeriod, value); }
        }

        public int LastValue
        {
            get { return _lastValue; }
            set { SetPropertyValue(nameof(LastValue), ref _lastValue, value); }
        }

        public int IncrementValue
        {
            get { return _incrementValue; }
            set { SetPropertyValue(nameof(IncrementValue), ref _incrementValue, value); }
        }

        public int MinValue
        {
            get { return _minValue; }
            set { SetPropertyValue(nameof(MinValue), ref _minValue, value); }
        }

        public string FormatedValue
        {
            get
            {
                string strFormat;
                if (this.FormatNumber == null)
                {
                    strFormat = "{0:0}";
                }
                else
                {
                    strFormat = String.Format("{{0:{0}}}", this.FormatNumber);
                }
                _formatedValue = this.Prefix + string.Format(strFormat, this.LastValue) + this.Suffix + this.Suffix2 + this.QuotationPeriode;
                return _formatedValue;
            }
        }



        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue(nameof(Default), ref _default, value); }
        }

        public bool Selection
        {
            get { return _selection; }
            set { SetPropertyValue(nameof(Selection), ref _selection, value); }
        }

        public bool Sign
        {
            get { return _sign; }
            set { SetPropertyValue(nameof(Sign), ref _sign, value); }
        }

        [Browsable(false)]
        public CustomProcess.NumberingType NumberingType
        {
            get { return _numberingType; }
            set { SetPropertyValue(nameof(NumberingType), ref _numberingType, value); }
        }

        [Association("NumberingHeader-NumberingLines")]
        public NumberingHeader NumberingHeader
        {
            get { return _numberingHeader; }
            set
            {
                SetPropertyValue(nameof(NumberingHeader), ref _numberingHeader, value);
                if (!IsLoading)
                {
                    if (_numberingHeader != null)
                    {
                        if (_numberingHeader.NumberingType != CustomProcess.NumberingType.None)
                        {
                            this.NumberingType = _numberingHeader.NumberingType;
                        }
                        else
                        {
                            this.NumberingType = CustomProcess.NumberingType.None;
                        }
                    }
                }
            }
        }

        #endregion Field

        #region code dan numbering
        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.NumberingHeader != null)
                    {
                        object _makRecord = Session.Evaluate<NumberingLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("NumberingHeader=?", this.NumberingHeader));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = NumberingLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.NumberingHeader != null)
                {
                    NumberingHeader _numHeader = Session.FindObject<NumberingHeader>
                                                (new BinaryOperator("Code", this.NumberingHeader.Code));

                    XPCollection<NumberingLine> _numLines = new XPCollection<NumberingLine>
                                                (Session, new BinaryOperator("NumberingHeader", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (NumberingLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = NumberingLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.NumberingHeader != null)
                {
                    NumberingHeader _numHeader = Session.FindObject<NumberingHeader>
                                                (new BinaryOperator("Code", this.NumberingHeader.Code));

                    XPCollection<NumberingLine> _numLines = new XPCollection<NumberingLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("NumberingHeader", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (NumberingLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = NumberingLine " + ex.ToString());
            }
        }


        #endregion code dan numbering 
    }
}