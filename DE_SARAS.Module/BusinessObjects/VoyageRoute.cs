﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Voyage Control")]
    [RuleCombinationOfPropertiesIsUnique("VoyageRouteRuleUnique", DefaultContexts.Save, "Code")]

    public class VoyageRoute : BaseObject
    {
        #region Initialization

        #region General 
        private string _code;
        private Vessel _vessel;
        private Vessel _vesselAssist;
        private Voyage _voyage;
        private ShippingMasterCustomer _customer;
        private ShippingMasterPort _portOfLoading;
        private DateTime _dateLoading;
        private ShippingMasterPort _portOfDischarge;
        private DateTime _dateDischarge;
        private ShippingMasterPort _portFrom;
        private ShippingMasterLocation _locationFrom;
        private ShippingMasterPort _portTo;
        private ShippingMasterLocation _locationTo;
        private double _distance;
        private double _speed;
        private double _flowRateMuat;
        private double _flowRateBongkar;
        private string _cargo;
        private double _qtyCargo;
        private DateTime _docDate;
        #endregion General

        #region Other Variable

        private Status _status;
        private int _no;

        private GlobalFunction _globFunc;

        #endregion Other Variable

        #endregion Initialization

        public VoyageRoute(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VoyageRoute);
            #endregion Numberingeed
        }

        #region Override
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Override

        #region Field

        [VisibleInListView(false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Appearance("NoDisable", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue(nameof(No), ref _no, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        [Appearance("DocDateDisable", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [VisibleInListView(false)]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        [VisibleInListView(false)]
        public Vessel VesselAssist
        {
            get { return _vesselAssist; }
            set
            { SetPropertyValue(nameof(VesselAssist), ref _vesselAssist, value); }
        }

        [ImmediatePostData()]
        [Association("Voyage-VoyageRoutes")]
        [Appearance("VoyageDisable", Enabled = false)]
        //[DataSourceProperty("VoyageStatus", DataSourcePropertyIsNullMode.SelectNothing)]
        public Voyage Voyage
        {
            get { return _voyage; }
            set { SetPropertyValue(nameof(Voyage), ref _voyage, value);
                if (!IsLoading)
                {
                    if (_voyage != null)
                    {
                        if (_voyage.Vessel != null)
                        {
                            this.Vessel = _voyage.Vessel;
                        }
                        this.Status = _voyage.StatusVoyage;
                    }
                    RouteNumbering();
                }
            }
        }

        [Browsable(false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue(nameof(Status), ref _status, value); }
        }

        public ShippingMasterCustomer Customer
        {
            get { return _customer; }
            set { SetPropertyValue(nameof(Customer), ref _customer, value); }
        }

        public ShippingMasterPort PortOfLoading
        {
            get { return _portOfLoading; }
            set { SetPropertyValue(nameof(PortOfLoading), ref _portOfLoading, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DateLoading
        {
            get { return _dateLoading; }
            set { SetPropertyValue(nameof(DateLoading), ref _dateLoading, value); }
        }

        public ShippingMasterPort PortOfDischarge
        {
            get { return _portOfDischarge; }
            set { SetPropertyValue(nameof(PortOfDischarge), ref _portOfDischarge, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DateDischarge
        {
            get { return _dateDischarge; }
            set { SetPropertyValue(nameof(DateDischarge), ref _dateDischarge, value); }
        }

        public ShippingMasterPort PortFrom
        {
            get { return _portFrom; }
            set { SetPropertyValue(nameof(PortFrom), ref _portFrom, value); }
        }

        public ShippingMasterLocation LocationFrom
        {
            get { return _locationFrom; }
            set { SetPropertyValue(nameof(LocationFrom), ref _locationFrom, value); }
        }

        public ShippingMasterPort PortTo
        {
            get { return _portTo; }
            set { SetPropertyValue(nameof(PortTo), ref _portTo, value); }
        }

        public ShippingMasterLocation LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue(nameof(LocationTo), ref _locationTo, value); }
        }

        [VisibleInListView(false)]
        public double Distance
        {
            get { return _distance; }
            set { SetPropertyValue(nameof(Distance), ref _distance, value); }
        }

        [VisibleInListView(false)]
        public double Speed
        {
            get { return _speed; }
            set { SetPropertyValue(nameof(Speed), ref _speed, value); }
        }

        [VisibleInListView(false)]
        public double FlowRateMuat
        {
            get { return _flowRateMuat; }
            set { SetPropertyValue(nameof(FlowRateMuat), ref _flowRateMuat, value); }
        }

        [VisibleInListView(false)]
        public double FlowRateBongkar
        {
            get { return _flowRateBongkar; }
            set { SetPropertyValue(nameof(FlowRateBongkar), ref _flowRateBongkar, value); }
        }

        public string Cargo
        {
            get { return _cargo; }
            set { SetPropertyValue(nameof(Cargo), ref _cargo, value); }
        }

        public double QtyCargo
        {
            get { return _qtyCargo; }
            set { SetPropertyValue(nameof(QtyCargo), ref _qtyCargo, value); }
        }

        #endregion Field

        #region Code

        private void RouteNumbering()
        {
            XPCollection<VoyageRoute> _locVoyageRoutes = new XPCollection<VoyageRoute>
                (Session, new GroupOperator(GroupOperatorType.And,
                new BinaryOperator("Voyage", this.Voyage),
                new BinaryOperator("Vessel", this.Vessel)
                ));

            if (_locVoyageRoutes != null)
            {
                if (_locVoyageRoutes.Count() > 0)
                {
                    object _lastRecord = Session.Evaluate<VoyageRoute>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Voyage=?", this.Voyage));
                    _no = Convert.ToInt32(_lastRecord);

                    VoyageRoute _lastVoyageRoute = Session.FindObject<VoyageRoute>
                                        (new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("No", _no),
                                        new BinaryOperator("Voyage", this.Voyage)
                                        ));
                    if (_lastVoyageRoute != null)
                    {
                        this.No = _lastVoyageRoute.No + 1;
                    }
                }
                else
                {
                    this.No = 1;
                }
            }
        }

        #region Numbering Route
        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Voyage != null)
                    {
                        object _makRecord = Session.Evaluate<NumberingLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Voyage=?", this.Voyage));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = VoyageRoute " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Voyage != null)
                {
                    Voyage _locVoyage = Session.FindObject<Voyage>
                                                (new BinaryOperator("Code", this.Voyage.Code));

                    XPCollection<VoyageRoute> _routeLines = new XPCollection<VoyageRoute>
                                                (Session, new BinaryOperator("Voyage", _locVoyage),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_routeLines != null)
                    {
                        int i1 = 0;
                        foreach (VoyageRoute _routeLine in _routeLines)
                        {
                            i1 += 1;
                            _routeLine.No = i1;
                            _routeLine.Save();
                        }
                        i1 = 1;
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = VoyageRoute " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Voyage != null)
                {
                    Voyage _locVoyage = Session.FindObject<Voyage>
                                                (new BinaryOperator("Code", this.Voyage.Code));

                    XPCollection<VoyageRoute> _routeLines = new XPCollection<VoyageRoute>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Voyage", _locVoyage)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_routeLines != null)
                    {
                        int i = 0;
                        foreach (VoyageRoute _routeLine in _routeLines)
                        {
                            i += 1;
                            _routeLine.No = i;
                            _routeLine.Save();
                        }
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = VoyageRoute " + ex.ToString());
            }
        }

        #endregion Numbering Route

        #endregion Code

        #region Manny


        [Appearance("VoyageBunkersOpenDisable", Criteria = "Status = 'Open'", Enabled = false)]
        [Appearance("VoyageBunkersCloseDisable", Criteria = "Status = 'Close'", Enabled = false)]
        [Association("VoyageRoute-VoyageBunkers")]
        public XPCollection<VoyageBunker> VoyageBunkers
        {
            get { return GetCollection<VoyageBunker>(nameof(VoyageBunkers)); }
        }

        [Appearance("VoyageCycleTimesOpenDisable", Criteria = "Status = 'Open'", Enabled = false)]
        [Appearance("VoyageCycleTimesCloseDisable", Criteria = "Status = 'Close'", Enabled = false)]
        [Association("VoyageRoute-VoyageCycleTimes")]
        public XPCollection<VoyageCycleTime> VoyageCycleTimes
        {
            get { return GetCollection<VoyageCycleTime>(nameof(VoyageCycleTimes)); }
        }

        [Appearance("VoyageCargosOpenDisable", Criteria = "Status = 'Open'", Enabled = false)]
        [Appearance("VoyageCargosCloseDisable", Criteria = "Status = 'Close'", Enabled = false)]
        [Association("VoyageRoute-VoyageCargos")]
        public XPCollection<VoyageCargo> VoyageCargos
        {
            get { return GetCollection<VoyageCargo>(nameof(VoyageCargos)); }
        }

        #endregion Manny

    }
}