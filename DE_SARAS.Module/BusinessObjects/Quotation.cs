﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Map.Native;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("QuotationRuleUnique", DefaultContexts.Save, "Code")]

    public class Quotation : BaseObject
    {
        #region Initialization

        private string _code;
        private string _docNo;
        private QuotationPeriode _quotationPeriod;
        private string _name;
        private DateTime _quotationDate;
        private QuotationType _type;
        private bool _mops;
        private QuotationStatus _quotationStatus;
        private DateTime _renewelDate;
        private AreaJual _areaJual;
        private string _noted;
        private DateTime _docDate;
        private Employee _employee;
        private Company _company;
        private MediaDataObject _letterHead;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        private XPCollection<Company> _availableCompany;
        private XPCollection<QuotationPeriode> _availablePeriod;


        #endregion Initialization
        public Quotation(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            { 
                this.Type = QuotationType.PERSONAL;
                this.AreaJual = AreaJual.BANJARMASIN;
                this.Status = QuotationStatus.NEW;
                this.QuotationDate = DateTime.Now;
                this.DocDate = DateTime.Now;


                #region User Access
                _globFunc = new GlobalFunction();
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));

                Company _locCompany = null;
                Employee _locEmployee = null;

                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        _locEmployee = _locUserAccess.Employee;
                        if (_locUserAccess.Employee.Company != null)
                        { 
                            _locCompany = _locUserAccess.Employee.Company;
                        }
                    }
                    if (this.Session != null)
                    {
                        this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Quotation);
                        
                    }

                    this.Company = _locCompany;
                    this.Employee = _locEmployee;
                }
                #endregion User Access
            }
        }

        //protected override void OnSaving()
        //{
        //    base.OnSaving();

        //    if (Session.IsNewObject(this))
        //    {
        //        #region Numbering

        //        _localUserAccess = SecuritySystem.CurrentUserName;
        //        UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));

        //        if (this.Company != null)  // Memeriksa apakah Company dan Periode bukan null
        //        {
        //            if (this.Session != null)
        //            {
        //                Company _locCompany = this.Company;
        //                QuotationPeriode _locPeriode = this.Periode;
        //                _globFunc = new GlobalFunction();
        //                //if( _locPeriode != null )
        //                //{
        //                //    this.DocNo = _globFunc.GetNumberingUnlockOptimisticRecord2(this.Session.DataLayer, ObjectList.Quotation, _locCompany, _locPeriode);
        //                //}
        //                //else
        //                //{
        //                //    this.DocNo = _globFunc.GetNumberingUnlockOptimisticRecord3(this.Session.DataLayer, ObjectList.Quotation, _locCompany);
        //                //}
                       
        //            }
        //        }
        //        #endregion Numbering
        //    }
        //}


        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string DocNo
        {
            get { return _docNo; }
            set { SetPropertyValue(nameof(DocNo), ref _docNo, value); }
        }

        [Browsable(false)]
        public XPCollection<QuotationPeriode> AvailablePeriod
        {
            get
            {
                if (!IsLoading)
                {
                    DateTime currentDate = DateTime.Now.Date;
                    XPCollection<QuotationPeriode> _quotationPeriods = new XPCollection<QuotationPeriode>(this.Session,
                                                                          new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("StartDate", currentDate, BinaryOperatorType.LessOrEqual),
                                                                          new BinaryOperator("EndDate", currentDate, BinaryOperatorType.GreaterOrEqual)
                                                                          ));
                    if (_quotationPeriods != null && _quotationPeriods.Count() > 0)
                    {
                        _availablePeriod = _quotationPeriods;
                    }
                }
                return _availablePeriod;
            }
        }

       // [DataSourceProperty("AvailablePeriod", DataSourcePropertyIsNullMode.SelectNothing)]
        public QuotationPeriode Periode
        {
            get { return _quotationPeriod; }
            set { SetPropertyValue(nameof(Periode), ref _quotationPeriod, value);}
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public DateTime QuotationDate
        {
            get { return _quotationDate; }
            set { SetPropertyValue(nameof(QuotationDate), ref _quotationDate, value); }
        }

        public QuotationType Type
        {
            get { return _type; }
            set { SetPropertyValue(nameof(Type), ref _type, value); }
        }
        public bool Mops
        {
            get { return _mops; }
            set { SetPropertyValue(nameof(Mops), ref _mops, value); }
        }

        public QuotationStatus Status
        {
            get { return _quotationStatus; }
            set { SetPropertyValue(nameof(Status), ref _quotationStatus, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime RenewelDate
        {
            get { return _renewelDate; }
            set { SetPropertyValue(nameof(RenewelDate), ref _renewelDate, value); }
        }

        public AreaJual AreaJual 
        {
            get { return _areaJual; }
            set { SetPropertyValue(nameof(AreaJual), ref _areaJual, value); }
        }

        [Size(512)]
        public string Noted
        {
            get { return _noted; }
            set { SetPropertyValue(nameof(Noted), ref _noted, value); }
        }

        [Appearance("DateTimeQuotationClosed", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }


        #region Organization

        [XafDisplayName("Created By")]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        [Browsable(false)]
        public XPCollection<Company> AvailableCompany
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<Company> _locCompanys = new XPCollection<Company>(Session,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Active", true)
                                                    ));
                    if (_locCompanys != null && _locCompanys.Count() > 0)
                    {
                        _availableCompany = _locCompanys;
                    }
                }
                return _availableCompany;
            }
        }

        [ImmediatePostData]
        [DataSourceProperty("_availableCompany", DataSourcePropertyIsNullMode.SelectNothing)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value);
                if(!IsLoading)
                {
                    if(this._company!= null) 
                    {
                        this.LetterHead = _company.LetterHead;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [DevExpress.Xpo.Size(SizeAttribute.Unlimited)]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.PictureEdit, DetailViewImageEditorMode = ImageEditorMode.PictureEdit, ListViewImageEditorCustomHeight = 40)]
        public MediaDataObject LetterHead
        {
            get { return _letterHead; }
            set { SetPropertyValue(nameof(LetterHead), ref _letterHead, value); }
        }

        #endregion Organization

        #endregion Field

        #region Manny 


        [XafDisplayName("Customer")]
        [Association("Quotation-QuotationListCustomerLines")]
        public XPCollection<QuotationListCustomerLine> QuotationListCustomerLines
        {
            get { return GetCollection<QuotationListCustomerLine>(nameof(QuotationListCustomerLines)); }
        }

        [XafDisplayName("Price")]
        [Association("Quotation-QuotationPriceLines")]
        public XPCollection<QuotationPriceLine> QuotationPriceLines
        {
            get { return GetCollection<QuotationPriceLine>(nameof(QuotationPriceLines)); }
        }

        [XafDisplayName("Mechanism")]
        [Association("Quotation-QuotationMechanismLines")]
        public XPCollection<QuotationMechanismLine> QuotationMechanismLines
        {
            get { return GetCollection<QuotationMechanismLine>(nameof(QuotationMechanismLines)); }
        }

        [XafDisplayName("Bank Account")]
        [Association("Quotation-QuotationBankLines")]
        public XPCollection<QuotationBankLine> QuotationBankLines
        {
            get { return GetCollection<QuotationBankLine>(nameof(QuotationBankLines)); }
        }


        [XafDisplayName("Mops")]
        [Association("Quotation-QuotationAlphaLines")]
        public XPCollection<QuotationAlphaLine> QuotationAlphaLines
        {
            get { return GetCollection<QuotationAlphaLine>(nameof(QuotationAlphaLines)); }
        }



        #endregion Manny
    }
}