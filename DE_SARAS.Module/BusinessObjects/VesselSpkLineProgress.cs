﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("VesselPart")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselSpkLineProgressRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselSpkLineProgress : BaseObject
    {

        #region initialization

        private string _code;
        private VesselSpk _vesselSpk;
        private VesselSpkLine _vesselSpkLine;
        private VesselPart _vesselPart;
        private DateTime _repairDate;
        private RepairStatus _status;
        private FileData _file;
        private string _remarks;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public VesselSpkLineProgress(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselSpkLineProgress);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Association("VesselSpk-SpkProgress")]
        public VesselSpk VesselSpk
        {
            get { return _vesselSpk; }
            set { SetPropertyValue(nameof(VesselSpk), ref _vesselSpk, value); }
        }

        [Association("VesselSpkLine-DetailProgress")]
        public VesselSpkLine VesselSpkLine
        {
            get { return _vesselSpkLine; }
            set { SetPropertyValue(nameof(VesselSpkLine), ref _vesselSpkLine, value); }
        }

        public VesselPart VesselPart
        {
            get { return _vesselPart; }
            set { SetPropertyValue(nameof(VesselPart), ref _vesselPart, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime RepairDate
        {
            get { return _repairDate; }
            set { SetPropertyValue(nameof(RepairDate), ref _repairDate, value); }
        }

        public RepairStatus Status
        {
            get { return _status; }
            set { SetPropertyValue(nameof(Status), ref _status, value); }
        }

        public FileData File
        {
            get { return _file; }
            set { SetPropertyValue(nameof(File), ref _file, value); }
        }

        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue(nameof(Remarks), ref _remarks, value); }
        }

        #endregion Field

    }
}