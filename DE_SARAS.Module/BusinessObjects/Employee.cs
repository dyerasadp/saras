﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.XtraRichEdit.Import.Html;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Organization")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("EmployeeRuleUnique", DefaultContexts.Save, "Code")]
    public class Employee : BaseObject
    {
        #region Initialization

        private string _code;
        private string _name;
        private Employee _leader;
        private string _email;
        private MediaDataObject _photo;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private bool _active;
        private JobPosition _defaultPosition;
        private JobPositionLevel _jobPositionLevel;
        private XPCollection<Employee> _availableEmployee;

        #endregion Initialization
        public Employee(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public string Email
        {
            get { return _email; }
            set { SetPropertyValue(nameof(Email), ref _email, value); }
        }

        [ImmediatePostData()]
        [DevExpress.Xpo.Size(SizeAttribute.Unlimited)]
        [VisibleInListViewAttribute(true)]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.PictureEdit, DetailViewImageEditorMode = ImageEditorMode.PictureEdit, ListViewImageEditorCustomHeight = 40)]
        public MediaDataObject Photo
        {
            get { return _photo; }
            set { SetPropertyValue(nameof(Photo), ref _photo, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Association("Company-Employees")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Workplace")]
        [DataSourceCriteria("Company = '@This.Company' And Active = 'true'")]
        [Association("OrganizationDimension1-Employees")]
        public OrganizationDimension1 OrganizationDimension1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrganizationDimension1), ref _orgDim1, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Departement")]
        [DataSourceCriteria("OrgDim1 = '@This.OrganizationDimension1' And Active = 'true'")]
        [Association("OrganizationDimension2-Employees")]
        public OrganizationDimension2 OrganizationDimension2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrganizationDimension2), ref _orgDim2, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Sub Departement")]
        [DataSourceCriteria("OrgDim1 = '@This.OrganizationDimension1' And Active = 'true'")]
        public OrganizationDimension3 OrganizationDimension3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrganizationDimension3), ref _orgDim3, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("-")]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public OrganizationDimension4 OrganizationDimension4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrganizationDimension4), ref _orgDim4, value); }
        }

        

        [ImmediatePostData]
        [XafDisplayName("Default Position")]
        //[Appearance("DefaultPositionDefault", Enabled = false)]
        public JobPosition DefaultPosition
        {
            get { return _defaultPosition; }
            set { 
                SetPropertyValue(nameof(DefaultPosition), ref _defaultPosition, value); 
                if(!IsLoading)
                {
                    if(_defaultPosition != null)
                    {
                        this.JobPositionLevel = _defaultPosition.JobPositionLevel;
                    }
                }
            }
        }

        [Browsable(false)]
        public JobPositionLevel JobPositionLevel
        {
            get { return _jobPositionLevel; }
            set { SetPropertyValue(nameof(JobPositionLevel), ref _jobPositionLevel, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailableEmployee
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<Employee> _locEmployees = new XPCollection<Employee>(this.Session,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", this.Company),
                                                                        new BinaryOperator("OrganizationDimension1", this.OrganizationDimension1),
                                                                        new BinaryOperator("OrganizationDimension2", this.OrganizationDimension2),
                                                                        new BinaryOperator("JobPositionLevel", JobPositionLevel.ManagementLevel),
                                                                        new BinaryOperator("Active", true)
                                                                        ));
                    if (_locEmployees != null && _locEmployees.Count() > 0)
                    {
                        _availableEmployee = _locEmployees;
                    }
                }
                return _availableEmployee;
            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableEmployee", DataSourcePropertyIsNullMode.SelectNothing)]
        public Employee Leader
        {
            get { return _leader; }
            set { SetPropertyValue(nameof(Leader), ref _leader, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }



        #endregion Field

        #region Manny
        [Association("Employee-EmployeePositionLines")]
        public XPCollection<EmployeePositionLine> EmployeePositionLines
        {
            get { return GetCollection<EmployeePositionLine>(nameof(EmployeePositionLines)); }
        }

        [Association("Employee-EmployeeAssetss")]
        public XPCollection<EmployeeAssets> EmployeeAssetss
        {
            get { return GetCollection<EmployeeAssets>(nameof(EmployeeAssetss)); }
        }

        [Association("Employee-EmployeSubCoordinateLines")]
        public XPCollection<EmployeSubCoordinateLine> EmployeSubCoordinateLines
        {
            get { return GetCollection<EmployeSubCoordinateLine>(nameof(EmployeSubCoordinateLines)); }
        }


        [Association("Employee-UserAcesses")]
        public XPCollection<UserAccess> UserAccesses
        {
            get { return GetCollection<UserAccess>(nameof(UserAccesses)); }
        }
        #endregion Manny

    }
}