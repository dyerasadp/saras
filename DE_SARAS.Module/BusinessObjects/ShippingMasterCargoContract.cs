﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Voyage Control")]
    [RuleCombinationOfPropertiesIsUnique("ShippingMasterCargoContractRuleUnique", DefaultContexts.Save, "Code")]

    public class ShippingMasterCargoContract : BaseObject
    {
        #region initialization

        private string _code;
        private string _name;
        private ContractRole _contractRole;
        private double _tollerancePercentage;
        private DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public ShippingMasterCargoContract(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShippingMasterCargoContract);
            #endregion Numbering
        }

        #region Field
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public ContractRole ContractRole
        {
            get { return _contractRole; }
            set { SetPropertyValue(nameof(ContractRole), ref _contractRole, value); }
        }

        public double TollerancePercentage
        {
            get { return _tollerancePercentage; }
            set { SetPropertyValue(nameof(TollerancePercentage), ref _tollerancePercentage, value); }
        }

        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field
    }
}