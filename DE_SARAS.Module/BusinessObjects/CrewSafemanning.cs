﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("JobPosition")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("CrewSafeManningRuleUnique", DefaultContexts.Save, "Code")]

    public class CrewSafemanning : BaseObject
    {
        #region initialization

        private string _code;
        private Vessel _vessel;
        private JobPosition _jobPosition;
        private CrewClassList _crewClass;
        private int _crewTotal;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public CrewSafemanning(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CrewSafemanning);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Association("Vessel-CrewSafemannings")]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        public JobPosition JobPosition
        {
            get { return _jobPosition; }
            set { SetPropertyValue(nameof(JobPosition), ref _jobPosition, value); }
        }

        public CrewClassList CrewClass
        {
            get { return _crewClass; }
            set { SetPropertyValue(nameof(CrewClass), ref _crewClass, value); }
        }

        public int CrewTotal
        {
            get { return _crewTotal; }
            set { SetPropertyValue(nameof(CrewTotal), ref _crewTotal, value); }
        }

        #endregion Field
    }
}