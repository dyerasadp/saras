﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselInspectionRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselInspection : BaseObject
    {
        #region initialization

        private string _code;
        private string _name;
        private Vessel _vessel;
        private ShippingHeadInspection _templateChecklist;
        private DateTime _dateInspection;
        private string _location;

        private DateTime _docDate;
        private bool _active;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public VesselInspection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.Active = true;
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselInspection);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [Association("Vessel-VesselInspections")]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        public ShippingHeadInspection TemplateChecklist
        {
            get { return _templateChecklist; }
            set { SetPropertyValue(nameof(TemplateChecklist), ref _templateChecklist, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime DateInspection
        {
            get { return _dateInspection; }
            set { SetPropertyValue(nameof(DateInspection), ref _dateInspection, value); }
        }

        public string Location
        {
            get { return _location; }
            set { SetPropertyValue(nameof(Location), ref _location, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field

        #region Manny

        [Association("VesselInspection-VesselInspectionLines")]
        public XPCollection<VesselInspectionLine> ChecklistResult
        {
            get { return GetCollection<VesselInspectionLine>(nameof(ChecklistResult)); }
        }

        #endregion Manny

    }
}