﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VoyageCycleTimeRuleUnique", DefaultContexts.Save, "Code")]

    public class VoyageCycleTime : BaseObject
    {
        #region Initialization

        private string _code;
        private VoyageRoute _voyageRoute;
        private TimeSpan _clearanceAct;
        private TimeSpan _clearanceStd;
        private string _clearanceRemarks;
        private TimeSpan _ohnAct;
        private TimeSpan _ohnStd;
        private string _ohnRemarks;
        private TimeSpan _manueversAct;
        private TimeSpan _manueversStd;
        private string _manueversRemarks;
        private TimeSpan _sailingBallastAct;
        private TimeSpan _sailingBallastStd;
        private string _sailingBallastRemarks;
        private TimeSpan _loadingPrepAct;
        private TimeSpan _loadingPrepStd;
        private string _loadingPrepRemarks;
        private TimeSpan _loadingAct;
        private TimeSpan _loadingStd;
        private string _loadingRemarks;
        private TimeSpan _soundingAndCalcAct;
        private TimeSpan _soundingAndCalcStd;
        private string _soundingAndCalcRemarks;
        private TimeSpan _sailingLadenAct;
        private TimeSpan _sailingLadenStd;
        private string _sailingLadenRemarks;
        private TimeSpan _dischargePrepAct;
        private TimeSpan _dischargePrepStd;
        private string _dischargePrepRemarks;
        private TimeSpan _dischargeAct;
        private TimeSpan _dischargeStd;
        private string _dischargeRemarks;
        private TimeSpan _waitDocCargoAct;
        private TimeSpan _waitDocCargoStd;
        private string _waitDocCargoRemarks;
        private TimeSpan _waitDocClearAct;
        private TimeSpan _waitDocClearStd;
        private string _waitDocClearRemarks;
        private TimeSpan _waitLoadingAct;
        private TimeSpan _waitLoadingStd;
        private string _waitLoadingRemarks;
        private TimeSpan _waitUnloadingAct;
        private TimeSpan _waitUnloadingStd;
        private string _waitUnloadingRemarks;
        private TimeSpan _standbyAct;
        private TimeSpan _standbyStd;
        private string _standbyRemarks;
        private TimeSpan _breakdownAct;
        private TimeSpan _breakdownStd;
        private string _breakdownRemarks;
        private TimeSpan _totalActivityAct;
        private TimeSpan _totalActivityStd;
        private TimeSpan _totalWaitingAct;
        private StatusPerformance _statusCycleTime;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public VoyageCycleTime(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VoyageCycleTime);
            #endregion Numbering
        }

        #region Field

        [VisibleInListView(false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [ImmediatePostData()]
        [Association("VoyageRoute-VoyageCycleTimes")]
        [Appearance("VoyageRouteDisable", Enabled = false)]
        public VoyageRoute VoyageRoute
        {
            get { return _voyageRoute; }
            set
            { SetPropertyValue(nameof(VoyageRoute), ref _voyageRoute, value); }
        }

        #region Activity Time

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [ModelDefault("DisplayFormat", "{0:HHHH:mm:ss}")]
        [ModelDefault("EditMask", "HHHH:mm:ss")]
        public TimeSpan ClearanceAct
        {
            get { return _clearanceAct; }
            set { 
                SetPropertyValue(nameof(ClearanceAct), ref _clearanceAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan ClearanceStd
        {
            get { return _clearanceStd; }
            set { 
                SetPropertyValue(nameof(ClearanceStd), ref _clearanceStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string ClearanceRemarks
        {
            get { return _clearanceRemarks; }
            set { SetPropertyValue(nameof(ClearanceRemarks), ref _clearanceRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan OhnAct
        {
            get { return _ohnAct; }
            set { 
                SetPropertyValue(nameof(OhnAct), ref _ohnAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan OhnStd
        {
            get { return _ohnStd; }
            set { 
                SetPropertyValue(nameof(OhnStd), ref _ohnStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string OhnRemarks
        {
            get { return _ohnRemarks; }
            set { SetPropertyValue(nameof(OhnRemarks), ref _ohnRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan ManueversAct
        {
            get { return _manueversAct; }
            set { 
                SetPropertyValue(nameof(ManueversAct), ref _manueversAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan ManueversStd
        {
            get { return _manueversStd; }
            set { 
                SetPropertyValue(nameof(ManueversStd), ref _manueversStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string ManueversRemarks
        {
            get { return _manueversRemarks; }
            set { SetPropertyValue(nameof(ManueversRemarks), ref _manueversRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan SailingBallastAct
        {
            get { return _sailingBallastAct; }
            set { 
                SetPropertyValue(nameof(SailingBallastAct), ref _sailingBallastAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan SailingBallastStd
        {
            get { return _sailingBallastStd; }
            set { 
                SetPropertyValue(nameof(SailingBallastStd), ref _sailingBallastStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string SailingBallastRemarks
        {
            get { return _sailingBallastRemarks; }
            set { SetPropertyValue(nameof(SailingBallastRemarks), ref _sailingBallastRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan LoadingPrepAct
        {
            get { return _loadingPrepAct; }
            set { 
                SetPropertyValue(nameof(LoadingPrepAct), ref _loadingPrepAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan LoadingPrepStd
        {
            get { return _loadingPrepStd; }
            set { 
                SetPropertyValue(nameof(LoadingPrepStd), ref _loadingPrepStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string LoadingPrepRemarks
        {
            get { return _loadingPrepRemarks; }
            set { SetPropertyValue(nameof(LoadingPrepRemarks), ref _loadingPrepRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan LoadingAct
        {
            get { return _loadingAct; }
            set { 
                SetPropertyValue(nameof(LoadingAct), ref _loadingAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan LoadingStd
        {
            get { return _loadingStd; }
            set { 
                SetPropertyValue(nameof(LoadingStd), ref _loadingStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string LoadingRemarks
        {
            get { return _loadingRemarks; }
            set { SetPropertyValue(nameof(LoadingRemarks), ref _loadingRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan SoundingAndCalcAct
        {
            get { return _soundingAndCalcAct; }
            set { 
                SetPropertyValue(nameof(SoundingAndCalcAct), ref _soundingAndCalcAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan SoundingAndCalcStd
        {
            get { return _soundingAndCalcStd; }
            set { 
                SetPropertyValue(nameof(SoundingAndCalcStd), ref _soundingAndCalcStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string SoundingAndCalcRemarks
        {
            get { return _soundingAndCalcRemarks; }
            set { SetPropertyValue(nameof(SoundingAndCalcRemarks), ref _soundingAndCalcRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan SailingLadenAct
        {
            get { return _sailingLadenAct; }
            set { 
                SetPropertyValue(nameof(SailingLadenAct), ref _sailingLadenAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan SailingLadenStd
        {
            get { return _sailingLadenStd; }
            set { 
                SetPropertyValue(nameof(OhnStd), ref _sailingLadenStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string SailingLadenRemarks
        {
            get { return _sailingLadenRemarks; }
            set { SetPropertyValue(nameof(SailingLadenRemarks), ref _sailingLadenRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan DischargePrepAct
        {
            get { return _dischargePrepAct; }
            set { 
                SetPropertyValue(nameof(DischargePrepAct), ref _dischargePrepAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan DischargePrepStd
        {
            get { return _dischargePrepStd; }
            set { 
                SetPropertyValue(nameof(DischargePrepStd), ref _dischargePrepStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string DischargePrepRemarks
        {
            get { return _dischargePrepRemarks; }
            set { SetPropertyValue(nameof(DischargePrepRemarks), ref _dischargePrepRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan DischargeAct
        {
            get { return _dischargeAct; }
            set { 
                SetPropertyValue(nameof(DischargePrepAct), ref _dischargeAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan DischargeStd
        {
            get { return _dischargeStd; }
            set { 
                SetPropertyValue(nameof(DischargePrepStd), ref _dischargeStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string DischargeRemarks
        {
            get { return _dischargeRemarks; }
            set { SetPropertyValue(nameof(DischargeRemarks), ref _dischargeRemarks, value); }
        }

        #endregion Activity Time

        #region Waiting Time

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan WaitDocCargoAct
        {
            get { return _waitDocCargoAct; }
            set { 
                SetPropertyValue(nameof(WaitDocCargoAct), ref _waitDocCargoAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan WaitDocCargoStd
        {
            get { return _waitDocCargoStd; }
            set { 
                SetPropertyValue(nameof(WaitDocCargoStd), ref _waitDocCargoStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string WaitDocCargoRemarks
        {
            get { return _waitDocCargoRemarks; }
            set { SetPropertyValue(nameof(WaitDocCargoRemarks), ref _waitDocCargoRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan WaitDocClearAct
        {
            get { return _waitDocClearAct; }
            set { 
                SetPropertyValue(nameof(WaitDocClearAct), ref _waitDocClearAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan WaitDocClearStd
        {
            get { return _waitDocClearStd; }
            set { 
                SetPropertyValue(nameof(WaitDocClearStd), ref _waitDocClearStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string WaitDocClearRemarks
        {
            get { return _waitDocClearRemarks; }
            set { SetPropertyValue(nameof(WaitDocClearRemarks), ref _waitDocClearRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan WaitLoadingAct
        {
            get { return _waitLoadingAct; }
            set { 
                SetPropertyValue(nameof(WaitLoadingAct), ref _waitLoadingAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan WaitLoadingStd
        {
            get { return _waitLoadingStd; }
            set { 
                SetPropertyValue(nameof(WaitLoadingStd), ref _waitLoadingStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string WaitLoadingRemarks
        {
            get { return _waitLoadingRemarks; }
            set { SetPropertyValue(nameof(WaitLoadingRemarks), ref _waitLoadingRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan WaitUnloadingAct
        {
            get { return _waitUnloadingAct; }
            set { 
                SetPropertyValue(nameof(WaitUnloadingAct), ref _waitUnloadingAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan WaitUnloadingStd
        {
            get { return _waitUnloadingStd; }
            set { 
                SetPropertyValue(nameof(WaitUnloadingStd), ref _waitUnloadingStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string WaitUnloadingRemarks
        {
            get { return _waitUnloadingRemarks; }
            set { SetPropertyValue(nameof(WaitUnloadingRemarks), ref _waitUnloadingRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan StandbyAct
        {
            get { return _standbyAct; }
            set { 
                SetPropertyValue(nameof(StandbyAct), ref _standbyAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan StandbyStd
        {
            get { return _standbyStd; }
            set { 
                SetPropertyValue(nameof(StandbyStd), ref _standbyStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string StandbyRemarks
        {
            get { return _standbyRemarks; }
            set { SetPropertyValue(nameof(StandbyRemarks), ref _standbyRemarks, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan BreakdownAct
        {
            get { return _breakdownAct; }
            set { 
                SetPropertyValue(nameof(BreakdownAct), ref _breakdownAct, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public TimeSpan BreakdownStd
        {
            get { return _breakdownStd; }
            set { 
                SetPropertyValue(nameof(BreakdownStd), ref _breakdownStd, value);
                if (!IsLoading)
                {
                    HitungTotal();
                }
            }
        }

        [VisibleInListView(false)]
        public string BreakdownRemarks
        {
            get { return _breakdownRemarks; }
            set { SetPropertyValue(nameof(BreakdownRemarks), ref _breakdownRemarks, value); }
        }

        #endregion Waiting Time

        #region Total and Status

        [VisibleInListView(false)]
        [Appearance("TotalActivityActDisable", Enabled = false)]
        public TimeSpan TotalActivityAct
        {
            get { return _totalActivityAct; }
            set { SetPropertyValue(nameof(TotalActivityAct), ref _totalActivityAct, value); }
        }

        [VisibleInListView(false)]
        [Appearance("TotalActivityStdDisable", Enabled = false)]
        public TimeSpan TotalActivityStd
        {
            get { return _totalActivityStd; }
            set { SetPropertyValue(nameof(TotalActivityStd), ref _totalActivityStd, value); }
        }

        [VisibleInListView(false)]
        [Appearance("TotalWaitingActDisable", Enabled = false)]
        public TimeSpan TotalWaitingAct
        {
            get { return _totalWaitingAct; }
            set { SetPropertyValue(nameof(TotalWaitingAct), ref _totalWaitingAct, value); }
        }

        [Appearance("StatusCycleTimeDisable", Enabled = false)]
        public StatusPerformance StatusCycleTime
        {
            get { return _statusCycleTime; }
            set { SetPropertyValue(nameof(StatusCycleTime), ref _statusCycleTime, value); }
        }

        #endregion Total and Status

        #endregion Field

        #region Code

        private void HitungTotal()
        {
            this.TotalWaitingAct = this.WaitDocCargoAct + this.WaitDocClearAct + this.WaitLoadingAct + this.WaitUnloadingAct + this.StandbyAct + this.BreakdownAct;
            this.TotalActivityAct = this.ClearanceAct + this.OhnAct + this.ManueversAct + this.SailingBallastAct + this.LoadingPrepAct + this.LoadingAct +
                                    this.SoundingAndCalcAct + this.SailingLadenAct + this.DischargePrepAct + this.DischargeAct;
            this.TotalActivityStd = this.ClearanceStd + this.OhnStd + this.ManueversStd + this.SailingBallastStd + this.LoadingPrepStd + this.LoadingStd +
                                    this.SoundingAndCalcStd + this.SailingLadenStd + this.DischargePrepStd + this.DischargeStd;
            if (this.TotalActivityStd >= this.TotalActivityAct)
            {
                this.StatusCycleTime = StatusPerformance.Achieved;
            }
            else
            {
                this.StatusCycleTime = StatusPerformance.Failed;
            }
        }

        #endregion Code
    }
}