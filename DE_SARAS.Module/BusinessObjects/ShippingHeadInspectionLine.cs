﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Item")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("ShippingHeadInspectionLineRuleUnique", DefaultContexts.Save, "Code")]

    public class ShippingHeadInspectionLine : BaseObject
    {
        #region initialization

        private string _code;
        private ShippingMasterInspection _item;
        private ShippingHeadInspection _statusHead;

        private DateTime _docDate;
        private bool _active;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public ShippingHeadInspectionLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.Active = true;
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShippingHeadInspectionLine);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public ShippingMasterInspection Item
        {
            get { return _item; }
            set { SetPropertyValue(nameof(Item), ref _item, value); }
        }

        [Association("ShippingHeadInspection-ShippingHeadInspectionLines")]
        public ShippingHeadInspection StatusHead
        {
            get { return _statusHead; }
            set { SetPropertyValue(nameof(StatusHead), ref _statusHead, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field

    }
}