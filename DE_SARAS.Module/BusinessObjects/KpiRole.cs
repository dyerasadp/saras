﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Map.Native;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("KPI Setting")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("KpiRoleRuleUnique", DefaultContexts.Save, "Code")]

    public class KpiRole : BaseObject
    { 
       #region Initialization

        private string _code;
        private string _name;
        private double _ytdtarget;
        private KpiYear _yearKpi;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private JobPosition _defaultPosition;
        private bool _active;
        private string _localUserAccess;
        private GlobalFunction _globFunc;


        #endregion Initialization
        public KpiRole(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                //this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.KpiRole);
                #region UserAccess
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));

                Company _locCompany = null;
                OrganizationDimension1 _locOrgDim1 = null;
                OrganizationDimension2 _locOrgDim2 = null;
                OrganizationDimension3 _locOrgDim3 = null;
                OrganizationDimension4 _locOrgDim4 = null;
                Employee _locEmployee = null;



                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        _locEmployee = _locUserAccess.Employee;
                        if (_locUserAccess.Employee.Company != null) { _locCompany = _locUserAccess.Employee.Company; }
                        if (_locUserAccess.Employee.OrganizationDimension1 != null) { _locOrgDim1 = _locUserAccess.Employee.OrganizationDimension1; }
                        if (_locUserAccess.Employee.OrganizationDimension2 != null) { _locOrgDim2 = _locUserAccess.Employee.OrganizationDimension2; }
                        if (_locUserAccess.Employee.OrganizationDimension3 != null) { _locOrgDim3 = _locUserAccess.Employee.OrganizationDimension3; }
                        if (_locUserAccess.Employee.OrganizationDimension4 != null) { _locOrgDim4 = _locUserAccess.Employee.OrganizationDimension4; }

                    }
                    if (this.Session != null)
                    {
                        this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.KpiRole);
                    }

                    this.Company = _locCompany;
                    this.OrgDim1 = _locOrgDim1;
                    this.OrgDim2 = _locOrgDim2;
                    this.OrgDim3 = _locOrgDim3;
                    this.OrgDim4 = _locOrgDim4;
                }
                #endregion UserAccess
            }
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [XafDisplayName("YTD Target %")]
        public double Ytdtarget
        {
            get { return _ytdtarget; }
            set { SetPropertyValue(nameof(Ytdtarget), ref _ytdtarget, value); }
        }

        [XafDisplayName("Year")]
        public KpiYear YearKpi
        {
            get { return _yearKpi; }
            set { SetPropertyValue(nameof(YearKpi), ref _yearKpi, value); }
        }
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Departement")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Departement")]
        [Appearance("OrganizationDimension3Close", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("-")]
        [Appearance("OrganizationDimension4Close", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }


        [ImmediatePostData]
        [XafDisplayName("Position")]
        public JobPosition DefaultPosition
        {
            get { return _defaultPosition; }
            set { SetPropertyValue(nameof(DefaultPosition), ref _defaultPosition, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field

        #region Manny

        [Association("KpiRole-KpiRoleLines")]
        public XPCollection<KpiRoleLine> KpiRoleLines
        {
            get { return GetCollection<KpiRoleLine>(nameof(KpiRoleLines)); }
        }

        #endregion Manny
    }
}