﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselDockingCategoryRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselDocking : BaseObject
    {

        #region initialization

        private string _code;
        private string _projectName;
        private DateTime _planDateStart;
        private DateTime _planDateFinish;
        private DateTime _actualDateStart;
        private DateTime _actualDateFinish;
        private Vessel _vessel;
        private Status _status;
        private DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public VesselDocking(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Status = Status.Open;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselDocking);
            #endregion Numbering
        }

        #region Field

        [VisibleInListView(false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string ProjectName
        {
            get { return _projectName; }
            set { SetPropertyValue(nameof(ProjectName), ref _projectName, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime PlanDateStart
        {
            get { return _planDateStart; }
            set { SetPropertyValue(nameof(PlanDateStart), ref _planDateStart, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime PlanDateFinish
        {
            get { return _planDateFinish; }
            set { SetPropertyValue(nameof(PlanDateFinish), ref _planDateFinish, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime ActualDateStart
        {
            get { return _actualDateStart; }
            set { SetPropertyValue(nameof(ActualDateStart), ref _actualDateStart, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime ActualDateFinish
        {
            get { return _actualDateFinish; }
            set { SetPropertyValue(nameof(ActualDateFinish), ref _actualDateFinish, value); }
        }

        [Association("Vessel-VesselDockings")]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue(nameof(Status), ref _status, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

        #region Manny

        [Association("VesselDocking-VesselDockingLinesPlan")]
        public XPCollection<VesselDockingLinePlan> Plan
        {
            get { return GetCollection<VesselDockingLinePlan>(nameof(Plan)); }
        }

        [Association("VesselDocking-VesselDockingLinesActual")]
        public XPCollection<VesselDockingLineActual> Actual
        {
            get { return GetCollection<VesselDockingLineActual>(nameof(Actual)); }
        }

        #endregion Manny
    }
}