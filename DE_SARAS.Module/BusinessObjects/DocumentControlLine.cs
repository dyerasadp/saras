﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Document Control")]
    [RuleCombinationOfPropertiesIsUnique("DocumentControlLineRuleUnique", DefaultContexts.Save, "Code")]

    public class DocumentControlLine : BaseObject
    {
        #region Initialization
        private string _code;
        private string _name;
        private string _document;
        private FileData _attachment;
        private DateTime _startDate;
        private DateTime _endDate;
        private DateTime _docDate;
        private bool _active;
        private DocumentControl _documentControl;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public DocumentControlLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [Size(SizeAttribute.Unlimited)]
        [EditorAlias(EditorAliases.RichTextPropertyEditor)]
        public string Document
        {
            get { return _document; }
            set { SetPropertyValue(nameof(Document), ref _document, value); }
        }

        public FileData Attachment
        {
            get { return _attachment; }
            set { SetPropertyValue(nameof(Attachment), ref _attachment, value); }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue(nameof(StartDate), ref _startDate, value); }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue(nameof(EndDate), ref _endDate, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [Association("DocumentControl-DocumentControlLines")]
        public DocumentControl DocumentControl
        {
            get { return _documentControl; }
            set { SetPropertyValue(nameof(DocumentControl), ref _documentControl, value); }
        }

        #endregion Field

    }
}