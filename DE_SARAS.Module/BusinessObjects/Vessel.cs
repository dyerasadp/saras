﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.PivotGrid.PivotTable;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Master")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("VesselRuleUnique", DefaultContexts.Save, "Code")]
    public class Vessel : BaseObject
    {

        #region initialization

        private string _code;
        private string _name;
        private string _kodeVoyage;
        private VesselType _type;
        private string _engine;
        private VesselHull _hullStructure;
        private string _gt;
        private Company _operationalCompany;
        private string _documentCompany;
        private string _dimension;
        private string _year;
        private string _cargo;
        private StatusOwnershipVessel _vesselState;
        private XPCollection<Employee> _availableMsEmployee;
        private XPCollection<Employee> _availableTsEmployee;
        private Employee _marineSuperintendent;
        private Employee _technicalSupport;
        private bool _active;
        private GlobalFunction _globFunc;
        private DateTime _docDate;

        #endregion initialization

        public Vessel(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            //_globFunc = new GlobalFunction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Active = true;

                #region Numbering
                    _globFunc = new GlobalFunction();
                    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Vessel);
                    //var (result, lastValue) = _globFunc.GetNumberingUnlockOptimisticRecordAlt(this.Session.DataLayer, ObjectList.Vessel);
                    //this.Code = result;
                    //this.LastValue = lastValue;
                #endregion Numbering
            }
        }

        //protected override void OnSaving()
        //{
        //    base.OnSaving();
        //    if (!IsLoading && Session.IsNewObject(this))
        //    {
        //        UpdateNumbering();
        //    }
        //}

        #region Field

        [VisibleInListView(false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [XafDisplayName("Vessel Name")]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [XafDisplayName("Vessel Code")]
        [VisibleInListView(false)]
        public string KodeVoyage
        {
            get { return _kodeVoyage; }
            set { SetPropertyValue(nameof(KodeVoyage), ref _kodeVoyage, value); }
        }
        
        [VisibleInListView(false)]
        public VesselType Type
        {
            get { return _type; }
            set { SetPropertyValue(nameof(Type), ref _type, value); }
        }

        public string Engine
        {
            get { return _engine; }
            set { SetPropertyValue(nameof(Engine), ref _engine, value); }
        }

        public VesselHull HullStructure
        {
            get { return _hullStructure; }
            set { SetPropertyValue(nameof(HullStructure), ref _hullStructure, value); }
        }

        [XafDisplayName("Gross Tonnage (GT)")]
        public string GT
        {
            get { return _gt; }
            set { SetPropertyValue(nameof(GT), ref _gt, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Company")]
        public Company OperationalCompany
        {
            get { return _operationalCompany; }
            set { SetPropertyValue(nameof(OperationalCompany), ref _operationalCompany, value); }
        }

        [VisibleInListView(false)]
        public string DocumentCompany
        {
            get { return _documentCompany; }
            set { SetPropertyValue(nameof(DocumentCompany), ref _documentCompany, value); }
        }

        public string Dimension
        {
            get { return _dimension; }
            set { SetPropertyValue(nameof(Dimension), ref _dimension, value); }
        }

        public string Year
        {
            get { return _year; }
            set { SetPropertyValue(nameof(Year), ref _year, value); }
        }

        [VisibleInListView(false)]
        public string Cargo
        {
            get { return _cargo; }
            set { SetPropertyValue(nameof(Cargo), ref _cargo, value); }
        }

        [VisibleInListView(false)]
        public StatusOwnershipVessel State
        {
            get { return _vesselState; }
            set { SetPropertyValue(nameof(State), ref _vesselState, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailableMs
        {
            get
            {
                if (!IsLoading)
                {
                    JobPosition _jobMarineSuperXPO = this.Session.FindObject<JobPosition>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Name", "Marine Superintendent")
                                                                    ));
                    if (_jobMarineSuperXPO != null)
                    {
                        XPCollection<Employee> _locMsEmployees = new XPCollection<Employee>(this.Session,
                                                                           new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("Active", true),
                                                                           new BinaryOperator("DefaultPosition", _jobMarineSuperXPO)
                                                                           ));
                        if (_locMsEmployees != null && _locMsEmployees.Count > 0)
                        {
                            _availableMsEmployee = _locMsEmployees;
                        }
                    }
                }
                return _availableMsEmployee;
            }
        }

        [VisibleInListView(false)]
        [DataSourceProperty("AvailableMs", DataSourcePropertyIsNullMode.SelectNothing)]
        public Employee MarineSuperintendent
        {
            get { return _marineSuperintendent; }
            set { SetPropertyValue(nameof(_marineSuperintendent), ref _marineSuperintendent, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailableTs
        {
            get
            {
                if (!IsLoading)
                {
                    JobPosition _jobTechSuppXPO = this.Session.FindObject<JobPosition>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Name", "Technical Support")
                                                                    ));

                    if (_jobTechSuppXPO != null)
                    {
                        XPCollection<Employee> _locTsEmployees = new XPCollection<Employee>(this.Session,
                                                                           new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("Active", true),
                                                                           new BinaryOperator("DefaultPosition", _jobTechSuppXPO)
                                                                           ));
                        

                        if (_locTsEmployees != null && _locTsEmployees.Count > 0)
                        {
                            _availableTsEmployee = _locTsEmployees;
                        }
                    }
                }
                return _availableTsEmployee;
            }
        }

        [VisibleInListView(false)]
        [DataSourceProperty("AvailableTs", DataSourcePropertyIsNullMode.SelectNothing)]
        public Employee TechnicalSupport
        {
            get { return _technicalSupport; }
            set { SetPropertyValue(nameof(TechnicalSupport), ref _technicalSupport, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

        #region Manny

        [Association("Vessel-VesselDocuments")]
        public XPCollection<VesselDocument> Certificates
        {
            get { return GetCollection<VesselDocument>(nameof(Certificates)); }
        }

        [Association("Vessel-Crews")]
        public XPCollection<Crew> CrewList
        {
            get { return GetCollection<Crew>(nameof(CrewList)); }
        }

        [Association("Vessel-Voyages")]
        public XPCollection<Voyage> Voyages
        {
            get { return GetCollection<Voyage>(nameof(Voyages)); }
        }

        [Association("Vessel-DailyVesselMonitorings")]
        public XPCollection<DailyVesselMonitoring> DailyStatus
        {
            get { return GetCollection<DailyVesselMonitoring>(nameof(DailyStatus)); }
        }

        [Association("Vessel-VesselParts")]
        public XPCollection<VesselPart> VesselItem
        {
            get { return GetCollection<VesselPart>(nameof(VesselItem)); }
        }

        [Association("Vessel-VesselInspections")]
        public XPCollection<VesselInspection> InspectionResult
        {
            get { return GetCollection<VesselInspection>(nameof(InspectionResult)); }
        }

        [Association("Vessel-CrewSafemannings")]
        public XPCollection<CrewSafemanning> Safemanning
        {
            get { return GetCollection<CrewSafemanning>(nameof(Safemanning)); }
        }

        [Association("Vessel-VesselDockings")]
        public XPCollection<VesselDocking> Docking
        {
            get { return GetCollection<VesselDocking>(nameof(Docking)); }
        }

        #endregion Manny

        #region numbering
        //public void UpdateNumbering()
        //{
        //    try
        //    {
        //            _globFunc = new GlobalFunction();
        //            _globFunc.SaveNumberingUnlockOptimisticRecordAlt(this.Session.DataLayer, ObjectList.Vessel, this.LastValue);
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError(" BusinessObject = Vessel " + ex.ToString());
        //    }
        //}

        #endregion numbering
    }
}