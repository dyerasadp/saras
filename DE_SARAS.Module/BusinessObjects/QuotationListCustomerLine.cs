﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("QuotationListCustomerRuleUnique", DefaultContexts.Save, "Code")]

    public class QuotationListCustomerLine : BaseObject
    {
        #region Initialization

        private string _code;
        private QuotationCustomer _name;
        private string _email;
        private DateTime _docDate;
        private Quotation _quotation;
        private Company _company;
        private XPCollection<Quotation> _availableQuotation;
        private XPCollection<QuotationCustomer> _availableQuotationCustomer;


        #endregion Initialization

        public QuotationListCustomerLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }
            
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<QuotationCustomer> AvailableQuotationCustomer
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<QuotationCustomer> _locQuotationCustomers = new XPCollection<QuotationCustomer>(this.Session);
                    if (_locQuotationCustomers != null && _locQuotationCustomers.Count() > 0)
                    {
                        _availableQuotationCustomer = _locQuotationCustomers;
                    }
                }
                return _availableQuotationCustomer;
            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableQuotationCustomer", DataSourcePropertyIsNullMode.SelectNothing)]
        public QuotationCustomer Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value);
                if (!IsLoading)
                {
                    if (_name != null)
                    {
                        this.Email = _name.Email;
                    }
                }
            }
        }

        public string Email
        {
            get { return _email; }
            set { SetPropertyValue(nameof(Email), ref _email, value); }
        }

        [Appearance("DateTimeClosed", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [Browsable(false)]
        public XPCollection<Quotation> AvailableQuotation
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<Quotation> _locQuotations = new XPCollection<Quotation>(this.Session);
                    if (_locQuotations != null && _locQuotations.Count() > 0)
                    {
                        _availableQuotation = _locQuotations;
                    }
                }
                return _availableQuotation;
            }
        }

        [ImmediatePostData]
        [Appearance("QuotationListCustomerLineClosed", Enabled = false)]
        [DataSourceProperty("AvailableQuotation", DataSourcePropertyIsNullMode.SelectNothing)]
        [Association("Quotation-QuotationListCustomerLines")]
        public Quotation Quotation
        {
            get { return _quotation; }
            set { SetPropertyValue(nameof(Quotation), ref _quotation, value);
                if (!IsLoading)
                {
                    if (_quotation != null)
                    {
                        this.Company = _quotation.Company;
                    }
                }
            }
        }

        #region Organization

        [Appearance("CompanyQuotationListCustomerLineClosed", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        #endregion Organization

        #endregion Field

    }
}