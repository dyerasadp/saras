﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("QuotationPeriodePertaminaLineRuleUnique", DefaultContexts.Save, "Code")]

    public class QuotationPeriodePertaminaLine : BaseObject
    {
        #region Initialization

        private string _code;
        private double _basePrice;
        private double _taxTotal;
        private string _note;
        private QuotationPeriode _quotationPeriode;
        private Employee _employee;
        private Company _company;
        private GlobalFunction _globFunc;
        private XPCollection<Company> _availableCompany;


        #endregion Initialization

        public QuotationPeriodePertaminaLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public double BasePrice
        {
            get { return _basePrice; }
            set { SetPropertyValue(nameof(BasePrice), ref _basePrice, value); }
        }
                
        public double TaxTotal
        {
            get { return _taxTotal; }
            set { SetPropertyValue(nameof(TaxTotal), ref _taxTotal, value); }
        }

        public string Note
        {
            get { return _note; }
            set { SetPropertyValue(nameof(Note), ref _note, value); }
        }

        [Association("QuotationPeriode-QuotationPeriodePertaminaLine")]
        public QuotationPeriode QuotationPeriode
        {
            get { return _quotationPeriode; }
            set { SetPropertyValue(nameof(QuotationPeriode), ref _quotationPeriode, value); }
        }

        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        #endregion Field


    }
}