﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Organization")]
    [RuleCombinationOfPropertiesIsUnique("CallCenterRuleUnique", DefaultContexts.Save, "Code")]

    public class CallCenter : BaseObject
    {
        #region Initialization
        private bool _activationPosting;
        private string _code;
        private Employee _employee;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private JobPosition _jobPosition;
        private OrganizationDimension2 _requestToCallCenter;
        private TicketHelpType _helpType;
        private TicketTopicTypeLine _topicType;
        private EmployeeAssets _employeeAsset;
        private string _ticketSubject;
        private string _ticketDescription;
        private StatusTicket _statusTicket;
        private DateTime _docDate;
        private TimeSpan _docTime;
        private FileData _attachment;
        private GlobalFunction _globFunc;
        private string _localUserAccess;
        private XPCollection<Employee> _availableEmployee;
        private XPCollection<OrganizationDimension2> _availableOrganizationDimension2;



        #endregion Initialization
        public CallCenter(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                this.StatusTicket = StatusTicket.Open;
                this.DocDate = DateTime.Now;

                #region UserAccess
                _globFunc = new GlobalFunction();
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));

                Company _locCompany = null;
                OrganizationDimension1 _locOrgDim1 = null;
                OrganizationDimension2 _locOrgDim2 = null;
                OrganizationDimension3 _locOrgDim3 = null;
                OrganizationDimension4 _locOrgDim4 = null;
                Employee _locEmployee = null;

                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        _locEmployee = _locUserAccess.Employee;
                        if (_locUserAccess.Employee.Company != null) { _locCompany = _locUserAccess.Employee.Company; }
                        if (_locUserAccess.Employee.OrganizationDimension1 != null) { _locOrgDim1 = _locUserAccess.Employee.OrganizationDimension1; }
                        if (_locUserAccess.Employee.OrganizationDimension2 != null) { _locOrgDim2 = _locUserAccess.Employee.OrganizationDimension2; }
                        if (_locUserAccess.Employee.OrganizationDimension3 != null) { _locOrgDim3 = _locUserAccess.Employee.OrganizationDimension3; }
                        if (_locUserAccess.Employee.OrganizationDimension4 != null) { _locOrgDim4 = _locUserAccess.Employee.OrganizationDimension4; }
                    }
                    if (this.Session != null)
                    {
                        this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CallCenter);
                    }


                    this.Company = _locCompany;
                    this.OrgDim1 = _locOrgDim1;
                    this.OrgDim2 = _locOrgDim2;
                    this.OrgDim3 = _locOrgDim3;
                    this.OrgDim4 = _locOrgDim4;
                    this.Employee = _locEmployee;

                }
                #endregion UserAccess
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue(nameof(ActivationPosting), ref _activationPosting, value); }
        }

        [Appearance("CodeClosed", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        #region Organization

        [Browsable(false)]
        public XPCollection<Employee> AvailableEmployee
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<Employee> _locEmployees = new XPCollection<Employee>(Session,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true)));
                    if (_locEmployees != null && _locEmployees.Count() > 0)
                    {
                        _availableEmployee = _locEmployees;
                    }
                }
                return _availableEmployee;
            }
        }

        [ImmediatePostData()]
        [Appearance("EmployeeClosed", Enabled = false)]
        [DataSourceProperty("AvailableEmployee", DataSourcePropertyIsNullMode.SelectNothing)]

        public Employee Employee
        {
            get { return _employee; }
            set
            {
                SetPropertyValue(nameof(Employee), ref _employee, value);
                if (!IsLoading)
                {
                    if (_employee != null)
                    {
                        this.Company = _employee.Company;
                        this.OrgDim1 = _employee.OrganizationDimension1;
                        this.OrgDim2 = _employee.OrganizationDimension2;
                        this.OrgDim3 = _employee.OrganizationDimension3;
                        this.OrgDim4 = _employee.OrganizationDimension4;
                        this.JobPosition = _employee.DefaultPosition;
                    }
                }
                else
                {
                    this.Company = null;
                    this.OrgDim1 = null;
                    this.OrgDim2 = null;
                    this.OrgDim3 = null;
                    this.OrgDim4 = null;
                    this.JobPosition = null;
                }
            }
        }

        [Appearance("JobPositionClosed", Enabled = false)]
        public JobPosition JobPosition
        {
            get { return _jobPosition; }
            set { SetPropertyValue(nameof(JobPosition), ref _jobPosition, value); }
        }

        [ImmediatePostData()]
        [Appearance("CompanyCallCenterClosed", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1CallCenterClosed", Enabled = false)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Dept")]
        [Appearance("OrganizationDimension2CallCenterClosed", Enabled = false)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Dept")]
        [Appearance("OrganizationDimension3CallCenterClosed", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Section")]
        [Appearance("OrganizationDimension4CallCenterClosed", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        #endregion Organization

        [Browsable(false)]
        public XPCollection<OrganizationDimension2> AvailableOrganizationDimension2
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<OrganizationDimension2> _locOrgDim2s = new XPCollection<OrganizationDimension2>(this.Session,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Active", true),
                                                    new GroupOperator(GroupOperatorType.Or,
                                                    new BinaryOperator("Name", "IT"),
                                                    new BinaryOperator("Name", "MEP")
                                                    )));
                    if (_locOrgDim2s != null && _locOrgDim2s.Count() > 0)
                    {
                        _availableOrganizationDimension2 = _locOrgDim2s;
                    } 
                }
                return _availableOrganizationDimension2;
            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableOrganizationDimension2", DataSourcePropertyIsNullMode.SelectNothing)]
        [DataSourceCriteria("Active = true")]
        public OrganizationDimension2 RequestTo
        {
            get { return _requestToCallCenter; }
            set { SetPropertyValue(nameof(RequestTo), ref _requestToCallCenter, value); }
        }

        [Appearance("TicketHelpTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("OrgDim2 = '@This.RequestTo' And Active = 'true'")]
        public TicketHelpType HelpType
        {
            get { return _helpType; }
            set { SetPropertyValue(nameof(HelpType), ref _helpType, value); }
        }

        [Appearance("TicketTopicTypeLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("HelpType = '@This.HelpType' And Active = 'true'")]
        public TicketTopicTypeLine TopicType
        {
            get { return _topicType; }
            set { SetPropertyValue(nameof(TopicType), ref _topicType, value); }
        }

        [Appearance("EmployeeAssetsClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public EmployeeAssets EmployeeAsset
        {
            get { return _employeeAsset; }
            set { SetPropertyValue(nameof(EmployeeAsset), ref _employeeAsset, value); }
        }

        [ImmediatePostData()]
        [Appearance("CallCenterAttachmentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public FileData Attachment
        {
            get { return _attachment; }
            set { SetPropertyValue(nameof(Attachment), ref _attachment, value); }
        }

        [Appearance("TicketSubjectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TicketSubject
        {
            get { return _ticketSubject; }
            set { SetPropertyValue(nameof(TicketSubject), ref _ticketSubject, value); }
        }

        [Appearance("TicketDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(SizeAttribute.Unlimited)]
        [EditorAlias(EditorAliases.RichTextPropertyEditor)]
        public string TicketDescription
        {
            get { return _ticketDescription; }
            set { SetPropertyValue(nameof(TicketDescription), ref _ticketDescription, value); }
        }

        [Appearance("StatusTicketClosed", Enabled = false)]
        public StatusTicket StatusTicket
        {
            get { return _statusTicket; }
            set { SetPropertyValue(nameof(StatusTicket), ref _statusTicket, value); }
        }

        [Appearance("DateTimeClosed", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [Appearance("TimeSpanClosed", Enabled = false)]
        public TimeSpan DocTime
        {
            get { return _docTime; }
            set { SetPropertyValue(nameof(DocTime), ref _docTime, value); }
        }


        #endregion Field

        #region Manny

        [Association("CallCenter-CallCenterFeedbackUsers")]
        public XPCollection<CallCenterFeedbackUser> CallCenterFeedbackUsers
        {
            get { return GetCollection<CallCenterFeedbackUser>(nameof(CallCenterFeedbackUsers)); }
        }


        #endregion Manny
    }
}