﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [XafDisplayName("Departement")]
    [NavigationItem("Organization")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("OrganizationDimension2RuleUnique", DefaultContexts.Save, "Code")]
    public class OrganizationDimension2 : BaseObject
    {
        #region Initialization

        private string _code;
        private string _name;
        private Company _company;
        private bool _active;
        private OrganizationDimension1 _orgDim1;
        private XPCollection<OrganizationDimension1> _availableOrgDim1;
        private DateTime _docDate;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization
        public OrganizationDimension2(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }

        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        [Association("OrganizationDimension1-OrganizationDimension2s")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }


        #endregion Field

        #region Manny

        [XafDisplayName("Sub Departement")]
        [Association("OrganizationDimension2-OrganizationDimension3s")]
        public XPCollection<OrganizationDimension3> OrganizationDimension3s
        {
            get { return GetCollection<OrganizationDimension3>(nameof(OrganizationDimension3s)); }
        }

        [XafDisplayName("Employee")]
        [Association("OrganizationDimension2-Employees")]
        public XPCollection<Employee> Employees
        {
            get { return GetCollection<Employee>(nameof(Employees)); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Manny
    }
}