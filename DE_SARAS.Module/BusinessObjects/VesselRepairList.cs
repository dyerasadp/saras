﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.CodeParser;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselRepairListRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselRepairList : BaseObject
    {

        #region initialization

        private string _code;
        private string _name;
        private Dock _dock;

        private string _refNumber;
        private int _attachNum;
        private string _about;
        private Vessel _vessel;

        private Crew _createdBy;
        private Crew _reportedBy;
        private Crew _knownBy;
        private bool _verifyMS;

        public DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public VesselRepairList(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselRepairList);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        private Dock Dock
        { get { return _dock; }
          set { SetPropertyValue(nameof(Dock), ref _dock, value); }
        }

        public string RefNumber
        {
            get { return _name; }
            set { SetPropertyValue(nameof(RefNumber), ref _name, value); }
        }

        public int AttachNum
        {
            get { return _attachNum; }
            set { SetPropertyValue(nameof(AttachNum), ref _attachNum, value); }
        }

        public string About
        {
            get { return _about; }
            set { SetPropertyValue(nameof(About), ref _about, value); }
        }

        private Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        public Crew CreatedBy
        {
            get { return _createdBy; }
            set { SetPropertyValue(nameof(CreatedBy), ref _createdBy, value); }
        }

        public Crew ReportedBy
        {
            get { return _reportedBy; }
            set { SetPropertyValue(nameof(ReportedBy), ref _reportedBy, value); }
        }

        public Crew KnownBy
        {
            get { return _knownBy; }
            set { SetPropertyValue(nameof(KnownBy), ref _knownBy, value); }
        }

        public bool VerifyMS
        {
            get { return _verifyMS; }
            set { SetPropertyValue(nameof(VerifyMS), ref _verifyMS, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

        #region Manny

        [Association("VesselRepairList-VesselRepairListLines")]
        public XPCollection<VesselRepairListLine> VesselRepairListLines
        {
            get { return GetCollection<VesselRepairListLine>(nameof(VesselRepairListLines)); }
        }

        #endregion Manny

    }
}