﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Task Base")]
    [RuleCombinationOfPropertiesIsUnique("KpiSubmitTaskBaseRuleUnique", DefaultContexts.Save, "Code")]

    public class KpiSubmitTaskBase : BaseObject
    {
        #region Initialization

        private bool _activationPosting;
        private string _code;

        #region InitialOrganization        
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private Employee _employee;
        #endregion InitialOrganization      

        //private double _totAmount;
        private KpiSubmit _kpiSubmit;
        private string _remark;
        private ApprovalLevel _approvalLevel;
        private Status _approvalStatus;
        private DateTime _approvalDate;
        private string _localUserAccess;
        private ApplicationApprovalSetting _applicationApprovalSetting;
        private GlobalFunction _globFunc;
        //AuditTrail
        //private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization
        public KpiSubmitTaskBase(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                #region UserAccess
                _globFunc = new GlobalFunction();
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));

                Company _locCompany = null;
                OrganizationDimension1 _locOrgDim1 = null;
                OrganizationDimension2 _locOrgDim2 = null;
                OrganizationDimension3 _locOrgDim3 = null;
                OrganizationDimension4 _locOrgDim4 = null;
                Employee _locEmployee = null;

                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        _locEmployee = _locUserAccess.Employee;
                        if (_locUserAccess.Employee.Company != null) { _locCompany = _locUserAccess.Employee.Company; }
                        if (_locUserAccess.Employee.OrganizationDimension1 != null) { _locOrgDim1 = _locUserAccess.Employee.OrganizationDimension1; }
                        if (_locUserAccess.Employee.OrganizationDimension2 != null) { _locOrgDim2 = _locUserAccess.Employee.OrganizationDimension2; }
                        if (_locUserAccess.Employee.OrganizationDimension3 != null) { _locOrgDim3 = _locUserAccess.Employee.OrganizationDimension3; }
                        if (_locUserAccess.Employee.OrganizationDimension4 != null) { _locOrgDim4 = _locUserAccess.Employee.OrganizationDimension4; }
                    }
                    if (this.Session != null)
                    {
                        this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.KpiSubmitTaskBase);
                    }


                    this.Company = _locCompany;
                    this.OrgDim1 = _locOrgDim1;
                    this.OrgDim2 = _locOrgDim2;
                    this.OrgDim3 = _locOrgDim3;
                    this.OrgDim4 = _locOrgDim4;
                    this.Employee = _locEmployee;

                }
                #endregion UserAccess
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue(nameof(ActivationPosting), ref _activationPosting, value); }
        }

        [Appearance("PurchaseTaskBaseCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }


        #region Organization

        [ImmediatePostData()]
        [Appearance("PurchaseTaskBaseCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public Company Company
        {
            get { return _company; }
            set
            {
                SetPropertyValue(nameof(Company), ref _company, value);
                if (!IsLoading)
                {
                    if (this._company == null)
                    {
                        this.OrgDim1 = null;
                        this.OrgDim2 = null;
                        this.OrgDim3 = null;
                        this.OrgDim4 = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [XafDisplayName("Workplace")]
        [Appearance("PurchaseTaskBaseOrgDim1Close", Criteria = "ActivationPosting = true", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set
            {
                SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value);
                if (!IsLoading)
                {
                    if (this._orgDim1 == null)
                    {
                        this.OrgDim2 = null;
                        this.OrgDim3 = null;
                        this.OrgDim4 = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [XafDisplayName("Department")]
        [Appearance("PurchaseTaskBaseOrgDim2Close", Criteria = "ActivationPosting = true", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set
            {
                SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value);
                if (!IsLoading)
                {
                    if (this._orgDim2 == null)
                    {
                        this.OrgDim3 = null;
                        this.OrgDim4 = null;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [XafDisplayName("Sub Department")]
        [Appearance("PurchaseTaskBaseOrgDim3Close", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set
            {
                SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value);
                if (!IsLoading)
                {
                    if (this._orgDim3 == null)
                    {
                        this.OrgDim4 = null;
                    }
                }
            }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [ImmediatePostData()]
        [XafDisplayName("Section")]
        [Appearance("PurchaseTaskBaseOrgDim4Close", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("SignatureBy")]
        [Appearance("PurchaseTaskBaseEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        #endregion Organization

        [Appearance("PurchaseTaskBasePurchaseOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public KpiSubmit KpiSubmit
        {
            get { return _kpiSubmit; }
            set { SetPropertyValue("KpiSubmit", ref _kpiSubmit, value); }
        }

        public string Remark
        {
            get { return _remark; }
            set { SetPropertyValue(nameof(Remark), ref _remark, value); }
        }

        [Appearance("PurchaseTaskBaseApprovalLevelEnabled", Enabled = false)]
        public ApprovalLevel ApprovalLevel
        {
            get { return _approvalLevel; }
            set { SetPropertyValue(nameof(ApprovalLevel), ref _approvalLevel, value); }
        }

        [Appearance("PurchaseTaskBaseApprovalStatusEnabled", Enabled = false)]
        public Status ApprovalStatus
        {
            get { return _approvalStatus; }
            set { SetPropertyValue(nameof(ApprovalStatus), ref _approvalStatus, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseTaskBaseApprovalDateEnabled", Enabled = false)]
        public DateTime ApprovalDate
        {
            get { return _approvalDate; }
            set { SetPropertyValue(nameof(ApprovalDate), ref _approvalDate, value); }
        }

        [Browsable(false)]
        public ApplicationApprovalSetting ApplicationApprovalSetting
        {
            get { return _applicationApprovalSetting; }
            set { SetPropertyValue(nameof(ApplicationApprovalSetting), ref _applicationApprovalSetting, value); }
        }

        #endregion Field 

        #region Manny

        //AuditTrail
        //[CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        //public XPCollection<AuditDataItemPersistent> ChangeHistory
        //{
        //    get
        //    {
        //        if (changeHistory == null)
        //        {
        //            changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
        //        }
        //        return changeHistory;
        //    }
        //}

        #endregion Manny
    }
}