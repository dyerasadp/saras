﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Repair Action")]
    [RuleCombinationOfPropertiesIsUnique("VesselRepairProgressRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselRepairProgress : BaseObject
    {
        #region initialization

        private string _code;
        private DateTime _repairDate;
        private Status _status;
        private FileData _file;
        private string _remarks;

        #endregion Initialization

        public VesselRepairProgress(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime RepairDate
        {
            get { return _repairDate; }
            set { SetPropertyValue(nameof(RepairDate), ref _repairDate, value); }
        }

        public Status Type
        {
            get { return _status; }
            set { SetPropertyValue(nameof(Status), ref _status, value); }
        }

        public FileData File
        {
            get { return _file; }
            set { SetPropertyValue(nameof(File), ref _file, value); }
        }

        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue(nameof(Remarks), ref _remarks, value); }
        }

        #endregion Field
    }
}