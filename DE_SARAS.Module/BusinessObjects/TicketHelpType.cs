﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("HelpType")]
    [NavigationItem("Organization")]
    [RuleCombinationOfPropertiesIsUnique("TicketHelpTypeRuleUnique", DefaultContexts.Save, "Code")]

    public class TicketHelpType : BaseObject
    {
        #region Initialization

        private string _code;
        private string _helpType;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private bool _active;
        private string _noted;
        private DateTime _docDate;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization
        public TicketHelpType(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Filed

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string HelpType
        {
            get { return _helpType; }
            set { SetPropertyValue(nameof(HelpType), ref _helpType, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        [Size(512)]
        public string Noted
        {
            get { return _noted; }
            set { SetPropertyValue(nameof(Noted), ref _noted, value); }
        }


        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Workplace")]
        [DataSourceCriteria("Company = '@This.Company' And Active = 'true'")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Departement")]
        [DataSourceCriteria("OrgDim1 = '@This.OrgDim1' And Active = 'true'")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Sub Departement")]
        [DataSourceCriteria("OrgDim2 = '@This.OrgDim2' And Active = 'true'")]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("-")]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }


        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field


        #region Manny

        [Association("TicketHelpType-TicketTopicTypeLines")]
        public XPCollection<TicketTopicTypeLine> TicketTopicTypeLines
        {
            get { return GetCollection<TicketTopicTypeLine>(nameof(TicketTopicTypeLines)); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Manny

    }
}