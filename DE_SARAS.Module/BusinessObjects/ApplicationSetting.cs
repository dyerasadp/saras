﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Setting")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("ApplicationSettingRuleUnique", DefaultContexts.Save, "Code")]
    public class ApplicationSetting : BaseObject
    { 
        #region Initialization

        private string _code;
        private string _name;
        private bool _active;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public ApplicationSetting(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Appearance("ApplicationSettingCodeClose", Enabled = true)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field

        #region Manny
        [Association("ApplicationSetting-ApplicationActionSettings")]
        public XPCollection<ApplicationActionSetting> ApplicationActionSettings
        {
            get { return GetCollection<ApplicationActionSetting>(nameof(ApplicationActionSettings)); }
        }

        [Association("ApplicationSetting-ApplicationApprovalSettings")]
        public XPCollection<ApplicationApprovalSetting> ApplicationApprovalSettings
        {
            get { return GetCollection<ApplicationApprovalSetting>(nameof(ApplicationApprovalSettings)); }
        }

        [Association("ApplicationSetting-ApplicationOrganizationSettings")]
        public XPCollection<ApplicationOrganizationSetting> ApplicationOrganizationSettings
        {
            get { return GetCollection<ApplicationOrganizationSetting>(nameof(ApplicationOrganizationSettings)); }
        }

        //[Association("ApplicationSetting-ApplicationMailSetting")]
        //public XPCollection<ApplicationMailSetting> ApplicationMailSettings
        //{
        //    get { return GetCollection<ApplicationMailSetting>(nameof(ApplicationMailSettings)); }
        //}

        #endregion Manny
    }
}