﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselRepairListLineRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselRepairListLine : BaseObject
    {

        #region initialization

        private string _code;
        private string _name;
        private VesselRepairList _repairList;
        private string _workDesc;
        private string _reccomendation;
        private MediaDataObject _photo;
        private string _remarks;
        private DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion initialization

        public VesselRepairListLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselRepairListLine);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [Association("VesselRepairList-VesselRepairListLines")]
        public VesselRepairList RepairList
        {
            get { return _repairList; }
            set { SetPropertyValue(nameof(RepairList), ref _repairList, value); }
        }

        public string WorkDesc
        {
            get { return _workDesc; }
            set { SetPropertyValue(nameof(WorkDesc), ref _workDesc, value); }
        }

        public string Reccomendation
        {
            get { return _reccomendation; }
            set { SetPropertyValue(nameof(Reccomendation), ref _reccomendation, value); }
        }

        [DevExpress.Xpo.Size(SizeAttribute.Unlimited)]
        //[VisibleInListViewAttribute(true)]
        [VisibleInListView(false)]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.PictureEdit, DetailViewImageEditorMode = ImageEditorMode.PictureEdit, ListViewImageEditorCustomHeight = 40)]
        public MediaDataObject Photo
        {
            get { return _photo; }
            set { SetPropertyValue(nameof(Photo), ref _photo, value); }
        }

        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue(nameof(Remarks), ref _remarks, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

    }
}