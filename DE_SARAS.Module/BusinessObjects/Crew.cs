﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Crewing")]
    [RuleCombinationOfPropertiesIsUnique("CrewRuleUnique", DefaultContexts.Save, "Code")]

    public class Crew : BaseObject
    {
        #region initialization

        private string _code;
        private string _name;
        private string _nrp;
        private JobPosition _defaultPosition;
        private Vessel _defaultVessel;
        private CrewClassList _class;
        private Company _branch;
        private ShippingMasterLocation _portCall;
        private string _email;
        private ShippingMasterInstitusi _institution;
        private string _contactEmg;
        private string _contactPerson;
        private string _placeOfBirth;
        private DateTime _dateOfBirth;
        private MaritalStatus _marital;
        private PTKP _ptkp;
        private Agama _religion;
        private Goldar _bloodType;
        private Gender _gender;
        private string _kk;
        private string _nik;
        private string _npwp;
        private string _address;
        private string _city;
        private string _province;
        private string _zip;
        private string _bpjsTenagaKerja;
        private string _bpjsKesehatan;
        private string _bankName;
        private string _bankAcc;
        private string _bankHolder;
        private FileData _cv;

        private MediaDataObject _photo;

        private StatusForCrew _statusCrew;
        private FileData _blacklistAttachment;

        private DateTime _docDate;
        private bool _active;
        private GlobalFunction _globFunc;

        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization

        public Crew(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Active = true;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Crew);
            #endregion Numbering
        }

        #region Field

        [VisibleInListView(false)]
        
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string NRP
        {
            get { return _nrp; }
            set { SetPropertyValue(nameof(NRP), ref _nrp, value); }
        }

        [Appearance("FontColorForName", Criteria = "StatusCrew = 1", FontColor = "Yellow")]
        [Appearance("FontColorForName2", Criteria = "StatusCrew = 2", FontColor = "Green")]
        [Appearance("FontColorForName3", Criteria = "StatusCrew = 3", FontColor = "Red")]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [ImmediatePostData]
        [XafDisplayName("Default Position")]
        [Appearance("DefaultPositionDisable", Enabled = false)]
        public JobPosition DefaultPosition
        {
            get { return _defaultPosition; }
            set
            { SetPropertyValue(nameof(DefaultPosition), ref _defaultPosition, value); }
        }

        [ImmediatePostData]
        [XafDisplayName("Default Vessel")]
        [Association("Vessel-Crews")]
        [Appearance("DefaultVesselDisable", Enabled = false)]
        public Vessel DefaultVessel
        {
            get { return _defaultVessel; }
            set
            { SetPropertyValue(nameof(DefaultVessel), ref _defaultVessel, value); }
        }

        [ImmediatePostData]
        [Appearance("ClassDisable", Enabled = false)]
        public CrewClassList Class
        {
            get { return _class; }
            set
            { SetPropertyValue(nameof(Class), ref _class, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        public Company Branch
        {
            get { return _branch; }
            set { SetPropertyValue(nameof(Company), ref _branch, value); }
        }

        [VisibleInListView(false)]
        [XafDisplayName("Port of Call")]
        public ShippingMasterLocation PortCall
        {
            get { return _portCall; }
            set { SetPropertyValue(nameof(PortCall), ref _portCall, value); }
        }

        [VisibleInListView(false)]
        public ShippingMasterInstitusi Institution
        {
            get { return _institution; }
            set { SetPropertyValue(nameof(Institution), ref _institution, value); }
        }

        [VisibleInListView(false)]
        public string PlaceOfBirth
        {
            get { return _placeOfBirth; }
            set { SetPropertyValue(nameof(PlaceOfBirth), ref _placeOfBirth, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime DateOfBirth
        {
            get { return _dateOfBirth; }
            set { SetPropertyValue(nameof(DateOfBirth), ref _dateOfBirth, value); }
        }

        [VisibleInListView(false)]
        public MaritalStatus Marital
        {
            get { return _marital; }
            set { SetPropertyValue(nameof(Marital), ref _marital, value); }
        }

        [VisibleInListView(false)]
        [XafDisplayName("PTKP Status")]
        public PTKP Ptkp
        {
            get { return _ptkp; }
            set { SetPropertyValue(nameof(Ptkp), ref _ptkp, value); }
        }

        [VisibleInListView(false)]
        public Agama Religion
        {
            get { return _religion; }
            set { SetPropertyValue(nameof(Religion), ref _religion, value); }
        }

        [VisibleInListView(false)]
        public Gender Gender
        {
            get { return _gender; }
            set { SetPropertyValue(nameof(Gender), ref _gender, value); }
        }

        [VisibleInListView(false)]
        public Goldar BloodType
        {
            get { return _bloodType; }
            set { SetPropertyValue(nameof(BloodType), ref _bloodType, value); }
        }

        [VisibleInListView(false)]
        public string KK
        {
            get { return _kk; }
            set { SetPropertyValue(nameof(KK), ref _kk, value); }
        }

        [VisibleInListView(false)]
        public string Nik
        {
            get { return _nik; }
            set { SetPropertyValue(nameof(Nik), ref _nik, value); }
        }

        [VisibleInListView(false)]
        public string Npwp
        {
            get { return _npwp; }
            set { SetPropertyValue(nameof(Npwp), ref _npwp, value); }
        }

        [VisibleInListView(false)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue(nameof(Address), ref _address, value); }
        }

        [VisibleInListView(false)]
        public string City
        {
            get { return _city; }
            set { SetPropertyValue(nameof(City), ref _city, value); }
        }

        [VisibleInListView(false)]
        public string Province
        {
            get { return _province; }
            set { SetPropertyValue(nameof(Province), ref _province, value); }
        }

        [VisibleInListView(false)]
        public string ZipCode
        {
            get { return _zip; }
            set { SetPropertyValue(nameof(ZipCode), ref _zip, value); }
        }

        [VisibleInListView(false)]
        public string BpjsTenagaKarja
        {
            get { return _bpjsTenagaKerja; }
            set { SetPropertyValue(nameof(BpjsTenagaKarja), ref _bpjsTenagaKerja, value); }
        }

        [VisibleInListView(false)]
        public string BpjsKesehatan
        {
            get { return _bpjsKesehatan; }
            set { SetPropertyValue(nameof(BpjsKesehatan), ref _bpjsKesehatan, value); }
        }

        [VisibleInListView(false)]
        [XafDisplayName("Bank Name")]
        public string BankName
        {
            get { return _bankName; }
            set { SetPropertyValue(nameof(BankName), ref _bankName, value); }
        }

        [VisibleInListView(false)]
        [XafDisplayName("Bank Account")]
        public string BankAcc
        {
            get { return _bankAcc; }
            set { SetPropertyValue(nameof(BankAcc), ref _bankAcc, value); }
        }

        [VisibleInListView(false)]
        [XafDisplayName("Bank Holder")]
        public string BankHolder
        {
            get { return _bankHolder; }
            set { SetPropertyValue(nameof(BankHolder), ref _bankHolder, value); }
        }

        [VisibleInListView(false)]
        public string Email
        {
            get { return _email; }
            set { SetPropertyValue(nameof(Email), ref _email, value); }
        }

        [VisibleInListView(false)]
        [XafDisplayName("Emergency Contact")]
        public string ContactEmg
        {
            get { return _contactEmg; }
            set { SetPropertyValue(nameof(ContactEmg), ref _contactEmg, value); }
        }

        [VisibleInListView(false)]
        [XafDisplayName("Contact Person")]
        public string ContactPerson
        {
            get { return _contactPerson; }
            set { SetPropertyValue(nameof(ContactPerson), ref _contactPerson, value); }
        }

        [DevExpress.Xpo.Size(SizeAttribute.Unlimited)]
        //[VisibleInListViewAttribute(true)]
        [VisibleInListView(false)]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.PictureEdit, DetailViewImageEditorMode = ImageEditorMode.PictureEdit, ListViewImageEditorCustomHeight = 40)]
        public MediaDataObject Photo
        {
            get { return _photo; }
            set { SetPropertyValue(nameof(Photo), ref _photo, value); }
        }

        [VisibleInListView(false)]
        public FileData CV
        {
            get { return _cv; }
            set { SetPropertyValue(nameof(CV), ref _cv, value); }
        }

        [VisibleInListView(false)]
        public FileData BlacklistAttachment
        {
            get { return _blacklistAttachment; }
            set { SetPropertyValue(nameof(BlacklistAttachment), ref _blacklistAttachment, value); }
        }

        public StatusForCrew StatusCrew
        {
            get { return _statusCrew; }
            set { SetPropertyValue(nameof(StatusCrew), ref _statusCrew, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [VisibleInListView(false)]
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field

        #region Manny

        [Association("Crew-CrewContracts")]
        public XPCollection<CrewContract> Contracts
        {
            get { return GetCollection<CrewContract>(nameof(Contracts)); }
        }

        [Association("Crew-CrewCertificateOfCompetences")]
        public XPCollection<CrewCertificateOfCompetence> CertificateOfCompetence
        {
            get { return GetCollection<CrewCertificateOfCompetence>(nameof(CertificateOfCompetence)); }
        }

        [Association("Crew-CrewDocuments")]
        public XPCollection<CrewDocument> OtherCertificates
        {
            get { return GetCollection<CrewDocument>(nameof(OtherCertificates)); }
        }

        [Association("Crew-CrewFamilies")]
        public XPCollection<CrewFamily> Family
        {
            get { return GetCollection<CrewFamily>(nameof(Family)); }
        }

        [Association("Crew-WarningLetters")]
        public XPCollection<WarningLetter> WarningLetters
        {
            get { return GetCollection<WarningLetter>(nameof(WarningLetters)); }
        }

        #endregion Manny

        #region AuditTrail

        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion AuditTrail

    }
}