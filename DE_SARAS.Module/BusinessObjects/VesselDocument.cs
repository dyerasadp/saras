﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.PivotGrid.PivotTable;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Document")]
    [NavigationItem("Document Control")]
    [RuleCombinationOfPropertiesIsUnique("VesselDocumentRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselDocument : BaseObject
    {

        #region Initialization

        private string _code;
        private DocumentShipping _document; 
        private Vessel _vessel;
        private DateTime _issuedDate;
        private DateTime _expiryDate;
        private DateTime _endorseDate;
        private bool _statusPermanent;
        private DocumentRenewalType _renewalType;
        private int _reminder;
        private FileData _documentFile;
        private string _remarks;

        private bool _active;
        private DateTime _docDate;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization

        public VesselDocument(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.Active = true;
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselDocument);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; } 
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [ImmediatePostData()]
        [DataSourceProperty(nameof(FilteredDocuments))]
        public DocumentShipping Document
        {
            get { return _document; }
            set { SetPropertyValue(nameof(Document), ref _document, value); }
        }

        [Browsable(false)]
        public XPCollection<DocumentShipping> FilteredDocuments
        {
            get
            {
                // Filter the DocumentShipping objects with 'Sea' status
                // return new XPCollection<DocumentShipping>(Session, CriteriaOperator.Parse("Type In (?, ?)", DocumentShippingType.SeaVessel, DocumentShippingType.RiverVessel));
                CriteriaOperator criteria = CriteriaOperator.Parse("Type = ? or Type = ?", DocumentShippingType.SeaVessel, DocumentShippingType.RiverVessel);
                return new XPCollection<DocumentShipping>(Session, criteria);
            }
        }

        [ImmediatePostData()]
        [Association("Vessel-VesselDocuments")]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime IssuedDate
        {
            get { return _issuedDate; }
            set { SetPropertyValue(nameof(IssuedDate), ref _issuedDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
            set { SetPropertyValue(nameof(ExpiryDate), ref _expiryDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime EndorseDate
        {
            get { return _endorseDate; }
            set { SetPropertyValue(nameof(EndorseDate), ref _endorseDate, value); }
        }

        public bool StatusPermanent
        {
            get { return _statusPermanent; }
            set { SetPropertyValue(nameof(StatusPermanent), ref _statusPermanent, value); }
        }

        [ImmediatePostData()]
        public DocumentRenewalType RenewalType
        {
            get { return _renewalType; }
            set { SetPropertyValue(nameof(RenewalType), ref _renewalType, value); }
        }

        public FileData DocumentFile
        {
            get { return _documentFile; }
            set { SetPropertyValue(nameof(DocumentFile), ref _documentFile, value); }
        }

        public int Reminder
        {
            get { return _reminder; }
            set { SetPropertyValue(nameof(Reminder), ref _reminder, value); }
        }

        [Size(2000)]
        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue(nameof(Remarks), ref _remarks, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        [Appearance("DocDateDisable", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

        #region Manny

        [Association("VesselDocument-VesselDocumentRenewalLines")]
        public XPCollection<VesselDocumentRenewalLine> HistoryRenewal
        {
            get { return GetCollection<VesselDocumentRenewalLine>(nameof(HistoryRenewal)); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Manny
    }
}