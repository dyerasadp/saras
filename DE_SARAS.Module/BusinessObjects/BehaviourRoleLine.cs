﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Question")]
    [NavigationItem("Behaviour Setting")]
    [RuleCombinationOfPropertiesIsUnique("BehaviourRoleLineRuleUnique", DefaultContexts.Save, "Code")]
    public class BehaviourRoleLine : BaseObject
    {
        #region Initialization

        private string _code;
        private string _question;
        private BehaviourType _type;
        private BehaviourCategory _category;
        private BehaviourRole _behaviourRole;
        private KpiYear _behaviourPeriod;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private bool _active;
        private XPCollection<BehaviourRole> _availableBehaviourRole;


        #endregion Initialization
        public BehaviourRoleLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }


        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Question
        {
            get { return _question; }
            set { SetPropertyValue(nameof(Question), ref _question, value); }
        }

        public KpiYear BehaviourPeriod
        {
            get { return _behaviourPeriod; }
            set { SetPropertyValue(nameof(KpiYear), ref _behaviourPeriod, value); }
        }

        public BehaviourType BehaviourType
        {
            get { return _type; }
            set { SetPropertyValue(nameof(BehaviourType), ref _type, value); }
        }

        public BehaviourCategory BehaviourCategory
        {
            get { return _category; }
            set { SetPropertyValue(nameof(BehaviourCategory), ref _category, value); }
        }

        [Browsable(false)]
        public XPCollection<BehaviourRole> AvailableBehaviourRole
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<BehaviourRole> _locBehaviourRoles = new XPCollection<BehaviourRole>(this.Session,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true)));
                    if (_locBehaviourRoles != null && _locBehaviourRoles.Count() > 0)
                    {
                        _availableBehaviourRole = _locBehaviourRoles;
                    }
                }
                return _availableBehaviourRole;
            }
        }

        [Association("BehaviourRole-BehaviourRoleLines")]
        [DataSourceProperty("AvailableBehaviourRole", DataSourcePropertyIsNullMode.SelectNothing)]
        public BehaviourRole BehaviourRole
        {
            get { return _behaviourRole; }
            set
            {
                SetPropertyValue(nameof(BehaviourRole), ref _behaviourRole, value);
                if (!IsLoading)
                {
                    if (_behaviourRole != null)
                    {
                        this.Company = _behaviourRole.Company;
                        this.OrgDim1 = _behaviourRole.OrgDim1;
                        this.OrgDim2 = _behaviourRole.OrgDim2;
                        this.OrgDim3 = _behaviourRole.OrgDim3;
                        this.OrgDim4 = _behaviourRole.OrgDim4;
                        this.BehaviourPeriod = _behaviourRole.YearBehaviour;

                    }
                    else
                    {
                        this.Company = null;
                        this.OrgDim1 = null;
                        this.OrgDim2 = null;
                        this.OrgDim3 = null;
                        this.OrgDim4 = null;
                        this.BehaviourPeriod = null;
                    }
                }
            }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Departement")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }


        [XafDisplayName("Sub Departement")]
        [Appearance("OrganizationDimension3Close", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [XafDisplayName("-")]
        [Appearance("OrganizationDimension4Close", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }


        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }


        #endregion Field

    }
}