﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Setting")]
    [RuleCombinationOfPropertiesIsUnique("ApplicationApprovalSetupRuleUnique", DefaultContexts.Save, "Code")]
    public class ApplicationApprovalSetting : BaseObject
    {
        #region Initialization

        private string _code;
        private string _name;
        #region InitialOrganization        
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private Employee _employee;
        #endregion InitialOrganization
        private UserAccess _userAccess;
        private ObjectList _objectList;
        private FunctionList _functionList;
        private ApprovalLevel _approvalLevel;
        private bool _lock;
        private bool _posting;
        private bool _endApproval;
        private bool _allInObjectList;
        private bool _active;
        private ApplicationSetting _applicationSetting;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization
        public ApplicationApprovalSetting(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Workplace")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Departement")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Sub Department")]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Section")]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        #endregion Organization

        [ImmediatePostData()]
        [DataSourceCriteria("IsActive = true ")]
        public UserAccess UserAccess
        {
            get { return _userAccess; }
            set
            {
                SetPropertyValue(nameof(UserAccess), ref _userAccess, value);
                if (!IsLoading)
                {
                    if (this._userAccess != null)
                    {
                        if (this._userAccess.Employee != null)
                        {
                            if (this._userAccess.Employee.Name != null)
                            {
                                this.Name = this._userAccess.Employee.Name;
                            }
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue(nameof(ObjectList), ref _objectList, value); }
        }

        public FunctionList FunctionList
        {
            get { return _functionList; }
            set { SetPropertyValue(nameof(FunctionList), ref _functionList, value); }
        }

        public ApprovalLevel ApprovalLevel
        {
            get { return _approvalLevel; }
            set { SetPropertyValue(nameof(ApprovalLevel), ref _approvalLevel, value); }
        }

        public bool Lock
        {
            get { return _lock; }
            set { SetPropertyValue(nameof(Lock), ref _lock, value); }
        }

        public bool Posting
        {
            get { return _posting; }
            set { SetPropertyValue(nameof(Posting), ref _posting, value); }
        }

        public bool EndApproval
        {
            get { return _endApproval; }
            set { SetPropertyValue(nameof(EndApproval), ref _endApproval, value); }
        }

        public bool AllInObjectList
        {
            get { return _allInObjectList; }
            set { SetPropertyValue(nameof(AllInObjectList), ref _allInObjectList, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        //[Appearance("ApplicationApprovalSetupApplicationSettingEnabled", Enabled = false)]s
        [RuleRequiredField(DefaultContexts.Save), ImmediatePostData()]
        [Association("ApplicationSetting-ApplicationApprovalSettings")]
        public ApplicationSetting ApplicationSetting
        {
            get { return _applicationSetting; }
            set { SetPropertyValue(nameof(ApplicationSetting), ref _applicationSetting, value); }
        }

        #endregion Field

        #region Manny

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Manny
    }
}