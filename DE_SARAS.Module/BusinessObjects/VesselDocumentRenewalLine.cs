﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Document")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselDocumentRenewalLineRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselDocumentRenewalLine : BaseObject
    {
        #region initialization

        private string _code;
        private VesselDocumentRenewal _documentRenewal;
        private Vessel _vessel;
        private VesselDocument _document;
        private DateTime _issuedDate;
        private DateTime _proposalDate;
        private DateTime _finishDate;
        private DateTime _endorseDateBefore;
        private DateTime _endorseDateAfter;
        private DateTime _expiredDateBefore;
        private DateTime _expiredDateAfter;
        private Status _status;
        private string _remarks;

        private DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public VesselDocumentRenewalLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.IssuedDate = DateTime.Now;
                this.Status = Status.Progress;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselDocumentRenewalLine);
            #endregion Numbering
        }

        #region Field

        //[Appearance("CodeDisable", Criteria = "Status = C", Enabled = false)]
        [VisibleInListView(false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Association("VesselDocumentRenewal-VesselDocumentRenewalLines")]
        [Appearance("DocumentRenewalDisable", Enabled = false)]
        public VesselDocumentRenewal DocumentRenewal
        {
            get { return _documentRenewal; }
            set { 
                SetPropertyValue(nameof(DocumentRenewal), ref _documentRenewal, value);
                if (!IsLoading)
                {
                    if (_documentRenewal != null)
                    {
                        if (_documentRenewal.Vessel != null)
                        {
                            this.Vessel = _documentRenewal.Vessel;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        [ImmediatePostData]
        [DataSourceProperty(nameof(FilteredDocuments))]
        [Association("VesselDocument-VesselDocumentRenewalLines")]
        public VesselDocument Document
        {
            get { return _document; }
            set { 
                SetPropertyValue(nameof(Document), ref _document, value);
                if (!IsLoading)
                {
                    if (_document != null)
                    {
                        if (_document.EndorseDate != null)
                        {
                            this.EndorseDateBefore = _document.EndorseDate;
                        }
                        if (_document.ExpiryDate != null)
                        {
                            this.ExpiredDateBefore = _document.ExpiryDate;
                        }
                    }
                }
            }
        }


        [Browsable(false)]
        public XPCollection<VesselDocument> FilteredDocuments
        {
            get
            {
                CriteriaOperator criteria = CriteriaOperator.Parse("Vessel = ?", this.Vessel);
                return new XPCollection<VesselDocument>(Session, criteria);
            }
        }


        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime IssuedDate
        {
            get { return _issuedDate; }
            set { SetPropertyValue(nameof(IssuedDate), ref _issuedDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ProposalDate
        {
            get { return _proposalDate; }
            set { SetPropertyValue(nameof(ProposalDate), ref _proposalDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime FinishDate
        {
            get { return _finishDate; }
            set { SetPropertyValue(nameof(FinishDate), ref _finishDate, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("EndorseDateBeforeDisable", Enabled = false)]
        public DateTime EndorseDateBefore
        {
            get { return _endorseDateBefore; }
            set { SetPropertyValue(nameof(EndorseDateBefore), ref _endorseDateBefore, value); }
        }

        [XafDisplayName("Next Endorse Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime EndorseDateAfter
        {
            get { return _endorseDateAfter; }
            set { SetPropertyValue(nameof(EndorseDateAfter), ref _endorseDateAfter, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("ExpiredDateBeforeDisable", Enabled = false)]
        public DateTime ExpiredDateBefore
        {
            get { return _expiredDateBefore; }
            set { SetPropertyValue(nameof(ExpiredDateBefore), ref _expiredDateBefore, value); }
        }

        [XafDisplayName("Next Expiry Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ExpiredDateAfter
        {
            get { return _expiredDateAfter; }
            set { SetPropertyValue(nameof(ExpiredDateAfter), ref _expiredDateAfter, value); }
        }

        [Appearance("StatusDisable", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue(nameof(Status), ref _status, value); }
        }

        [VisibleInListView(false)]
        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue(nameof(Remarks), ref _remarks, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field
    }
}