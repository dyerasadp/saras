﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Organization")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("EmployeeAssetsRuleUnique", DefaultContexts.Save, "Code")]

    public class EmployeeAssets : BaseObject
    {
        #region Initialization

        private string _code;
        private string _name;
        private MediaDataObject _photo;
        private bool _assignee;
        private Employee _employee;
        private JobPosition _jobPosition;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private XPCollection<Employee> _availableEmployee;


        #endregion Initialization
        public EmployeeAssets(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Field
        public bool Assignee
        {
            get { return _assignee; }
            set { SetPropertyValue(nameof(Assignee), ref _assignee, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [ImmediatePostData()]
        [DevExpress.Xpo.Size(SizeAttribute.Unlimited)]
        [VisibleInListViewAttribute(true)]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.PictureEdit, DetailViewImageEditorMode = ImageEditorMode.PictureEdit, ListViewImageEditorCustomHeight = 40)]
        public MediaDataObject Photo
        {
            get { return _photo; }
            set { SetPropertyValue(nameof(Photo), ref _photo, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailableEmployee
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<Employee> _locEmployees = new XPCollection<Employee>(this.Session,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true)
                                                                        ));
                    if (_locEmployees != null && _locEmployees.Count() > 0)
                    {
                        _availableEmployee = _locEmployees;
                    }
                }
                return _availableEmployee;
            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableEmployee", DataSourcePropertyIsNullMode.SelectNothing)]
        [Association("Employee-EmployeeAssetss")]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); 
                if(!IsLoading)
                {
                    this.JobPosition = _employee.DefaultPosition;
                    this.Company = _employee.Company;
                    this.OrgDim1 = _employee.OrganizationDimension1;
                    this.OrgDim2 = _employee.OrganizationDimension2;
                    this.OrgDim3 = _employee.OrganizationDimension3;
                    this.OrgDim4 = _employee.OrganizationDimension4;
                }
            }
        }

        [XafDisplayName("Position")]
        public JobPosition JobPosition
        {
            get { return _jobPosition; }
            set { SetPropertyValue(nameof(JobPosition), ref _jobPosition, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Departement")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Departement")]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("-")]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }


        #endregion Field
    }
}