﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Numbering Setting")]

    public class NumberingHeader : BaseObject
    {
        #region Initialization

        private string _code;
        private string _name;
        private CustomProcess.NumberingType _numberingType;
        private bool _active;

        #endregion Initialization

        public NumberingHeader(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public CustomProcess.NumberingType NumberingType
        {
            get { return _numberingType; }
            set { SetPropertyValue(nameof(NumberingType), ref _numberingType, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field

        #region Manny

        [Association("NumberingHeader-NumberingLines")]
        public XPCollection<NumberingLine> NumberingLines
        {
            get { return GetCollection<NumberingLine>(nameof(NumberingLines)); }
        }

        #endregion Manny


    }
}