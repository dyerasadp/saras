﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Class")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("CrewCertificateOfCompetenceRuleUnique", DefaultContexts.Save, "Code")]

    public class CrewCertificateOfCompetence : BaseObject
    {

        #region initialization

        private string _code;
        private Crew _crew;
        private string _numDocument;
        private CrewClassList _class;
        private DateTime _issuedDate;
        private DateTime _expiryDate;
        private FileData _documentFile;
        private bool _default;
        private DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public CrewCertificateOfCompetence(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CrewCertificateOfCompetence);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [ImmediatePostData]
        [Association("Crew-CrewCertificateOfCompetences")]
        public Crew Crew
        {
            get { return _crew; }
            set { SetPropertyValue(nameof(Crew), ref _crew, value); }
        }

        [XafDisplayName("Document Number")]
        public string NumDocument
        {
            get { return _numDocument; }
            set { SetPropertyValue(nameof(NumDocument), ref _numDocument, value); }
        }

        public CrewClassList Class
        {
            get { return _class; }
            set { SetPropertyValue(nameof(Class), ref _class, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime IssuedDate
        {
            get { return _issuedDate; }
            set { SetPropertyValue(nameof(IssuedDate), ref _issuedDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
            set { SetPropertyValue(nameof(ExpiryDate), ref _expiryDate, value); }
        }

        public FileData DocumentFile
        {
            get { return _documentFile; }
            set { SetPropertyValue(nameof(DocumentFile), ref _documentFile, value); }
        }

        [VisibleInDetailView(false)]
        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue(nameof(Default), ref _default, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field
    }
}