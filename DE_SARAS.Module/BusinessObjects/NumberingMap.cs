﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Numbering Setting")]

    public class NumberingMap : BaseObject
    {
        #region Initialization

        private int _no;
        private string _name;
        private CustomProcess.ObjectList _objectList;
        private string _prefix;
        private string _formatNumber;
        private string _suffix;
        private KpiYear _suffix2;
        private QuotationPeriode _quotationPeriod;
        private int _lastValue;
        private int _incrementValue;
        private int _minValue;
        private string _formatedValue;
        private Company _company;
        private bool _active;
        private bool _default;
        private bool _selection;
        private bool _sign;
        private CustomProcess.NumberingType _numberingType;
        private NumberingHeader _numberingHeader;

        #endregion Initialization

        public NumberingMap(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}