﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Crewing")]
    [RuleCombinationOfPropertiesIsUnique("CrewContractRuleUnique", DefaultContexts.Save, "Code")]

    public class CrewContract : BaseObject
    {

        #region initialization

        private string _code;
        private bool _default;
        private Crew _crew;
        private Vessel _vessel;
        private JobPosition _jobPosition;
        private DateTime _signOnDate;
        private DateTime _signOffDate;
        private DateTime _actualOffDate;
        private StatusCrewOn _statusCrewOn;
        private StatusCrewOff _statusCrewOff;
        private string _remarksOn;
        private string _remarksOff;

        private DateTime _docDate;
        private bool _active;
        private GlobalFunction _globFunc;

        //AuditTrail
        //private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization

        public CrewContract(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Active = true;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CrewContract);
            #endregion Numbering
        }

        #region Field

        [VisibleInListView(false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [Association("Crew-CrewContracts")]
        public Crew Crew
        {
            get { return _crew; }
            set { SetPropertyValue(nameof(Crew), ref _crew, value); }
        }

        [ImmediatePostData()]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        [ImmediatePostData()]
        public JobPosition JobPosition
        {
            get { return _jobPosition; }
            set { SetPropertyValue(nameof(JobPosition), ref _jobPosition, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime SignOnDate
        {
            get { return _signOnDate; }
            set { SetPropertyValue(nameof(SignOnDate), ref _signOnDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime SignOffDate
        {
            get { return _signOffDate; }
            set { SetPropertyValue(nameof(SignOffDate), ref _signOffDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ActualOffDate
        {
            get { return _actualOffDate; }
            set { SetPropertyValue(nameof(ActualOffDate), ref _actualOffDate, value); }
        }

        [ImmediatePostData()]
        public StatusCrewOn StatusCrewOn
        {
            get { return _statusCrewOn; }
            set { SetPropertyValue(nameof(StatusCrewOn), ref _statusCrewOn, value); }
        }

        [ImmediatePostData()]
        public StatusCrewOff StatusCrewOff
        {
            get { return _statusCrewOff; }
            set { SetPropertyValue(nameof(StatusCrewOff), ref _statusCrewOff, value); }
        }

        [VisibleInListView(false)]
        public string RemarksOn
        {
            get { return _remarksOn; }
            set { SetPropertyValue(nameof(RemarksOn), ref _remarksOn, value); }
        }

        [VisibleInListView(false)]
        public string RemarksOff
        {
            get { return _remarksOff; }
            set { SetPropertyValue(nameof(RemarksOff), ref _remarksOff, value); }
        }

        [VisibleInDetailView(false)]
        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue(nameof(Default), ref _default, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [VisibleInListView(false)]
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field
    }
}