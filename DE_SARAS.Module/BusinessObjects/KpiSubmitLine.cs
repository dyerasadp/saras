﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.XtraRichEdit.Import.Html;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Kpi Submit")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("KpiSubmitLineRuleUnique", DefaultContexts.Save, "Code")]

    public class KpiSubmitLine : BaseObject
    {
        #region Initialization

        private bool _activationPosting;
        private string _code;
        private KpiRoleLine _name;
        private double _pencapaian;
        private double _resultvalue;
        private KpiStatusPencapaian _kpiStatusPencapaian;
        private FileData _attachment;
        private Status _kpiSubmitLineStatus;
        private double _weight;
        private double _proportion;
        private double _ytdtarget;
        private KpiPeriod _kpiPeriod;
        private KpiFormat _kpiFormat;
        private double _kpiTarget;
        private KpiValue _kpiValue;
        private KpiGroupType _kpiGroupType;
        private KpiYtdType _kpiYtdType;
        private KpiYear _kpiYear;
        private Month _monthKpi;
        private DateTime _statusDate;
        private KpiSubmit _kpiSubmit;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private JobPosition _defaultPosition;
        private Employee _employee;
        private Employee _leader;
        private bool _showPica;
        private XPCollection<KpiSubmit> _availableKpiSubmit;
        private GlobalFunction _globFunc;


        #endregion Initialization
        public KpiSubmitLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.ShowPica = false;
            this.KpiStatusPencapaian = KpiStatusPencapaian.None;
            if (!IsLoading)
            {
                this.KpiSubmitLineStatus = Status.Open;
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.KpiSubmitLine);
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue(nameof(ActivationPosting), ref _activationPosting, value); }
        }

        [Appearance("CodeKpiSubmitDisabled", Enabled = false)]
        [Appearance("KpiSubmitLineCodeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("BackgroundColorForCode", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForCode2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [XafDisplayName("Item")]
        [Appearance("KpiRoleLineDisabled" , Enabled = false)]       
        [Appearance("BackgroundColorForName", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForName2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public KpiRoleLine Name
        {
            get { return _name; }
            set
            {
                SetPropertyValue(nameof(Name), ref _name, value);
            }
        }


        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [XafDisplayName("Ytd Target")]
        [Appearance("YtdtargetDisabled", Enabled = false)]
        [Appearance("BackgroundColorForYtdtarget", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForYtdtarget2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public double Ytdtarget
        {
            get { return _ytdtarget; }
            set { SetPropertyValue(nameof(Ytdtarget), ref _ytdtarget, value); }
        }

        [ImmediatePostData]
        [Appearance("KpiSubmitLinePencapaianClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("BackgroundColorForValuePencapaian", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForValuePencapaian2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")] 
        public double Pencapaian
        {
            get { return _pencapaian; }
            set
            {
                SetPropertyValue(nameof(Pencapaian), ref _pencapaian, value);
                if (!IsLoading)
                {
                    HitungKpi();
                }

            }
        }

        [XafDisplayName("Target")]
        [ImmediatePostData]
        [Appearance("KpiTargetKpiSubmitLineSubmit", Enabled = false)]
        [Appearance("BackgroundColorForKpiTarget", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForKpiTarget2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public double KpiTarget
        {   
            get { return _kpiTarget; }
            set
            {
                SetPropertyValue(nameof(KpiTarget), ref _kpiTarget, value);
                if (!IsLoading)
                {
                    HitungKpi();
                }
            }
        }

        [XafDisplayName("Result")]
        [Appearance("ResultValueDisabled", Enabled = false)]
        [Appearance("BackgroundColorForResultValue", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForResultValue2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public double ResultValue
        {
            get { return _resultvalue; }
            set { SetPropertyValue(nameof(ResultValue), ref _resultvalue, value); }
        }

        [ImmediatePostData]
        [XafDisplayName("Status Pencapaian")]
        [Appearance("KpiStatusPencapaianDisabled", Enabled = false)]
        [Appearance("BackgroundColorForPencapaian", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForPencapaian2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]

        public KpiStatusPencapaian KpiStatusPencapaian
        {
            get { return _kpiStatusPencapaian; }
            set { 
                SetPropertyValue(nameof(KpiStatusPencapaian), ref _kpiStatusPencapaian, value); 
                if(!IsLoading)
                {
                    if(_kpiStatusPencapaian == KpiStatusPencapaian.Tercapai)
                    {
                        this.ShowPica = false;
                    }
                    else if (_kpiStatusPencapaian == KpiStatusPencapaian.TidakTercapai)
                    {
                        this.ShowPica = true;
                    }
                    else
                    {
                        this.ShowPica = false;
                    }
                }
            } 
        }

        [ImmediatePostData()]
        [Appearance("KpiSubmitLineAttachmentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("BackgroundColorForAttachment", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForAttachment2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public FileData Attachment
        {
            get { return _attachment; }
            set { SetPropertyValue(nameof(Attachment), ref _attachment, value); }
        }

        [XafDisplayName("Status")]
        [Appearance("KpiSubmitLineKpiSubmitLineStatusClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("FontColorForStatus", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("FontColorForStatus2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public Status KpiSubmitLineStatus
        {
            get { return _kpiSubmitLineStatus; }
            set { SetPropertyValue(nameof(KpiSubmitLineStatus), ref _kpiSubmitLineStatus, value); }
        }

        [XafDisplayName("Weight %")]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [Appearance("WeightKpiSubmitLineSubmit", Enabled = false)]
        [Appearance("BackgroundColorForWeight", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForWeight2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public double Weight
        {
            get { return _weight; }
            set { SetPropertyValue(nameof(Weight), ref _weight, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [Appearance("BackgroundColorForProportion", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForProportion2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        [Appearance("ProportionKpiSubmitLineSubmit", Enabled = false)]
        public double Proportion
        {
            get { return _proportion; }
            set { SetPropertyValue(nameof(Proportion), ref _proportion, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Appearance("KpiPeriodKpiSubmitLineSubmit", Enabled = false)]
        [Appearance("BackgroundColorForKpiPeriod", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForKpiPeriod2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public KpiPeriod KpiPeriod
        {
            get { return _kpiPeriod; }
            set { SetPropertyValue(nameof(KpiPeriod), ref _kpiPeriod, value); }
        }


        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Appearance("KpiFormatKpiSubmitLineSubmit", Enabled = false)]
        [Appearance("BackgroundColorForKpiFormat", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForKpiFormat2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public KpiFormat KpiFormat
        {
            get { return _kpiFormat; }
            set { SetPropertyValue(nameof(KpiFormat), ref _kpiFormat, value); }
        }

        [VisibleInDetailView(true)]
        [VisibleInListView(true)]
        [Appearance("KpiValueKpiSubmitLineSubmit", Enabled = false)]
        [Appearance("BackgroundColorForKpiValue", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForKpiValue2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public KpiValue KpiValue
        {
            get { return _kpiValue; }
            set { SetPropertyValue(nameof(KpiValue), ref _kpiValue, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Type Item")]
        [Appearance("KpiGroupTypeKpiSubmitLineSubmit", Enabled = false)]
        [Appearance("BackgroundColorForKpiGroupType", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForKpiGroupType2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public KpiGroupType KpiGroupType
        {
            get { return _kpiGroupType; }
            set { SetPropertyValue(nameof(KpiGroupType), ref _kpiGroupType, value); }
        }

        [Browsable(false)]
        public XPCollection<KpiSubmit> AvailableKpiSubmit
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<KpiSubmit> _locKpiSubmits = new XPCollection<KpiSubmit>(this.Session);
                    if (_locKpiSubmits != null && _locKpiSubmits.Count() > 0)
                    {
                        _availableKpiSubmit = _locKpiSubmits;
                    }
                }
                return _availableKpiSubmit;
            }
        }

        [ImmediatePostData()]
        [Appearance("KpiSubmitKpiSubmitLineSubmit", Enabled = false)]
        [DataSourceProperty("AvailableKpiSubmit", DataSourcePropertyIsNullMode.SelectNothing)]
        [Appearance("BackgroundColorForKpiSubmit", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForKpiSubmit2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        [Association("KpiSubmit-KpiSubmitLines")]
        public KpiSubmit KpiSubmit
        {
            get { return _kpiSubmit; }
            set { 
                SetPropertyValue(nameof(KpiSubmit), ref _kpiSubmit, value);
                if (!IsLoading)
                {
                    if (_kpiSubmit != null)
                    {
                        this.MonthKpi = _kpiSubmit.KpiSubmitMonth;
                        this.KpiYear = _kpiSubmit.KpiSubmitYear;
                        this.Employee = _kpiSubmit.Name;
                        this.Leader = _kpiSubmit.Leader;
                    }
                }
                else
                {
                    this.MonthKpi = 0;
                    this.KpiYear = null;
                    this.Employee = null;
                    this.Leader = null;
                }
            }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Appearance("BackgroundColorForKpiYtdType", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForKpiYtdType2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        [Appearance("KpiYtdTypeSubmitLineSubmit", Enabled = false)]
        public KpiYtdType KpiYtdType
        {
            get { return _kpiYtdType; }
            set { SetPropertyValue(nameof(KpiYtdType), ref _kpiYtdType, value); }
        }

        [XafDisplayName("Year")]
        [Appearance("KpiYearSubmitLineSubmit", Enabled = false)]
        [Appearance("BackgroundColorForKpiYear", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForKpiYear2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public KpiYear KpiYear      
        {
            get { return _kpiYear; }
            set { SetPropertyValue(nameof(KpiYear), ref _kpiYear, value); }
        }

        [XafDisplayName("Month")]
        [Appearance("MonthKpiSubmitLineSubmit", Enabled = false)]
        [Appearance("BackgroundColorForMonthKpi", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForMonthKpi2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public Month MonthKpi
        {
            get { return _monthKpi; }
            set { SetPropertyValue(nameof(MonthKpi), ref _monthKpi, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseOrderStatusDateClose", Enabled = false)]
        [Appearance("BackgroundColorForStatusDate", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForStatusDate2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue(nameof(StatusDate), ref _statusDate, value); }
        }

        [Appearance("CompanyDisabled", Enabled = false)]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Appearance("BackgroundColorForCompany", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForCompany2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1Disabled", Enabled = false)]
        [Appearance("BackgroundColorForOrgDim1", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForOrgDim1_2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Departement")]
        [Appearance("OrganizationDimension2Disabled", Enabled = false)]
        [Appearance("BackgroundColorForOrgDim2", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForOrgDim2_2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public OrganizationDimension2 OrgDim2                                   
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Sub Departement")]
        [Appearance("OrganizationDimension3Close", Enabled = false)]
        [Appearance("BackgroundColorForOrgDim3", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForOrgDim3_2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("-")]
        [Appearance("OrganizationDimension4Close", Enabled = false)]
        [Appearance("BackgroundColorForOrgDim4", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForOrgDim4_2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }


        [ImmediatePostData]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Position")]
        [Appearance("DefaultPositionDefault", Enabled = false)]
        [Appearance("BackgroundColorForDafaultPosition", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForDafaultPosition2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public JobPosition DefaultPosition
        {
            get { return _defaultPosition; }
            set { SetPropertyValue(nameof(DefaultPosition), ref _defaultPosition, value); }
        }

        [ImmediatePostData]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Appearance("EmployeeDisabled", Enabled = false)]
        [Appearance("BackgroundColorForEmployee", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForEmployee2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        [Appearance("LeaderDisabled", Enabled = false)]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Appearance("BackgroundColorForLeader", Criteria = "KpiStatusPencapaian = 1", FontColor = "Green")]
        [Appearance("BackgroundColorForLeader2", Criteria = "KpiStatusPencapaian = 2", FontColor = "Red")]
        public Employee Leader
        {
            get { return _leader; }
            set { SetPropertyValue(nameof(Leader), ref _leader, value); }
        }

        #endregion Field

        #region Manny

        [Association("KpiSubmitLine-KpiSubmitLineCascadings")]
        public XPCollection<KpiSubmitLineCascading> KpiSubmitLineCascadings
        {
            get { return GetCollection<KpiSubmitLineCascading>(nameof(KpiSubmitLineCascadings)); }
        }

        [ImmediatePostData]
        [Browsable(false)]
        public bool ShowPica
        {
            get { return _showPica; }
            set { SetPropertyValue(nameof(ShowPica), ref _showPica, value); }
        }

        [Appearance("PicasHide",Criteria = "ShowPica = 'False'",Enabled = false)]
        [Association("KpiSubmitLine-Picas")]
        public XPCollection<Pica> Picas
        {
            get { return GetCollection<Pica>(nameof(Picas)); }
        }


        #endregion Manny

        #region Code 
        private void HitungKpi()
        {
            if (this.KpiPeriod != KpiPeriod.Monthly)
            {
                if(this.KpiPeriod == KpiPeriod.Semester) 
                {
                    DateTime dateTime = DateTime.Now;
                }
            }
            else
            {
                if (this.KpiTarget != 0)
                {
                    if (this.Pencapaian != 0)
                    {
                        double _locScoreGrading = 0;

                        XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(this.Session,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("KpiRoleLine", this.Name),
                                                                       new BinaryOperator("Active", true)),
                                                                       new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                       );
                        if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                        {

                            foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                            {

                                if (_availableKpiRoleGrading.OperationGrading == ">")
                                {
                                    if(this.Pencapaian  > _availableKpiRoleGrading.ValueGrading)
                                    {
                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                    }
                                }
                                if (_availableKpiRoleGrading.OperationGrading == "<=")
                                {
                                    if (this.Pencapaian <= _availableKpiRoleGrading.ValueGrading)
                                    {
                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                    }
                                }
                                if (_availableKpiRoleGrading.OperationGrading == "<")
                                {
                                    if (this.Pencapaian < _availableKpiRoleGrading.ValueGrading)
                                    {
                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                    }
                                }
                                if (_availableKpiRoleGrading.OperationGrading == "=")
                                {
                                    if (this.Pencapaian == _availableKpiRoleGrading.ValueGrading)
                                    {
                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                    }
                                }
                            }
                        }
                        this.ResultValue = _locScoreGrading;

                        //# Membuat grading di kpi role grading kalo minimize, urutkan nomer dari yang paling max 1 dan minimal 5 
                        //  berarti value yg paling gede no 1 dst.
                        //# Membuat grading di kpi role grading kalo maximize, urutkan nomer dari yang paling minimal 1 dan max 5 
                        //  berarti value yang paling kecil no 1 dst.

                        if (this.KpiValue == KpiValue.Minimize)
                        {
                            if (this.KpiTarget >= this.Pencapaian)
                            {
                                this.KpiStatusPencapaian = KpiStatusPencapaian.Tercapai;
                            }
                            else
                            {
                                this.KpiStatusPencapaian = KpiStatusPencapaian.TidakTercapai;
                            }
                        }
                        else if(this.KpiValue == KpiValue.Maximize)
                        {
                           
                            if (this.KpiTarget <= this.Pencapaian)
                            {
                                this.KpiStatusPencapaian = KpiStatusPencapaian.Tercapai;
                            }
                            else
                            {
                                this.KpiStatusPencapaian = KpiStatusPencapaian.TidakTercapai;
                            }

                        }
                        else
                        {
                            this.KpiStatusPencapaian = KpiStatusPencapaian.None;
                        }

                        _locScoreGrading = 0;

                    }
                    else
                    {
                        this.KpiStatusPencapaian = KpiStatusPencapaian.None;
                    }
                }
                else
                {

                }
            }
        }

        #endregion Code
    }
}




    