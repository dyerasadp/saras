﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Fullname")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("ShippingMasterPortRuleUnique", DefaultContexts.Save, "Code")]

    public class ShippingMasterPort : BaseObject
    {
        #region initialization

        private string _code;
        private string _fullname;
        private string _name;
        private ShippingMasterLocation _location;
        private bool _active;
        private DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion Initialization
        public ShippingMasterPort(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Active = true;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShippingMasterPort);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public ShippingMasterLocation Location
        {
            get { return _location; }
            set { SetPropertyValue(nameof(Location), ref _location, value); }
        }

        [Appearance("FullNameDisable", Enabled = false)]
        public string Fullname
        {
            get { return $"{Name}, {Location?.Name}"; }
            set { SetPropertyValue(nameof(Fullname), ref _fullname, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field
    }
}