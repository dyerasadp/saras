﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("SubCategoryName")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("ShippingMasterPartVesselSubCategoryRuleUnique", DefaultContexts.Save, "Code")]

    public class ShippingMasterPartVesselSubCategory : BaseObject
    {
        #region Initialization

        private string _code;
        private string _subCategoryName;
        private ShippingMasterPartVesselCategory _itemCategory;
        private ItemPosition _itemPart;

        private DateTime _docDate;
        private bool _active;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public ShippingMasterPartVesselSubCategory(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Active = true;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShippingMasterPartVesselSubCategory);
            #endregion Numbering
        }

        #region field

        [VisibleInListView(false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string SubCategoryName
        {
            get { return _subCategoryName; }
            set { SetPropertyValue(nameof(SubCategoryName), ref _subCategoryName, value); }
        }

        [Association("ShippingMasterPartVesselCategory-ShippingMasterPartVesselSubCategories")]
        public ShippingMasterPartVesselCategory ItemCategory
        {
            get { return _itemCategory; }
            set { SetPropertyValue(nameof(ItemCategory), ref _itemCategory, value); }
        }

        public ItemPosition ItemPart
        {
            get { return _itemPart; }
            set { SetPropertyValue(nameof(ItemPart), ref _itemPart, value); }
        }

        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion field
    }
}