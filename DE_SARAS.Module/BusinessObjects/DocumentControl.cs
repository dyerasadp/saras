﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Document Control")]
    [RuleCombinationOfPropertiesIsUnique("DocumentControlRuleUnique", DefaultContexts.Save, "Code")]

    public class DocumentControl : BaseObject
    {
        #region Initialization
        private string _code;
        private string _name;
        private string _document;
        private FileData _attachment;
        private DateTime _docDate;
        private DateTime _startDate;
        private DateTime _endDate;
        private bool _active;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization


        public DocumentControl(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Active = true;
            }
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [Size(SizeAttribute.Unlimited)]
        [EditorAlias(EditorAliases.RichTextPropertyEditor)]
        public string Document
        {
            get { return _document; }
            set { SetPropertyValue(nameof(Document), ref _document, value); }
        }

        public FileData Attachment
        {
            get { return _attachment; }
            set { SetPropertyValue(nameof(Attachment), ref _attachment, value); }
        }

        [Appearance("StartDateDisabled", Enabled = false)]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue(nameof(StartDate), ref _startDate, value); }
        }

        [Appearance("EndDateDisabled", Enabled = false)]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue(nameof(EndDate), ref _endDate, value); }
        }

        [Appearance("ActiveDisabled", Enabled = false)]
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Dept")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }


        [XafDisplayName("Sub Dept")]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("-")]
        [Appearance("OrganizationDimension4Close", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        [Appearance("DocDateDisabled", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }


        #endregion Field

        #region Manny
        [Association("DocumentControl-DocumentControlLines")]
        public XPCollection<DocumentControlLine> DocumentControlLines
        {
            get { return GetCollection<DocumentControlLine>(nameof(DocumentControlLines)); }
        }

        [Association("DocumentControl-AdditionalDocuments")]
        public XPCollection<AdditionalDocument> AdditionalDocuments
        {
            get { return GetCollection<AdditionalDocument>(nameof(AdditionalDocuments)); }
        }

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Manny

    }
}