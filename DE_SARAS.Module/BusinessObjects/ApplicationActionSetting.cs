﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Setting")]

    public class ApplicationActionSetting : BaseObject
    {
        #region Initialization

        private string _code;
        private string _name;
        private ApplicationActionList _applicationActionList;
        private FilterListSetup _filterListSetup;
        private Company _company;
        private XPCollection<OrganizationDimension1> _availableOrgDim1;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private ObjectList _objectList;

        private bool _active;
        private ApplicationSetting _applicationSetting;
        private string _userAccessIn;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization
        public ApplicationActionSetting(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [ImmediatePostData()]
        public ApplicationActionList ApplicationActionList
        {
            get { return _applicationActionList; }
            set { SetPropertyValue(nameof(ApplicationActionList), ref _applicationActionList, value); }
        }

        public FilterListSetup FilterListSetup
        {
            get { return _filterListSetup; }
            set { SetPropertyValue(nameof(FilterListSetup), ref _filterListSetup, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true ")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [Browsable(false)]
        public XPCollection<OrganizationDimension1> AvailableOrgDim1
        {
            get
            {
                if (this.Company != null)
                {
                    _availableOrgDim1 = new XPCollection<OrganizationDimension1>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Company", this.Company),
                                        new BinaryOperator("Active", true)));
                }
                else
                {
                    _availableOrgDim1 = new XPCollection<OrganizationDimension1>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Active", true)));
                }

                return _availableOrgDim1;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableOrgDim1")]
        [XafDisplayName("Workplace")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Departement")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Departement")]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [XafDisplayName("Section")]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue(nameof(ObjectList), ref _objectList, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        //[Appearance("ApplicationActionSetupApplicationSetupEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [Association("ApplicationSetting-ApplicationActionSettings")]
        public ApplicationSetting ApplicationSetting
        {
            get { return _applicationSetting; }
            set { SetPropertyValue(nameof(ApplicationSetting), ref _applicationSetting, value); }
        }

        #endregion Field

    }
}