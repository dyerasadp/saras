﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselDamageReportRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselDamageReport : BaseObject
    {

        #region initialization

        public string _code;
        private string _name;
        public Vessel _vessel;
        private DateTime _date;
        private string _at;
        private VesselPart _damagedOn;
        private string _damagedTo;
        private string _causeOfDamage;
        private Answer _vesselStopped;
        private TimeSpan _from;
        private TimeSpan _to;
        private Answer _repaired;
        private RepairedBy _repairedBy;
        private RepairsCarried _repairsCarriedOut;
        private RepairsRequired _repairsRequired;
        private string _detailsDamage;
        private MediaDataObject _sketch;
        private MediaDataObject _damagePicture;
        private string _probableCause;

        public string _action;
        public DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion initialization

        public VesselDamageReport(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselDamageReport);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [ImmediatePostData]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime Date
        {
            get { return _date; }
            set { SetPropertyValue(nameof(Date), ref _date, value); }
        }

        [VisibleInListView(false)]
        public string At
        {
            get { return _at; }
            set { SetPropertyValue(nameof(At), ref _at, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Vessel = '@This.Vessel' And Result = 'Abnormal' And Active = 'true'")]
        public VesselPart DamagedOn
        {
            get { return _damagedOn; }
            set { SetPropertyValue(nameof(DamagedOn), ref _damagedOn, value); }
        }

        [VisibleInListView(false)]
        public string DamagedTo
        {
            get { return _damagedTo; }
            set { SetPropertyValue(nameof(DamagedTo), ref _damagedTo, value); }
        }

        [VisibleInListView(false)]
        public string CauseOfDamage
        {
            get { return _causeOfDamage; }
            set { SetPropertyValue(nameof(CauseOfDamage), ref _causeOfDamage, value); }
        }

        [VisibleInListView(false)]
        [ImmediatePostData]
        public Answer VesselStopped
        {
            get { return _vesselStopped; }
            set { SetPropertyValue(nameof(VesselStopped), ref _vesselStopped, value); }
        }

        [VisibleInListView(false)]
        public TimeSpan From
        {
            get { return _from; }
            set { SetPropertyValue(nameof(From), ref _from, value); }
        }

        [VisibleInListView(false)]
        public TimeSpan To
        {
            get { return _to; }
            set { SetPropertyValue(nameof(To), ref _to, value); }
        }

        [VisibleInListView(false)]
        public Answer Repaired
        {
            get { return _repaired; }
            set { SetPropertyValue(nameof(Repaired), ref _repaired, value); }
        }

        [VisibleInListView(false)]
        public RepairedBy RepairedBy
        {
            get { return _repairedBy; }
            set { SetPropertyValue(nameof(RepairedBy), ref _repairedBy, value); }
        }

        [VisibleInListView(false)]
        public RepairsCarried RepairsCarriedOut
        {
            get { return _repairsCarriedOut; }
            set { SetPropertyValue(nameof(RepairsCarriedOut), ref _repairsCarriedOut, value); }
        }

        [VisibleInListView(false)]
        public RepairsRequired RepairsRequired
        {
            get { return _repairsRequired; }
            set { SetPropertyValue(nameof(RepairsRequired), ref _repairsRequired, value); }
        }

        [VisibleInListView(false)]
        public string DetailsDamage
        {
            get { return _detailsDamage; }
            set { SetPropertyValue(nameof(DetailsDamage), ref _detailsDamage, value); }
        }

        [DevExpress.Xpo.Size(SizeAttribute.Unlimited)]
        //[VisibleInListViewAttribute(true)]
        [VisibleInListView(false)]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.PictureEdit, DetailViewImageEditorMode = ImageEditorMode.PictureEdit, ListViewImageEditorCustomHeight = 40)]
        public MediaDataObject Sketch
        {
            get { return _sketch; }
            set { SetPropertyValue(nameof(Sketch), ref _sketch, value); }
        }

        [DevExpress.Xpo.Size(SizeAttribute.Unlimited)]
        //[VisibleInListViewAttribute(true)]
        [VisibleInListView(false)]
        [ImageEditor(ListViewImageEditorMode = ImageEditorMode.PictureEdit, DetailViewImageEditorMode = ImageEditorMode.PictureEdit, ListViewImageEditorCustomHeight = 40)]
        public MediaDataObject DamagePicture
        {
            get { return _damagePicture; }
            set { SetPropertyValue(nameof(DamagePicture), ref _damagePicture, value); }
        }

        [VisibleInListView(false)]
        public string ProbableCause
        {
            get { return _probableCause; }
            set { SetPropertyValue(nameof(ProbableCause), ref _probableCause, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

        #region Manny

        [Association("VesselDamageReport-VesselReportSpares")]
        public XPCollection<VesselDamageReportSpare> VesselDamageReportSpares
        {
            get { return GetCollection<VesselDamageReportSpare>(nameof(VesselDamageReportSpares)); }
        }

        #endregion Manny
    }
}