﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("ShippingPaymentRequestRuleUnique", DefaultContexts.Save, "Code")]

    public class ShippingPaymentRequest : BaseObject
    {

        #region initialization

        private string _code;
        private string _name;
        private DateTime _date;
        private ShippingMasterPaymentRequestAccount _account;
        private int _amount;
        private Company _company;
        private OrganizationDimension1 _workplace;
        private OrganizationDimension2 _departement;
        private ShippingMasterPaymentRequestProject _project;
        private Voyage _trip;
        private string _remarks;

        private string _chequeNum;
        private string _recipient;
        private string _accountBank;
        private string _otherRemarks;

        private bool _active;
        private DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public ShippingPaymentRequest(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Active = true;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShippingPaymentRequest);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime Date
        {
            get { return _date; }
            set { SetPropertyValue(nameof(Date), ref _docDate, value); }
        }

        public ShippingMasterPaymentRequestAccount Account
        {
            get { return _account; }
            set { SetPropertyValue(nameof(Account), ref _account, value); }
        }

        public int Amount
        {
            get { return _amount; }
            set { SetPropertyValue(nameof(Amount), ref _amount, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        public OrganizationDimension1 Workplace
        {
            get { return _workplace; }
            set { SetPropertyValue(nameof(Workplace), ref _workplace, value); }
        }

        public OrganizationDimension2 Departement
        {
            get { return _departement; }
            set { SetPropertyValue(nameof(Departement), ref _departement, value); }
        }

        public ShippingMasterPaymentRequestProject Project
        {
            get { return _project; }
            set { SetPropertyValue(nameof(Project), ref _project, value); }
        }

        public Voyage Trip
        {
            get { return _trip; }
            set { SetPropertyValue(nameof(Trip), ref _trip, value); }
        }

        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue(nameof(Remarks), ref _remarks, value); }
        }

        public string ChequeNum
        {
            get { return _chequeNum; }
            set { SetPropertyValue(nameof(ChequeNum), ref _chequeNum, value); }
        }

        public string Recipient
        {
            get { return _recipient; }
            set { SetPropertyValue(nameof(Recipient), ref _recipient, value); }
        }

        public string AccountBank
        {
            get { return _accountBank; }
            set { SetPropertyValue(nameof(AccountBank), ref _accountBank, value); }
        }

        public string OtherRemarks
        {
            get { return _otherRemarks; }
            set { SetPropertyValue(nameof(OtherRemarks), ref _otherRemarks, value); }
        }
        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field
    }
}