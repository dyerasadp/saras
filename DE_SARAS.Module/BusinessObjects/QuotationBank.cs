﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("QuotationBankRuleUnique", DefaultContexts.Save, "Code")]
    public class QuotationBank : BaseObject
    {
        #region Initialization

        private string _code;
        private string _name;
        private string _account;
        private string _branch;
        private string _noted;
        private bool _active;
        private DateTime _docDate;
        private Company _company;



        #endregion Initialization
        public QuotationBank(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }


        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public string Account
        {
            get { return _account; }
            set { SetPropertyValue(nameof(Account), ref _account, value); }
        }

        public string Branch
        {
            get { return _branch; }
            set { SetPropertyValue(nameof(Branch), ref _branch, value); }
        }

        public string Noted
        {
            get { return _noted; }
            set { SetPropertyValue(nameof(Noted), ref _noted, value); }
        }

        [XafDisplayName("Account Holder")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        [Appearance("DateTimeClosed", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }


        #endregion Field
    }
}