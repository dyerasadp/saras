﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Map.Native;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Kpi Submit")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("KpiSubmitRuleUnique", DefaultContexts.Save, "Code")]

    public class KpiSubmit : BaseObject
    {
        #region Initialization

        private bool _activationPosting;
        private string _code;
        private Employee _name;
        private Month _kpiSubmitMonth;
        private KpiYear _kpiSubmitYear;
        private Status _kpiSubmitStatus;
        private DateTime _statusDate;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private JobPosition _jobPosition;
        private Employee _leader;
        private string _note;
        private XPCollection<Employee> _availableEmployee;
        private string _localUserAccess;
        private GlobalFunction _globFunc;


        #endregion Initialization

        public KpiSubmit(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.KpiSubmitStatus = Status.Open;

                #region Bulan KPI Automatic

                if (DateTime.Now.Month == 1)
                {
                    this.KpiSubmitMonth = Month.December;
                }
                else if(DateTime.Now.Month == 2)
                {
                    this.KpiSubmitMonth = Month.January;
                }
                else if (DateTime.Now.Month == 3)
                {
                    this.KpiSubmitMonth = Month.February;
                }
                else if (DateTime.Now.Month == 4)
                {
                    this.KpiSubmitMonth = Month.March;
                }
                else if (DateTime.Now.Month == 5)
                {
                    this.KpiSubmitMonth = Month.April;
                }
                else if (DateTime.Now.Month == 6)
                {
                    this.KpiSubmitMonth = Month.May;
                }
                else if (DateTime.Now.Month == 7)
                {
                    this.KpiSubmitMonth = Month.June;
                }
                else if (DateTime.Now.Month == 8)
                {
                    this.KpiSubmitMonth = Month.July;
                }
                else if (DateTime.Now.Month == 9)
                {
                    this.KpiSubmitMonth = Month.August;
                }
                else if (DateTime.Now.Month == 10)
                {
                    this.KpiSubmitMonth = Month.September;
                }
                else if (DateTime.Now.Month == 11)
                {
                    this.KpiSubmitMonth = Month.October;
                }
                else if (DateTime.Now.Month == 12)
                {
                    this.KpiSubmitMonth = Month.November;
                }
                else
                {
                    this.KpiSubmitMonth = 0;
                }

                #endregion Bulan KPI Automatic

                #region Tahun KPI Automatic 
                if (DateTime.Now.Year == 2023)
                {
                    if (DateTime.Now.Month == 1)
                    {
                        KpiYear _locKpiYear = Session.FindObject<KpiYear>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Year", "2022"),
                                                                     new BinaryOperator("Active", true)));
                        if(_locKpiYear != null)
                        {
                            this.KpiSubmitYear = _locKpiYear;
                        }
                        
                    }
                    else
                    {
                        KpiYear _locKpiYear = Session.FindObject<KpiYear>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Year", "2023"),
                                                                     new BinaryOperator("Active", true)));
                        if (_locKpiYear != null)
                        {
                            this.KpiSubmitYear = _locKpiYear;
                        }
                    }
                }
                if (DateTime.Now.Year == 2024)
                {
                    if (DateTime.Now.Month == 1)
                    {
                        KpiYear _locKpiYear = Session.FindObject<KpiYear>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Year", "2023"),
                                                                     new BinaryOperator("Active", true)));
                        if (_locKpiYear != null)
                        {
                            this.KpiSubmitYear = _locKpiYear;
                        }

                    }
                    else
                    {
                        KpiYear _locKpiYear = Session.FindObject<KpiYear>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Year", "2024"),
                                                                     new BinaryOperator("Active", true)));
                        if (_locKpiYear != null)
                        {
                            this.KpiSubmitYear = _locKpiYear;
                        }
                    }
                }
                if (DateTime.Now.Year == 2025)
                {
                    if (DateTime.Now.Month == 1)
                    {
                        KpiYear _locKpiYear = Session.FindObject<KpiYear>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Year", "2024"),
                                                                     new BinaryOperator("Active", true)));
                        if (_locKpiYear != null)
                        {
                            this.KpiSubmitYear = _locKpiYear;
                        }

                    }
                    else
                    {
                        KpiYear _locKpiYear = Session.FindObject<KpiYear>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Year", "2025"),
                                                                     new BinaryOperator("Active", true)));
                        if (_locKpiYear != null)
                        {
                            this.KpiSubmitYear = _locKpiYear;
                        }
                    }
                }

                #endregion Tahun KPI Automatic

                #region UserAccess
                _globFunc = new GlobalFunction();
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));

                Company _locCompany = null;
                OrganizationDimension1 _locOrgDim1 = null;
                OrganizationDimension2 _locOrgDim2 = null;
                OrganizationDimension3 _locOrgDim3 = null;
                OrganizationDimension4 _locOrgDim4 = null;
                Employee _locEmployee = null;

                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        _locEmployee = _locUserAccess.Employee;
                        if (_locUserAccess.Employee.Company != null) { _locCompany = _locUserAccess.Employee.Company; }
                        if (_locUserAccess.Employee.OrganizationDimension1 != null) { _locOrgDim1 = _locUserAccess.Employee.OrganizationDimension1; }
                        if (_locUserAccess.Employee.OrganizationDimension2 != null) { _locOrgDim2 = _locUserAccess.Employee.OrganizationDimension2; }
                        if (_locUserAccess.Employee.OrganizationDimension3 != null) { _locOrgDim3 = _locUserAccess.Employee.OrganizationDimension3; }
                        if (_locUserAccess.Employee.OrganizationDimension4 != null) { _locOrgDim4 = _locUserAccess.Employee.OrganizationDimension4; }
                    }
                    if (this.Session != null)
                    {
                        this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.KpiSubmit);
                    }

                  
                    this.Company = _locCompany;
                    this.OrgDim1 = _locOrgDim1;
                    this.OrgDim2 = _locOrgDim2;
                    this.OrgDim3 = _locOrgDim3;
                    this.OrgDim4 = _locOrgDim4;
                    this.Name = _locEmployee;

                }
                #endregion UserAccess
            }
        }


        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue(nameof(ActivationPosting), ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("KpiSubmitCodeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("KpiSubmitCodeClose2", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailableEmployee
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<Employee> _locEmployees = new XPCollection<Employee>(this.Session,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true)));
                    if (_locEmployees != null && _locEmployees.Count() > 0)
                    {
                        _availableEmployee = _locEmployees;
                    }
                }
                return _availableEmployee;
            }
        }

        [ImmediatePostData()]
        [Appearance("KpiSubmitNameClose", Enabled = false)]
        [DataSourceProperty("AvailableEmployee", DataSourcePropertyIsNullMode.SelectNothing)]

        public Employee Name
        {
            get { return _name; }
            set { 
                SetPropertyValue(nameof(Name), ref _name, value);
                if (!IsLoading)
                {
                    if (_name != null)
                    {
                        this.Company = _name.Company;  
                        this.OrgDim1 = _name.OrganizationDimension1;
                        this.OrgDim2 = _name.OrganizationDimension2;
                        this.OrgDim3 = _name.OrganizationDimension3;
                        this.OrgDim4 = _name.OrganizationDimension4;
                        this.JobPosition = _name.DefaultPosition;
                        this.Leader = _name.Leader;
                    }
                }
                else
                {
                    this.Company = null;
                    this.OrgDim1 = null;
                    this.OrgDim2 = null;
                    this.OrgDim3 = null;
                    this.OrgDim4 = null;
                    this.JobPosition = null;
                    this.Leader = null;
                }
            }
        }

        [XafDisplayName("Month")]
        [Appearance("KpiSubmitKpiSubmitMonthClose", Enabled = true)]
        public Month KpiSubmitMonth
        {
            get { return _kpiSubmitMonth; }
            set { SetPropertyValue(nameof(KpiSubmitMonth), ref _kpiSubmitMonth, value); }
        }

        [XafDisplayName("Year")]
        [Appearance("KpiSubmitKpiSubmitYearClose", Enabled = false)]
        public KpiYear KpiSubmitYear
        {
            get { return _kpiSubmitYear; }
            set { SetPropertyValue(nameof(KpiSubmitYear), ref _kpiSubmitYear, value);}
        }

        [XafDisplayName("Status")]
        [Appearance("KpiSubmitKpiSubmitStatusClose", Enabled = false)]
        [Appearance("BackgroundColorForStatusKpi", Criteria = "KpiSubmitStatus = 1", FontColor = "Green")] //Open
        [Appearance("BackgroundColorForStatusKpi2", Criteria = "KpiSubmitStatus = 2", FontColor = "GoldenRod")] //Progress
        [Appearance("BackgroundColorForStatusKpi3", Criteria = "KpiSubmitStatus = 3", FontColor = "Orange")] //Cancel
        [Appearance("BackgroundColorForStatusKpi4", Criteria = "KpiSubmitStatus = 5", FontColor = "Blue")] //Approve
        [Appearance("BackgroundColorForStatusKpi5", Criteria = "KpiSubmitStatus = 6", FontColor = "Red")] //Reject
        public Status KpiSubmitStatus
        {
            get { return _kpiSubmitStatus; }
            set { SetPropertyValue(nameof(KpiSubmitStatus), ref _kpiSubmitStatus, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("PurchaseOrderStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue(nameof(StatusDate), ref _statusDate, value); }
        }

        [Size(512)]
        public string Note
        {
            get { return _note; }
            set { SetPropertyValue(nameof(Note), ref _note, value); }
        }

        [Appearance("LeaderKpiSubmitSubmit", Enabled = false)]
        public Employee Leader
        {
            get { return _leader; }
            set { SetPropertyValue(nameof(Leader), ref _leader, value); }
        }

        [Appearance("JobPositionKpiSubmitSubmit", Enabled = false)]
        public JobPosition JobPosition
        {
            get { return _jobPosition; }
            set { SetPropertyValue(nameof(JobPosition), ref _jobPosition, value); }
        }

        [ImmediatePostData()]
        [Appearance("CompanyKpiSubmitSubmit", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1KpiSubmitSubmit", Enabled = false)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Dept")]
        [Appearance("OrganizationDimension2KpiSubmitSubmit", Enabled = false)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Dept")]
        [Appearance("OrganizationDimension3Default", Enabled = false)]
        [Appearance("OrganizationDimension3KpiSubmitSubmit", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Section")]
        [Appearance("DefaultPositionDefault", Enabled = false)]
        [Appearance("OrganizationDimension4KpiSubmitSubmit", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        #endregion Field

        #region Manny
        [XafDisplayName("Input Pencapaian")]
        [Association("KpiSubmit-KpiSubmitLines")]
        public XPCollection<KpiSubmitLine> KpiSubmitLines
        {
            get { return GetCollection<KpiSubmitLine>(nameof(KpiSubmitLines)); }
        }

        [XafDisplayName("Input Behaviour")]
        [Association("KpiSubmit-BehaviourSubmitLines")]
        public XPCollection<BehaviourSubmitLine> BehaviourSubmitLines
        {
            get { return GetCollection<BehaviourSubmitLine>(nameof(BehaviourSubmitLines)); }
        }

        [XafDisplayName("Data Approval")]
        [Association("KpiSubmit-KpiSubmitApprovals")]
        public XPCollection<KpiSubmitApproval> KpiSubmitApprovals
        {
            get { return GetCollection<KpiSubmitApproval>(nameof(KpiSubmitApprovals)); }
        }


        #endregion Manny
    }
}