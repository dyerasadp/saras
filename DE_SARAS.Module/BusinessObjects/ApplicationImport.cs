﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Setting")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("ApplicationImportRuleUnique", DefaultContexts.Save, "Code")]
    public class ApplicationImport : BaseObject
    {
        #region Initialization

        private string _code;
        private FileData _dataImport;
        private ObjectList _objectsList;
        private string _message;

        #endregion Initialization

        public ApplicationImport(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Field 

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        //[Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectsList; }
            set { SetPropertyValue(nameof(ObjectList), ref _objectsList, value); }
        }

        [ImmediatePostData()]
        public FileData DataImport
        {
            get { return _dataImport; }
            set { SetPropertyValue(nameof(DataImport), ref _dataImport, value); }
        }

        [Appearance("ApplicationImportMessageClose", Enabled = false)]
        public string Message
        {
            get { return _message; }
            set { SetPropertyValue(nameof(Message), ref _message, value); }
        }

        #endregion Field

    }
}