﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Map.Native;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("Information")]
    [RuleCombinationOfPropertiesIsUnique("QuotationMechanismLineRuleUnique", DefaultContexts.Save, "Code")]

    public class QuotationMechanismLine : BaseObject
    {
        #region Initialization

        private string _code;
        private QuotationInformation _information;
        private QuotationInformationLine _detailInformation;
        private MechanismGroup _group;
        private DateTime _docDate;
        private Quotation _quotation;
        private Company _company;
        private XPCollection<Quotation> _availableQuotation;
        private XPCollection<QuotationInformation> _availableQuotationInformation;


        #endregion Initialization

        public QuotationMechanismLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                DocDate = DateTime.Now;
            }
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<QuotationInformation> AvailableQuotationInformation
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<QuotationInformation> _locQuotationInformations = new XPCollection<QuotationInformation>
                                                                       (this.Session, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("MechanismGroup", MechanismGroup.PEMESANAN),
                                                                        new BinaryOperator("MechanismGroup", MechanismGroup.PENERIMAAN),
                                                                        new BinaryOperator("MechanismGroup", MechanismGroup.PEMBAYARAN)
                                                                       )));
                    if (_locQuotationInformations != null && _locQuotationInformations.Count() > 0)
                    {
                        _availableQuotationInformation = _locQuotationInformations;
                    }
                }
                return _availableQuotationInformation;
            }
        }

        [ImmediatePostData()]
        [Size(3000)]
        [DataSourceCriteria("Active = true")]
        [DataSourceProperty("AvailableQuotationInformation", DataSourcePropertyIsNullMode.SelectNothing)]
        public QuotationInformation Information
        {
            get { return _information; }
            set { SetPropertyValue(nameof(Information), ref _information, value);
                if (!IsLoading)
                {
                    if (_information != null)
                    {
                        this.Group = _information.MechanismGroup;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("QuotationInformation = '@This.Information' And Active = 'true'")]
        public QuotationInformationLine DetailInformation
        {
            get { return _detailInformation; }
            set { SetPropertyValue(nameof(DetailInformation), ref _detailInformation, value); }
        }        
        public MechanismGroup Group
        {
            get { return _group; }
            set { SetPropertyValue(nameof(Group), ref _group, value); }
        }

        [Appearance("DateTimeClosed", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [Browsable(false)]
        public XPCollection<Quotation> AvailableQuotation
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<Quotation> _locQuotations = new XPCollection<Quotation>(this.Session);
                    if (_locQuotations != null && _locQuotations.Count() > 0)
                    {
                        _availableQuotation = _locQuotations;
                    }
                }
                return _availableQuotation;
            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableQuotation", DataSourcePropertyIsNullMode.SelectNothing)]
        [Association("Quotation-QuotationMechanismLines")]
        public Quotation Quotation
        {
            get { return _quotation; }
            set { SetPropertyValue(nameof(Quotation), ref _quotation, value);
                if (!IsLoading)
                {
                    if (_quotation != null)
                    {
                        this.Company = _quotation.Company;
                    }
                }
            }
        }

        #region Organization

        [Appearance("CompanyQuotationMechanismLineDisabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }
  
        #endregion Organization

        #endregion Field

    }
}