﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("DetailInformation")]
    [RuleCombinationOfPropertiesIsUnique("QuotationInformationLineRuleUnique", DefaultContexts.Save, "Code")]

    public class QuotationInformationLine : BaseObject
    {
        #region Initialization

        private string _code;
        private string _detailInformation;
        private bool _active;
        private DateTime _docDate;
        private Company _company;
        private QuotationInformation _quotationInformation;
        private XPCollection<QuotationInformation> _availableQuotationInformation;


        #endregion Initialization
        public QuotationInformationLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Active = true;
            }
        }


        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string DetailInformation
        {
            get { return _detailInformation; }
            set { SetPropertyValue(nameof(DetailInformation), ref _detailInformation, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        [Appearance("DateTimeQuotationInformationLineClosed", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }


        [Browsable(false)]
        public XPCollection<QuotationInformation> AvailableQuotationInformation
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<QuotationInformation> _locQuotationInformations = new XPCollection<QuotationInformation>(this.Session);
                    if (_locQuotationInformations != null && _locQuotationInformations.Count() > 0)
                    {
                        _availableQuotationInformation = _locQuotationInformations;
                    }
                }
                return _availableQuotationInformation;
            }
        }

        [ImmediatePostData]
        [Association("QuotationInformation-QuotationInformationLines")]
        [Appearance("QuotationInformationQuotationInformationLineClosed", Enabled = false)]
        [DataSourceProperty("AvailableQuotationInformation", DataSourcePropertyIsNullMode.SelectNothing)]
        public QuotationInformation QuotationInformation
        {
            get { return _quotationInformation; }
            set { SetPropertyValue(nameof(QuotationInformation), ref _quotationInformation, value);
                if(!IsLoading)
                {
                    if(_quotationInformation != null)
                    {
                        this.Company = _quotationInformation.Company;
                    }
                }
            }
        }

        [Appearance("CompanyQuotationInformationLineDisabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        #endregion Field
    }
}