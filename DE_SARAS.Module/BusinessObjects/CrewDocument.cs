﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Map.Native;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.PivotGrid.PivotTable;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Type")]
    [NavigationItem("Crewing")]
    [RuleCombinationOfPropertiesIsUnique("CrewDocumentRuleUnique", DefaultContexts.Save, "Code")]

    public class CrewDocument : BaseObject
    {
        #region initialization

        private string _code;
        private DocumentShipping _document;
        private DocumentShippingType _documentType;
        private Crew _crew;
        private string _numDocument;
        private DateTime _issuedDate;
        private DateTime _expiryDate;
        private string _remarks;
        private bool _documentPermanent;
        private FileData _documentFile;

        private DateTime _docDate;
        private bool _active;
        private GlobalFunction _globFunc;

        //AuditTrail
        //private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization

        public CrewDocument(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Active = true;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CrewDocument);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [ImmediatePostData]
        [Association("Crew-CrewDocuments")]
        public Crew Crew
        {
            get { return _crew; }
            set { SetPropertyValue(nameof(Crew), ref _crew, value); }
        }

        public DocumentShippingType DocumentType
        {
            get { return _documentType; }
            set { SetPropertyValue(nameof(DocumentType), ref _documentType, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Type = '@This.DocumentType'")]
        public DocumentShipping Document
        {
            get { return _document; }
            set { SetPropertyValue(nameof(Document), ref _document, value); }
        }

        [XafDisplayName("Document Number")]
        public string NumDocument
        {
            get { return _numDocument; }
            set { SetPropertyValue(nameof(NumDocument), ref _numDocument, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime IssuedDate
        {
            get { return _issuedDate; }
            set { SetPropertyValue(nameof(IssuedDate), ref _issuedDate, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
            set { SetPropertyValue(nameof(ExpiryDate), ref _expiryDate, value); }
        }

        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue(nameof(Remarks), ref _remarks, value); }
        }

        public bool DocumentPermanent
        {
            get { return _documentPermanent; }
            set { SetPropertyValue(nameof(DocumentPermanent), ref _documentPermanent, value); }
        }

        public FileData DocumentFile
        {
            get { return _documentFile; }
            set { SetPropertyValue(nameof(DocumentFile), ref _documentFile, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [VisibleInListView(false)]
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field
    }
}