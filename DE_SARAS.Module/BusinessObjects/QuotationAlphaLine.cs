﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.DataAccess.Native.Json;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    //[NavigationItem("Quotation")]
    //[DefaultProperty("Name")]
    //[RuleCombinationOfPropertiesIsUnique("QuotationAlphaLineRuleUnique", DefaultContexts.Save, "Code")]

    public class QuotationAlphaLine : BaseObject
    {
        #region Initialization

        private string _code;
        private string _name;
        private double _avgMop;
        private double _avgKurs;
        private double _avgMopsPerBarel;
        private double _alpha;
        private double _mopsPerBarel;
        private double _barelPerLiter;
        private double _mopsPerLiter;
        private double _hargaDasar;
        private Quotation _quotation;
        private DateTime _docDate;
        private Employee _employee;
        private Company _company;
        private QuotationPeriode _quotationPeriod;
        private XPCollection<QuotationPeriodeMopsLine> _quotationPeriodeMops;


        #endregion Initialization
        public QuotationAlphaLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading) 
            {
                this.DocDate = DateTime.Now;
                this.BarelperLiter = 6.289;

                XPCollection<QuotationPeriode> _locPeriodes = new XPCollection<QuotationPeriode>( Session, CriteriaOperator.And(
                                               new BinaryOperator("StartDate", DocDate, BinaryOperatorType.LessOrEqual), // StartDate <= DocDate
                                               new BinaryOperator("EndDate", DocDate, BinaryOperatorType.GreaterOrEqual)));  // EndDate >= DocDate

                if (_locPeriodes != null && _locPeriodes.Count() > 0)
                {
                    foreach(QuotationPeriode _locPeriode in _locPeriodes) 
                    {
                        XPCollection<QuotationPeriodeMopsLine> _locMops = new XPCollection<QuotationPeriodeMopsLine>(Session, CriteriaOperator.And(
                                                 new BinaryOperator("QuotationPeriode", _locPeriode)
                                                 ));  
                        if(_locMops != null && _locMops.Count() > 0) 
                        {
                            // Calculate the average of the 'Mops' field within _locMops
                            double averageMops = _locMops.Average(item => item.Mops);
                            double roundedAverageMops = Math.Round(averageMops, 3);
                            this.AvgMop = roundedAverageMops;
                            this.AvgperBarel = roundedAverageMops;

                            // Calculate the average of the 'Kurs' field within _locMops
                            double averageKurs = _locMops.Average(item => item.KursUsd);
                            double roundAverageKurs = Math.Round(averageKurs, 3);
                            this.AvgKurs = roundAverageKurs;
                        }
                    }
                }

            }
        }

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public double AvgMop
        {
            get { return _avgMop; }
            set { SetPropertyValue(nameof(AvgMop), ref _avgMop, value); }
        }

        public double AvgKurs
        {
            get { return _avgKurs; }
            set { SetPropertyValue(nameof(AvgKurs), ref _avgKurs, value); }
        }

        public double AvgperBarel
        {
            get { return _avgMopsPerBarel; }
            set { SetPropertyValue(nameof(AvgperBarel), ref _avgMopsPerBarel, value); }
        }

        public double Alpha
        {
            get { return _alpha; }
            set { SetPropertyValue(nameof(Alpha), ref _alpha, value);
                if(!IsLoading)
                {
                    CalculationPriceMopPerBarel();
                }
            }
        }

        public double MopperBarel
        {
            get { return _mopsPerBarel; }
            set { SetPropertyValue(nameof(MopperBarel), ref _mopsPerBarel, value); }
        }

        public double BarelperLiter
        {
            get { return _barelPerLiter; }
            set { SetPropertyValue(nameof(MopperBarel), ref _barelPerLiter, value); }
        }

        public double MopPerLiter
        {
            get { return _mopsPerLiter; }
            set { SetPropertyValue(nameof(MopPerLiter), ref _mopsPerLiter, value); }
        }

        public double BasePrice
        {
            get { return _hargaDasar; }
            set { SetPropertyValue(nameof(BasePrice), ref _hargaDasar, value); }
        }

        [Association("Quotation-QuotationAlphaLines")]
        public Quotation Quotation
        {
            get { return _quotation; }
            set { SetPropertyValue(nameof(Quotation), ref _quotation, value); }
        }

        [XafDisplayName("Created By")]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        [ImmediatePostData]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [Appearance("DateTimeQuotationClosed", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }


        #region Code 
        private void CalculationPriceMopPerBarel()
        {
            if(this.Alpha != 0)
            {
                this.MopperBarel = (this.AvgperBarel + this.Alpha);   

                double MopPerL= ((this.AvgperBarel + this.Alpha) * this.BarelperLiter)/1000;
                this.MopPerLiter = Math.Round(MopPerL, 3);
                double Bprice = (this.MopPerLiter * this.AvgKurs);
                this.BasePrice = Math.Round(Bprice, 3);

            }
            
        }

        #endregion Code
    }
}