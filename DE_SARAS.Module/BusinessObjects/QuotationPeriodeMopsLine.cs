﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("QuotationPeriodeMopsLineRuleUnique", DefaultContexts.Save, "Code")]
    public class QuotationPeriodeMopsLine : BaseObject
    {
        #region Initialization

        private string _code;
        private DateTime _date;
        private double _kursUsd;
        private double _mops;
        private QuotationPeriode _quotationPeriode;
        private Employee _employee;
        private Company _company;
        private GlobalFunction _globFunc;
        private XPCollection<Company> _availableCompany;


        #endregion Initialization


        public QuotationPeriodeMopsLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public DateTime Date
        {
            get { return _date; }
            set { SetPropertyValue(nameof(Date), ref _date, value); }
        }

        public double KursUsd
        {
            get { return _kursUsd; }
            set { SetPropertyValue(nameof(KursUsd), ref _kursUsd, value); }
        }
                
        public double Mops
        {
            get { return _mops; }
            set { SetPropertyValue(nameof(Mops), ref _mops, value); }
        }

        [Association("QuotationPeriode-QuotationPeriodeMopsLines")]
        public QuotationPeriode QuotationPeriode
        {
            get { return _quotationPeriode; }
            set { SetPropertyValue(nameof(QuotationPeriode), ref _quotationPeriode, value); }
        }

        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        #endregion Field
    }
}