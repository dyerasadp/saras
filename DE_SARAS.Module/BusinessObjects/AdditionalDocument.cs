﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Model.Core;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Spreadsheet;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Updating;
using System.Reflection;
using DevExpress.ExpressApp.ConditionalAppearance;
using System.Web;
using System.Web.UI;
using DevExpress.XtraEditors.Controls;
using System.Text.RegularExpressions;
using System.Collections.Generic;


namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Document Control")]
    [RuleCombinationOfPropertiesIsUnique("AdditionalDocumentRuleUnique", DefaultContexts.Save, "Code")]
    public class AdditionalDocument : BaseObject
    {
        private string _code;
        private string _name;
        private string _urlLink;
        private string _displayUrlLink;
        private FileData _attachment;
        private DateTime _startDate;
        private DateTime _endDate;
        private DateTime _docDate;
        private bool _active;
        private DocumentControl _documentControl;
        private GlobalFunction _globFunc;


        public AdditionalDocument(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }
        }

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }


        [VisibleInListView(true)]
        [VisibleInDetailView(true)]
        [XafDisplayName("Link Video")]
        [ModelDefault("PropertyEditorType", "DE_SARAS.Blazor.Server.CustomStringPropertyEditor")]
        public string UrlLink
        {
            get { return _urlLink; }
            set { SetPropertyValue(nameof(UrlLink), ref _urlLink, value); 
                if(!IsLoading)
                {
                    TransformUrlLinkBeforeSaving();
                }
            }
        }

        //[VisibleInListView(true)]
        //[VisibleInDetailView(false)]
        //[XafDisplayName("Link Video")]
        //[Appearance("DisplayUrlLinkClose", Enabled = false)]
        //public string DisplayUrlLink
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(_urlLink))
        //        {
        //            // Use a regular expression to extract the text content from the URL stored in UrlLink
        //            Match match = Regex.Match(_urlLink, @"<a [^>]*href=[""']([^""']*)[""'][^>]*>(.*?)</a>");

        //            // If a match is found, return the text content of the hyperlink
        //            if (match.Success && match.Groups.Count >= 3)
        //            {
        //                return match.Groups[2].Value;
        //            }
        //        }

        //        // If no match is found or _urlLink is empty, return the original value from UrlLink
        //        return _urlLink;
        //    }
        //}


        public FileData Attachment
        {
            get { return _attachment; }
            set { SetPropertyValue(nameof(Attachment), ref _attachment, value); }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue(nameof(StartDate), ref _startDate, value); }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue(nameof(EndDate), ref _endDate, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [Association("DocumentControl-AdditionalDocuments")]
        public DocumentControl DocumentControl
        {
            get { return _documentControl; }
            set { SetPropertyValue(nameof(DocumentControl), ref _documentControl, value); }
        }

        public void TransformUrlLinkBeforeSaving()
        {
            // Lakukan transformasi Regex pada _urlLink di sini
            Match match = Regex.Match(_urlLink, @"<a [^>]*href=[""']([^""']*)[""'][^>]*>(.*?)</a>");
            if (match.Success && match.Groups.Count >= 3)
            {
                // Ubah nilai _urlLink sesuai dengan hasil Regex yang Anda inginkan sebelum disimpan
                _urlLink = match.Groups[1].Value;
            }
        }
    }
}