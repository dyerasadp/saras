﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Final Grading")]
    [DefaultProperty("Employee")]
    [RuleCombinationOfPropertiesIsUnique("KpiFinalGradingSettingRuleUnique", DefaultContexts.Save, "Code")]
    public class KpiFinalGradingSetting : BaseObject
    {

        #region Initialization

        private string _code;
        private int _no;
        private double _value;
        private string _operation;
        private string _predicate;
        private string _result;
        private KpiYear _year;
        private bool _active;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private JobPosition _jobPosition;
        private string _localUserAccess;
        private GlobalFunction _globFunc;


        #endregion Initialization

        public KpiFinalGradingSetting(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public int No
        {
            get { return _no; }
            set { SetPropertyValue(nameof(No), ref _no, value); }
        }

        public double Value
        {
            get { return _value; }
            set { SetPropertyValue(nameof(Value), ref _value, value); }
        }

        public string Operation
        {
            get { return _operation; }
            set { SetPropertyValue(nameof(Operation), ref _operation, value); }
        }

        public string Predicate
        {
            get { return _predicate; }
            set { SetPropertyValue(nameof(Predicate), ref _predicate, value); }
        }

        public string Result
        {
            get { return _result; }
            set { SetPropertyValue(nameof(Result), ref _result, value); }
        }

        public KpiYear Year
        {
            get { return _year; }
            set { SetPropertyValue(nameof(Year), ref _year, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #region Organization 

        [ImmediatePostData()]
        [Appearance("CompanyKpiSubmitSubmit", Enabled = true)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1KpiSubmitSubmit", Enabled = true)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Dept")]
        [Appearance("OrganizationDimension2KpiSubmitSubmit", Enabled = true)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Dept")]
        [Appearance("OrganizationDimension3Default", Enabled = false)]
        [Appearance("OrganizationDimension3KpiSubmitSubmit", Enabled = true)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Section")]
        [Appearance("DefaultPositionDefault", Enabled = false)]
        [Appearance("OrganizationDimension4KpiSubmitSubmit", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        [Appearance("JobPositionKpiSubmitSubmit", Enabled = true)]
        public JobPosition JobPosition
        {
            get { return _jobPosition; }
            set { SetPropertyValue(nameof(JobPosition), ref _jobPosition, value); }
        }

        #endregion Organization
    }
}