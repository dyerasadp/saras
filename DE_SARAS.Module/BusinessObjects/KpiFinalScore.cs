﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("KPI Submit")]
    [DefaultProperty("Employee")]
    [RuleCombinationOfPropertiesIsUnique("KpiFinalScoreRuleUnique", DefaultContexts.Save, "Code")]

    public class KpiFinalScore : BaseObject
    {
        #region Initialization

        private string _code;
        private Employee _employee;
        private double _totalScore;
        private double _totalPinalty;
        private double _bonusSpecialAssign;
        private double _bonusInovasi;
        private double _totalFinalTarget;
        private DateTime _statusDate;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private JobPosition _jobPosition;
        private XPCollection<Employee> _availableEmployee;
        private string _localUserAccess;
        private GlobalFunction _globFunc;


        #endregion Initialization

        public KpiFinalScore(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<Employee> AvailableEmployee
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<Employee> _locEmployees = new XPCollection<Employee>(this.Session,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true)));
                    if (_locEmployees != null && _locEmployees.Count() > 0)
                    {
                        _availableEmployee = _locEmployees;
                    }
                }
                return _availableEmployee;
            }
        }

        [DataSourceProperty("AvailableEmployee", DataSourcePropertyIsNullMode.SelectNothing)]
        public Employee Employee
        {
            get { return _employee; }
            set
            {
                SetPropertyValue(nameof(Employee), ref _employee, value);
                if (!IsLoading)
                {
                    if (_employee != null)
                    {
                        this.Company = _employee.Company;
                        this.OrgDim1 = _employee.OrganizationDimension1;
                        this.OrgDim2 = _employee.OrganizationDimension2;
                        this.OrgDim3 = _employee.OrganizationDimension3;
                        this.OrgDim4 = _employee.OrganizationDimension4;
                        this.JobPosition = _employee.DefaultPosition;
                    }
                }
                else
                {
                    this.Company = null;
                    this.OrgDim1 = null;
                    this.OrgDim2 = null;
                    this.OrgDim3 = null;
                    this.OrgDim4 = null;
                    this.JobPosition = null;
                }
            }
        }

        public double TotalScore
        {
            get { return _totalScore; }
            set { SetPropertyValue(nameof(TotalScore), ref _totalScore, value); }
        }

        public double TotalPinalty
        {
            get { return _totalPinalty; }
            set { SetPropertyValue(nameof(TotalPinalty), ref _totalPinalty, value); }
        }

        public double BonusSpecialAssign
        {
            get { return _bonusSpecialAssign; }
            set { SetPropertyValue(nameof(BonusSpecialAssign), ref _bonusSpecialAssign, value); }
        }

        public double BonusInovasi
        {
            get { return _bonusInovasi; }
            set { SetPropertyValue(nameof(BonusInovasi), ref _bonusInovasi, value); }
        }

        public double TotalFinalTarget
        {
            get { return _totalFinalTarget; }
            set { SetPropertyValue(nameof(TotalFinalTarget), ref _totalFinalTarget, value); }
        }

        #region Organization 

        [ImmediatePostData()]
        [Appearance("CompanyKpiSubmitSubmit", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1KpiSubmitSubmit", Enabled = false)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Dept")]
        [Appearance("OrganizationDimension2KpiSubmitSubmit", Enabled = false)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Dept")]
        [Appearance("OrganizationDimension3Default", Enabled = false)]
        [Appearance("OrganizationDimension3KpiSubmitSubmit", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Section")]
        [Appearance("DefaultPositionDefault", Enabled = false)]
        [Appearance("OrganizationDimension4KpiSubmitSubmit", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        [Appearance("JobPositionKpiSubmitSubmit", Enabled = false)]
        public JobPosition JobPosition
        {
            get { return _jobPosition; }
            set { SetPropertyValue(nameof(JobPosition), ref _jobPosition, value); }
        }

        #endregion Organization

    }
}