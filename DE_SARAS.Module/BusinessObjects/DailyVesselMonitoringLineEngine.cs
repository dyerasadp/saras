﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("ItemChecklist")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("DailyVesselMonitoringLineEngineRuleUnique", DefaultContexts.Save, "Code")]

    public class DailyVesselMonitoringLineEngine : BaseObject
    {
        #region initialization

        public string _code;
        public DailyVesselMonitoring _monitoringHead;
        public VesselPart _itemChecklist;
        private ChecklistOption _result;
        private string _remarks;
        public DateTime _docDate;

        #endregion initialization

        public DailyVesselMonitoringLineEngine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Association("DailyVesselMonitoring-DailyVesselMonitoringLinesEngine")]
        public DailyVesselMonitoring MonitoringHead
        {
            get { return _monitoringHead; }
            set { SetPropertyValue(nameof(MonitoringHead), ref _monitoringHead, value); }
        }

        public VesselPart ItemChecklist
        {
            get { return _itemChecklist; }
            set { SetPropertyValue(nameof(ItemChecklist), ref _itemChecklist, value); }
        }

        public ChecklistOption Result
        {
            get { return _result; }
            set { SetPropertyValue(nameof(Result), ref _result, value); }
        }

        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue(nameof(Remarks), ref _remarks, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field
    }
}