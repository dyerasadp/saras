﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.PivotGrid.PivotTable;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Crewing")]
    [RuleCombinationOfPropertiesIsUnique("CrewFamilyRuleUnique", DefaultContexts.Save, "Code")]

    public class CrewFamily : BaseObject
    {

        #region initialization

        private string _code;
        private string _nik;
        private string _name;
        private FamilyStatus _status;
        private string _placeofBirth;
        private DateTime _dateofBirth;
        private string _noBpjs;
        private Crew _crew;
        private GlobalFunction _globFunc;

        //AuditTrail
        //private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization
        public CrewFamily(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CrewFamily);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string NIK
        {
            get { return _nik; }
            set { SetPropertyValue(nameof(NIK), ref _nik, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public FamilyStatus Status
        {
            get { return _status; }
            set { SetPropertyValue(nameof(Status), ref _status, value); }
        }

        public string PlaceofBirth
        {
            get { return _placeofBirth; }
            set { SetPropertyValue(nameof(PlaceofBirth), ref _placeofBirth, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime DateofBirth
        {
            get { return _dateofBirth; }
            set { SetPropertyValue(nameof(DateofBirth), ref _dateofBirth, value); }
        }

        public string NoBpjs
        {
            get { return _noBpjs; }
            set { SetPropertyValue(nameof(NoBpjs), ref _noBpjs, value); }
        }

        [ImmediatePostData()]
        [Association("Crew-CrewFamilies")]
        public Crew Crew
        {
            get { return _crew; }
            set { SetPropertyValue(nameof(Crew), ref _crew, value); }
        }

        #endregion Field
    }
}