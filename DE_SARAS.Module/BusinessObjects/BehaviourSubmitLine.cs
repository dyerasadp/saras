﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Kpi Submit")]
    [DefaultProperty("Behaviour")]
    [RuleCombinationOfPropertiesIsUnique("BehaviourSubmitLineRuleUnique", DefaultContexts.Save, "Code")]
    public class BehaviourSubmitLine : BaseObject
    {
        #region Initialization

        private string _code;
        private BehaviourRoleLine _behaviourRoleLine;
        private BehaviourType _type;
        private BehaviourCategory _category;
        private BehaviourScale _value;
        private double _valueHide;
        private KpiYear _behaviourPeriod;
        private KpiSubmit _kpiSubmit;
        private Employee _employee;
        private Employee _staff;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private string _defaultPosition;
        private XPCollection<BehaviourRole> _availableBehaviourRole;
        private XPCollection<KpiSubmit> _availableKpiSubmit;
        private GlobalFunction _globFunc;


        #endregion Initialization
        public BehaviourSubmitLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if(!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BehaviourSubmitLine);
            }
        }

        #region Field

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Appearance("BehaviourDisabled", Enabled = false)]
        public BehaviourRoleLine Behaviour
        {
            get { return _behaviourRoleLine; }
            set { SetPropertyValue(nameof(Behaviour), ref _behaviourRoleLine, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public KpiYear BehaviourPeriod
        {
            get { return _behaviourPeriod; }
            set { SetPropertyValue(nameof(KpiYear), ref _behaviourPeriod, value); }
        }

        [Appearance("BehaviourTypeDisabled", Enabled = false)]
        public BehaviourType BehaviourType
        {
            get { return _type; }
            set { SetPropertyValue(nameof(BehaviourType), ref _type, value); }
        }

        [Appearance("BehaviourCategoryDisabled", Enabled = false)]
        public BehaviourCategory BehaviourCategory
        {
            get { return _category; }
            set { SetPropertyValue(nameof(BehaviourCategory), ref _category, value); }
        }

        public BehaviourScale Value
        {
            get { return _value; }
            set { 
                SetPropertyValue(nameof(Value), ref _value, value);
                if(!IsLoading)
                {
                    if(this.BehaviourType == BehaviourType.Negatif)
                    {
                        if(this.Value == BehaviourScale.SangatSesuai)
                        {
                            this.ValueHide = 1;
                        }
                        else if (this.Value == BehaviourScale.Sesuai)
                        {
                            this.ValueHide = 2;
                        }
                        else if (this.Value == BehaviourScale.TidakSesuai)
                        {
                            this.ValueHide = 3;
                        }
                        else if (this.Value == BehaviourScale.SangatTidakSesuai)
                        {
                            this.ValueHide = 4;
                        }
                        else
                        {
                            this.ValueHide = 0;
                        }
                    }
                    else
                    {
                        this.ValueHide = ((int)this.Value);
                    }

                }
            }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public double ValueHide
        {
            get { return _valueHide; }
            set { SetPropertyValue(nameof(ValueHide), ref _valueHide, value); }
        }


        [Browsable(false)]
        public XPCollection<KpiSubmit> AvailableKpiSubmit
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<KpiSubmit> _locKpiSubmits = new XPCollection<KpiSubmit>(this.Session);
                    if (_locKpiSubmits != null && _locKpiSubmits.Count() > 0)
                    {
                        _availableKpiSubmit = _locKpiSubmits;
                    }
                }
                return _availableKpiSubmit;
            }
        }

        [ImmediatePostData]
        [Association("KpiSubmit-BehaviourSubmitLines")]
        [DataSourceProperty("AvailableKpiSubmit", DataSourcePropertyIsNullMode.SelectNothing)]
        public KpiSubmit KpiSubmit
        {
            get { return _kpiSubmit; }
            set { 
                SetPropertyValue(nameof(KpiSubmit), ref _kpiSubmit, value);
                if (!IsLoading)
                {
                    if (_kpiSubmit != null)
                    {
                        this.Company = _kpiSubmit.Company;
                        this.OrgDim1 = _kpiSubmit.OrgDim1;
                        this.OrgDim2 = _kpiSubmit.OrgDim2;
                        this.OrgDim3 = _kpiSubmit.OrgDim3;
                        this.OrgDim4 = _kpiSubmit.OrgDim4;
                        this.Employee = _kpiSubmit.Name;
                    }
                }
                else
                {
                    this.Company = null;
                    this.OrgDim1 = null;
                    this.OrgDim2 = null;
                    this.OrgDim3 = null;
                    this.OrgDim4 = null;
                    this.Employee = null;
                }
            }
        }


        [Appearance("EmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        [Appearance("StaffClose", Enabled = false)]
        public Employee Staff
        {
            get { return _staff; }
            set { SetPropertyValue(nameof(Staff), ref _staff, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [Appearance("CompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1Close", Enabled = false)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [XafDisplayName("Departement")]
        [Appearance("OrganizationDimension2Close", Enabled = false)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [XafDisplayName("Sub Departement")]
        [Appearance("OrganizationDimension3Close", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [XafDisplayName("-")]
        [Appearance("OrganizationDimension4Close", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [XafDisplayName("Position")]
        [Appearance("JobPositionClose", Enabled = false)]
        public string DefaultPosition
        {
            get { return _defaultPosition; }
            set { SetPropertyValue(nameof(DefaultPosition), ref _defaultPosition, value); }
        }


        #endregion Field

    }
}