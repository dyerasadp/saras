﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("AssetType")]
    [NavigationItem("Organization")]
    [RuleCombinationOfPropertiesIsUnique("AssetsRuleUnique", DefaultContexts.Save, "Code")]

    public class Assets : BaseObject
    {
        #region Initialization
        private string _code;
        private AssetType _assetType;
        private HardwareCategory _hardwareCategory;
        private SoftwareCategory _softwareCategory;
        private NetworkCategory _networkCategory;
        private bool _hdCategory;
        private bool _sfCategory;
        private bool _ntCategory;
        private string _assetBrand;
        private string _model;
        private string _serialNumber;
        private double _assetPrice;
        private DateTime _datePurcahsed;
        private string _poNumber;
        private string _accountingNumber;
        private ColorAsset _colorAsset;
        private FlagAsset _flagAsset;
        private DateTime _assetDate;
        private TimeSpan _agingAsset;
        private string _noted;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private DateTime _docDate;
        private bool _active;
        private string _photoAsset;
        private DateTime _currDate;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;
        private System.Timers.Timer agingUpdateTimer;

        #endregion Initialization

        public Assets(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (IsLoading)
            {
                this.CurrentDate = DateTime.Now.AddMinutes(1);
                //this.AssetDate = new DateTime();
                this.DocDate = DateTime.Now;
                //this.Aging = this.CurrentDate - this.AssetDate;
            }
        }

        #region Field
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [ImmediatePostData]
        public AssetType AssetType
        {
            get { return _assetType; }
            set { SetPropertyValue(nameof(AssetType), ref _assetType, value); 
                if(!IsLoading)
                {
                    if(this.AssetType == AssetType.HARDWARE)
                    {
                        this.HdCategory = true;
                        this.SfCategory = false;
                        this.NtCategory = false;
                    }
                    else if(this.AssetType == AssetType.SOFTWARE)
                    {
                        this.HdCategory = false;
                        this.SfCategory = true;
                        this.NtCategory = false;
                    }
                    else if (this.AssetType == AssetType.NETWORKING)
                    {
                        this.HdCategory = false;
                        this.SfCategory = false;
                        this.NtCategory = true;
                    }
                    else
                    {
                        this.HdCategory = false;
                        this.SfCategory = false;
                        this.NtCategory = false;
                    }
                }
            }
        }


        [Browsable(false)]
        public bool HdCategory
        {
            get { return _hdCategory; }
            set { SetPropertyValue(nameof(HdCategory), ref _hdCategory, value); }
        }

        [Browsable(false)]
        public bool SfCategory
        {
            get { return _sfCategory; }
            set { SetPropertyValue(nameof(SfCategory), ref _sfCategory, value); }
        }

        [Browsable(false)]
        public bool NtCategory
        {
            get { return _ntCategory; }
            set { SetPropertyValue(nameof(NtCategory), ref _ntCategory, value); }
        }

        [Appearance("HdCategoryHide", Criteria = "[HdCategory] = 'False'", Visibility = ViewItemVisibility.Hide)]
        public HardwareCategory HardwareCategory
        {
            get { return _hardwareCategory; }
            set { SetPropertyValue(nameof(HardwareCategory), ref _hardwareCategory, value); }
        }

        [Appearance("SfCategoryHide", Criteria = "[SfCategory] = 'False'", Visibility = ViewItemVisibility.Hide)]
        public SoftwareCategory SoftwareCategory
        {
            get { return _softwareCategory; }
            set { SetPropertyValue(nameof(SoftwareCategory), ref _softwareCategory, value); }
        }

        [Appearance("NtCategoryHide", Criteria = "[NtCategory] = 'False'", Visibility = ViewItemVisibility.Hide)]
        public NetworkCategory NetworkCategory
        {
            get { return _networkCategory; }
            set { SetPropertyValue(nameof(NetworkCategory), ref _networkCategory, value); }
        }


        public DateTime AssetDate
        {
            get { return _assetDate; }
            set
            {
                SetPropertyValue(nameof(AssetDate), ref _assetDate, value);
                hitungAging(); // Panggil fungsi perhitungan umur setiap kali AssetDate berubah
            }
        }

        //[Persistent("Aging"), PersistentAlias("_aging")]
        //[VisibleInDetailView(false), VisibleInListView(false)]
        //public TimeSpan Aging
        //{
        //    get
        //    {
        //        if (!IsLoading && !IsSaving && _agingAsset == TimeSpan.Zero && _assetDate != default(DateTime))
        //        {
        //            hitungAging(); // Perhitungan pertama kali ketika Aging masih nol dan AssetDate tidak kosong
        //        }
        //        return _agingAsset;
        //    }
        //}

        public string AssetBrand
        {
            get { return _assetBrand; }
            set { SetPropertyValue(nameof(AssetBrand), ref _assetBrand, value); }
        }

        public string Model
        {
            get { return _model; }
            set { SetPropertyValue(nameof(Model), ref _model, value); ; }
        }

        public string SerialNumber
        {
            get { return _serialNumber; }
            set { SetPropertyValue(nameof(SerialNumber), ref _serialNumber, value); ; }
        }

        public double AssetPrice
        {
            get { return _assetPrice; }
            set { SetPropertyValue(nameof(AssetPrice), ref _assetPrice, value); ; }
        }

        public DateTime DatePurchased
        {
            get { return _datePurcahsed; }
            set { SetPropertyValue(nameof(DatePurchased), ref _datePurcahsed, value); ; }
        }

        public string PoNumber
        {
            get { return _poNumber; }
            set { SetPropertyValue(nameof(PoNumber), ref _poNumber, value); ; }
        }

        public string AccountingNumber
        {
            get { return _accountingNumber; }
            set { SetPropertyValue(nameof(AccountingNumber), ref _accountingNumber, value); ; }
        }        
        
        public ColorAsset ColorAsset
        {
            get { return _colorAsset; }
            set { SetPropertyValue(nameof(ColorAsset), ref _colorAsset, value); ; }
        }

        public FlagAsset FlagAsset
        {
            get { return _flagAsset; }
            set { SetPropertyValue(nameof(FlagAsset), ref _flagAsset, value); }
        }

        //[ImmediatePostData]
        //public DateTime AssetDate
        //{
        //    get { return _assetDate; }
        //    set { SetPropertyValue(nameof(AssetDate), ref _assetDate, value); }
        //}

        [ImmediatePostData]
        [Browsable(false)]
        [Appearance("CurrDisabled", Enabled = false)]
        public DateTime CurrentDate
        {
            get { return _currDate; }
            set { SetPropertyValue(nameof(CurrentDate), ref _currDate, value);}
        }

        [Appearance("AgingDisabled", Enabled = false)]
        public TimeSpan Aging
        {
            get
            {
                if (!IsLoading && !IsSaving && _agingAsset == TimeSpan.Zero && _assetDate != default(DateTime))
                {
                    hitungAging(); // Perhitungan pertama kali ketika Aging masih nol dan AssetDate tidak kosong
                }
                return _agingAsset;
            }
        }

        [Size(512)]
        public string Noted
        {
            get { return _noted; }
            set { SetPropertyValue(nameof(Noted), ref _noted, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Workplace")]
        [DataSourceCriteria("Company = '@This.Company' And Active = 'true'")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Departement")]
        [DataSourceCriteria("OrgDim1 = '@This.OrgDim1' And Active = 'true'")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Sub Departement")]
        [DataSourceCriteria("OrgDim2 = '@This.OrgDim2' And Active = 'true'")]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("-")]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }


        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }


        [Size(SizeAttribute.Unlimited)]
        [EditorAlias(EditorAliases.RichTextPropertyEditor)]
        public string PhotoAsset
        {
            get { return _photoAsset; }
            set { SetPropertyValue(nameof(PhotoAsset), ref _photoAsset, value); }
        }

        #endregion Field

        #region Code

        //private void hitungAging()
        //{
        //    if(this.AssetDate != null)
        //    {
        //        this.Aging = (DateTime.Now - this.AssetDate);
        //    }
        //}

        private void hitungAging()
        {
            _agingAsset = DateTime.Now - _assetDate;
            OnChanged(nameof(Aging)); // Memanggil event PropertyChanged untuk memberitahu bahwa properti Aging telah berubah
        }

        #endregion Code

    }
}