﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.DataAccess.Wizard.Presenters;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Policy;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("Tax")]
    [RuleCombinationOfPropertiesIsUnique("QuotationTaxPriceLineRuleUnique", DefaultContexts.Save, "Code")]

    public class QuotationTaxPriceLine : BaseObject
    {
        #region Initialization

        private string _code;
        private QuotationTax _tax;
        private double _value;
        private double _basePrice;
        private double _nominal;
        private DateTime _docDate;
        private QuotationPriceLine _quotationPriceLine;
        private Company _company;
        private XPCollection<QuotationPriceLine> _availableQuotationPriceLine;
        private XPCollection<QuotationTax> _availableQuotationTax;
        private double _a;
        private double _b;
        private double _c;
        private double _nominaltax;
        private string _codeQuotationPriceLine;

        #endregion Initialization
        public QuotationTaxPriceLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<QuotationTax> AvailableQuotationTax
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<QuotationTax> _locQuotationTaxs = new XPCollection<QuotationTax>
                                                                       (Session, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true)
                                                                       ));
                    if (_locQuotationTaxs != null && _locQuotationTaxs.Count() > 0)
                    {
                        _availableQuotationTax = _locQuotationTaxs;
                    }
                }
                return _availableQuotationTax;
            }
        }

        [DataSourceProperty("AvailableQuotationTax", DataSourcePropertyIsNullMode.SelectNothing)]
        public QuotationTax Tax
        {
            get { return _tax; }
            set { SetPropertyValue(nameof(Tax), ref _tax, value);
                if (!IsLoading)
                {
                    if (_tax != null)
                    {
                        this.Value = _tax.Value;
                    }
                }
            }
        }

        public double Value
        {
            get { return _value; }
            set { SetPropertyValue(nameof(Value), ref _value, value); if (!IsLoading) { NominalTaxCalculation(); } }
        }

        [Browsable(false)]
        public XPCollection<QuotationPriceLine> AvailableQuotationPriceLineTaxPrice
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<QuotationPriceLine> _locQuotationPriceLines = new XPCollection<QuotationPriceLine>(Session);
                    if (_locQuotationPriceLines != null && _locQuotationPriceLines.Count() > 0)
                    {
                        _availableQuotationPriceLine = _locQuotationPriceLines;
                    }
                }
                return _availableQuotationPriceLine;
            }
        }

        [ImmediatePostData]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        //[Association("QuotationPriceLine-QuotationTaxPriceLines")]
        [Appearance("QuotationPriceLineQuotationPriceLineDisabled", Enabled = false)]
        [DataSourceProperty("AvailableQuotationPriceLineTaxPrice", DataSourcePropertyIsNullMode.SelectNothing)]
        public QuotationPriceLine QuotationPriceLine
        {
            get { return _quotationPriceLine; }
            set
            {
                SetPropertyValue(nameof(QuotationPriceLine), ref _quotationPriceLine, value);
                if (!IsLoading){if (_quotationPriceLine != null) {this.BasePrice = _quotationPriceLine.BasePrice;this.Company = _quotationPriceLine.Company; }
                }
            }
        }

        [ImmediatePostData]
        [Appearance("BasePriceQuotationTaxPriceLineDisabled", Enabled = false)]
        public double BasePrice
        {
            get { return _basePrice; }
            set { SetPropertyValue(nameof(BasePrice), ref _basePrice, value); if(!IsLoading){ NominalTaxCalculation();}}
        }

        [ImmediatePostData]
        public double Nominal
        {
            get { return _nominal; }
            set { SetPropertyValue(nameof(Nominal), ref _nominal, value); }
        }


        [Appearance("DateTimeQuotationTaxPriceLineClosed", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #region Organization

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        #endregion Organization

        #endregion Field


        #region Code

        private void NominalTaxCalculation()
        {
            if (this.Value != 0)
            {
                _a = this.Value;
                _b = this.BasePrice;
                _c = (_a / 100) * _b;
                this.Nominal = _c;
                double _locTotTax = 0;

                if (this.QuotationPriceLine != null)
                {
                    XPCollection<QuotationTaxPriceLine> _locQuotationTaxPriceLines = new XPCollection<QuotationTaxPriceLine>
                                                        (Session, new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("QuotationPriceLine", this.QuotationPriceLine),
                                                         new BinaryOperator("This", this, BinaryOperatorType.NotEqual)));

                    if (_locQuotationTaxPriceLines != null && _locQuotationTaxPriceLines.Count() > 0)
                    {
                        foreach (QuotationTaxPriceLine _locQuotationTaxPriceLine in _locQuotationTaxPriceLines)
                        {
                            if (_locQuotationTaxPriceLine != null)
                            {
                                _locTotTax = _locTotTax + _locQuotationTaxPriceLine.Nominal;
                            }

                        }
                    }
                    this.QuotationPriceLine.TotalTax = 0;
                    this.QuotationPriceLine.TotalTax = _locTotTax + this.Nominal;
                    this.QuotationPriceLine.Save();
                    this.QuotationPriceLine.Session.CommitTransaction();
                }
            }
        }

        #endregion Code

    }
}