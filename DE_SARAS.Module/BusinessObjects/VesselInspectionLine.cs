﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Item")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselInspectionLineRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselInspectionLine : BaseObject
    {
        #region initialization

        private string _code;
        private ShippingHeadInspectionLine _item;
        private ChecklistOption _result;
        private string _remarks;
        private VesselInspection _vesselInspection;

        private DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public VesselInspectionLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselInspectionLine);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public ShippingHeadInspectionLine Item
        {
            get { return _item; }
            set { SetPropertyValue(nameof(Item), ref _item, value); }
        }

        public ChecklistOption Result
        {
            get { return _result; }
            set { SetPropertyValue(nameof(Result), ref _result, value); }
        }

        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue(nameof(Remarks), ref _remarks, value); }
        }

        [Association("VesselInspection-VesselInspectionLines")]
        public VesselInspection VesselInspection
        {
            get { return _vesselInspection; }
            set { SetPropertyValue(nameof(VesselInspection), ref _vesselInspection, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

    }
}