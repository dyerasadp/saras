﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Item")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("ShippingVesselDailySetLineDeckRuleUnique", DefaultContexts.Save, "Code")]

    public class ShippingVesselDailySetLineDeck : BaseObject
    {
        #region initialization

        private string _code;
        private VesselPart _item;
        private ShippingVesselDailySetHead _checklistHead;
        private bool _active;
        private DateTime _docDate;

        #endregion initialization

        public ShippingVesselDailySetLineDeck(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.Active = true;
                this.DocDate = DateTime.Now;
            }
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public VesselPart Item
        {
            get { return _item; }
            set { SetPropertyValue(nameof(Item), ref _item, value); }
        }

        [Association("ShippingVesselDailySetHead-ShippingVesselDailySetLinesDeck")]
        public ShippingVesselDailySetHead ChecklistHead
        {
            get { return _checklistHead; }
            set { SetPropertyValue(nameof(ChecklistHead), ref _checklistHead, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field
    }
}