﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("Item")]
    [RuleCombinationOfPropertiesIsUnique("QuotationPriceLineRuleUnique", DefaultContexts.Save, "Code")]

    public class QuotationPriceLine : BaseObject
    {
        #region Initialization

        private string _code;
        private string _item;
        private QuotationInformation _itemType;
        private QuotationTax _taxPPN;
        private QuotationTax _taxPBBKB;
        private QuotationTax _taxPPH;
        private double _basePrice;
        private double _valuePPN;
        private double _valuePBBKB;
        private double _valuePPH;
        private double _nominalTaxPPN;
        private double _nominalTaxPBBKB;
        private double _nominalTaxPPH;
        private double _totalTax;
        private double _totalPrice;
        private DateTime _docDate;
        private Quotation _quotation;
        private Company _company;
        private XPCollection<Quotation> _availableQuotation;
        private XPCollection<QuotationInformation> _availableQuotationInformation;
        private XPCollection<QuotationTax> _availableQuotationTax;
        private string _codeQuotationPriceLine;
        private double _nominaltax;
        private string _localUserAccess;
        private GlobalFunction _globFunc;

        #endregion Initialization
        public QuotationPriceLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                this.DocDate = DateTime.Now;
                _globFunc = new GlobalFunction();
                _localUserAccess = SecuritySystem.CurrentUserName;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.QuotationPriceLine);
            }
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _item; }
            set { SetPropertyValue(nameof(Name), ref _item, value); }
        }


        [Browsable(false)]
        public XPCollection<QuotationInformation> AvailableQuotationInformationPticeLine
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<QuotationInformation> _locQuotationInformations = new XPCollection<QuotationInformation>
                                                                       (this.Session, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("MechanismGroup", MechanismGroup.ITEM)
                                                                       )));
                    if (_locQuotationInformations != null && _locQuotationInformations.Count() > 0)
                    {
                        _availableQuotationInformation = _locQuotationInformations;
                    }
                }
                return _availableQuotationInformation;
            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableQuotationInformationPticeLine", DataSourcePropertyIsNullMode.SelectNothing)]
        public QuotationInformation ItemType
        {
            get { return _itemType; }
            set { SetPropertyValue(nameof(ItemType), ref _itemType, value); }
        }

        [ImmediatePostData]
        public double BasePrice
        {
            get { return _basePrice; }
            set
            {
                SetPropertyValue(nameof(BasePrice), ref _basePrice, value);
                if (!IsLoading)
                {
                    TotalPriceCalculation();
                    ChangeBasePriceOnTaxPriceLine();
                }
            }
        }

        [Browsable(false)]
        public XPCollection<QuotationTax> AvailableQuotationTax
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<QuotationTax> _locQuotationTaxs = new XPCollection<QuotationTax>
                                                                       (Session, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true)
                                                                       ));
                    if (_locQuotationTaxs != null && _locQuotationTaxs.Count() > 0)
                    {
                        _availableQuotationTax = _locQuotationTaxs;
                    }
                }
                return _availableQuotationTax;
            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableQuotationTax", DataSourcePropertyIsNullMode.SelectNothing)]
        public QuotationTax TaxPPN
        {
            get { return _taxPPN; }
            set
            {
                SetPropertyValue(nameof(TaxPPN), ref _taxPPN, value);
                if (!IsLoading)
                {
                    if (_taxPPN != null)
                    {
                        this.ValuePPN = _taxPPN.Value;
                    }
                    else
                    {
                        this.ValuePPN = 0;
                    }
                }
            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableQuotationTax", DataSourcePropertyIsNullMode.SelectNothing)]
        public QuotationTax TaxPBBKB
        {
            get { return _taxPBBKB; }
            set
            {
                SetPropertyValue(nameof(TaxPBBKB), ref _taxPBBKB, value);
                if (!IsLoading)
                {
                    if (_taxPBBKB != null)
                    {
                        this.ValuePBBKB = _taxPBBKB.Value;
                    }
                    else
                    {
                        this.ValuePBBKB = 0;
                    }
                }
            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableQuotationTax", DataSourcePropertyIsNullMode.SelectNothing)]
        public QuotationTax TaxPPH
        {
            get { return _taxPPH; }
            set
            {
                SetPropertyValue(nameof(TaxPPH), ref _taxPPH, value);
                if (!IsLoading)
                {
                    if (_taxPPH != null)
                    {
                        this.ValuePPH = _taxPPH.Value;
                    }
                    else
                    {
                        this.ValuePPH = 0;
                    }
                }
            }
        }

       

        [ImmediatePostData]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public double ValuePPN
        {
            get { return _valuePPN; }
            set
            {
                SetPropertyValue(nameof(ValuePPN), ref _valuePPN, value);
                if (!IsLoading)
                {
                    HitungNominalPPN();
                }
            }
        }

        [ImmediatePostData]
        public double NominalPPN
        {
            get { return _nominalTaxPPN; }
            set { SetPropertyValue(nameof(NominalPPN), ref _nominalTaxPPN, value);
                if (!IsLoading) { HitungTotalTax(); }
            }
        }


        [ImmediatePostData]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public double ValuePBBKB
        {
            get { return _valuePBBKB; }
            set
            {
                SetPropertyValue(nameof(ValuePBBKB), ref _valuePBBKB, value);
                if (!IsLoading)
                {
                    HitungNominalPBBKB();
                }
            }
        }

        [ImmediatePostData]
        public double NominalPBBKB
        {
            get { return _nominalTaxPBBKB; }
            set { SetPropertyValue(nameof(NominalPBBKB), ref _nominalTaxPBBKB, value);
                if(!IsLoading) { HitungTotalTax(); }
            }
        }

        [ImmediatePostData]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public double ValuePPH
        {
            get { return _valuePPH; }
            set
            {
                SetPropertyValue(nameof(ValuePPH), ref _valuePPH, value);
                if (!IsLoading)
                {
                    HitungNominalPPH();
                }
            }
        }

        [ImmediatePostData]
        public double NominalPPH
        {
            get { return _nominalTaxPPH; }
            set { SetPropertyValue(nameof(NominalPPH), ref _nominalTaxPPH, value);
                if (!IsLoading) { HitungTotalTax(); }
            }
        }

        [ImmediatePostData]
        public double TotalTax
        {
            get { return _totalTax; }
            set { SetPropertyValue(nameof(TotalTax), ref _totalTax, value);
                if (!IsLoading)
                {
                    TotalPriceCalculation();
                }
            }
        }

        public double TotalPrice
        {
            get { return _totalPrice; }
            set { SetPropertyValue(nameof(TotalPrice), ref _totalPrice, value); }
        }


        [Appearance("DateTimeQuotationPriceLinesClosed", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [Browsable(false)]
        public XPCollection<Quotation> AvailableQuotationPriceLine
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<Quotation> _locQuotations = new XPCollection<Quotation>(this.Session);
                    if (_locQuotations != null && _locQuotations.Count() > 0)
                    {
                        _availableQuotation = _locQuotations;
                    }
                }
                return _availableQuotation;
            }
        }

        [ImmediatePostData]
        [DataSourceProperty("AvailableQuotationPriceLine", DataSourcePropertyIsNullMode.SelectNothing)]
        [Association("Quotation-QuotationPriceLines")]
        [Appearance("QuotationQuotationPriceLineDisabled",Enabled = false)]
        public Quotation Quotation
        {
            get { return _quotation; }
            set { SetPropertyValue(nameof(Quotation), ref _quotation, value);
                if (!IsLoading)
                {
                    if (_quotation != null)
                    {
                        this.Company = _quotation.Company;
                    }
                }
            }
        }


        #region Organization

        [Appearance("CompanyQuotationPriceLineDisabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        #endregion Organization

        #endregion Field

        #region Manny 

        //[Association("QuotationPriceLine-QuotationTaxPriceLines")]
        //public XPCollection<QuotationTaxPriceLine> QuotationTaxPriceLines
        //{
        //    get { return GetCollection<QuotationTaxPriceLine>(nameof(QuotationTaxPriceLines)); }
        //}

        #endregion Manny

        #region Code

        private void TotalPriceCalculation()
        {
            if (this.BasePrice != 0)
            {
                this.TotalPrice = this.BasePrice + this.TotalTax;
            }
        }

        private void HitungNominalPPN()
        {
            if (this.ValuePPN != 0)
            {
                this.NominalPPN = (this.ValuePPN / 100) * this.BasePrice;
            }
        }

        private void HitungNominalPBBKB()
        {
            if (this.ValuePBBKB != 0)
            {
                this.NominalPBBKB = (this.ValuePBBKB / 100) * this.BasePrice;
            }
        }

        private void HitungNominalPPH()
        {
            if (this.ValuePPH != 0)
            {
                this.NominalPPH = (this.ValuePPH / 100) * this.BasePrice;
            }
        }

        private void HitungTotalTax()
        {
            this.TotalTax = this.NominalPPN + this.NominalPBBKB + this.NominalPPH;
        }

        private void ChangeBasePriceOnTaxPriceLine()
        {
            if (this.BasePrice != 0)
            {
                XPCollection<QuotationTaxPriceLine> _locQuotationTaxPriceLines = new XPCollection<QuotationTaxPriceLine>
                                                      (Session, new BinaryOperator("QuotationPriceLine.Code", this.Code));
                if (_locQuotationTaxPriceLines != null && _locQuotationTaxPriceLines.Count() > 0)
                {
                    foreach(QuotationTaxPriceLine _locQuotationTaxPriceLine in _locQuotationTaxPriceLines)
                    {
                        _locQuotationTaxPriceLine.BasePrice = this.BasePrice;
                        _locQuotationTaxPriceLine.Save();
                        _locQuotationTaxPriceLine.Session.CommitTransaction();
                    }
                }
            }
        }
        #endregion Code
    }
}