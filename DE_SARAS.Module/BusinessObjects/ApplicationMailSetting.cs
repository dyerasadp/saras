﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Setting")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("AplicationMailSettingRuleUnique", DefaultContexts.Save, "Code")]

    public class ApplicationMailSetting : BaseObject
    {

        private string _code;
        private string _email;
        private int _smtpPort;
        private string _smtpHost;
        private string _mailFrom;
        private string _mailFromPassword;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private Employee _employee;
        private XPCollection<UserAccess> _availableUserAccess;
        private UserAccess _userAccess;

        private PageType _pageType;
        private ObjectListGroup _objectListGroup;
        private bool _active;
        private ApplicationSetting _applicationSetting;


        public ApplicationMailSetting(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Field

        [Appearance("ApplicationOrganizationSettingCodeClose", Enabled = true)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Email
        {
            get { return _email; }
            set { SetPropertyValue(nameof(Email), ref _email, value); }
        }

        public int SmtpPort
        {
            get { return _smtpPort; }
            set { SetPropertyValue(nameof(SmtpPort), ref _smtpPort, value); }
        }

        public string SmtpHost
        {
            get { return _smtpHost; }
            set { SetPropertyValue(nameof(SmtpHost), ref _smtpHost, value); }
        }       
        
        
        public string MailFrom
        {
            get { return _mailFrom; }
            set { SetPropertyValue(nameof(MailFrom), ref _mailFrom, value); }
        }       
        
        public string MailFromPassword
        {
            get { return _mailFromPassword; }
            set { SetPropertyValue(nameof(MailFromPassword), ref _mailFromPassword, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        [DataSourceCriteria("Active = true")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Department")]
        [DataSourceCriteria("Active = true")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Sub Departement")]
        [DataSourceCriteria("OrgDim2 = '@This.OrgDim2' And Active = true")]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [ImmediatePostData()]
        [XafDisplayName("Section")]
        [DataSourceCriteria("OrgDim3 = '@This.OrgDim3' And Active = true")]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        [Browsable(false)]
        public XPCollection<UserAccess> AvailableUserAccess
        {
            get
            {
                if (this.Employee != null)
                {
                    _availableUserAccess = new XPCollection<UserAccess>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator(nameof(Employee), this.Employee),
                                        new BinaryOperator("IsActive", true))); ;
                }
                else
                {
                    _availableUserAccess = new XPCollection<UserAccess>(Session,
                                        new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("IsActive", true))); 
                }

                return _availableUserAccess;

            }
        }

        [DataSourceProperty("AvailableUserAccess", DataSourcePropertyIsNullMode.SelectNothing)]
        public UserAccess UserAccess
        {
            get { return _userAccess; }
            set { SetPropertyValue(nameof(UserAccess), ref _userAccess, value); }
        }

        //[Association("ApplicationSetting-ApplicationMailSetting")]
        //public ApplicationSetting ApplicationSetting
        //{
        //    get { return _applicationSetting; }
        //    set { SetPropertyValue(nameof(ApplicationSetting), ref _applicationSetting, value); }
        //}

        #endregion Organization

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field
    }
}