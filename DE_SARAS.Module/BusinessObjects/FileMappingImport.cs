﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Setting")]
    [DefaultProperty("FieldName")]
    [RuleCombinationOfPropertiesIsUnique("FileMappingImportRuleUnique", DefaultContexts.Save, "Code")]

    public class FileMappingImport : BaseObject
    {
        #region Initialization

        private string _code;
        private string _fieldName;
        private string _oldFieldName;
        private ObjectList _objectList;
        private bool _active;
        private DateTime _docDate;
        private GlobalFunction _globFunc;


        #endregion Initialization
        public FileMappingImport(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
           
        }

        #region Field

        [Appearance("FileMappingImportCodeClose", Enabled = true)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string FieldName
        {
            get { return _fieldName; }
            set { SetPropertyValue(nameof(FieldName), ref _fieldName, value); }
        }

        public string OldFieldName
        {
            get { return _oldFieldName; }
            set { SetPropertyValue(nameof(OldFieldName), ref _oldFieldName, value); }
        }

        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue(nameof(ObjectList), ref _objectList, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field
    }
}