﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VoyageCheckOpsSuppRuleUnique", DefaultContexts.Save, "Code")]

    public class VoyageCheckOpsSupp : BaseObject
    {
        #region initialization

        public string _code;
        public Voyage _voyage;
        public DailyVesselMonitoring _dvm;

        #endregion initialization

        public VoyageCheckOpsSupp(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public Voyage Voyage
        {
            get { return _voyage; }
            set { SetPropertyValue(nameof(Voyage), ref _voyage, value); }
        }

        public DailyVesselMonitoring DVM
        {
            get { return _dvm; }
            set { SetPropertyValue(nameof(DVM), ref _dvm, value); }
        }

        #endregion Field

    }
}