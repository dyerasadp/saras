﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("RefNumber")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselDocumentRenewalRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselDocumentRenewal : BaseObject
    {
        #region initialization

        private string _code;
        private DateTime _issuedDate;
        private Vessel _vessel;
        private string _refNumber;
        private ShippingMasterDocRenewAbout _renewalAbout;
        private DocumentShippingAgency _agency;
        private Employee _personInCharge;
        private string _externalAgent;
        private bool _external;
        private DateTime _docDate;
        //private DateTime _closeDate;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public VesselDocumentRenewal(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                //this.Status = Status.Progress;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselDocumentRenewal);
            #endregion Numbering
        }

        #region Field
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime IssuedDate
        {
            get { return _issuedDate; }
            set { SetPropertyValue(nameof(IssuedDate), ref _issuedDate, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        [ImmediatePostData]
        public string RefNumber
        {
            get { return _refNumber; }
            set { SetPropertyValue(nameof(RefNumber), ref _refNumber, value); }
        }

        public ShippingMasterDocRenewAbout RenewalAbout
        {
            get { return _renewalAbout; }
            set { SetPropertyValue(nameof(RenewalAbout), ref _renewalAbout, value); }
        }

        public DocumentShippingAgency Agency
        {
            get { return _agency; }
            set { SetPropertyValue(nameof(Agency), ref _agency, value); }
        }

        [Appearance("PersonInCharge", Criteria = "External = true", Enabled = false)]
        public Employee PersonInCharge
        {
            get {
                if (External == true)
                {
                    return null;
                } else
                {
                    return _personInCharge;
                }
            }
            set { SetPropertyValue(nameof(PersonInCharge), ref _personInCharge, value); }
        }

        public bool External
        {
            get { return _external; }
            set { SetPropertyValue(nameof(External), ref _external, value); }
        }

        [Appearance("ExternalAgent", Criteria = "External = false", Enabled = false)]
        public string ExternalAgent
        {
            get {
                if (External == true)
                {
                    return _externalAgent;
                }
                else
                {
                    return null;
                }
            }
            set { SetPropertyValue(nameof(ExternalAgent), ref _externalAgent, value); }
        }

        //[Appearance("StatusDisable", Enabled = false)]
        //public Status Status
        //{
        //    get { return _status; }
        //    set { SetPropertyValue(nameof(Status), ref _status, value); }
        //}

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        //[VisibleInListView(false)]
        //[Appearance("CloseDateDisable", Enabled = false)]
        //[ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        //[ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        //public DateTime CloseDate
        //{
        //    get { return _closeDate; }
        //    set { SetPropertyValue(nameof(CloseDate), ref _closeDate, value); }
        //}

        #endregion Field

        #region Manny

        [Association("VesselDocumentRenewal-VesselDocumentRenewalLines")]
        public XPCollection<VesselDocumentRenewalLine> DocumentList
        {
            get { return GetCollection<VesselDocumentRenewalLine>(nameof(DocumentList)); }
        }

        #endregion Manny
    }
}