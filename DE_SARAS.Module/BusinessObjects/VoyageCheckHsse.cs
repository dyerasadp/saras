﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VoyageCheckHsseRuleUnique", DefaultContexts.Save, "Code")]

    public class VoyageCheckHsse : BaseObject
    {

        #region initialization
        
        public string _code;
        public ShippingMasterChecklistHsse _item;
        public Voyage _voyage;
        public string _result;
        public DateTime _docDate;

        #endregion initialization

        public VoyageCheckHsse(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public ShippingMasterChecklistHsse Item
        {
            get { return _item; }
            set { SetPropertyValue(nameof(Item), ref _item, value); }
        }

        [Association("Voyage-VoyageCheckHsses")]
        public Voyage Voyage
        {
            get { return _voyage; }
            set { SetPropertyValue(nameof(Voyage), ref _voyage, value); }
        }

        public string Result
        {
            get { return _result; }
            set { SetPropertyValue(nameof(Result), ref _result, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field
    }
}