﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("DailyVesselMonitoringRuleUnique", DefaultContexts.Save, "Code")]

    public class DailyVesselMonitoring : BaseObject
    {
        #region initialization

        public string _code;
        public ShippingVesselDailySetHead _checklistHead;
        public Vessel _vessel;
        public DateTime _dateStatus;
        public string DeckStatus;
        public string EngineStatus;
        public VerifyStatus _verify;
        public DateTime _verifiedDate;
        public DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion initialization

        public DailyVesselMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.DailyVesselMonitoring);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public ShippingVesselDailySetHead ChecklistHead
        {
            get { return _checklistHead; }
            set { SetPropertyValue(nameof(ChecklistHead), ref _checklistHead, value); }
        }

        [Association("Vessel-DailyVesselMonitorings")]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime DateStatus
        {
            get { return _dateStatus; }
            set { SetPropertyValue(nameof(DateStatus), ref _dateStatus, value); }
        }

        [Appearance("VerifyDisable", Enabled = false)]
        public VerifyStatus Verify
        {
            get { return _verify; }
            set { SetPropertyValue(nameof(Verify), ref _verify, value); }
        }

        [VisibleInListView(false)]
        [Appearance("VerifiedDateDisable", Enabled = false)]
        [XafDisplayName("Verified Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime VerifiedDate
        {
            get { return _verifiedDate; }
            set { SetPropertyValue(nameof(VerifiedDate), ref _verifiedDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

        #region manny

        [Association("DailyVesselMonitoring-DailyVesselMonitoringLinesDeck")]
        public XPCollection<DailyVesselMonitoringLineDeck> Deck
        {
            get { return GetCollection<DailyVesselMonitoringLineDeck>(nameof(Deck)); }
        }

        [Association("DailyVesselMonitoring-DailyVesselMonitoringLinesEngine")]
        public XPCollection<DailyVesselMonitoringLineEngine> Engine
        {
            get { return GetCollection<DailyVesselMonitoringLineEngine>(nameof(Engine)); }
        }

        #endregion manny
    }
}