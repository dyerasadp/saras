﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VoyageBunkerRuleUnique", DefaultContexts.Save, "Code")]

    public class VoyageBunker : BaseObject
    {
        #region Initialization

        private string _code;
        private VoyageRoute _voyageRoute;
        private double _stdBunker;
        private double _actualBunker;
        private double _bunkerOnBoard;
        private double _totalBunker;
        private DateTime _dateBunker;
        private double _lastBunkerQty;
        private string _remarksBunker;
        private StatusPerformance _statusBunker;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public VoyageBunker(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VoyageBunker);
            #endregion Numbering
        }

        #region Field 

        #region Relation

        [VisibleInListView(false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }


        [ImmediatePostData()]
        [Association("VoyageRoute-VoyageBunkers")]
        [Appearance("VoyageDisable", Enabled = false)]
        public VoyageRoute VoyageRoute
        {
            get { return _voyageRoute; }
            set { 
                SetPropertyValue(nameof(VoyageRoute), ref _voyageRoute, value);
                if (!IsLoading)
                {
                    if (_voyageRoute != null)
                    {
                        LastDataGet();
                    }
                }
            }
        }

        #endregion Relation

        [VisibleInListView(false)]
        [ImmediatePostData]
        public double StdBunker
        {
            get { return _stdBunker; }
            set
            {
                SetPropertyValue(nameof(StdBunker), ref _stdBunker, value);
                if (!IsLoading)
                {
                    HitungBunker();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData]
        public double ActualBunker
        {
            get { return _actualBunker; }
            set
            {
                SetPropertyValue(nameof(ActualBunker), ref _actualBunker, value);
                if (!IsLoading)
                {
                    HitungBunker();
                }
            }
        }

        [VisibleInListView(false)]
        [Appearance("BunkerOnBoardDisable", Enabled = false)]
        public double BunkerOnBoard
        {
            get { return _bunkerOnBoard; }
            set { SetPropertyValue(nameof(BunkerOnBoard), ref _bunkerOnBoard, value); }
        }

        [VisibleInListView(false)]
        [Appearance("TotalBunkerDisable", Enabled = false)]
        public double TotalBunker
        {
            get { return _totalBunker; }
            set { SetPropertyValue(nameof(TotalBunker), ref _totalBunker, value); }
        }

        [VisibleInListView(false)]
        public DateTime DateBunker
        {
            get { return _dateBunker; }
            set { SetPropertyValue(nameof(DateBunker), ref _dateBunker, value); }
        }

        [XafDisplayName("Remaining Bunker On Board")]
        [VisibleInListView(false)]
        public double LastBunkerQty
        {
            get { return _lastBunkerQty; }
            set { SetPropertyValue(nameof(LastBunkerQty), ref _lastBunkerQty, value); }
        }

        [VisibleInListView(false)]
        [Size(300)]
        public string RemarksBunker
        {
            get { return _remarksBunker; }
            set { SetPropertyValue(nameof(RemarksBunker), ref _remarksBunker, value); }
        }


        [Appearance("StatusBunkerDisable", Enabled = false)]
        public StatusPerformance StatusBunker
        {
            get { return _statusBunker; }
            set { SetPropertyValue(nameof(StatusBunker), ref _statusBunker, value); }
        }

        #endregion Field

        #region Code

        private void LastDataGet()
        {
            if (_voyageRoute.Vessel != null)
            {
                int _routeNumBefore = _voyageRoute.No - 1;
                if (_voyageRoute.No == 1)
                {
                    VesselRemains _locVesselRemains = Session.FindObject<VesselRemains>
                                        (new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Vessel", _voyageRoute.Vessel)
                                        ));
                    if (_locVesselRemains != null)
                    {
                        this.BunkerOnBoard = _locVesselRemains.QtyBunkerLeft;
                    }
                }
                else
                {
                    VoyageRoute _voyageRouteBefore = Session.FindObject<VoyageRoute>
                                        (new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("No", _routeNumBefore),
                                        new BinaryOperator("Voyage", _voyageRoute.Voyage)
                                        ));

                    if (_voyageRouteBefore != null)
                    {
                        VoyageBunker _lastVoyageRouteBunker = Session.FindObject<VoyageBunker>
                                (new GroupOperator(GroupOperatorType.And,
                                new BinaryOperator("VoyageRoute", _voyageRouteBefore)
                                ));
                        if (_lastVoyageRouteBunker != null)
                        {
                            this.BunkerOnBoard = _lastVoyageRouteBunker.LastBunkerQty;
                        }
                    }
                }
            }
        }

        private void HitungBunker()
        {
            this.TotalBunker = this.ActualBunker + this.BunkerOnBoard;

            if (this.StdBunker >= this.ActualBunker)
            {
                this.StatusBunker = StatusPerformance.Achieved;
            }
            else
            {
                this.StatusBunker = StatusPerformance.Failed;
            }
        }

        #endregion Code
    }
}