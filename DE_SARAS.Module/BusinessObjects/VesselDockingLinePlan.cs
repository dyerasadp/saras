﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("WorkDescription")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselDockingLinePlanCategoryRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselDockingLinePlan : BaseObject
    {
        #region initialization

        private string _code;
        private VesselDocking _dockingProject;
        private ItemPosition _itemPart;
        private VesselPart _workItem;
        private string _workDescription;
        private double _workWeight;
        private double _workPercentage;
        private DateTime _workStart;
        private DateTime _workEnd;
        private double _duration;
        private DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public VesselDockingLinePlan(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselDockingLinePlan);
            #endregion Numbering
        }

        #region Field

        [VisibleInListView(false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Association("VesselDocking-VesselDockingLinesPlan")]
        public VesselDocking DockingProject
        {
            get { return _dockingProject; }
            set { SetPropertyValue(nameof(DockingProject), ref _dockingProject, value); }
        }

        public ItemPosition ItemPart
        {
            get { return _itemPart; }
            set { SetPropertyValue(nameof(ItemPart), ref _itemPart, value); }
        }

        public VesselPart WorkItem
        {
            get { return _workItem; }
            set { SetPropertyValue(nameof(WorkItem), ref _workItem, value); }
        }

        public string WorkDescription
        {
            get { return _workDescription; }
            set { SetPropertyValue(nameof(WorkDescription), ref _workDescription, value); }
        }

        public double WorkWeight
        {
            get { return _workWeight; }
            set { SetPropertyValue(nameof(WorkWeight), ref _workWeight, value); }
        }

        public double WorkPercentage
        {
            get { return _workPercentage; }
            set { SetPropertyValue(nameof(WorkPercentage), ref _workPercentage, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime WorkStart
        {
            get { return _workStart; }
            set { SetPropertyValue(nameof(WorkStart), ref _workStart, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime WorkEnd
        {
            get { return _workEnd; }
            set { SetPropertyValue(nameof(WorkEnd), ref _workEnd, value); }
        }

        public double Duration
        {
            get { return _duration; }
            set { SetPropertyValue(nameof(Duration), ref _duration, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

    }
}