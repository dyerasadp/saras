﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Organization")]
    [RuleCombinationOfPropertiesIsUnique("CallCenterFeedbackUserRuleUnique", DefaultContexts.Save, "Code")]

    public class CallCenterFeedbackUser : BaseObject
    {
        #region Initialization
        private string _code;
        private OrganizationDimension2 _orgDim2;
        private bool _activeIT;
        private bool _activeNonIT;
        private string _admin;   
        private string _user;
        private CallCenter _callCenter;
        private GlobalFunction _globFunc;
        private XPCollection<OrganizationDimension2> _availableOrganizationDimension2;
        #endregion Initialization
        public CallCenterFeedbackUser(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                #region UserAccess
                _globFunc = new GlobalFunction();
                 this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CallCenterFeedbackUser);
                #endregion UserAccess
            }
        }

        #region Field

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [Appearance("CodeCallCenterFeedbackClosed", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [ImmediatePostData]
        //[VisibleInDetailView(false)]
        //[VisibleInListView(false)]
        [Association("CallCenter-CallCenterFeedbackUsers")]
        public CallCenter CallCenter
        {
            get { return _callCenter; }
            set { SetPropertyValue(nameof(CallCenter), ref _callCenter, value);
                if(!IsLoading)
                {
                    if(_callCenter != null)
                    {
                        this.OrgDim2 = _callCenter.OrgDim2;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<OrganizationDimension2> AvailableOrganizationDimension2
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<OrganizationDimension2> _locOrgDim2s = new XPCollection<OrganizationDimension2>(Session,
                                                    new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Active", true),
                                                    new GroupOperator(GroupOperatorType.Or,
                                                    new BinaryOperator("Name", "IT"),
                                                    new BinaryOperator("Name", "MEP")
                                                    )));
                    if (_locOrgDim2s != null && _locOrgDim2s.Count() > 0)
                    {
                        _availableOrganizationDimension2 = _locOrgDim2s;
                    }
                }
                return _availableOrganizationDimension2;
            }
        }

        [ImmediatePostData()]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Appearance("OrganizationDimension2CallCenterFeedbackClosed", Enabled = false)]
        [DataSourceProperty("AvailableOrganizationDimension2", DataSourcePropertyIsNullMode.SelectNothing)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); 
            if(!IsLoading)
                {
                    if(_orgDim2 != null) { 
                        if(_orgDim2.Name == "IT")
                        {
                            this.ActiveIT = true;
                        }
                        else if(_orgDim2.Name == "MEP")
                        {
                            this.ActiveIT = true;
                        }
                        else
                        {
                            this.ActiveNonIT = true;
                        }
                    }
                }
            }
        }

        [ImmediatePostData]
        [Browsable(false)]
        public bool ActiveIT
        {
            get { return _activeIT; }
            set { SetPropertyValue(nameof(ActiveIT), ref _activeIT, value); }
        }

        [ImmediatePostData]
        [Browsable (false)]
        public bool ActiveNonIT
        {
            get { return _activeNonIT; }
            set { SetPropertyValue(nameof(ActiveNonIT), ref _activeNonIT, value); }
        }

        [ImmediatePostData]
        [Appearance("AdminCallCenterFeedbackDisable",Criteria = "ActiveNonIT = 'False'", Enabled = true)]
        [Appearance("AdminCallCenterFeedbackEnable",Criteria = "ActiveNonIT = 'True'", Enabled = false)]
        public string Admin
        {
            get { return _admin; }
            set { SetPropertyValue(nameof(Admin), ref _admin, value); }
        }

        [ImmediatePostData]
        [Appearance("UserCallCenterFeedbackDisable",Criteria = "ActiveIT = 'True'", Enabled = false)]
        [Appearance("UserCallCenterFeedbackEnable",Criteria = "ActiveIT = 'False'", Enabled = true)]
        public string User
        {
            get { return _user; }
            set { SetPropertyValue(nameof(User), ref _user, value);
                if(!IsLoading)
                {
                    SendFeedbackToAdmin();
                }
            }
        }

        #endregion Field

        #region Code 
        private void SendFeedbackToAdmin()
        {
            if (this.User != null)
            {
                CallCenterMonitoring _availableMonitoringAdmin = Session.FindObject<CallCenterMonitoring>
                                                                        (new BinaryOperator("CallCenter", this.CallCenter));

                if (_availableMonitoringAdmin != null)
                {
                    CallCenterFeedbackAdmin _availableFeedbackAdmin = Session.FindObject<CallCenterFeedbackAdmin>
                                                                       (new BinaryOperator("CallCenter", this.CallCenter));
                    if (_availableFeedbackAdmin != null)
                    {
                        CallCenterFeedbackAdmin _sendFeedBackToAdmin = new CallCenterFeedbackAdmin(Session)
                        {
                            CallCenterMonitoring = _availableMonitoringAdmin,
                            CallCenter = this.CallCenter,
                            User = this.User,
                        };
                        _sendFeedBackToAdmin.Save();
                        _sendFeedBackToAdmin.Session.CommitTransaction();
                    }
                    else
                    {
                        CallCenterFeedbackAdmin _sendNewFeedBackToAdmin = new CallCenterFeedbackAdmin(Session)
                        {
                            CallCenter = this.CallCenter,
                            CallCenterMonitoring = _availableMonitoringAdmin,
                            User = this.User,
                        };
                        _sendNewFeedBackToAdmin.Save();
                        _sendNewFeedBackToAdmin.Session.CommitTransaction();
                    }
                }
                else
                {

                }
            }

        }

        #endregion Code
    }
}