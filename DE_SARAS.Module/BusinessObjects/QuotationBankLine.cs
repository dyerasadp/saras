﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("QuotationBank")]
    [RuleCombinationOfPropertiesIsUnique("QuotationBankLineRuleUnique", DefaultContexts.Save, "Code")]

    public class QuotationBankLine : BaseObject
    {
        #region Initialization

        private string _code;
        private QuotationBank _quotationBank;
        private string _accountBank;
        private string _branch;
        private Company _company;
        private DateTime _docDate;
        private Quotation _quotation;
        private XPCollection<QuotationBank> _availableQuotationBank;

        #endregion Initialization
        public QuotationBankLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }


        [Browsable(false)]
        public XPCollection<QuotationBank> AvailableQuotationBank
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<QuotationBank> _locQuotationBanks = new XPCollection<QuotationBank>(this.Session);
                    if (_locQuotationBanks != null && _locQuotationBanks.Count() > 0)
                    {
                        _availableQuotationBank = _locQuotationBanks;
                    }
                }
                return _availableQuotationBank;
            }
        }

        [ImmediatePostData]
        [XafDisplayName("Name")]
        [DataSourceProperty("AvailableQuotationBank", DataSourcePropertyIsNullMode.SelectNothing)]
        public QuotationBank QuotationBank
        {
            get { return _quotationBank; }
            set { SetPropertyValue(nameof(QuotationBank), ref _quotationBank, value);
                if (!IsLoading)
                {
                    if (_quotationBank != null)
                    {
                        this.AccountBank = _quotationBank.Account;
                        this.Branch = _quotationBank.Branch;
                        this.Company = _quotationBank.Company;
                    }
                }
            }
        }

        [XafDisplayName("Account")]
        [Appearance("AccountBankQuotationBankLineDisabled", Enabled = false)]
        public string AccountBank
        {
            get { return _accountBank; }
            set { SetPropertyValue(nameof(AccountBank), ref _accountBank, value); }
        }

        [Appearance("BranchBankQuotationBankLineDisabled", Enabled = false)]
        public string Branch
        {
            get { return _branch; }
            set { SetPropertyValue(nameof(Branch), ref _branch, value); }
        }
        #region Organization

        [XafDisplayName("Account Holder")]
        [Appearance("CompanyBankQuotationBankLineDisabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }
        #endregion Organization

        [Appearance("DateTimeClosed", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [Association("Quotation-QuotationBankLines")]
        public Quotation Quotation
        {
            get { return _quotation; }
            set { SetPropertyValue(nameof(Quotation), ref _quotation, value); }
        }

        #endregion Field
    }
}