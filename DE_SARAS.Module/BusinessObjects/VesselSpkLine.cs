﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("VesselPart")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselSpkLineRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselSpkLine : BaseObject
    {

        #region initialization

        public string _code;
        public VesselSpk _spk;
        public VesselPart _vesselPart;
        public string _description;
        public string _action;
        public Status _status;
        private DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion initialization

        public VesselSpkLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Status = Status.Progress;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselSpkLine);
            #endregion Numbering
        }

        #region Field
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Association("VesselSpk-VesselSpkLines")]
        public VesselSpk Spk
        {
            get { return _spk; }
            set { SetPropertyValue(nameof(Spk), ref _spk, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Vessel = '@This.Spk.Vessel' And Result = 'Abnormal' And Active = 'true'")]
        public VesselPart VesselPart
        {
            get { return _vesselPart; }
            set { SetPropertyValue(nameof(VesselPart), ref _vesselPart, value); }
        }

        public string Description
        {
            get { return _description; }
            set { SetPropertyValue(nameof(Description), ref _description, value); }
        }

        public string Action
        {
            get { return _action; }
            set { SetPropertyValue(nameof(Action), ref _action, value); }
        }

        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue(nameof(Status), ref _status, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

        #region Manny

        [Association("VesselSpkLine-DetailProgress")]
        public XPCollection<VesselSpkLineProgress> DetailProgress
        {
            get { return GetCollection<VesselSpkLineProgress>(nameof(DetailProgress)); }
        }

        #endregion Manny
    }
}