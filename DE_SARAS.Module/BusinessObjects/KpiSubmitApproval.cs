﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Purchase")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseOrderRuleUnique", DefaultContexts.Save, "Code")]

    public class KpiSubmitApproval : BaseObject
    {
        #region Initialization

        private int _no;
        private string _code;
        #region InitialOrganization
        private XPCollection<Company> _availableCompany;
        private Company _company;
        private XPCollection<OrganizationDimension1> _availableOrgDim1;
        private OrganizationDimension1 _orgDim1;
        private XPCollection<OrganizationDimension2> _availableOrgDim2;
        private OrganizationDimension2 _orgDim2;
        private XPCollection<OrganizationDimension3> _availableOrgDim3;
        private OrganizationDimension3 _orgDim3;
        private XPCollection<OrganizationDimension4> _availableOrgDim4;
        private OrganizationDimension4 _orgDim4;
        private XPCollection<Employee> _availableEmployee;
        private Employee _employee;
        #endregion InitialOrganization       
        private ApprovalLevel _approvalLevel;
        private Status _approvalStatus;
        private DateTime _approvalDate;
        private bool _lockApproval;
        private bool _endApproval;
        private Employee _approvedBy;
        private KpiSubmit _kpiSubmit;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization
        public KpiSubmitApproval(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.KpiSubmitApproval);
            }
        }

        #region Override

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Override

        #region Field

        [Appearance("KpiSubmitApprovalNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue(nameof(No), ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("KpiSubmitApprovalCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        #region Organization

        [ImmediatePostData()]
        [Appearance("KpiSubmitApprovalCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public Company Company
        {
            get { return _company; }
            set
            {
                SetPropertyValue(nameof(Company), ref _company, value);
                if (!IsLoading)
                {
                    if (this._company == null)
                    {
                        this.OrgDim1 = null;
                        this.OrgDim2 = null;
                        this.OrgDim3 = null;
                        this.OrgDim4 = null;
                    }
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData()]
        [XafDisplayName("Workplace")]
        [Appearance("KpiSubmitApprovalOrgDim1Close", Criteria = "ActivationPosting = true", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set
            {
                SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value);
                if (!IsLoading)
                {
                    if (this._orgDim1 == null)
                    {
                        this.OrgDim2 = null;
                        this.OrgDim3 = null;
                        this.OrgDim4 = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [XafDisplayName("Division")]
        [Appearance("KpiSubmitApprovalOrgDim2Close", Criteria = "ActivationPosting = true", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set
            {
                SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value);
                if (!IsLoading)
                {
                    if (this._orgDim2 == null)
                    {
                        this.OrgDim3 = null;
                        this.OrgDim4 = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [XafDisplayName("Department")]
        [Appearance("KpiSubmitApprovalOrgDim3Close", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set
            {
                SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value);
                if (!IsLoading)
                {
                    if (this._orgDim3 == null)
                    {
                        this.OrgDim4 = null;
                    }
                }
            }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [XafDisplayName("Section")]
        [Appearance("KpiSubmitApprovalOrgDim4Close", Criteria = "ActivationPosting = true", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [XafDisplayName("Purchaser")]
        [Appearance("KpiSubmitApprovalEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        #endregion Organization

        [Appearance("KpiSubmitApprovalLevelEnabled", Enabled = false)]
        public ApprovalLevel ApprovalLevel
        {
            get { return _approvalLevel; }
            set { SetPropertyValue(nameof(ApprovalLevel), ref _approvalLevel, value); }
        }

        [Appearance("KpiSubmitApprovalStatusEnabled", Enabled = false)]
        public Status ApprovalStatus
        {
            get { return _approvalStatus; }
            set { SetPropertyValue(nameof(ApprovalStatus), ref _approvalStatus, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy hh:mm}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Appearance("KpiSubmitApprovalDateEnabled", Enabled = false)]
        public DateTime ApprovalDate
        {
            get { return _approvalDate; }
            set { SetPropertyValue(nameof(ApprovalDate), ref _approvalDate, value); }
        }

        [Appearance("KpiSubmitApprovalLockApprovalEnabled", Enabled = false)]
        public bool LockApproval
        {
            get { return _lockApproval; }
            set { SetPropertyValue(nameof(LockApproval), ref _lockApproval, value); }
        }

        [Appearance("KpiSubmitApprovalEndApprovalEnabled", Enabled = false)]
        public bool EndApproval
        {
            get { return _endApproval; }
            set { SetPropertyValue(nameof(EndApproval), ref _endApproval, value); }
        }

        [Appearance("KpiSubmitApprovalApprovedByEnabled", Enabled = false)]
        public Employee ApprovedBy
        {
            get { return _approvedBy; }
            set { SetPropertyValue(nameof(ApprovedBy), ref _approvedBy, value); }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save), ImmediatePostData()]
        [Appearance("KpiSubmitApprovalKpiSubmitEnabled", Enabled = false)]
        [Association("KpiSubmit-KpiSubmitApprovals")]
        public KpiSubmit KpiSubmit
        {
            get { return _kpiSubmit; }
            set
            {
                SetPropertyValue(nameof(KpiSubmit), ref _kpiSubmit, value);
                if (!IsLoading)
                {
                    if (this._kpiSubmit != null)
                    {
                        if (this._kpiSubmit.Company != null) { this.Company = this._kpiSubmit.Company; }
                        if (this._kpiSubmit.OrgDim1 != null) { this.OrgDim1 = this._kpiSubmit.OrgDim1; }
                        if (this._kpiSubmit.OrgDim2 != null) { this.OrgDim2 = this._kpiSubmit.OrgDim2; }
                        if (this._kpiSubmit.OrgDim3 != null) { this.OrgDim3 = this._kpiSubmit.OrgDim3; }
                        if (this._kpiSubmit.OrgDim4 != null) { this.OrgDim4 = this._kpiSubmit.OrgDim4; }
                        if (this._kpiSubmit.Name != null) { this.Employee = this._kpiSubmit.Name; }
                        //if (this._purchaseOrder.TransactionPeriodType != TransactionPeriodType.None) { this.TransactionPeriodType = this._purchaseOrder.TransactionPeriodType; }
                        //if (this._purchaseOrder.TransactionPeriod != null) { this.TransactionPeriod = this._purchaseOrder.TransactionPeriod; }
                    }
                }
            }
        }

        #endregion Field

        #region Manny

        //AuditTrail
        [CollectionOperationSet(AllowAdd = false, AllowRemove = true)]
        public XPCollection<AuditDataItemPersistent> ChangeHistory
        {
            get
            {
                if (changeHistory == null)
                {
                    changeHistory = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                }
                return changeHistory;
            }
        }

        #endregion Manny

        #region Code

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.KpiSubmit != null)
                    {
                        object _makRecord = Session.Evaluate<KpiSubmitApproval>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("KpiSubmit=?", this.KpiSubmit));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrderApproval " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.KpiSubmit != null)
                {
                    KpiSubmit _noHeader = Session.FindObject<KpiSubmit>
                                                (new BinaryOperator("Code", this.KpiSubmit.Code));

                    XPCollection<KpiSubmitApproval> _noLines = new XPCollection<KpiSubmitApproval>
                                                (Session, new BinaryOperator("KpiSubmit", _noHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_noLines != null)
                    {
                        int i1 = 0;
                        foreach (KpiSubmitApproval _numLine in _noLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrderApproval " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.KpiSubmit != null)
                {
                    KpiSubmit _noHeader = Session.FindObject<KpiSubmit>
                                                (new BinaryOperator("Code", this.KpiSubmit.Code));

                    XPCollection<KpiSubmitApproval> _noLines = new XPCollection<KpiSubmitApproval>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("KpiSubmit", _noHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_noLines != null)
                    {
                        int i = 0;
                        foreach (KpiSubmitApproval _noLine in _noLines)
                        {
                            i += 1;
                            _noLine.No = i;
                            _noLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrderApproval " + ex.ToString());
            }
        }

        #endregion No

        #endregion Code

    }
}