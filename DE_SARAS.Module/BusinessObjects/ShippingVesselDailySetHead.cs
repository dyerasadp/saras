﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Shippingg")]
    [RuleCombinationOfPropertiesIsUnique("ShippingVesselDailySetHeadRuleUnique", DefaultContexts.Save, "Code")]

    public class ShippingVesselDailySetHead : BaseObject
    {
        #region initialization

        public string _code;
        public string _name;
        public Vessel _vessel;
        //public ItemPosition _itemPart;
        public bool _active;
        public DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion initialization

        public ShippingVesselDailySetHead(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ShippingVesselDailySetHead);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

        #region manny

        [Association("ShippingVesselDailySetHead-ShippingVesselDailySetLinesDeck")]
        public XPCollection<ShippingVesselDailySetLineDeck> DeckChecklist
        {
            get { return GetCollection<ShippingVesselDailySetLineDeck>(nameof(DeckChecklist)); }
        }

        [Association("ShippingVesselDailySetHead-ShippingVesselDailySetLinesEngine")]
        public XPCollection<ShippingVesselDailySetLineEngine> EngineChecklist
        {
            get { return GetCollection<ShippingVesselDailySetLineEngine>(nameof(EngineChecklist)); }
        }

        #endregion manny
    }
}