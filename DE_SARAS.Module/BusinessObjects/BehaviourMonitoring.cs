﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("KPI Submit")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("BehaviourMonitoringRuleUnique", DefaultContexts.Save, "Code")]

    public class BehaviourMonitoring : BaseObject
    {
        #region Initialization

        private string _code;
        private KpiYear _kpiYear;
        private BehaviourRole _behaviourRole;
        private double _bobot;
        private double _value;
        private double _scorebehaviour;
        private Employee _employee;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private GlobalFunction _globFunc;
        private string _localUserAccess;


        #endregion Initialization
        public BehaviourMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));

                Company _locCompany = null;
                OrganizationDimension1 _locOrgDim1 = null;
                OrganizationDimension2 _locOrgDim2 = null;
                OrganizationDimension3 _locOrgDim3 = null;
                OrganizationDimension4 _locOrgDim4 = null;
                Employee _locEmployee = null;

                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        _locEmployee = _locUserAccess.Employee;
                        if (_locUserAccess.Employee.Company != null) { _locCompany = _locUserAccess.Employee.Company; }
                        if (_locUserAccess.Employee.OrganizationDimension1 != null) { _locOrgDim1 = _locUserAccess.Employee.OrganizationDimension1; }
                        if (_locUserAccess.Employee.OrganizationDimension2 != null) { _locOrgDim2 = _locUserAccess.Employee.OrganizationDimension2; }
                        if (_locUserAccess.Employee.OrganizationDimension3 != null) { _locOrgDim3 = _locUserAccess.Employee.OrganizationDimension3; }
                        if (_locUserAccess.Employee.OrganizationDimension4 != null) { _locOrgDim4 = _locUserAccess.Employee.OrganizationDimension4; }
                    }
                    if (this.Session != null)
                    {
                        this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BehaviourMonitoring);
                    }


                    this.Company = _locCompany;
                    this.OrgDim1 = _locOrgDim1;
                    this.OrgDim2 = _locOrgDim2;
                    this.OrgDim3 = _locOrgDim3;
                    this.OrgDim4 = _locOrgDim4;
                    this.Employee = _locEmployee;
                }
            }
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public KpiYear KpiYear
        {
            get { return _kpiYear; }
            set { SetPropertyValue(nameof(KpiYear), ref _kpiYear, value); }
        }

        [XafDisplayName("Behaviour")]
        public BehaviourRole BehaviourRole
        {
            get { return _behaviourRole; }
            set { SetPropertyValue(nameof(BehaviourRole), ref _behaviourRole, value); }
        }

        public double Bobot
        {
            get { return _bobot; }
            set { SetPropertyValue(nameof(Bobot), ref _bobot, value); }
        }

        public double Value
        {
            get { return _value; }
            set { SetPropertyValue(nameof(Value), ref _value, value); }
        }

        [XafDisplayName("Score")]
        public double ScoreBehaviour
        {
            get { return _scorebehaviour; }
            set { SetPropertyValue(nameof(ScoreBehaviour), ref _scorebehaviour, value); }
        }

        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1Disabled", Enabled = false)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Departement")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Departement")]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [XafDisplayName("Section")]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        #endregion Field

    }
}