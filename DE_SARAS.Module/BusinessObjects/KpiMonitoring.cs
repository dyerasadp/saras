﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("KPI Submit")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("KpiMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    public class KpiMonitoring : BaseObject
    {
        #region Initialization

        private string _code;
        private KpiYear _kpiYear;
        private KpiRoleLine _name;
        private double _proportionKpi;
        private double _targetKpi;
        private double _january;
        private double _february;
        private double _march;
        private double _april;
        private double _may;
        private double _june;
        private double _july;
        private double _august;
        private double _september;
        private double _october;
        private double _november;
        private double _december;
        private double _ytd;
        private double _value;
        private double _scoreytd;
        private KpiYtdType _kpiYtdType;
        private KpiPeriod _kpiPeriod;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private JobPosition _defaultPosition;
        private KpiSubmit _kpiSubmit;
        private Employee _employee;
        private XPCollection<KpiSubmit> _availableKpiSubmit;
        private string _localUserAccess;
        private GlobalFunction _globFunc;


        #endregion Initialization
        public KpiMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if (!IsLoading)
            {
                #region UserAccess
                _globFunc = new GlobalFunction();
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));

                Company _locCompany = null;
                OrganizationDimension1 _locOrgDim1 = null;
                OrganizationDimension2 _locOrgDim2 = null;
                OrganizationDimension3 _locOrgDim3 = null;
                OrganizationDimension4 _locOrgDim4 = null;
                Employee _locEmployee = null;

                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        _locEmployee = _locUserAccess.Employee;
                        if (_locUserAccess.Employee.Company != null) { _locCompany = _locUserAccess.Employee.Company; }
                        if (_locUserAccess.Employee.OrganizationDimension1 != null) { _locOrgDim1 = _locUserAccess.Employee.OrganizationDimension1; }
                        if (_locUserAccess.Employee.OrganizationDimension2 != null) { _locOrgDim2 = _locUserAccess.Employee.OrganizationDimension2; }
                        if (_locUserAccess.Employee.OrganizationDimension3 != null) { _locOrgDim3 = _locUserAccess.Employee.OrganizationDimension3; }
                        if (_locUserAccess.Employee.OrganizationDimension4 != null) { _locOrgDim4 = _locUserAccess.Employee.OrganizationDimension4; }
                    }
                    //if (this.Session != null)
                    //{
                    //    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.KpiSubmit);
                    //}


                    this.Company = _locCompany;
                    this.OrgDim1 = _locOrgDim1;
                    this.OrgDim2 = _locOrgDim2;
                    this.OrgDim3 = _locOrgDim3;
                    this.OrgDim4 = _locOrgDim4;
                    this.Employee = _locEmployee;

                }
                #endregion UserAccess
            }
        }

        [VisibleInListView(false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public KpiYear KpiYear
        {
            get { return _kpiYear; }
            set { SetPropertyValue(nameof(KpiYear), ref _kpiYear, value); }
        }

        public KpiRoleLine Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [XafDisplayName("Proportion")]
        public double ProportionKpi
        {
            get { return _proportionKpi; }
            set { SetPropertyValue(nameof(ProportionKpi), ref _proportionKpi, value); }
        }

        [XafDisplayName("Target")]
        public double TargetKpi
        {
            get { return _targetKpi; }
            set { SetPropertyValue(nameof(TargetKpi), ref _targetKpi, value); }
        }

        [XafDisplayName("Jan")]
        public double January
        {
            get { return _january; }
            set { SetPropertyValue(nameof(January), ref _january, value); }
        }

        [XafDisplayName("Feb")]
        public double February
        {
            get { return _february; }
            set { SetPropertyValue(nameof(February), ref _february, value); }
        }

        [XafDisplayName("Mar")]
        public double March
        {
            get { return _march; }
            set { SetPropertyValue(nameof(March), ref _march, value); }
        }

        [XafDisplayName("Apr")]
        public double April
        {
            get { return _april; }
            set { SetPropertyValue(nameof(April), ref _april, value); }
        }

        [XafDisplayName("May")]
        public double May
        {
            get { return _may; }
            set { SetPropertyValue(nameof(May), ref _may, value); }
        }

        [XafDisplayName("Jun")]
        public double June
        {
            get { return _june; }
            set { SetPropertyValue(nameof(June), ref _june, value); }
        }

        [XafDisplayName("Jul")]
        public double July
        {
            get { return _july; }
            set { SetPropertyValue(nameof(July), ref _july, value); }
        }

        [XafDisplayName("Aug")]
        public double August
        {
            get { return _august; }
            set { SetPropertyValue(nameof(August), ref _august, value); }
        }

        [XafDisplayName("Sep")]
        public double September
        {
            get { return _september; }
            set { SetPropertyValue(nameof(September), ref _september, value); }
        }

        [XafDisplayName("Oct")]
        public double October
        {
            get { return _october; }
            set { SetPropertyValue(nameof(October), ref _october, value); }
        }

        [XafDisplayName("Nov")]
        public double November
        {
            get { return _november; }
            set { SetPropertyValue(nameof(November), ref _november, value); }
        }

        [XafDisplayName("Dec")]
        public double December
        {
            get { return _december; }
            set { SetPropertyValue(nameof(December), ref _december, value); }
        }

        [DbType("decimal(19,11)")]
        public double Ytd
        {
            get { return _ytd; }
            set { SetPropertyValue(nameof(Ytd), ref _ytd, value); }
        }
        public double Value
        {
            get { return _value; }
            set { SetPropertyValue(nameof(Value), ref _value, value) ; }
        }
        public double ScoreYtd
        {
            get { return _scoreytd; }
            set { SetPropertyValue(nameof(ScoreYtd), ref _scoreytd, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Appearance("KpiYtdTypeKpiMonitoringDisabled", Enabled = false)]
        public KpiYtdType KpiYtdType
        {
            get { return _kpiYtdType; }
            set { SetPropertyValue(nameof(KpiYtdType), ref _kpiYtdType, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Appearance("KpiPeriodKpiMonitoringDisabled", Enabled = false)]
        public KpiPeriod KpiPeriod
        {
            get { return _kpiPeriod; }
            set { SetPropertyValue(nameof(KpiPeriod), ref _kpiPeriod, value); }
        }

        [VisibleInListView(false)]
        [XafDisplayName("Com")]
        [Appearance("CompanyDisabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [VisibleInListView(false)]
        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1Disabled", Enabled = false)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [VisibleInListView(false)]
        [XafDisplayName("Dept")]
        [Appearance("OrganizationDimension2Disabled", Enabled = false)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [VisibleInListView(false)]
        [XafDisplayName("SubDept")]
        [Appearance("OrganizationDimension3Close", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("-")]
        [Appearance("OrganizationDimension4Close", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }


        [ImmediatePostData]
        [VisibleInListView(false)]
        [XafDisplayName("Position")]
        [Appearance("DefaultPositionDefault", Enabled = false)]
        public JobPosition DefaultPosition
        {
            get { return _defaultPosition; }
            set { SetPropertyValue(nameof(DefaultPosition), ref _defaultPosition, value); }
        }

        [Browsable(false)]
        public XPCollection<KpiSubmit> AvailableKpiSubmit
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<KpiSubmit> _locKpiSubmits = new XPCollection<KpiSubmit>(this.Session);
                    if (_locKpiSubmits != null && _locKpiSubmits.Count() > 0)
                    {
                        _availableKpiSubmit = _locKpiSubmits;
                    }
                }
                return _availableKpiSubmit;
            }
        }


        [ImmediatePostData()]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [Appearance("KpiSubmitKpiSubmitLineSubmit", Enabled = false)]
        [DataSourceProperty("AvailableKpiSubmit", DataSourcePropertyIsNullMode.SelectNothing)]
        public KpiSubmit KpiSubmit
        {
            get { return _kpiSubmit; }
            set
            {
                SetPropertyValue(nameof(KpiSubmit), ref _kpiSubmit, value);
            }
        }

        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }
    }
}