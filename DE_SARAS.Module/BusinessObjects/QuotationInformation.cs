﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("Information")]
    [RuleCombinationOfPropertiesIsUnique("QuotationInformationRuleUnique", DefaultContexts.Save, "Code")]

    public class QuotationInformation : BaseObject
    {
        #region Initialization

        private string _code;
        private string _information;
        private MechanismGroup _mechanismGroup;
        private bool _active;
        private DateTime _docDate;
        private Company _company;




        #endregion Initialization

        public QuotationInformation(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }


        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Size(3000)]
        public string Information
        {
            get { return _information; }
            set { SetPropertyValue(nameof(Information), ref _information, value); }
        }

        [XafDisplayName("Group")]
        public MechanismGroup MechanismGroup
        {
            get { return _mechanismGroup; }
            set { SetPropertyValue(nameof(MechanismGroup), ref _mechanismGroup, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        [Appearance("DateTimeClosed", Enabled = false)]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        #endregion Field

        #region Manny 

        [Association("QuotationInformation-QuotationInformationLines")]
        public XPCollection<QuotationInformationLine> QuotationInformationLines
        {
            get { return GetCollection<QuotationInformationLine>(nameof(QuotationInformationLines)); }
        }

        #endregion Manny

    }
}