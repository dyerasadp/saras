﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.PivotChart;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Additional Reports")]
    public class AdditionalAnalyze : Analysis
    { 
        public AdditionalAnalyze(Session session)
            : base(session)
        {
            //RequiredModuleTypes.Add(typeof(DevExpress.ExpressApp.PivotChart.PivotChartModuleBase));
            //AdditionalExportedTypes.Add(typeof(DevExpress.Persistent.BaseImpl.Analysis));


            //AdditionalReport : ReportDataV2
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }
    }
}