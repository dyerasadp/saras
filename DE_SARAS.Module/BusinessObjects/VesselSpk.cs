﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VesselSpkRuleUnique", DefaultContexts.Save, "Code")]

    public class VesselSpk : BaseObject
    {
        #region initialization

        private string _code;
        private string _name;
        private DateTime _startDate;
        private SpkType _type;
        private Dock _dock;
        private string _up;
        private Vessel _vessel;
        private string _vesselLocation;
        private DateTime _estimatedFinishDate;
        private DateTime _docDate;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public VesselSpk(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VesselSpk);
            #endregion Numbering
        }
        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue(nameof(StartDate), ref _startDate, value); }
        }

        public SpkType Type
        {
            get { return _type; }
            set { SetPropertyValue(nameof(Type), ref _type, value); }
        }

        [ImmediatePostData]
        public Dock Dock
        {
            get { return _dock; }
            set { SetPropertyValue(nameof(Dock), ref _dock, value); }
        }

        public string Up
        {
            get { return _up; }
            set { SetPropertyValue(nameof(Up), ref _up, value); }
        }

        [ImmediatePostData]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        public string VesselLocation
        {
            get { return _vesselLocation; }
            set { SetPropertyValue(nameof(VesselLocation), ref _vesselLocation, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime EstimatedFinishDate
        {
            get { return _estimatedFinishDate; }
            set { SetPropertyValue(nameof(EstimatedFinishDate), ref _estimatedFinishDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created Date")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

        #region Manny

        [Association("VesselSpk-VesselSpkLines")]
        public XPCollection<VesselSpkLine> VesselSpkLines
        {
            get { return GetCollection<VesselSpkLine>(nameof(VesselSpkLines)); }
        }

        [Association("VesselSpk-SpkProgress")]
        public XPCollection<VesselSpkLineProgress> SpkProgress
        {
            get { return GetCollection<VesselSpkLineProgress>(nameof(SpkProgress)); }
        }

        #endregion Manny
    }
}