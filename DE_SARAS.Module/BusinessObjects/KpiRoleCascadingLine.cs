﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System.ComponentModel;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("KPI Setting")]
    [DefaultProperty("KpiRoleLineSelect")]
    [RuleCombinationOfPropertiesIsUnique("KpiRoleCascadingLineRuleUnique", DefaultContexts.Save, "Code")]

    public class KpiRoleCascadingLine : BaseObject
    {
        #region Initialization

        private string _code;
        //private string _name;
        private KpiRoleLine _kpiRoleLineSelect;
        private Employee _employee;
        private KpiRoleLine _kpiRoleLine;
        private double _weight;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private XPCollection<KpiRoleLine> _availableKpiRoleLine;
        private bool _active;
        private DateTime _docDate;


        #endregion Initialization

        public KpiRoleCascadingLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                this.DocDate = DateTime.Now;
            }
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<KpiRoleLine> AvailableKpiRoleLine
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<KpiRoleLine> _locKpiRoleLines = new XPCollection<KpiRoleLine>(this.Session,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true)));
                    if (_locKpiRoleLines != null && _locKpiRoleLines.Count() > 0)
                    {
                        _availableKpiRoleLine = _locKpiRoleLines;
                    }
                }
                return _availableKpiRoleLine;
            }
        }

        [DataSourceProperty("AvailableKpiRoleLine", DataSourcePropertyIsNullMode.SelectNothing)]
        public KpiRoleLine KpiRoleLineSelect
        {
            get { return _kpiRoleLineSelect; }
            set { SetPropertyValue(nameof(KpiRoleLineSelect), ref _kpiRoleLineSelect, value); }
        }

        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        public double Weight
        {
            get { return _weight; }
            set { SetPropertyValue(nameof(Weight), ref _weight, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }


        [ImmediatePostData]
        [Association("KpiRoleLine-KpiRoleCascadingLines")]
        [Appearance("KpiRoleLineDisable", Enabled = false)]
        public KpiRoleLine KpiRoleLine
        {
            get { return _kpiRoleLine; }
            set { 
                SetPropertyValue(nameof(KpiRoleLine), ref _kpiRoleLine, value); 
                if(!IsLoading)
                {
                    this.Company = this._kpiRoleLine.Company;
                    this.OrgDim1 = this._kpiRoleLine.OrgDim1;
                    this.OrgDim2 = this._kpiRoleLine.OrgDim2;
                    this.OrgDim3 = this._kpiRoleLine.OrgDim3;
                    this.OrgDim4 = this._kpiRoleLine.OrgDim4;
                }
            }
        }

        [Appearance("CompanyDisable", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1Disable", Enabled = false)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Departement")]
        [Appearance("OrganizationDimension2Disable", Enabled = false)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }


        [XafDisplayName("Sub Departement")]
        [Appearance("KpiRoleLineAssociation", Enabled = false)]
        [Appearance("OrganizationDimension3Disable", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [XafDisplayName("-")]
        [Appearance("OrganizationDimension4Disable", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

    }
}