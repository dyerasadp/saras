﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("KPI Setting")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("KpiRoleGradingRuleUnique", DefaultContexts.Save, "Code")]
    public class KpiRoleGrading : BaseObject
    {
        #region Initialization

        private int _no;
        private string _code;
        private bool _active;
        private double _valueGrading;
        private string _operationGrading;
        private double _scoreGrading;
        private KpiRoleLine _kpiRoleLine;
        private KpiRole _kpiRole;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private JobPosition _defaultPosition;
        private XPCollection<KpiRoleLine> _availableKpiRoleLine;
        private DateTime _docDate;


        #endregion Initialization


        public KpiRoleGrading(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.Active = true;
                this.Code = "KRG-";
            }
        }

        #region Override
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Override

        #region Field
        public int No
        {
            get { return _no; }
            set { SetPropertyValue(nameof(No), ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<KpiRoleLine> AvailableKpiRoleLine
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<KpiRoleLine> _locKpiRoleLines = new XPCollection<KpiRoleLine>(this.Session,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true)));
                    if (_locKpiRoleLines != null && _locKpiRoleLines.Count() > 0)
                    {
                        _availableKpiRoleLine = _locKpiRoleLines;
                    }
                }
                return _availableKpiRoleLine;
            }
        }

        [Association("KpiRoleLine-KpiRoleGradings")]
        public KpiRoleLine KpiRoleLine
        {
            get { return _kpiRoleLine; }
            set
            {
                SetPropertyValue(nameof(KpiRole), ref _kpiRoleLine, value);
                if (!IsLoading)
                {
                    if (_kpiRoleLine != null)
                    {
                        this.Company = _kpiRoleLine.Company;
                        this.OrgDim1 = _kpiRoleLine.OrgDim1;
                        this.OrgDim2 = _kpiRoleLine.OrgDim2;
                        this.OrgDim3 = _kpiRoleLine.OrgDim3;
                        this.OrgDim4 = _kpiRoleLine.OrgDim4;
                        this.DefaultPosition = _kpiRoleLine.DefaultPosition;
                    }
                    else
                    {
                        this.Company = null;
                        this.OrgDim1 = null;
                        this.OrgDim2 = null;
                        this.OrgDim3 = null;
                        this.OrgDim4 = null;
                        this.DefaultPosition = null;
                    }
                }
            }
        }
        public double ValueGrading
        {
            get { return _valueGrading; }
            set { SetPropertyValue(nameof(ValueGrading), ref _valueGrading, value); }
        }

        public string OperationGrading
        {
            get { return _operationGrading; }
            set { SetPropertyValue(nameof(OperationGrading), ref _operationGrading, value); }
        }

        public double ScoreGrading
        {
            get { return _scoreGrading; }
            set { SetPropertyValue(nameof(ScoreGrading), ref _scoreGrading, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Departement")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Departement")]
        [Appearance("OrganizationDimension3Close", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [XafDisplayName("-")]
        [Appearance("OrganizationDimension4Close", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        [ImmediatePostData]
        [XafDisplayName("Position")]
        [Appearance("DefaultPositionDefault", Enabled = false)]
        public JobPosition DefaultPosition
        {
            get { return _defaultPosition; }
            set { SetPropertyValue(nameof(DefaultPosition), ref _defaultPosition, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

        #region code dan numbering
        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.KpiRoleLine != null)
                    {
                        object _makRecord = Session.Evaluate<KpiRoleGrading>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("KpiRoleLine=?", this.KpiRoleLine));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = NumberingLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.KpiRoleLine != null)
                {
                    KpiRoleLine _numHeader = Session.FindObject<KpiRoleLine>
                                                (new BinaryOperator("Code", this.KpiRoleLine.Code));

                    XPCollection<KpiRoleGrading> _numLines = new XPCollection<KpiRoleGrading>
                                                (Session, new BinaryOperator("KpiRoleLine", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (KpiRoleGrading _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = NumberingLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.KpiRoleLine != null)
                {
                    KpiRoleLine _numHeader = Session.FindObject<KpiRoleLine>
                                                (new BinaryOperator("Code", this.KpiRoleLine.Code));

                    XPCollection<KpiRoleGrading> _numLines = new XPCollection<KpiRoleGrading>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("KpiRoleLine", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (KpiRoleGrading _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = KpiRoleGrading " + ex.ToString());
            }
        }


        #endregion code dan numbering 
    }
}