﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("NumVoyage")]
    [NavigationItem("Voyage Control")]
    [RuleCombinationOfPropertiesIsUnique("VoyageRuleUnique", DefaultContexts.Save, "Code")]

    public class Voyage : BaseObject
    {
        #region initialization

        private string _code;
        private string _numVoyage;
        private Vessel _vessel;
        private Vessel _vesselAssist;
        private DateTime _etd;
        private DateTime _etb;
        private Status _statusVoyage;
        private string _remarks;
        private StatusReadyList _hsseCheck;
        private bool _hsseVerif;
        private StatusReadyList _oprCheck;
        private bool _oprVerif;
        private StatusReadyList _crewCheck;
        private bool _crewVerif;
        private StatusReadyList _oprSuppCheck;
        private bool _oprSuppVerif;
        private StatusReadyList _purchasingCheck;
        private bool _purchasingVerif;
        private DateTime _docDate;
        private DateTime _progressDate;
        private DateTime _closeDate;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public Voyage(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.DocDate = DateTime.Now;
                this.StatusVoyage = Status.Open;
            }

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Voyage);
            #endregion Numbering
        }

        #region Field

        [VisibleInListView(false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string NumVoyage
        {
            get { return _numVoyage; }
            set { SetPropertyValue(nameof(NumVoyage), ref _numVoyage, value); }
        }

        [Association("Vessel-Voyages")]
        public Vessel Vessel
        {
            get { return _vessel; }
            set { SetPropertyValue(nameof(Vessel), ref _vessel, value); }
        }

        public Vessel VesselAssist
        {
            get { return _vesselAssist; }
            set
            { SetPropertyValue(nameof(VesselAssist), ref _vesselAssist, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime Etd
        {
            get { return _etd; }
            set { SetPropertyValue(nameof(Etd), ref _etd, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime Etb
        {
            get { return _etb; }
            set { SetPropertyValue(nameof(Etb), ref _etb, value); }
        }

        [Appearance("StatusVoyageDisable", Enabled = false)]
        public Status StatusVoyage
        {
            get { return _statusVoyage; }
            set { SetPropertyValue(nameof(StatusVoyage), ref _statusVoyage, value); }
        }

        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue(nameof(Remarks), ref _remarks, value); }
        }

        [VisibleInListView(false)]
        [Appearance("HsseCheckDisable", Enabled = false)]
        public StatusReadyList HsseCheck
        {
            get { return _hsseCheck; }
            set { SetPropertyValue(nameof(HsseCheck), ref _hsseCheck, value); }
        }

        [VisibleInListView(false)]
        public bool HsseVerif
        {
            get { return _hsseVerif; }
            set { SetPropertyValue(nameof(HsseVerif), ref _hsseVerif, value); }
        }

        [VisibleInListView(false)]
        [Appearance("OprCheckDisable", Enabled = false)]
        public StatusReadyList OprCheck
        {
            get { return _oprCheck; }
            set { SetPropertyValue(nameof(OprCheck), ref _oprCheck, value); }
        }

        [VisibleInListView(false)]
        public bool OprVerif
        {
            get { return _oprVerif; }
            set { SetPropertyValue(nameof(OprVerif), ref _oprVerif, value); }
        }

        [VisibleInListView(false)]
        [Appearance("OprSuppCheckDisable", Enabled = false)]
        public StatusReadyList OprSuppCheck
        {
            get { return _oprSuppCheck; }
            set { SetPropertyValue(nameof(OprSuppCheck), ref _oprSuppCheck, value); }
        }

        [VisibleInListView(false)]
        public bool OprSuppVerif
        {
            get { return _oprSuppVerif; }
            set { SetPropertyValue(nameof(OprSuppVerif), ref _oprSuppVerif, value); }
        }

        [VisibleInListView(false)]
        [Appearance("CrewCheckDisable", Enabled = false)]
        public StatusReadyList CrewCheck
        {
            get { return _crewCheck; }
            set { SetPropertyValue(nameof(CrewCheck), ref _crewCheck, value); }
        }

        [VisibleInListView(false)]
        public bool CrewVerif
        {
            get { return _crewVerif; }
            set { SetPropertyValue(nameof(CrewVerif), ref _crewVerif, value); }
        }

        [VisibleInListView(false)]
        [Appearance("PurchasingCheckDisable", Enabled = false)]
        public StatusReadyList PurchasingCheck
        {
            get { return _purchasingCheck; }
            set { SetPropertyValue(nameof(PurchasingCheck), ref _purchasingCheck, value); }
        }

        [VisibleInListView(false)]
        public bool PurchasingVerif
        {
            get { return _purchasingVerif; }
            set { SetPropertyValue(nameof(PurchasingVerif), ref _purchasingVerif, value); }
        }

        [VisibleInListView(false)]
        [Appearance("DocDateDisable", Enabled = false)]
        [XafDisplayName("Created")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("ProgressDateDisable", Enabled = false)]
        [XafDisplayName("On Progress")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime ProgressDate
        {
            get { return _progressDate; }
            set { SetPropertyValue(nameof(ProgressDate), ref _progressDate, value); }
        }

        [VisibleInListView(false)]
        [Appearance("CloseDateDisable", Enabled = false)]
        [XafDisplayName("Closed")]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy HH:mm:ss}")]
        [ModelDefault("EditMask", "dd/MM/yyyy HH:mm:ss")]
        public DateTime CloseDate
        {
            get { return _closeDate; }
            set { SetPropertyValue(nameof(CloseDate), ref _closeDate, value); }
        }

        #endregion Field

        #region Manny

        [Association("Voyage-VoyageRoutes")]
        public XPCollection<VoyageRoute> VoyageRoutes
        {
            get { return GetCollection<VoyageRoute>(nameof(VoyageRoutes)); }
        }

        [Association("Voyage-VoyageCheckCrews")]
        public XPCollection<VoyageCheckCrew> CheckCrews
        {
            get { return GetCollection<VoyageCheckCrew>(nameof(CheckCrews)); }
        }

        [Association("Voyage-VoyageCheckHsses")]
        public XPCollection<VoyageCheckHsse> CheckHsse
        {
            get { return GetCollection<VoyageCheckHsse>(nameof(CheckHsse)); }
        }

        #endregion Manny
    }
}