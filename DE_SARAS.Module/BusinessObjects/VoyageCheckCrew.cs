﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Crew")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VoyageCheckCrewRuleUnique", DefaultContexts.Save, "Code")]

    public class VoyageCheckCrew : BaseObject
    {
        #region initialization

        private string _code;
        private Voyage _voyage;
        private Crew _crew;
        private JobPosition _jabatan;
        private CrewClassList _cocSafemanning;
        private CrewClassList _cocActual;
        private DateTime _seamanbookExpired;
        private DocumentVesselStatus _semanbookStatus;
        private StatusPerformance _statusCrew;

        #endregion Initialization

        public VoyageCheckCrew(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Association("Voyage-VoyageCheckCrews")]
        public Voyage Voyage
        {
            get { return _voyage; }
            set { SetPropertyValue(nameof(Voyage), ref _voyage, value); }
        }

        public Crew Crew
        {
            get { return _crew; }
            set { SetPropertyValue(nameof(Crew), ref _crew, value); }
        }

        public JobPosition Jabatan
        {
            get { return _jabatan; }
            set { SetPropertyValue(nameof(Jabatan), ref _jabatan, value); }
        }

        public CrewClassList CocSafemanning
        {
            get { return _cocSafemanning; }
            set { SetPropertyValue(nameof(CocSafemanning), ref _cocSafemanning, value); }
        }

        public CrewClassList CocActual
        {
            get { return _cocActual; }
            set { SetPropertyValue(nameof(CocActual), ref _cocActual, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime SeamanbookExpired
        {
            get { return _seamanbookExpired; }
            set { SetPropertyValue(nameof(SeamanbookExpired), ref _seamanbookExpired, value); }
        }

        public DocumentVesselStatus SemanbookStatus
        {
            get { return _semanbookStatus; }
            set { SetPropertyValue(nameof(SemanbookStatus), ref _semanbookStatus, value); }
        }

        public StatusPerformance StatusCrew
        {
            get { return _statusCrew; }
            set { SetPropertyValue(nameof(StatusCrew), ref _statusCrew, value); }
        }

        #endregion Field
    }
}