﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.XtraRichEdit.Import.Html;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("KPI Setting")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("KpiRoleLineRuleUnique", DefaultContexts.Save, "Code")]

    public class KpiRoleLine : BaseObject
    {
        #region Initialization

        private string _code;
        private string _name;
        private double _weight;
        private double _proportion;
        private double _ytdtarget;
        private KpiPeriod _kpiPeriod;
        private KpiFormat _kpiFormat;
        private double _kpiTarget;
        private KpiValue _kpiValue;
        private KpiGroupType _kpiGroupType;
        private KpiType _kpiType;
        private Employee _employee;
        private KpiYtdType _kpiYtdType;
        private KpiBscType _kpiBscType;
        private KpiYear _kpiYear;
        private DateTime _starDateKpi;
        private DateTime _endDateKpi;
        private KpiRole _kpiRole;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private JobPosition _defaultPosition;
        private XPCollection<KpiRole> _availableKpiRole;
        private bool _active;
        private DateTime _docDate;
        private GlobalFunction _globFunc;


        #endregion Initialization
        public KpiRoleLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.DocDate = DateTime.Now; 
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.KpiRoleLine);
                this.KpiType = KpiType.All;
                KpiRole _locKpiRoleXPO = Session.FindObject<KpiRole>
                                 (new GroupOperator(GroupOperatorType.And,
                                  new BinaryOperator("Active", true)));
                if (_locKpiRoleXPO != null)
                {
                    this.Ytdtarget = _locKpiRoleXPO.Ytdtarget;
                }
            }
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public double Weight
        {
            get { return _weight; }
            set { 
                SetPropertyValue(nameof(Weight), ref _weight, value);
                if (!IsLoading)
                {
                    HitungProportion();
                }
            }
        }
        public double Ytdtarget
        {
            get { return _ytdtarget; }
            set { 
                SetPropertyValue(nameof(Ytdtarget), ref _ytdtarget, value);
                if (!IsLoading)
                {
                    HitungProportion();
                }
            }
        }

        [Appearance("ProportionKpiRoleLineDisabled", Enabled = false)]
        public double Proportion
        {
            get { return _proportion; }
            set { SetPropertyValue(nameof(Proportion), ref _proportion, value); }
        }

        public KpiPeriod KpiPeriod
        {
            get { return _kpiPeriod; }
            set { SetPropertyValue(nameof(KpiPeriod), ref _kpiPeriod, value); }
        }
        public KpiFormat KpiFormat
        {
            get { return _kpiFormat; }
            set { SetPropertyValue(nameof(KpiFormat), ref _kpiFormat, value); }
        }

        public double KpiTarget
        {
            get { return _kpiTarget; }
            set { SetPropertyValue(nameof(KpiTarget), ref _kpiTarget, value); }
        }

        public KpiValue KpiValue
        {
            get { return _kpiValue; }
            set { SetPropertyValue(nameof(KpiValue), ref _kpiValue, value); }
        }

        public KpiGroupType KpiGroupType
        {
            get { return _kpiGroupType; }
            set { SetPropertyValue(nameof(KpiGroupType), ref _kpiGroupType, value); }
        }

        public KpiYtdType KpiYtdType
        {
            get { return _kpiYtdType; }
            set { SetPropertyValue(nameof(KpiYtdType), ref _kpiYtdType, value); }
        }

        public KpiBscType KpiBscType
        {
            get { return _kpiBscType; }
            set { SetPropertyValue(nameof(KpiBscType), ref _kpiBscType, value); }
        }

        public KpiType KpiType
        {
            get { return _kpiType; }
            set { SetPropertyValue(nameof(KpiType), ref _kpiType, value); }
        }

        [Appearance("EmployeeDisabled", Criteria = "KpiType = 2", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        public KpiYear KpiYear
        {
            get { return _kpiYear; }
            set { SetPropertyValue(nameof(KpiYear), ref _kpiYear, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public DateTime StarDateKpi
        {
            get { return _starDateKpi; }
            set { SetPropertyValue(nameof(StarDateKpi), ref _starDateKpi, value); }
        }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public DateTime EndDateKpi
        {
            get { return _endDateKpi; }
            set { SetPropertyValue(nameof(EndDateKpi), ref _endDateKpi, value); }
        }


        [Browsable(false)]
        public XPCollection<KpiRole> AvailableKpiRole
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<KpiRole> _locKpiRoles = new XPCollection<KpiRole>(this.Session,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true)));
                    if (_locKpiRoles != null && _locKpiRoles.Count() > 0)
                    {
                        _availableKpiRole = _availableKpiRole;
                    }
                }
                return _availableKpiRole;
            }
        }

        [Association("KpiRole-KpiRoleLines")]
        public KpiRole KpiRole
        {
            get { return _kpiRole; }
            set { 
                SetPropertyValue(nameof(KpiRole), ref _kpiRole, value);
                if (!IsLoading)
                {
                    if (_kpiRole != null)
                    {
                        this.Company = _kpiRole.Company;
                        this.OrgDim1 = _kpiRole.OrgDim1;
                        this.OrgDim2 = _kpiRole.OrgDim2;
                        this.OrgDim3 = _kpiRole.OrgDim3;
                        this.OrgDim4 = _kpiRole.OrgDim4;
                        this.DefaultPosition = _kpiRole.DefaultPosition;

                    }
                    else
                    {
                        this.Company = null;
                        this.OrgDim1 = null;
                        this.OrgDim2 = null;
                        this.OrgDim3 = null;
                        this.OrgDim4 = null;
                        this.DefaultPosition = null;
                    }
                }
            }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Workplace")]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Departement")]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }


        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Sub Departement")]
        [Appearance("OrganizationDimension3Close", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [XafDisplayName("-")]
        [Appearance("OrganizationDimension4Close", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }


        [ImmediatePostData]
        [XafDisplayName("Position")]
        [Appearance("DefaultPositionDefault", Enabled = false)]
        public JobPosition DefaultPosition
        {
            get { return _defaultPosition; }
            set { SetPropertyValue(nameof(DefaultPosition), ref _defaultPosition, value); }
        }


        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        public DateTime DocDate
        {
            get { return _docDate; }
            set { SetPropertyValue(nameof(DocDate), ref _docDate, value); }
        }

        #endregion Field

        #region Manny

        [Association("KpiRoleLine-KpiRoleGradings")]
        public XPCollection<KpiRoleGrading> KpiRoleGradings
        {
            get { return GetCollection<KpiRoleGrading>(nameof(KpiRoleGradings)); }
        }

        [Association("KpiRoleLine-KpiRoleCascadingLines")]
        public XPCollection<KpiRoleCascadingLine> KpiRoleCascadingLines
        {
            get { return GetCollection<KpiRoleCascadingLine>(nameof(KpiRoleCascadingLines)); }
        }

        #endregion Manny 


        #region Code 
        private void HitungProportion()
        {
            if (this.Ytdtarget != null)
            {
                this.Proportion = (this.Ytdtarget /100) * this.Weight;
            }
        }

        #endregion Code

    }
}