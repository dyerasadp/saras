﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Quotation")]
    [DefaultProperty("Name")]
    [RuleCombinationOfPropertiesIsUnique("QuotationPeriodeRuleUnique", DefaultContexts.Save, "Code")]
    public class QuotationPeriode : BaseObject
    {
        #region Initialization

        private string _code;
        private string _name;
        private DateTime _quotationPeriodStartDate;
        private DateTime _quotationPeriodEndDate;
        private Employee _employee;
        private Company _company;
        private GlobalFunction _globFunc;
        private XPCollection<Company> _availableCompany;


        #endregion Initialization

        public QuotationPeriode(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }        
        
        public DateTime StartDate
        {
            get { return _quotationPeriodStartDate; }
            set { SetPropertyValue(nameof(StartDate), ref _quotationPeriodStartDate, value); }
        }

        public DateTime EndDate
        {
            get { return _quotationPeriodEndDate; }
            set { SetPropertyValue(nameof(EndDate), ref _quotationPeriodEndDate, value); }
        }

        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }        
        
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        #endregion Field

        #region Manny 


        [XafDisplayName("Pertamina")]
        [Association("QuotationPeriode-QuotationPeriodePertaminaLine")]
        public XPCollection<QuotationPeriodePertaminaLine> QuotationPeriodePertaminaLines
        {
            get { return GetCollection<QuotationPeriodePertaminaLine>(nameof(QuotationPeriodePertaminaLines)); }
        }

        [XafDisplayName("Mops")]
        [Association("QuotationPeriode-QuotationPeriodeMopsLines")]
        public XPCollection<QuotationPeriodeMopsLine> QuotationPeriodeMopsLines
        {
            get { return GetCollection<QuotationPeriodeMopsLine>(nameof(QuotationPeriodeMopsLines)); }
        }

        #endregion Manny

    }
}