﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Kpi Submit")]
    [DefaultProperty("KpiRoleCascadingLine")]
    [RuleCombinationOfPropertiesIsUnique("KpiSubmitLineCascadingRuleUnique", DefaultContexts.Save, "Code")]

    public class KpiSubmitLineCascading : BaseObject
    {
        #region Initialization

        private string _code;
        //private string _name;
        private KpiRoleCascadingLine _kpiRoleCascadingLine;
        private Employee _employee;
        private KpiSubmitLine _kpiSubmitLine;
        private double _weight;
        private double _pencapaian;
        private double _score;
        private KpiYear _kpiYear;
        private Month _monthKpi;
        private Status _status;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private XPCollection<KpiRoleLine> _availableKpiRoleLine;
        private bool _active;


        #endregion Initialization


        public KpiSubmitLineCascading(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.Status = Status.Open;
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public KpiRoleCascadingLine KpiRoleCascadingLine
        {
            get { return _kpiRoleCascadingLine; }
            set { 
                SetPropertyValue(nameof(KpiRoleCascadingLine), ref _kpiRoleCascadingLine, value);
            }
        }


        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }


        public double Weight
        {
            get { return _weight; }
            set { SetPropertyValue(nameof(Weight), ref _weight, value); }
        }

        public double Pencapaian
        {
            get { return _pencapaian; }
            set { SetPropertyValue(nameof(Pencapaian), ref _pencapaian, value); }
        }

        public double Score
        {
            get { return _score; }
            set { SetPropertyValue(nameof(Score), ref _score, value); }
        }

        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue(nameof(Status), ref _status, value); }
        }

        [ImmediatePostData]
        [Association("KpiSubmitLine-KpiSubmitLineCascadings")]
        [Appearance("KpiRoleLineDisable", Enabled = false)]
        public KpiSubmitLine KpiSubmitLine
        {
            get { return _kpiSubmitLine; }
            set
            {
                SetPropertyValue(nameof(KpiSubmitLine), ref _kpiSubmitLine, value);
                if (!IsLoading)
                {
                    this.KpiYear = this._kpiSubmitLine.KpiYear;
                    this.MonthKpi = this._kpiSubmitLine.MonthKpi;
                    this.Company = this._kpiSubmitLine.Company;
                    this.OrgDim1 = this._kpiSubmitLine.OrgDim1;
                    this.OrgDim2 = this._kpiSubmitLine.OrgDim2;
                    this.OrgDim3 = this._kpiSubmitLine.OrgDim3;
                    this.OrgDim4 = this._kpiSubmitLine.OrgDim4;
                }
            }
        }

        [Appearance("KpiYearSubmitLineCascadingClose", Enabled = false)]
        public KpiYear KpiYear
        {
            get { return _kpiYear; }
            set { SetPropertyValue(nameof(KpiYear), ref _kpiYear, value); }
        }


        [Appearance("MonthKpiSubmitLineCascadingClose", Enabled = false)]
        public Month MonthKpi
        {
            get { return _monthKpi; }
            set { SetPropertyValue(nameof(MonthKpi), ref _monthKpi, value); }
        }


        [Appearance("CompanyDisable", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1Disable", Enabled = false)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Departement")]
        [Appearance("OrganizationDimension2Disable", Enabled = false)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }


        [XafDisplayName("Sub Departement")]
        [Appearance("KpiRoleLineAssociation", Enabled = false)]
        [Appearance("OrganizationDimension3Disable", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [XafDisplayName("-")]
        [Appearance("OrganizationDimension4Disable", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        #endregion Field
    }
}