﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Organization")]
    [RuleCombinationOfPropertiesIsUnique("CallCenterFeedbackAdminRuleUnique", DefaultContexts.Save, "Code")]

    public class CallCenterFeedbackAdmin : BaseObject
    {
        #region Initialization

        private string _code;
        private OrganizationDimension2 _orgDim2;
        private bool _activeIT;
        private bool _activeNonIT;
        private string _admin;
        private string _user;
        private CallCenter _callCenter;
        private CallCenterMonitoring _callCenterMonitoring;
        private Employee _employee;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public CallCenterFeedbackAdmin(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Field

        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [Appearance("CodeCallCenterFeedbackClosed", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [ImmediatePostData]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Association("CallCenterMonitoring-CallCenterFeedbackAdmins")]
        public CallCenterMonitoring CallCenterMonitoring
        {
            get { return _callCenterMonitoring; }
            set
            {
                SetPropertyValue(nameof(CallCenterMonitoring), ref _callCenterMonitoring, value);
                if (!IsLoading)
                {
                    if (_callCenterMonitoring != null)
                    {
                        this.CallCenter = _callCenterMonitoring.CallCenter;
                        this.PIC = _callCenterMonitoring.PicCallCenter;
                    }
                }
            }
        }

        [ImmediatePostData]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public Employee PIC
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(PIC), ref _employee, value);
                if (!IsLoading)
                {
                    if (_employee != null)
                    {
                        this.OrgDim2 = _employee.OrganizationDimension2;
                    }
                }
            }
        }

        [ImmediatePostData]
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        public CallCenter CallCenter
        {
            get { return _callCenter; }
            set { SetPropertyValue(nameof(CallCenter), ref _callCenter, value); }
        }

        [ImmediatePostData()]
        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [Appearance("OrganizationDimension2CallCenterFeedbackClosed", Enabled = false)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set
            {
                SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value);
                if (!IsLoading)
                {
                    if (_orgDim2 != null)
                    {
                        if (_orgDim2.Name == "IT")
                        {
                            this.ActiveIT = true;
                        }
                        else if (_orgDim2.Name == "MEP")
                        {
                            this.ActiveIT = true;
                        }
                        else
                        {
                            this.ActiveNonIT = true;
                        }
                    }
                }
            }
        }

        [ImmediatePostData]
        [Browsable(false)]
        public bool ActiveIT
        {
            get { return _activeIT; }
            set { SetPropertyValue(nameof(ActiveIT), ref _activeIT, value); }
        }

        [ImmediatePostData]
        [Browsable(false)]
        public bool ActiveNonIT
        {
            get { return _activeNonIT; }
            set { SetPropertyValue(nameof(ActiveNonIT), ref _activeNonIT, value); }
        }

        [ImmediatePostData]
        [Appearance("AdminCallCenterFeedbackAdminDisable", Criteria = "ActiveNonIT = 'False'", Enabled = true)]
        [Appearance("AdminCallCenterFeedbackAdminEnable", Criteria = "ActiveNonIT = 'True'", Enabled = false)]
        public string Admin
        {
            get { return _admin; }
            set { SetPropertyValue(nameof(Admin), ref _admin, value); 
                if(!IsLoading) { SendFeedback(); }
            }
        }

        [ImmediatePostData]
        [Appearance("UserCallCenterFeedbackAdminDisable", Criteria = "ActiveIT = 'True'", Enabled = false)]
        [Appearance("UserCallCenterFeedbackAdminEnable", Criteria = "ActiveIT = 'False'", Enabled = true)]
        public string User
        {
            get { return _user; }
            set { SetPropertyValue(nameof(User), ref _user, value); }
        }

        #endregion Field


        #region Code 
        private void SendFeedback()
        {
            if (this.Admin != null) {
                CallCenterFeedbackUser _availableFeedBackUser = Session.FindObject<CallCenterFeedbackUser>
                                                                        (new BinaryOperator("CallCenter", this.CallCenter));

                if(_availableFeedBackUser != null ) 
                {
                    CallCenterFeedbackUser _sendFeedBackAdmin = new CallCenterFeedbackUser(Session)
                    {
                        CallCenter = this.CallCenter,
                        Admin = this.Admin,
                    };
                    _sendFeedBackAdmin.Save();
                    _sendFeedBackAdmin.Session.CommitTransaction();
                }
                else
                {
                    CallCenterFeedbackUser _sendNewFeedBackAdmin = new CallCenterFeedbackUser(Session)
                    {
                        CallCenter = this.CallCenter,
                        Admin = this.Admin,
                    };
                    _sendNewFeedBackAdmin.Save();
                    _sendNewFeedBackAdmin.Session.CommitTransaction();

                }
            }
            
        }

        #endregion Code

    }
}