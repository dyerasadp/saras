﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Task Base")]
    [RuleCombinationOfPropertiesIsUnique("CallCenterMonitoringRuleUnique", DefaultContexts.Save, "Code")]

    public class CallCenterMonitoring : BaseObject
    {
        #region Initialization

        private bool _activationPosting;
        private string _code;
        private CallCenter _callCenter;
        private TicketHelpType _helpType;
        private TicketTopicTypeLine _topicType;
        private Employee _requestCallCenter;
        private Employee _picCallCenter;
        private StatusTicket _statusTicket;
        private DateTime _progressDate;
        private DateTime _closedDate;
        private TimeSpan _interval;
        private DateTime _currDate;
        private string _remark;
        private string _ticketSubject;
        private string _ticketDescription;

        #region InitialOrganization        
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        #endregion InitialOrganization    

        private string _localUserAccess;
        private GlobalFunction _globFunc;
        private XPCollection<Employee> _availableEmployee;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization

        public CallCenterMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this.ActivationPosting = false;
                _currDate = DateTime.Now.AddMinutes(1);
                this.ProgressDate = DateTime.Now;
                this.Interval = (_currDate - this.ProgressDate);

                #region UserAccess
                _globFunc = new GlobalFunction();
                _localUserAccess = SecuritySystem.CurrentUserName;

                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));

                Company _locCompany = null;
                OrganizationDimension1 _locOrgDim1 = null;
                OrganizationDimension2 _locOrgDim2 = null;
                OrganizationDimension3 _locOrgDim3 = null;
                OrganizationDimension4 _locOrgDim4 = null;
                Employee _locEmployee = null;

                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        _locEmployee = _locUserAccess.Employee;
                        if (_locUserAccess.Employee.Company != null) { _locCompany = _locUserAccess.Employee.Company; }
                        if (_locUserAccess.Employee.OrganizationDimension1 != null) { _locOrgDim1 = _locUserAccess.Employee.OrganizationDimension1; }
                        if (_locUserAccess.Employee.OrganizationDimension2 != null) { _locOrgDim2 = _locUserAccess.Employee.OrganizationDimension2; }
                        if (_locUserAccess.Employee.OrganizationDimension3 != null) { _locOrgDim3 = _locUserAccess.Employee.OrganizationDimension3; }
                        if (_locUserAccess.Employee.OrganizationDimension4 != null) { _locOrgDim4 = _locUserAccess.Employee.OrganizationDimension4; }
                    }
                    if (this.Session != null)
                    {
                        this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CallCenterMonitoring);
                    }


                    this.Company = _locCompany;
                    this.OrgDim1 = _locOrgDim1;
                    this.OrgDim2 = _locOrgDim2;
                    this.OrgDim3 = _locOrgDim3;
                    this.OrgDim4 = _locOrgDim4;
                    this.PicCallCenter = _locEmployee;

                }
                #endregion UserAccess
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue(nameof(ActivationPosting), ref _activationPosting, value); }
        }

        [Appearance("CodeClosed", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Appearance("CallCenterClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public CallCenter CallCenter
        {
            get { return _callCenter; }
            set { SetPropertyValue(nameof(CallCenter), ref _callCenter, value); }
        }

        [Appearance("HelpTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public TicketHelpType HelpType
        {
            get { return _helpType; }
            set { SetPropertyValue(nameof(HelpType), ref _helpType, value); }
        }

        [Appearance("TopicTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("HelpType = '@This.HelpType' And Active = 'true'")]
        public TicketTopicTypeLine TopicType
        {
            get { return _topicType; }
            set { SetPropertyValue(nameof(TopicType), ref _topicType, value); }
        }

        #region Organization

        [Browsable(false)]
        public XPCollection<Employee> AvailableEmployee
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<Employee> _locEmployees = new XPCollection<Employee>(this.Session,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Active", true)));
                    if (_locEmployees != null && _locEmployees.Count() > 0)
                    {
                        _availableEmployee = _locEmployees;
                    }
                }
                return _availableEmployee;
            }
        }

        [ImmediatePostData()]
        [XafDisplayName("Request by")]
        [Appearance("EmployeeClosed", Enabled = false)]
        [DataSourceProperty("AvailableEmployee", DataSourcePropertyIsNullMode.SelectNothing)]

        public Employee RequestCallCenter
        {
            get { return _requestCallCenter; }
            set
            {
                SetPropertyValue(nameof(RequestCallCenter), ref _requestCallCenter, value);
                if (!IsLoading)
                {
                    if (_requestCallCenter != null)
                    {
                        this.Company = _requestCallCenter.Company;
                        this.OrgDim1 = _requestCallCenter.OrganizationDimension1;
                        this.OrgDim2 = _requestCallCenter.OrganizationDimension2;
                        this.OrgDim3 = _requestCallCenter.OrganizationDimension3;
                        this.OrgDim4 = _requestCallCenter.OrganizationDimension4;
                    }
                }
                else
                {
                    this.Company = null;
                    this.OrgDim1 = null;
                    this.OrgDim2 = null;
                    this.OrgDim3 = null;
                    this.OrgDim4 = null;

                }
            }
        }

        [ImmediatePostData()]
        [Appearance("CompanyCallCenterClosed", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1CallCenterClosed", Enabled = false)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Dept")]
        [Appearance("OrganizationDimension2CallCenterClosed", Enabled = false)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Dept")]
        [Appearance("OrganizationDimension3CallCenterClosed", Enabled = false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Section")]
        [Appearance("OrganizationDimension4CallCenterClosed", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        #endregion Organization

        [XafDisplayName("PIC")]
        [Appearance("PicCallCenterMonClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee PicCallCenter
        {
            get { return _picCallCenter; }
            set { SetPropertyValue(nameof(PicCallCenter), ref _picCallCenter, value); }
        }

        [Appearance("StatusTicketMonClosed", Enabled = false)]
        public StatusTicket StatusTicket
        {
            get { return _statusTicket; }
            set { SetPropertyValue(nameof(StatusTicket), ref _statusTicket, value); }
        }

        [Appearance("ProgressDateMonClosed", Enabled = false)]
        public DateTime ProgressDate
        {
            get { return _progressDate; }
            set { SetPropertyValue(nameof(ProgressDate), ref _progressDate, value); }
        }

        [Appearance("ClosedDateMonClosed", Enabled = false)]
        public DateTime ClosedDate
        {
            get { return _closedDate; }
            set { SetPropertyValue(nameof(ClosedDate), ref _closedDate, value); }
        }

        [Appearance("IntervalMonClosed", Enabled = false)]
        public TimeSpan Interval
        {
            get { return _interval; }
            set { SetPropertyValue(nameof(Interval), ref _interval, value); }
        }

        [Appearance("RemarkClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Remark
        {
            get { return _remark; }
            set { SetPropertyValue(nameof(Remark), ref _remark, value); }
        }

        [Appearance("TicketSubjectTbClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TicketSubject
        {
            get { return _ticketSubject; }
            set { SetPropertyValue(nameof(TicketSubject), ref _ticketSubject, value); }
        }

        [Appearance("TicketDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(SizeAttribute.Unlimited)]
        [EditorAlias(EditorAliases.RichTextPropertyEditor)]
        public string TicketDescription
        {
            get { return _ticketDescription; }
            set { SetPropertyValue(nameof(TicketDescription), ref _ticketDescription, value); }
        }
        #endregion Field


        #region Manny

        [Association("CallCenterMonitoring-CallCenterFeedbackAdmins")]
        public XPCollection<CallCenterFeedbackAdmin> CallCenterFeedbackAdmins
        {
            get { return GetCollection<CallCenterFeedbackAdmin>(nameof(CallCenterFeedbackAdmins)); }
        }


        #endregion Manny
    }
}