﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Kpi Submit")]
    [DefaultProperty("KpiSubmitLine")]
    [RuleCombinationOfPropertiesIsUnique("PicaRuleUnique", DefaultContexts.Save, "Code")]

    public class Pica : BaseObject
    {
        #region Initialization

        private string _code;
        private KpiSubmitLine _kpiSubmitLine;
        private string _problemIdentification;
        private string _corectiveAction;
        private Employee _picPica;
        private DateTime _dueDatePica;
        private FileData _attachment;
        private Status _statusPica;
        private Company _company;
        private JobPosition _jobPosition;
        private Employee _employee;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private XPCollection<KpiSubmitLine> _availableKpiSubmit;
        private GlobalFunction _globFunc;
        private string _localUserAccess;


        #endregion Initialization
        public Pica(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                this.StatusPica = Status.Open;
                _globFunc = new GlobalFunction();
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));

                Company _locCompany = null;
                OrganizationDimension1 _locOrgDim1 = null;
                OrganizationDimension2 _locOrgDim2 = null;
                OrganizationDimension3 _locOrgDim3 = null;
                OrganizationDimension4 _locOrgDim4 = null;
                Employee _locEmployee = null;

                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        _locEmployee = _locUserAccess.Employee;
                        if (_locUserAccess.Employee.Company != null) { _locCompany = _locUserAccess.Employee.Company; }
                        if (_locUserAccess.Employee.OrganizationDimension1 != null) { _locOrgDim1 = _locUserAccess.Employee.OrganizationDimension1; }
                        if (_locUserAccess.Employee.OrganizationDimension2 != null) { _locOrgDim2 = _locUserAccess.Employee.OrganizationDimension2; }
                        if (_locUserAccess.Employee.OrganizationDimension3 != null) { _locOrgDim3 = _locUserAccess.Employee.OrganizationDimension3; }
                        if (_locUserAccess.Employee.OrganizationDimension4 != null) { _locOrgDim4 = _locUserAccess.Employee.OrganizationDimension4; }
                    }
                    //if (this.Session != null)
                    //{
                    //    this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BehaviourMonitoring);
                    //}

                    this.Company = _locCompany;
                    this.OrgDim1 = _locOrgDim1;
                    this.OrgDim2 = _locOrgDim2;
                    this.OrgDim3 = _locOrgDim3;
                    this.OrgDim4 = _locOrgDim4;
                    this.Employee = _locEmployee;
                }
            }
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<KpiSubmitLine> AvailableKpiSubmitLine
        {
            get
            {
                if (!IsLoading)
                {
                    XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>(this.Session);
                    if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                    {
                        _availableKpiSubmit = _locKpiSubmitLines;
                    }
                }
                return _availableKpiSubmit;
            }
        }

        [Association("KpiSubmitLine-Picas")]
        [XafDisplayName("Item Kpi")]
        public KpiSubmitLine KpiSubmitLine
        {
            get { return _kpiSubmitLine; }
            set { 
                SetPropertyValue(nameof(KpiSubmitLine), ref _kpiSubmitLine, value);
                if (!IsLoading)
                {
                    if (_kpiSubmitLine != null)
                    {
                        this.JobPosition = _kpiSubmitLine.DefaultPosition;
                        this.Employee = _kpiSubmitLine.Employee;
                        this.Company = _kpiSubmitLine.Company;
                        this.OrgDim1 = _kpiSubmitLine.OrgDim1;
                        this.OrgDim2 = _kpiSubmitLine.OrgDim2;
                        this.OrgDim3 = _kpiSubmitLine.OrgDim3;
                        this.OrgDim4 = _kpiSubmitLine.OrgDim4;
                    }
                }
                else
                {
                    this.JobPosition = null;
                    this.Employee = null;
                    this.Company = null;
                    this.OrgDim1 = null;
                    this.OrgDim2 = null;
                    this.OrgDim3 = null;
                    this.OrgDim4 = null;
                }
            }
        }

        public string ProblemIdentification
        {
            get { return _problemIdentification; }
            set { SetPropertyValue(nameof(ProblemIdentification), ref _problemIdentification, value); }
        }        
        
        public string CorectiveAction
        {
            get { return _corectiveAction; }
            set { SetPropertyValue(nameof(CorectiveAction), ref _corectiveAction, value); }
        }

        public Employee PicPica
        {
            get { return _picPica; }
            set { SetPropertyValue(nameof(PicPica), ref _picPica, value); }
        }

        public DateTime DueDatePica
        {
            get { return _dueDatePica; }
            set { SetPropertyValue(nameof(DueDatePica), ref _dueDatePica, value); }
        }

        public FileData Attachment
        {
            get { return _attachment; }
            set { SetPropertyValue(nameof(Attachment), ref _attachment, value); }
        }

        public Status StatusPica
        {
            get { return _statusPica; }
            set { SetPropertyValue(nameof(StatusPica), ref _statusPica, value); }
        }

        [Appearance("JobPositionKpiSubmitSubmit", Enabled = false)]
        public JobPosition JobPosition
        {
            get { return _jobPosition; }
            set { SetPropertyValue(nameof(JobPosition), ref _jobPosition, value); }
        }

        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        [ImmediatePostData()]
        [Appearance("CompanyKpiSubmitSubmit", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Departement")]
        [Appearance("OrganizationDimension1KpiSubmitSubmit", Enabled = false)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Sub Dept")]
        [Appearance("OrganizationDimension2KpiSubmitSubmit", Enabled = true)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("-")]
        [Appearance("OrganizationDimension3Default", Enabled = false)]
        [Appearance("OrganizationDimension3KpiSubmitSubmit", Enabled = false)]
        [VisibleInListView(false)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [XafDisplayName("-")]
        [Appearance("DefaultPositionDefault", Enabled = false)]
        [Appearance("OrganizationDimension4KpiSubmitSubmit", Enabled = false)]
        [VisibleInListView(false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        #endregion Field

    }
}