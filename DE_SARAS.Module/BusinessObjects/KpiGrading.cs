﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Grading")]
    [DefaultProperty("Employee")]
    [RuleCombinationOfPropertiesIsUnique("KpiGradingRuleUnique", DefaultContexts.Save, "Code")]

    public class KpiGrading : BaseObject
    {
        #region Initialization

        private string _code;
        private Employee _employee;
        private double _totNilaiAkhir;
        private string _finalResult;
        private string _finalPredicate;
        private Company _company;
        private OrganizationDimension1 _orgDim1;
        private OrganizationDimension2 _orgDim2;
        private OrganizationDimension3 _orgDim3;
        private OrganizationDimension4 _orgDim4;
        private JobPosition _jobPosition;
        private string _localUserAccess;
        private GlobalFunction _globFunc;

        #endregion

        public KpiGrading(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue(nameof(Employee), ref _employee, value); }
        }

        public double TotalNilaiAkhir
        {
            get { return _totNilaiAkhir; }
            set { SetPropertyValue(nameof(TotalNilaiAkhir), ref _totNilaiAkhir, value); }
        }

        public string FinalResult
        {
            get { return _finalResult; }
            set { SetPropertyValue(nameof(FinalResult), ref _finalResult, value); }
        }

        public string FinalPredicate
        {
            get { return _finalPredicate; }
            set { SetPropertyValue(nameof(FinalPredicate), ref _finalPredicate, value); }
        }

        #region Organization 

        [ImmediatePostData()]
        [Appearance("CompanyKpiSubmitSubmit", Enabled = true)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue(nameof(Company), ref _company, value); }
        }

        [XafDisplayName("Workplace")]
        [Appearance("OrganizationDimension1KpiSubmitSubmit", Enabled = true)]
        public OrganizationDimension1 OrgDim1
        {
            get { return _orgDim1; }
            set { SetPropertyValue(nameof(OrgDim1), ref _orgDim1, value); }
        }

        [XafDisplayName("Dept")]
        [Appearance("OrganizationDimension2KpiSubmitSubmit", Enabled = true)]
        public OrganizationDimension2 OrgDim2
        {
            get { return _orgDim2; }
            set { SetPropertyValue(nameof(OrgDim2), ref _orgDim2, value); }
        }

        [XafDisplayName("Sub Dept")]
        [Appearance("OrganizationDimension3Default", Enabled = false)]
        [Appearance("OrganizationDimension3KpiSubmitSubmit", Enabled = true)]
        public OrganizationDimension3 OrgDim3
        {
            get { return _orgDim3; }
            set { SetPropertyValue(nameof(OrgDim3), ref _orgDim3, value); }
        }

        [VisibleInDetailView(false)]
        [VisibleInListView(false)]
        [XafDisplayName("Section")]
        [Appearance("DefaultPositionDefault", Enabled = false)]
        [Appearance("OrganizationDimension4KpiSubmitSubmit", Enabled = false)]
        public OrganizationDimension4 OrgDim4
        {
            get { return _orgDim4; }
            set { SetPropertyValue(nameof(OrgDim4), ref _orgDim4, value); }
        }

        [Appearance("JobPositionKpiSubmitSubmit", Enabled = true)]
        public JobPosition JobPosition
        {
            get { return _jobPosition; }
            set { SetPropertyValue(nameof(JobPosition), ref _jobPosition, value); }
        }

        #endregion Organization


        #region Code

        private void ResultCalculation()
        {
            if (this.TotalNilaiAkhir != 0)
            {
                string _locScoreGrading = null;
                XPCollection<KpiFinalGradingSetting> _availableKpiFinalGradingSettings = new XPCollection<KpiFinalGradingSetting>(this.Session,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Company", this.Company),
                                                                       new BinaryOperator("Active", true)),
                                                                       new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                       );
                if (_availableKpiFinalGradingSettings != null && _availableKpiFinalGradingSettings.Count() > 0)
                {
                    foreach (KpiFinalGradingSetting _availableKpiFinalGradingSetting in _availableKpiFinalGradingSettings)
                    {
                        if (_availableKpiFinalGradingSetting.Operation == ">")
                        {
                            if (this.TotalNilaiAkhir > _availableKpiFinalGradingSetting.Value)
                            {
                                _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                            }
                        }
                        if (_availableKpiFinalGradingSetting.Operation == "<=")
                        {
                            if (this.TotalNilaiAkhir <= _availableKpiFinalGradingSetting.Value)
                            {
                                _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                            }
                        }
                        if (_availableKpiFinalGradingSetting.Operation == "<")
                        {
                            if (this.TotalNilaiAkhir < _availableKpiFinalGradingSetting.Value)
                            {
                                _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                            }
                        }
                        if (_availableKpiFinalGradingSetting.Operation == "=")
                        {
                            if (this.TotalNilaiAkhir == _availableKpiFinalGradingSetting.Value)
                            {
                                _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                            }
                        }
                        if (_availableKpiFinalGradingSetting.Operation == ">=")
                        {
                            if (this.TotalNilaiAkhir >= _availableKpiFinalGradingSetting.Value)
                            {
                                _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                            }
                        }
                        
                    }
                    this.FinalResult = _locScoreGrading;
                    this.Save();
                    this.Session.CommitTransaction();

                    _locScoreGrading = null;

                    if(this.FinalResult != null) 
                    {
                        KpiFinalGradingSetting _locKpiFinalGradingSetting = Session.FindObject<KpiFinalGradingSetting>(new GroupOperator(GroupOperatorType.And,
                                                          new BinaryOperator("Result",this.FinalResult)));
                        if(_locKpiFinalGradingSetting != null) 
                        {
                            this.FinalPredicate = _locKpiFinalGradingSetting.Predicate;
                            this.Save();
                            this.Session.CommitTransaction();
                        }
                    }
                }
            }
        }

        #endregion Code
    }
}