﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Master")]
    [RuleCombinationOfPropertiesIsUnique("DockRuleUnique", DefaultContexts.Save, "Code")]

    public class Dock : BaseObject
    {
        #region initialization

        private string _code;
        private string _name;
        private string _address;
        private string _region;

        private bool _active;
        private GlobalFunction _globFunc;

        #endregion Initialization

        public Dock(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Dock);
            #endregion Numbering
        }

        #region Field

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue(nameof(Name), ref _name, value); }
        }

        public string Address
        {
            get { return _address; }
            set { SetPropertyValue(nameof(Address), ref _address, value); }
        }

        public string Region
        {
            get { return _region; }
            set { SetPropertyValue(nameof(Region), ref _region, value); }
        }

        #endregion Field
    }
}