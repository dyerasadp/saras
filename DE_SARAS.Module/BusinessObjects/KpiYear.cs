﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Year")]
    [NavigationItem("Organization")]
    [RuleCombinationOfPropertiesIsUnique("KpiYearRuleUnique", DefaultContexts.Save, "Code")]
    [RuleCombinationOfPropertiesIsUnique("YearRuleUnique", DefaultContexts.Save, "Year")]

    public class KpiYear : BaseObject
    {
        #region Initialization
        private string _code;
        private string _year;
        private bool _active;
        private GlobalFunction _globFunc;
        //AuditTrail
        private XPCollection<AuditDataItemPersistent> changeHistory;

        #endregion Initialization

        public KpiYear(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        #region Field

       
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        public string Year
        {
            get { return _year; }
            set { SetPropertyValue(nameof(Year), ref _year, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue(nameof(Active), ref _active, value); }
        }

        #endregion Field

    }
}