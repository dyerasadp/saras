﻿using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Shipping")]
    [RuleCombinationOfPropertiesIsUnique("VoyageCargoRuleUnique", DefaultContexts.Save, "Code")]

    public class VoyageCargo : BaseObject
    {
        #region Initialization

        private string _code;
        private VoyageRoute _voyageRoute;
        private double _blObs;
        private double _bl15;
        private double _blTemperature;
        private double _blDensity;
        private double _sfalObs;
        private double _sfal15;
        private double _sfalTemperature;
        private double _sfalDensity;
        private double _sfbdObs;
        private double _sfbd15;
        private double _sfbdTemperature;
        private double _sfbdDensity;
        private double _sfdObs;
        private double _sfd15;
        private double _sfdTemperature;
        private double _sfdDensity;
        private double _rfObs;
        private double _rf15;
        private double _rfTemperature;
        private double _rfDensity;
        private double _cargoOnBoard;
        private double _remainingOnBoard;
        private double _r1DiffObs;
        private double _r1Diff15;
        private double _r1PercentageObs;
        private double _r1Percentage15;
        private double _r2DiffObs;
        private double _r2Diff15;
        private double _r2PercentageObs;
        private double _r2Percentage15;
        private double _r3DiffObs;
        private double _r3Diff15;
        private double _r3PercentageObs;
        private double _r3Percentage15;
        private double _r4DiffObs;
        private double _r4Diff15;
        private double _r4PercentageObs;
        private double _r4Percentage15;
        private ShippingMasterCargoContract _contract;
        private double _tolleranceLiter;
        private double _tollerancePercentage;
        private double _lossCargoLiter;
        private double _lossCargoPercentage;
        private string _idCargoContract;
        private double _x;
        private double _y;
        private double _z;
        private double _ratio;
        private double _ratioPercent;
        private double _lossCargoLtr;
        private double _lossCargoPercent;
        private StatusPerformance _statusCargo;
        private GlobalFunction _globFunc;

        #endregion Initialization
        public VoyageCargo(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();

            #region Numbering
            _globFunc = new GlobalFunction();
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.VoyageCargo);
            #endregion Numbering
        }

        #region Field

        [VisibleInListView(false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue(nameof(Code), ref _code, value); }
        }

        [ImmediatePostData()]
        [Association("VoyageRoute-VoyageCargos")]
        [Appearance("VoyageRouteDisable", Enabled = false)]
        public VoyageRoute VoyageRoute
        {
            get { return _voyageRoute; }
            set{ 
                SetPropertyValue(nameof(VoyageRoute), ref _voyageRoute, value);
                if (!IsLoading)
                {
                    if (_voyageRoute != null)
                    {
                        LastDataGet();
                    }
                }
            }
        }

        #region BL

        [VisibleInListView(false)]
        [ImmediatePostData]
        [ModelDefault("DisplayFormat", "{0:n2}")]
        [ModelDefault("EditMask", "########0.##")]
        [XafDisplayName("BL Obs")]
        public double BlObs
        {
            get { return _blObs; }
            set
            {
                SetPropertyValue(nameof(BlObs), ref _blObs, value);
                if (!IsLoading)
                {
                    HitungRatio();
                    CargoLossCalculation();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData]
        [ModelDefault("DisplayFormat", "{0:n2}")]
        [ModelDefault("EditMask", "########0.##")]
        [XafDisplayName("BL 15°C")]
        public double Bl15
        {
            get { return _bl15; }
            set
            {
                SetPropertyValue(nameof(Bl15), ref _bl15, value);
                if (!IsLoading)
                {
                    HitungRatio();
                    CargoLossCalculation();
                }
            }
        }

        [VisibleInListView(false)]
        public double BlTemperature
        {
            get { return _blTemperature; }
            set
            { SetPropertyValue(nameof(BlTemperature), ref _blTemperature, value); }
        }

        [VisibleInListView(false)]
        public double BlDensity
        {
            get { return _blDensity; }
            set
            { SetPropertyValue(nameof(BlDensity), ref _blDensity, value); }
        }

        #endregion BL

        #region SFAL

        [VisibleInListView(false)]
        [ImmediatePostData]
        [ModelDefault("DisplayFormat", "{0:n2}")]
        [ModelDefault("EditMask", "########0.##")]
        [XafDisplayName("SFAL Obs")]
        public double SfalObs
        {
            get { return _sfalObs; }
            set
            {
                SetPropertyValue(nameof(SfalObs), ref _sfalObs, value);
                if (!IsLoading)
                {
                    HitungRatio();
                    CargoLossCalculation();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData]
        [ModelDefault("DisplayFormat", "{0:n2}")]
        [ModelDefault("EditMask", "########0.##")]
        [XafDisplayName("SFAL 15°C")]
        public double Sfal15
        {
            get { return _sfal15; }
            set
            {
                SetPropertyValue(nameof(Sfal15), ref _sfal15, value);
                if (!IsLoading)
                {
                    HitungRatio();
                    CargoLossCalculation();
                }
            }
        }

        [VisibleInListView(false)]
        public double SfalTemperature
        {
            get { return _sfalTemperature; }
            set
            { SetPropertyValue(nameof(SfalTemperature), ref _sfalTemperature, value); }
        }

        [VisibleInListView(false)]
        public double SfalDensity
        {
            get { return _sfalDensity; }
            set
            { SetPropertyValue(nameof(SfalDensity), ref _sfalDensity, value); }
        }

        #endregion SFAL

        #region SFBD

        [VisibleInListView(false)]
        [ImmediatePostData]
        [ModelDefault("DisplayFormat", "{0:n2}")]
        [ModelDefault("EditMask", "########0.##")]
        [XafDisplayName("SFBD Obs")]
        public double SfbdObs
        {
            get { return _sfbdObs; }
            set
            {
                SetPropertyValue(nameof(SfbdObs), ref _sfbdObs, value);
                if (!IsLoading)
                {
                    HitungRatio();
                    CargoLossCalculation();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData]
        [ModelDefault("DisplayFormat", "{0:n2}")]
        [ModelDefault("EditMask", "########0.##")]
        [XafDisplayName("SFBD 15°C")]
        public double Sfbd15
        {
            get { return _sfbd15; }
            set
            {
                SetPropertyValue(nameof(Sfbd15), ref _sfbd15, value);
                if (!IsLoading)
                {
                    HitungRatio();
                    CargoLossCalculation();
                }
            }
        }

        [VisibleInListView(false)]
        public double SfbdTemperature
        {
            get { return _sfbdTemperature; }
            set
            { SetPropertyValue(nameof(SfbdTemperature), ref _sfbdTemperature, value); }
        }

        [VisibleInListView(false)]
        public double SfbdDensity
        {
            get { return _sfbdDensity; }
            set
            { SetPropertyValue(nameof(SfbdDensity), ref _sfbdDensity, value); }
        }

        #endregion SFBD

        #region SFD

        [VisibleInListView(false)]
        [ImmediatePostData]
        [ModelDefault("DisplayFormat", "{0:n2}")]
        [ModelDefault("EditMask", "########0.##")]
        [XafDisplayName("SFAD Obs")]
        public double SfdObs
        {
            get { return _sfdObs; }
            set
            {
                SetPropertyValue(nameof(SfdObs), ref _sfdObs, value);
                if (!IsLoading)
                {
                    HitungRatio();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData]
        [ModelDefault("DisplayFormat", "{0:n2}")]
        [ModelDefault("EditMask", "########0.##")]
        [XafDisplayName("SFAaD 15°C")]
        public double Sfd15
        {
            get { return _sfd15; }
            set
            {
                SetPropertyValue(nameof(Sfd15), ref _sfd15, value);
                if (!IsLoading)
                {
                    HitungRatio();
                }
            }
        }

        [XafDisplayName("SFAD Temperature")]
        [VisibleInListView(false)]
        public double SfdTemperature
        {
            get { return _sfdTemperature; }
            set
            { SetPropertyValue(nameof(SfdTemperature), ref _sfdTemperature, value); }
        }

        [XafDisplayName("SFAD Density")]
        [VisibleInListView(false)]
        public double SfdDensity
        {
            get { return _sfdDensity; }
            set
            { SetPropertyValue(nameof(SfdDensity), ref _sfdDensity, value); }
        }

        #endregion SFD

        #region RF

        [VisibleInListView(false)]
        [ImmediatePostData]
        [ModelDefault("DisplayFormat", "{0:n2}")]
        [ModelDefault("EditMask", "########0.##")]
        [XafDisplayName("RF Obs")]
        public double RfObs
        {
            get { return _rfObs; }
            set
            {
                SetPropertyValue(nameof(RfObs), ref _rfObs, value);
                if (!IsLoading)
                {
                    HitungRatio();
                    CargoLossCalculation();
                }
            }
        }

        [VisibleInListView(false)]
        [ImmediatePostData]
        [ModelDefault("DisplayFormat", "{0:n2}")]
        [ModelDefault("EditMask", "########0.##")]
        [XafDisplayName("RF 15°C")]
        public double Rf15
        {
            get { return _rf15; }
            set
            {
                SetPropertyValue(nameof(Rf15), ref _rf15, value);
                if (!IsLoading)
                {
                    HitungRatio();
                    CargoLossCalculation();
                }
            }
        }

        [VisibleInListView(false)]
        public double RfTemperature
        {
            get { return _rfTemperature; }
            set
            { SetPropertyValue(nameof(RfTemperature), ref _rfTemperature, value); }
        }

        [VisibleInListView(false)]
        public double RfDensity
        {
            get { return _rfDensity; }
            set
            { SetPropertyValue(nameof(RfDensity), ref _rfDensity, value); }
        }

        #endregion RF

        #region On Board

        [VisibleInListView(false)]
        [Appearance("CargoOnBoardDisable", Enabled = false)]
        [XafDisplayName("Cargo On Board")]
        public double CargoOnBoard
        {
            get { return _cargoOnBoard; }
            set
            { SetPropertyValue(nameof(CargoOnBoard), ref _cargoOnBoard, value); }
        }

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:n2}")]
        [ModelDefault("EditMask", "########0.##")]
        [XafDisplayName("Remaining Cargo On Board")]
        public double RemainingOnBoard
        {
            get { return _remainingOnBoard; }
            set
            { SetPropertyValue(nameof(RemainingOnBoard), ref _remainingOnBoard, value); }
        }

        #endregion On Board

        #region R1

        [VisibleInListView(false)]
        [Appearance("R1DiffObsDisable", Enabled = false)]
        [XafDisplayName("R1 Obs")]
        public double R1DiffObs
        {
            get { return _r1DiffObs; }
            set { SetPropertyValue(nameof(R1DiffObs), ref _r1DiffObs, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R1Diff15Disable", Enabled = false)]
        [XafDisplayName("R1 15°C")]
        public double R1Diff15
        {
            get { return _r1Diff15; }
            set { SetPropertyValue(nameof(R1Diff15), ref _r1Diff15, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R1PercentageObsDisable", Enabled = false)]
        [XafDisplayName("R1 Obs (%)")]
        public double R1PercentageObs
        {
            get { return _r1PercentageObs; }
            set { SetPropertyValue(nameof(R1PercentageObs), ref _r1PercentageObs, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R1Percentage15Disable", Enabled = false)]
        [XafDisplayName("R1 15°C (%)")]
        public double R1Percentage15
        {
            get { return _r1Percentage15; }
            set { SetPropertyValue(nameof(R1Percentage15), ref _r1Percentage15, value); }
        }

        #endregion R1

        #region R2

        [VisibleInListView(false)]
        [Appearance("R2DiffObsDisable", Enabled = false)]
        [XafDisplayName("R2 Obs")]
        public double R2DiffObs
        {
            get { return _r2DiffObs; }
            set { SetPropertyValue(nameof(R2DiffObs), ref _r2DiffObs, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R2Diff15Disable", Enabled = false)]
        [XafDisplayName("R2 15°C")]
        public double R2Diff15
        {
            get { return _r2Diff15; }
            set { SetPropertyValue(nameof(R2Diff15), ref _r2Diff15, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R2PercentageObsDisable", Enabled = false)]
        [XafDisplayName("R2 Obs (%)")]
        public double R2PercentageObs
        {
            get { return _r2PercentageObs; }
            set { SetPropertyValue(nameof(R2PercentageObs), ref _r2PercentageObs, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R2Percentage15Disable", Enabled = false)]
        [XafDisplayName("R2 15°C (%)")]
        public double R2Percentage15
        {
            get { return _r2Percentage15; }
            set { SetPropertyValue(nameof(R2Percentage15), ref _r2Percentage15, value); }
        }

        #endregion R2

        #region R3

        [VisibleInListView(false)]
        [Appearance("R3DiffObsDisable", Enabled = false)]
        [XafDisplayName("R3 Obs")]
        public double R3DiffObs
        {
            get { return _r3DiffObs; }
            set { SetPropertyValue(nameof(R3DiffObs), ref _r3DiffObs, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R3Diff15Disable", Enabled = false)]
        [XafDisplayName("R3 15°C")]
        public double R3Diff15
        {
            get { return _r3Diff15; }
            set { SetPropertyValue(nameof(R3Diff15), ref _r3Diff15, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R3PercentageObsDisable", Enabled = false)]
        [XafDisplayName("R3 Obs (%)")]
        public double R3PercentageObs
        {
            get { return _r3PercentageObs; }
            set { SetPropertyValue(nameof(R3PercentageObs), ref _r3PercentageObs, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R3Percentage15Disable", Enabled = false)]
        [XafDisplayName("R3 15°C (%)")]
        public double R3Percentage15
        {
            get { return _r3Percentage15; }
            set { SetPropertyValue(nameof(R3Percentage15), ref _r3Percentage15, value); }
        }

        #endregion R3

        #region R4

        [VisibleInListView(false)]
        [Appearance("R4DiffObsDisable", Enabled = false)]
        [XafDisplayName("R4 Obs")]
        public double R4DiffObs
        {
            get { return _r4DiffObs; }
            set { SetPropertyValue(nameof(R4DiffObs), ref _r4DiffObs, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R4Diff15Disable", Enabled = false)]
        [XafDisplayName("R4 15°C")]
        public double R4Diff15
        {
            get { return _r4Diff15; }
            set { SetPropertyValue(nameof(R4Diff15), ref _r4Diff15, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R4PercentageObsDisable", Enabled = false)]
        [XafDisplayName("R4 Obs (%)")]
        public double R4PercentageObs
        {
            get { return _r4PercentageObs; }
            set { SetPropertyValue(nameof(R4PercentageObs), ref _r4PercentageObs, value); }
        }

        [VisibleInListView(false)]
        [Appearance("R4Percentage15Disable", Enabled = false)]
        [XafDisplayName("R4 15°C (%)")]
        public double R4Percentage15
        {
            get { return _r4Percentage15; }
            set { SetPropertyValue(nameof(R4Percentage15), ref _r4Percentage15, value); }
        }

        #endregion R4

        #region Tollerance and Status

        [VisibleInListView(false)]
        [ImmediatePostData]
        public ShippingMasterCargoContract Contract
        {
            get { return _contract; }
            set
            {
                SetPropertyValue(nameof(Contract), ref _contract, value);
                if (!IsLoading)
                {
                    ContractTollerance();
                }
            }
        }

        [VisibleInListView(false)]
        [Appearance("TolleranceLiterDisable", Enabled = false)]
        public double TolleranceLiter
        {
            get { return _tolleranceLiter; }
            set { SetPropertyValue(nameof(TolleranceLiter), ref _tolleranceLiter, value); }
        }

        [VisibleInListView(false)]
        [Appearance("TollerancePercentageDisable", Enabled = false)]
        public double TollerancePercentage
        {
            get { return _tollerancePercentage; }
            set { SetPropertyValue(nameof(TollerancePercentage), ref _tollerancePercentage, value); }
        }

        [VisibleInListView(false)]
        [Appearance("LossCargoLiterDisable", Enabled = false)]
        public double LossCargoLiter
        {
            get { return _lossCargoLiter; }
            set { SetPropertyValue(nameof(LossCargoLiter), ref _lossCargoLiter, value); }
        }

        [VisibleInListView(false)]
        [Appearance("LossCargoPercentageDisable", Enabled = false)]
        public double LossCargoPercentage
        {
            get { return _lossCargoPercentage; }
            set { SetPropertyValue(nameof(LossCargoPercentage), ref _lossCargoPercentage, value); }
        }

        [Appearance("StatusCargoDisable", Enabled = false)]
        public StatusPerformance StatusCargo
        {
            get { return _statusCargo; }
            set { SetPropertyValue(nameof(StatusCargo), ref _statusCargo, value); }
        }

        #endregion Tollerance and Status

        #endregion Field

        #region Code

        private void LastDataGet()
        {
            if (_voyageRoute.Vessel != null)
            {
                int _routeNumBefore = _voyageRoute.No - 1;
                if (_voyageRoute.No == 1)
                {
                    VesselRemains _locVesselRemains = Session.FindObject<VesselRemains>
                                        (new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Vessel", _voyageRoute.Vessel)
                                        ));
                    if (_locVesselRemains != null)
                    {
                        this.CargoOnBoard = _locVesselRemains.QtyCargoLeft;
                    } else
                    {
                        _locVesselRemains = new VesselRemains(Session);
                        _locVesselRemains.Vessel = _voyageRoute.Vessel;
                        // Save the new VesselRemains data to the database
                        _locVesselRemains.Save();
                        //_locVesselRemains.Session.CommitTransaction();
                        // Assign the CargoOnBoard property
                        this.CargoOnBoard = _locVesselRemains.QtyCargoLeft;
                    }
                }
                else
                {
                    VoyageRoute _voyageRouteBefore = Session.FindObject<VoyageRoute>
                                        (new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("No", _routeNumBefore),
                                        new BinaryOperator("Voyage", _voyageRoute.Voyage)
                                        ));

                    if (_voyageRouteBefore != null)
                    {
                        VoyageCargo _lastVoyageRouteCargo = Session.FindObject<VoyageCargo>
                                (new GroupOperator(GroupOperatorType.And,
                                new BinaryOperator("VoyageRoute", _voyageRouteBefore)
                                ));
                        if (_lastVoyageRouteCargo != null)
                        {
                            this.CargoOnBoard = _lastVoyageRouteCargo.RemainingOnBoard;
                        }
                    }
                }
            }
        }

        private void HitungRatio()
        {

            if (this.BlObs != 0)
            {
                if (this.SfalObs != 0)
                {
                    if (this.SfbdObs != 0)
                    {
                        if (this.RfObs != 0)
                        {
                            this.R3DiffObs = (this.RfObs - this.SfbdObs);
                            this.R3PercentageObs = Math.Round( (((this.RfObs - this.SfbdObs) / this.SfbdObs) * 100), 2);
                            this.R4DiffObs = (this.RfObs - this.BlObs);
                            this.R4PercentageObs = Math.Round( (((this.RfObs - this.BlObs) / this.BlObs) * 100), 2);
                            this.RemainingOnBoard = this.SfbdObs - this.SfdObs;
                        }
                        this.R2DiffObs = (this.SfbdObs - this.SfalObs);
                        this.R2PercentageObs = Math.Round( (((this.SfbdObs - this.SfalObs) / this.SfalObs) * 100), 2);
                    }
                    this.R1DiffObs = (this.SfalObs - this.BlObs);
                    this.R1PercentageObs = Math.Round( (((this.SfalObs - this.BlObs) / this.BlObs) * 100), 2);
                }
            }
            if (this.Bl15 != 0)
            {
                if (this.Sfal15 != 0)
                {
                    if (this.Sfbd15 != 0)
                    {
                        if (this.Rf15 != 0)
                        {
                            this.R3Diff15 = (this.Rf15 - this.Sfbd15);
                            this.R3Percentage15 = Math.Round( (((this.Rf15 - this.Sfbd15) / this.Sfbd15) * 100), 2);
                            this.R4Diff15 = (this.Rf15 - this.Bl15);
                            this.R4Percentage15 = Math.Round( (((this.Rf15 - this.Bl15) / this.Bl15) * 100), 2);
                        }
                        this.R2Diff15 = (this.Sfbd15 - this.Sfal15);
                        this.R2Percentage15 = Math.Round( (((this.Sfbd15 - this.Sfal15) / this.Sfal15) * 100), 2);
                    }
                    this.R1Diff15 = (this.Sfal15 - this.Bl15);
                    this.R1Percentage15 = Math.Round( (((this.Sfal15 - this.Bl15) / this.Bl15) * 100), 2);
                }
            }

        }

        private void ContractTollerance()
        {
            if (this.Contract != null)
            {
                XPCollection<ShippingMasterCargoContract> _locCargoContracts = new XPCollection<ShippingMasterCargoContract>(Session);

                if (_locCargoContracts != null && _locCargoContracts.Count() > 0)
                {
                    foreach (ShippingMasterCargoContract CargoContract in _locCargoContracts)
                    {
                        _idCargoContract = CargoContract.Code;
                        ShippingMasterCargoContract _locCargoContract = Session.FindObject<ShippingMasterCargoContract>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Code", _idCargoContract)
                                                             ));
                        if (_locCargoContract != null)
                        {
                            if (this.Contract == _locCargoContract)
                            {
                                this.TollerancePercentage = _locCargoContract.TollerancePercentage;
                            }
                        }
                    }
                }
            }
        }

        private void CargoLossCalculation()
        {
            if (this.Contract != null)
            {
                XPCollection<ShippingMasterCargoContract> _locCargoContractRoles = new XPCollection<ShippingMasterCargoContract>(Session);


                if (_locCargoContractRoles != null && _locCargoContractRoles.Count() > 0)
                {
                    foreach (ShippingMasterCargoContract CargoContract in _locCargoContractRoles)
                    {
                        if (CargoContract == this.Contract)
                        {
                            // _y dan _x itu initialization dulu, 
                            _y = this.TollerancePercentage;

                            //buat kondisi isian _x nya, ratio enum,
                            if (CargoContract.ContractRole == ContractRole.R1Obs)
                            {
                                _x = this.BlObs;
                                _ratio = this.R1DiffObs;
                                _ratioPercent = this.R1PercentageObs;
                            }
                            if (CargoContract.ContractRole == ContractRole.R2Obs)
                            {
                                _x = this.SfalObs;
                                _ratio = this.R2DiffObs;
                                _ratioPercent = this.R2PercentageObs;
                            }
                            if (CargoContract.ContractRole == ContractRole.R3Obs)
                            {
                                _x = this.SfbdObs;
                                _ratio = this.R3DiffObs;
                                _ratioPercent = this.R3PercentageObs;
                            }
                            if (CargoContract.ContractRole == ContractRole.R4Obs)
                            {
                                _x = this.BlObs;
                                _ratio = this.R4DiffObs;
                                _ratioPercent = this.R4PercentageObs;
                            }

                            // TOLLERANCE LITER
                            _z = Math.Round( -((_x * _y) / 100));

                            //LossCargoLiter = R + tolleranceLiter
                            _lossCargoLtr = Math.Round(_ratio + _z);

                            //LossCargoPercentage = R% - tollerancePercentage
                            _lossCargoPercent = Math.Round(_ratioPercent - _y, 2);

                            this.TolleranceLiter = _z;
                            this.LossCargoLiter = _lossCargoLtr;
                            this.LossCargoPercentage = _lossCargoPercent;

                            if (_lossCargoLtr >= _z)
                            {
                                this.StatusCargo = StatusPerformance.Achieved;
                            }
                            else
                            {
                                this.StatusCargo = StatusPerformance.Failed;
                            }

                        }
                    }
                }
            }

        }
    }
    #endregion Code
}