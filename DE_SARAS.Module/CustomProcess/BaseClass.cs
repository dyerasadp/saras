﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DE_SARAS.Module.CustomProcess
{
    internal class BaseClass
    {
    }

    #region GENERAL
    public enum NumberingType
    {
        None = 0,
        Documents = 1,
        Objects = 2,
        Employee = 3,
        LotNumber = 4,
        Item = 5,
        Other = 10
    }

    public enum ObjectList
    {
        None = 0,
        Company = 1,
        OrganizationDimension1 = 2,
        OrganizationDimension2 = 3,
        OrganizationDimension3 = 4,
        OrganizationDimension4 = 5,
        JobPosition = 6,
        Employee = 7,
        EmployeePositionLine = 8,
        KpiYear = 9,
        KpiRole = 10,
        KpiRoleLine = 11,
        KpiRoleGrading = 12,
        KpiRoleCascadingLine = 13,
        KpiSubmit = 14,
        KpiSubmitLine = 15,
        KpiSubmitTaskBase = 16,
        KpiSubmitApproval = 17,
        BehaviourMonitoring = 18,
        Pica = 19,
        PicaMonitoring = 20,
        BehaviourSubmitLine = 21,
        CallCenter = 22,
        CallCenterTaskBase = 23,
        CallCenterMonitoring = 24,
        CallCenterDailyActivity = 25,
        Quotation = 26,
        QuotationPriceLine = 27,
        QuotationTaxPriceLine = 28,
        CallCenterFeedbackUser = 29,
        QuotationCustomer = 30,
        QuotationMechanismLine = 31,
        Crew = 32,
        CrewCertificateOfCompetence = 33,
        CrewClassList = 34,
        CrewContract = 35,
        CrewDocument = 36,
        CrewFamily = 37,
        CrewSafemanning = 38,
        DailyVesselMonitoring = 39,
        Dock = 40,
        DocumentShipping = 41,
        DocumentShippingAgency = 42,
        ShippingHeadInspection = 43,
        ShippingHeadInspectionLine = 44,
        ShippingMasterCustomer = 45,
        ShippingMasterCargoContract = 46,
        ShippingMasterChecklistHsse = 47,
        ShippingMasterDocRenewAbout = 48,
        ShippingMasterInspection = 49,
        ShippingMasterInstitusi = 50,
        ShippingMasterLocation = 51,
        ShippingMasterPort = 52,
        ShippingMasterPartVesselCategory = 53,
        ShippingMasterPartVesselSubCategory = 54,
        ShippingMasterPartVesselUnit = 55,
        ShippingMasterPaymentRequestAccount = 56,
        ShippingMasterPaymentRequestProject = 57,
        ShippingPaymentRequest = 58,
        ShippingVesselDailySetHead = 59,
        ShippingVesselDailySetLineDeck = 60,
        ShippingVesselDailySetLineEngine = 61,
        Vessel = 62,
        VesselDamageReport = 63,
        VesselDamageReportSpare = 64,
        VesselDocking = 65,
        VesselDockingLineActual = 66,
        VesselDockingLinePlan = 67,
        VesselDocument = 68,
        VesselDocumentFigure = 69,
        VesselDocumentRenewal = 70,
        VesselDocumentRenewalLine = 71,
        VesselInspection = 72,
        VesselInspectionLine = 73,
        VesselPart = 74,
        VesselRemains = 75,
        VesselRepairList = 76,
        VesselRepairListLine = 77,
        VesselSpk = 78,
        VesselSpkLine = 79,
        VesselSpkLineProgress = 80,
        Voyage = 81,
        VoyageBunker = 82,
        VoyageCargo = 83,
        VoyageCheckCrew = 84,
        VoyageCheckHsse = 85,
        VoyageCheckOps = 86,
        VoyageCheckOpsSupp = 87,
        VoyageCheckPurchasing = 88,
        VoyageCycleTime = 89,
        VoyageRoute = 90,
        WarningLetter = 91,
    }

    public enum ObjectListGroup
    {
        None = 0,
        KpiRole = 1,
        KpiSubmit = 2,
        KpiMonitoring = 3,
        BehaviourMonitoring = 4,
        Pica = 5,
        PicaMonitoring = 6,
        CallCenter = 7,
        CallCenterMonitoring = 8,
        CallCenterDailyActivity = 9,
        Quotation = 10,
        Vessel = 11,
        QuotationCustomer = 12,
        KpiSubmitTaskBase = 13,
        All = 100,
    }

    public enum PageType
    {
        None = 0,
        DetailView = 1,
        ListView = 2,
        AllView = 3,
    }

    public enum Satuan
    {
        Buah = 0,
        Kilogram = 1,
        Pcs = 2,
    }

    public enum Answer
    {
        Yes = 0,
        No = 1,
    }

    #endregion GENERAL

    #region KPI
    public enum Status
    {
        None = 0,
        Open = 1,
        Progress = 2,
        Cancel = 3,
        Close = 4,
        Approved = 5,
        Reject = 6,
    }

    public enum FunctionList
    {
        None = 0,
        Approval = 1,
        Posting = 2,
        Progress = 3,
        QcPassed = 4,
        Reopen = 5,
    }


    public enum ApprovalLevel
    {
        None = 0,
        Level1 = 1,
        Level2 = 2,
        Level3 = 3,
        Level4 = 4,
        Level5 = 5,
        Level6 = 6,
    }

    public enum ApplicationActionList
    {
        None = 0,
        Approval = 1,
        Posting = 2,
        Reopen = 3,
    }

    public enum FilterListSetup
    {
        None = 0,
        ByCompany = 1,
        ByCompanyAndWorkplace = 2,
        ByCompanyAndWorkplaceAndDepartment = 3,
        ByCompanyAndWorkplaceAndDepartmentAndDivision = 4,
        NotUseCompany = 5,
        NotUseCompanyByWorkplace = 6,
        NotUseCompanyAndWorkplace = 7,
        ByCompanyAndWorkplaceAndDepartmentAndEmployee = 8,
        ByCompanyAndWorkplaceAndDepartmentAndDivisionAndEmployee = 9,
    }

    public enum Month
    {
        None = 0,
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12,
    }

    public enum KpiPeriod
    {
        None = 0,
        Monthly = 1,
        Quarter = 2,
        Semester = 3,
        Yearly = 4,
    }

    public enum KpiFormat
    {
        None = 0,
        Persen = 1,
        Amount = 2,
        Scale = 3,
        Date = 4,
        Month = 5,
        Day = 6,
        Hour = 7,
        Liter = 8,
        Rupiah = 9,
        Ton = 10,
    }

    public enum KpiValue
    {
        None = 0,
        Maximize = 1,
        Minimize = 2,
    }

    public enum KpiGroupType
    {
        None = 0,
        Cascading = 1,
        Personal = 2,
    }

    public enum KpiType
    {
        None = 0,
        Specific = 1,
        All = 2,
    }

    public enum KpiYtdType
    {
        None = 0,
        Average = 1,
        SumAll = 2,
    }

    public enum KpiBscType
    {
        None = 0,
        FP = 1,
        IPP = 2,
        LGP =3,
        CP =4,
    }

    public enum KpiStatusPencapaian
    {
        None = 0,
        Tercapai = 1,
        TidakTercapai = 2,

    }

    public enum AdditionalScoreType
    {
        None = 0,
        Pinalty = 1,
        Reward = 2,
    }

    public enum BehaviourType
    {
        None = 0,
        Positif = 1,
        Negatif = 2,
    }
    public enum BehaviourCategory
    {
        None = 0,
        Sinergi = 1,
        Akuntabel = 2,
        Dinamis = 3,
        Profesional = 4,
    }

    public enum BehaviourScale
    {
        None = 0,
        SangatSesuai = 4,
        Sesuai = 3,
        TidakSesuai = 2,
        SangatTidakSesuai = 1,

    }

    public enum BehaviourScaleNegatif
    {
        None = 0,
        SangatSesuai = 1,
        Sesuai = 2,
        TidakSesuai = 3,
        SangatTidakSesuai = 4,
    }

    public enum JobPositionLevel
    {
        None = 0,
        ManagementLevel = 1,
        StaffLevel = 2,
    }

    #endregion KPI

    #region SHIPPING

    #region CREWING

    public enum Agama
    {
        _ = 0,
        Islam = 1,
        Kristen = 2,
        Katolik = 3,
        Hindu = 4,
        Buddha = 5,
        Khonghucu = 6,
    }

    public enum Goldar
    {
        _ = 0,
        A = 1,
        B = 2,
        AB = 3,
        O = 4,
    }

    public enum FamilyStatus
    {
        Suami = 0,
        Istri = 1,
        Anak = 2,
        Ayah = 3,
        Ibu = 4,
        Adik = 5,
        Kakak = 6,
        Lainnya = 7,
    }

    public enum StatusCrewOn
    {
        None = 0,
        NewHire = 1,
        Rejoin = 2,
        Mutasi = 3,
        Promosi = 4,
        Demosi = 5,
    }

    public enum StatusCrewOff
    {
        None = 0,
        Mutasi = 1,
        Demosi = 2,
        Promosi = 3,
        FinishContract = 4,
        Resign = 5,
        Terminated = 6,
        AWOL = 7,
    }

    public enum StatusForCrew
    {
        None = 0,
        Offboard = 1,
        Onboard = 2,
        Blacklist = 3,
    }

    public enum TypeForCrewClass
    {
        OilTanker = 0,
    }

    public enum Gender
    {
        Male = 0,
        Female = 1,
    }

    public enum MaritalStatus
    {
        _ = 0,
        Single = 1,
        Married = 2,
        Divorce = 3,
    }

    public enum PTKP
    {
        _ = 0,
        TK0 = 1,
        TK1 = 2,
        TK2 = 3,
        TK3 = 4,
        K0 = 5, 
        K1 = 6,
        K2 = 7,
        K3 = 8,
        KI0 = 9,
        KI1 = 10,
        KI2 = 11,
        KI3 = 12,
    }

    #endregion CREWING

    #region DOCUMENT CONTROL

    public enum DocumentShippingType
    {
        None = 0,
        SeaVessel = 1,
        RiverVessel = 2,
        DocumentCrew = 3,
        Travel = 4,
        //CertificatesOfCompetence = 5,
        CertificateOfProficiency = 6,
        HealthCrew = 7,
        EndorsementCrew = 8,
    }

    public enum DocumentRenewalType
    {
        Regular = 0,
        Endorsement = 1,
    }

    public enum VesselClassType
    {
        BKI = 0,
        BV = 1,
    }

    public enum DocumentVesselStatus
    {
        Safe = 0,
        Warning = 1,
        Expired = 2,
    }

    #endregion DOCUMENT CONTROL

    #region FOR MASTER

    public enum VesselType
    {
        None = 0,
        OilBarge = 1,
        TugBoat = 2,
        SelfPropelledOilBarges = 3,
        MotorTanker = 4,
        LandingCraftTank = 5,
        KapalMotorPenumpang = 6,
    }

    public enum VesselHull
    {
        None = 0,
        SingleBottom = 1,
        DoubleBottom = 2,
        SingleHull = 3,
        DoubleHull = 4,
    }

    public enum StatusOwnershipVessel
    {
        Shipping = 0,
        Carter = 1,
    }

    #endregion FOR MASTER

    #region VOYAGE

    public enum StatusReadyList
    {
        Empty = 0,
        Ready = 1,
        NotReady = 2
    }

    public enum ContractRole
    {
        None = 0,
        R1Obs = 1,
        R2Obs = 2,
        R3Obs = 3,
        R4Obs = 4,
    }

    public enum StatusPerformance
    {
        Achieved = 0,
        Failed = 1,
    }

    #endregion VOYAGE

    #region VESSEL STATUS

    public enum VerifyStatus
    {
        NotVerified = 0,
        Verified = 1,
    }

    public enum ItemPosition
    {
        Engine = 0,
        Deck = 1,
    }

    public enum Urgency
    {
        Critical = 0,
        NonCritical = 1,
    }

    public enum ChecklistOption
    {
        Normal = 0,
        Abnormal = 1,
        NotApplicable = 2,
    }

    public enum SpkType
    {
        Internal = 0,
        Eksternal = 1,
    }

    public enum RepairedBy
    {
        Shore = 0,
        Crew = 1,
    }

    public enum RepairsCarried
    {
        Permanently = 0,
        Temporarily = 1,
    }

    public enum RepairsRequired
    {
        Urgently = 0,
        NextPort = 1,
        Within3Months = 2,
        NextDryDock = 3,
    }

    public enum RepairStatus
    {
        Progress = 0,
        Done = 1,
    }

    #endregion VESSEL STATUS

    #endregion SHIPPING

    #region TICKETING IT
    public enum AssetType
    {
        None = 0,
        HARDWARE = 1,
        NETWORKING = 2,
        SOFTWARE = 3,
    }

    public enum SoftwareCategory
    {
        None = 0,
        ACCOUNTING = 1,
        ANTIVIRUS = 2,
        OFFICE = 3,
    }

    public enum HardwareCategory
    {
        None = 0,
        CCTV = 1,
        MONITOR = 2,
        PRINTER = 3,
    }

    public enum NetworkCategory
    {
        None = 0,
        ANTENA = 1,
        MODEM = 2,
        RADIO = 3,
    }

    public enum FlagAsset
    {
        None = 0,
        ASSET = 1,
        NONASSET = 2,
    }

    public enum ColorAsset
    {
        None = 0,
        BLACK = 1,
        WHITE = 2,
        BLUE = 3,
    }

    public enum StatusTicket
    {
        None = 0,
        Open = 1,
        Progress = 2,
        Closed = 3,
    }

    #endregion TICKETING IT

    #region QUOTATION

    public enum MechanismGroup
    {
        None = 0,
        PEMESANAN = 1,
        PENERIMAAN = 2,
        PEMBAYARAN = 3,
        ITEM = 4,
        NONITEM = 5,
    }

    public enum QuotationType
    {
        None = 0,
        PERSONAL = 1,
        GROUP = 2,
    }

    public enum AreaJual
    {
        None = 0,
        BANJARMASIN = 1,
    }

    public enum QuotationStatus
    {
        None = 0,
        NEW = 1,
        RENEWEL = 2,
    }

    #endregion QUOTATION

}
