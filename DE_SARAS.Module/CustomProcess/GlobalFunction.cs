﻿using DE_SARAS.Module.BusinessObjects;
using DevExpress.Charts.Native;
using DevExpress.CodeParser;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using DevExpress.XtraPrinting.Export.Web;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection.Metadata;

namespace DE_SARAS.Module.CustomProcess
{
    internal class GlobalFunction
    {
        #region Numbering

        #region Numbering Form 
        public string GetNumberingUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject)
        {
            string _result = null;
            try
            {
                if (_currIDataLayer != null)
                {
                    Session _generatorSession = new(_currIDataLayer);

                    if (_generatorSession != null)
                    {
                        XPCollection<NumberingHeader> _numberingHeaders = new XPCollection<NumberingHeader>
                                                                (_generatorSession, new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("NumberingType", NumberingType.Objects),
                                                                 new BinaryOperator("Active", true)));

                        if (_numberingHeaders != null && _numberingHeaders.Count() > 0)
                        {
                            foreach (NumberingHeader _numberingHeader in _numberingHeaders)
                            {
                                NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("ObjectList", _currObject),
                                                         new BinaryOperator("No", "1"),
                                                         new BinaryOperator("Default", true),
                                                         new BinaryOperator("Active", true),
                                                         new BinaryOperator("NumberingHeader", _numberingHeader)));
                                if (_numberingLine != null)
                                {
                                    _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                    _numberingLine.Save();
                                    _result = _numberingLine.FormatedValue;
                                    _numberingLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion Numbering Form

        #region Numbering Berdasarkan Company

        //#region SADP II
        //public string GetNumberingUnlockOptimisticRecord2(IDataLayer _currIDataLayer, ObjectList _currObject, Company _locCompany, QuotationPeriode _locPeriode)
        //{
        //    string _result = null;
        //    Company _company = _locCompany;
        //    try
        //    {
        //        if (_currIDataLayer != null)
        //        {
        //            Session _generatorSession = new(_currIDataLayer);

        //            if (_generatorSession != null)
        //            {
                        
        //                XPCollection<NumberingHeader> _numberingHeaders = new XPCollection<NumberingHeader>
        //                                                        (_generatorSession, new GroupOperator(GroupOperatorType.And,
        //                                                         new BinaryOperator("NumberingType", NumberingType.Objects),
        //                                                         new BinaryOperator("Active", true)));

        //                if (_numberingHeaders != null && _numberingHeaders.Count() > 0)
        //                {
        //                    foreach (NumberingHeader _numberingHeader in _numberingHeaders)
        //                    {
        //                        NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
        //                                                (new GroupOperator(GroupOperatorType.And,
        //                                                 new BinaryOperator("ObjectList", _currObject),
        //                                                 new BinaryOperator("Default", true),
        //                                                 new BinaryOperator("Active", true),
        //                                                 new BinaryOperator("NumberingHeader", _numberingHeader)));
        //                        if (_numberingLine != null)
        //                        {
        //                            if(_locCompany != null)
        //                            {
        //                                string _companyCode = _locCompany.Code;

        //                                if(_locPeriode != null)
        //                                {
        //                                    string _periodeCode = _locPeriode.Code;
        //                                    XPCollection<NumberingLine> _numberingLine2 = new XPCollection<NumberingLine>
        //                                                         (_generatorSession, new GroupOperator(GroupOperatorType.And,
        //                                                         new BinaryOperator("Company.Code", _companyCode),
        //                                                         new BinaryOperator("Default", true),
        //                                                         new BinaryOperator("Active", true),
        //                                                         new BinaryOperator("QuotationPeriode.Code", _periodeCode),
        //                                                         new BinaryOperator("NumberingHeader", _numberingHeader)));

        //                                    if (_numberingLine2 != null && _numberingLine2.Count() > 0)
        //                                    {
        //                                        foreach (NumberingLine _inNumberingLine in _numberingLine2)
        //                                        {
        //                                            _inNumberingLine.LastValue = _inNumberingLine.LastValue + _inNumberingLine.IncrementValue;
        //                                            _inNumberingLine.Save();
        //                                            _result = _inNumberingLine.FormatedValue;
        //                                            _inNumberingLine.Session.CommitTransaction();
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
        //    }
        //    return _result;
        //}

        //#endregion SADP II

        //#region SADP I
        //public string GetNumberingUnlockOptimisticRecord3(IDataLayer _currIDataLayer, ObjectList _currObject, Company _locCompany)
        //{
        //    string _result = null;
        //    Company _company = _locCompany;
        //    int _year = DateTime.Now.Date.Year;
        //    int _month = DateTime.Now.Date.Month;
        //    string _monthRomawi = null;

        //    if (_month == 1)
        //    {
        //        _monthRomawi = "I";
        //    }
        //    else if (_month == 2)
        //    {
        //        _monthRomawi = "II";
        //    }
        //    else if (_month == 3)
        //    {
        //        _monthRomawi = "III";
        //    }
        //    else if (_month == 4)
        //    {
        //        _monthRomawi = "IV";
        //    }
        //    else if (_month == 5)
        //    {
        //        _monthRomawi = "V";
        //    }
        //    else if (_month == 6)
        //    {
        //        _monthRomawi = "VI";
        //    }
        //    else if (_month == 7)
        //    {
        //        _monthRomawi = "VII";
        //    }
        //    else if (_month == 8)
        //    {
        //        _monthRomawi = "VIII";
        //    }
        //    else if (_month == 9)
        //    {
        //        _monthRomawi = "IX";
        //    }
        //    else if (_month == 10)
        //    {
        //        _monthRomawi = "X";
        //    }
        //    else if (_month == 11)
        //    {
        //        _monthRomawi = "XI";
        //    }
        //    else
        //    {
        //        _monthRomawi = "XII";
        //    }
        //    try
        //    {
        //        if (_currIDataLayer != null)
        //        {
        //            Session _generatorSession = new(_currIDataLayer);

        //            if (_generatorSession != null)
        //            {

        //                XPCollection<NumberingHeader> _numberingHeaders = new XPCollection<NumberingHeader>
        //                                                        (_generatorSession, new GroupOperator(GroupOperatorType.And,
        //                                                         new BinaryOperator("NumberingType", NumberingType.Objects),
        //                                                         new BinaryOperator("Active", true)));

        //                if (_numberingHeaders != null && _numberingHeaders.Count() > 0)
        //                {
        //                    foreach (NumberingHeader _numberingHeader in _numberingHeaders)
        //                    {
        //                        NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
        //                                                (new GroupOperator(GroupOperatorType.And,
        //                                                 new BinaryOperator("ObjectList", _currObject),
        //                                                 new BinaryOperator("Default", true),
        //                                                 new BinaryOperator("Active", true),
        //                                                 new BinaryOperator("NumberingHeader", _numberingHeader)));
        //                        if (_numberingLine != null)
        //                        {
        //                            if (_numberingLine.Company != null)
        //                            {
        //                                _company = _locCompany;
        //                                NumberingLine _numberingLine2 = _generatorSession.FindObject<NumberingLine>
        //                                                (new GroupOperator(GroupOperatorType.And,
        //                                                 new BinaryOperator("Company", _company),
        //                                                 new BinaryOperator("Default", true),
        //                                                 new BinaryOperator("Active", true),
        //                                                 new BinaryOperator("NumberingHeader", _numberingHeader)));
        //                                if (_numberingLine2 != null)
        //                                {
        //                                    {
        //                                        _numberingLine2.LastValue = _numberingLine2.LastValue + _numberingLine2.IncrementValue;
        //                                        _numberingLine2.Save();
        //                                        _result = _numberingLine2.FormatedValue + "/" + _monthRomawi + "/"+ _year;
        //                                        _numberingLine2.Session.CommitTransaction();
        //                                    }
        //                                }

        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
        //    }
        //    return _result;
        //}
        //#endregion SADP I


        #endregion Numbering Berdasarkan Company

        #endregion Numbering

        #region ImportData

        public void ImportDataExcel(Session _currSession, string _fullPath, string _ext, ObjectList _objList)
        {
            PropertyInfo[] _propInfo = null;
            object _newObject = null;
            IExcelDataReader excelReader = null;
            DataSet ds = new DataSet();
            Boolean _dataBool;
            try
            {
                FileStream stream = File.Open(_fullPath, FileMode.Open, FileAccess.Read);
                if (_ext == ".xlsx")
                {
                    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                    if (stream != null)
                    {
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                }
                else
                {
                    //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    if (stream != null)
                    {
                        excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                }

                ds = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
                DataTable dt = ds.Tables[0];
                _propInfo = GetPropInfo(_objList);
                excelReader.Close();

                if (_propInfo != null)
                {
                    foreach (DataRow dRow in dt.Rows)
                    {
                        _newObject = GetBusinessObject(_currSession, _objList);
                        if (_newObject != null)
                        {
                            for (int i = 0; i <= dRow.ItemArray.Length - 1; i++)
                            {
                                for (int j = 0; j <= _propInfo.Length - 1; j++)
                                {
                                    if (_propInfo[j].Name.ToLower().Replace(" ", "") == dt.Columns[i].ToString().Trim().ToLower().Replace(" ", ""))
                                    {
                                        #region DefaultProperty

                                        if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(string)))
                                        {
                                            _propInfo[j].SetValue(_newObject, dRow[i].ToString(), null);
                                        }
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(bool)))
                                        {
                                            if (dRow[i].ToString().Trim().ToLower() == "Checked".ToLower())
                                            {
                                                _dataBool = true;
                                            }
                                            else if (dRow[i].ToString().Trim().ToLower() == "TRUE".ToLower())
                                            {
                                                _dataBool = true;
                                            }
                                            else
                                            {
                                                _dataBool = false;
                                            }

                                            _propInfo[j].SetValue(_newObject, _dataBool, null);
                                        }
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(decimal)))
                                        {
                                            _propInfo[j].SetValue(_newObject, Convert.ToDecimal(dRow[i].ToString()), null);
                                        }
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(int)))
                                        {
                                            _propInfo[j].SetValue(_newObject, Convert.ToInt32(dRow[i].ToString()), null);
                                        }
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(double)))
                                        {
                                            _propInfo[j].SetValue(_newObject, Convert.ToDouble(dRow[i].ToString()), null);
                                        }
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(System.DateTime)))
                                        {
                                            if (CheckDate(dRow[i].ToString()))
                                            {
                                                _propInfo[j].SetValue(_newObject, Convert.ToDateTime(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion DefaultProperty

                                        #region Import Excel For Enum
                                        //Import Excel For Enum

                                        #region A

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Agama)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetAgama(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion A
                                        #region B

                                        #endregion B
                                        #region C

                                        #endregion C
                                        #region D

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DocumentShippingType)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetDocumentType(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DocumentRenewalType)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetDocumentRenewalType(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion D
                                        #region F

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FamilyStatus)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetFamilyStatus(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion F
                                        #region G

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Goldar)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetGoldar(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Gender)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetGender(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion G
                                        #region H

                                        #endregion H
                                        #region I

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ItemPosition)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetItemPosition(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion I
                                        #region J

                                        #endregion J
                                        #region K

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(KpiPeriod)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetKpiPeriod(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(KpiFormat)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetKpiFormat(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(KpiValue)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetKpiValue(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(KpiGroupType)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetKpiGroupType(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(KpiYtdType)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetKpiYtdType(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(KpiBscType)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetKpiBscType(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion K
                                        #region L

                                        #endregion L
                                        #region N

                                        #endregion N
                                        #region M

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(MaritalStatus)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetMarital(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion M
                                        #region O

                                        #endregion O
                                        #region P

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PTKP)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetPtkp(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion P
                                        #region R

                                        #endregion R
                                        #region S

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Status)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetStatus(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(StatusOwnershipVessel)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetStatusOwnershipVessel(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(StatusForCrew)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetStatusForCrew(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(StatusCrewOn)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetStatusCrewOn(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(StatusCrewOff)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetStatusCrewOff(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion S
                                        #region T

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TypeForCrewClass)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetTypeForCrewClass(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion T
                                        #region V

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselType)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetVesselType(dRow[i].ToString()), null);
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselHull)))
                                        {
                                            if (dRow[i].ToString() != String.Empty)
                                            {
                                                _propInfo[j].SetValue(_newObject, GetVesselHull(dRow[i].ToString()), null);
                                            }
                                        }

                                        #endregion V
                                        #region W

                                        #endregion W

                                        #endregion Import Excel For Enum

                                        #region Import Excel For Object

                                        #region A



                                        #endregion A
                                        #region B

                                        #endregion B
                                        #region C

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Company)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                Company _lineData = _currSession.FindObject<Company>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Crew)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                Crew _lineData = _currSession.FindObject<Crew>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CrewCertificateOfCompetence)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                CrewCertificateOfCompetence _lineData = _currSession.FindObject<CrewCertificateOfCompetence>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CrewContract)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                CrewContract _lineData = _currSession.FindObject<CrewContract>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CrewDocument)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                CrewDocument _lineData = _currSession.FindObject<CrewDocument>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CrewFamily)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                CrewFamily _lineData = _currSession.FindObject<CrewFamily>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CrewClassList)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                CrewClassList _lineData = _currSession.FindObject<CrewClassList>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CrewSafemanning)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                CrewSafemanning _lineData = _currSession.FindObject<CrewSafemanning>
                                                    (new BinaryOperator("JobPosition", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        #endregion C
                                        #region D

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Dock)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                Dock _lineData = _currSession.FindObject<Dock>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DocumentShipping)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                DocumentShipping _lineData = _currSession.FindObject<DocumentShipping>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DocumentShippingAgency)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                DocumentShippingAgency _lineData = _currSession.FindObject<DocumentShippingAgency>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        #endregion D
                                        #region E
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Employee)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                Employee _lineData = _currSession.FindObject<Employee>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }
                                        #endregion E
                                        #region F

                                        #endregion F
                                        #region H

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterChecklistHsse)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterChecklistHsse _lineData = _currSession.FindObject<ShippingMasterChecklistHsse>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        #endregion H
                                        #region I

                                        #endregion I
                                        #region J
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(JobPosition)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                JobPosition _lineData = _currSession.FindObject<JobPosition>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        #endregion J
                                        #region K

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(KpiRole)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                KpiRole _lineData = _currSession.FindObject<KpiRole>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(KpiRoleLine)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                KpiRoleLine _lineData = _currSession.FindObject<KpiRoleLine>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(KpiRoleGrading)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                KpiRoleGrading _lineData = _currSession.FindObject<KpiRoleGrading>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(KpiRoleCascadingLine)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                KpiRoleCascadingLine _lineData = _currSession.FindObject<KpiRoleCascadingLine>
                                                    (new BinaryOperator("KpiRoleLineSelect", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(KpiYear)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                KpiYear _lineData = _currSession.FindObject<KpiYear>
                                                    (new BinaryOperator("Year", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        #endregion K
                                        #region L

                                        #endregion L
                                        #region M

                                        #endregion M
                                        #region N

                                        #endregion N
                                        #region O

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(OrganizationDimension1)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                OrganizationDimension1 _lineData = _currSession.FindObject<OrganizationDimension1>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(OrganizationDimension2)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                OrganizationDimension2 _lineData = _currSession.FindObject<OrganizationDimension2>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }
                                        #endregion O
                                        #region P

                                        #endregion P
                                        #region R

                                        #endregion R
                                        #region S

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingHeadInspection)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingHeadInspection _lineData = _currSession.FindObject<ShippingHeadInspection>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingHeadInspectionLine)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingHeadInspectionLine _lineData = _currSession.FindObject<ShippingHeadInspectionLine>
                                                    (new BinaryOperator("Item", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterCargoContract)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterCargoContract _lineData = _currSession.FindObject<ShippingMasterCargoContract>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterChecklistHsse)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterChecklistHsse _lineData = _currSession.FindObject<ShippingMasterChecklistHsse>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterCustomer)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterCustomer _lineData = _currSession.FindObject<ShippingMasterCustomer>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterDocRenewAbout)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterDocRenewAbout _lineData = _currSession.FindObject<ShippingMasterDocRenewAbout>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterInspection)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterInspection _lineData = _currSession.FindObject<ShippingMasterInspection>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterInstitusi)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterInstitusi _lineData = _currSession.FindObject<ShippingMasterInstitusi>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterLocation)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterLocation _lineData = _currSession.FindObject<ShippingMasterLocation>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        //category
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterPartVesselCategory)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterPartVesselCategory _lineData = _currSession.FindObject<ShippingMasterPartVesselCategory>
                                                    (new BinaryOperator("CategoryName", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        //sub-category
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterPartVesselSubCategory)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterPartVesselSubCategory _lineData = _currSession.FindObject<ShippingMasterPartVesselSubCategory>
                                                    (new BinaryOperator("SubCategoryName", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        //item checklist
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterPartVesselUnit)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterPartVesselUnit _lineData = _currSession.FindObject<ShippingMasterPartVesselUnit>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        //accounting account
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterPaymentRequestAccount)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterPaymentRequestAccount _lineData = _currSession.FindObject<ShippingMasterPaymentRequestAccount>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        //accounting project
                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterPaymentRequestProject)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterPaymentRequestProject _lineData = _currSession.FindObject<ShippingMasterPaymentRequestProject>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }


                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingMasterPort)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingMasterPort _lineData = _currSession.FindObject<ShippingMasterPort>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingPaymentRequest)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingPaymentRequest _lineData = _currSession.FindObject<ShippingPaymentRequest>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingVesselDailySetHead)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingVesselDailySetHead _lineData = _currSession.FindObject<ShippingVesselDailySetHead>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingVesselDailySetLineDeck)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingVesselDailySetLineDeck _lineData = _currSession.FindObject<ShippingVesselDailySetLineDeck>
                                                    (new BinaryOperator("Item", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShippingVesselDailySetLineEngine)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                ShippingVesselDailySetLineEngine _lineData = _currSession.FindObject<ShippingVesselDailySetLineEngine>
                                                    (new BinaryOperator("Item", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        #endregion S
                                        #region T

                                        #endregion T
                                        #region U

                                        #endregion U
                                        #region V

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Vessel)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                Vessel _lineData = _currSession.FindObject<Vessel>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselDamageReport)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselDamageReport _lineData = _currSession.FindObject<VesselDamageReport>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselDamageReportSpare)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselDamageReportSpare _lineData = _currSession.FindObject<VesselDamageReportSpare>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselDocking)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselDocking _lineData = _currSession.FindObject<VesselDocking>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselDockingLineActual)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselDockingLineActual _lineData = _currSession.FindObject<VesselDockingLineActual>
                                                    (new BinaryOperator("WorkDescription", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselDockingLinePlan)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselDockingLinePlan _lineData = _currSession.FindObject<VesselDockingLinePlan>
                                                    (new BinaryOperator("WorkDescription", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselDocument)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselDocument _lineData = _currSession.FindObject<VesselDocument>
                                                    (new BinaryOperator("Document", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselDocumentFigure)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselDocumentFigure _lineData = _currSession.FindObject<VesselDocumentFigure>
                                                    (new BinaryOperator("Document", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselDocumentRenewal)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselDocumentRenewal _lineData = _currSession.FindObject<VesselDocumentRenewal>
                                                    (new BinaryOperator("RefNumber", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselDocumentRenewalLine)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselDocumentRenewalLine _lineData = _currSession.FindObject<VesselDocumentRenewalLine>
                                                    (new BinaryOperator("Document", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselInspection)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselInspection _lineData = _currSession.FindObject<VesselInspection>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselInspectionLine)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselInspectionLine _lineData = _currSession.FindObject<VesselInspectionLine>
                                                    (new BinaryOperator("Item", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselPart)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselPart _lineData = _currSession.FindObject<VesselPart>
                                                    (new BinaryOperator("Item", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselRepairList)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselRepairList _lineData = _currSession.FindObject<VesselRepairList>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselRepairListLine)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselRepairListLine _lineData = _currSession.FindObject<VesselRepairListLine>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselSpk)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselSpk _lineData = _currSession.FindObject<VesselSpk>
                                                    (new BinaryOperator("Name", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselSpkLine)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselSpkLine _lineData = _currSession.FindObject<VesselSpkLine>
                                                    (new BinaryOperator("VesselPart", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VesselSpkLineProgress)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VesselSpkLineProgress _lineData = _currSession.FindObject<VesselSpkLineProgress>
                                                    (new BinaryOperator("VesselPart", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Voyage)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                Voyage _lineData = _currSession.FindObject<Voyage>
                                                    (new BinaryOperator("NumVoyage", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VoyageBunker)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VoyageBunker _lineData = _currSession.FindObject<VoyageBunker>
                                                    (new BinaryOperator("Code", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VoyageCargo)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VoyageCargo _lineData = _currSession.FindObject<VoyageCargo>
                                                    (new BinaryOperator("Code", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VoyageCycleTime)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VoyageCycleTime _lineData = _currSession.FindObject<VoyageCycleTime>
                                                    (new BinaryOperator("Code", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(VoyageRoute)))
                                        {
                                            if (dRow[i].ToString() != null)
                                            {
                                                VoyageRoute _lineData = _currSession.FindObject<VoyageRoute>
                                                    (new BinaryOperator("Code", dRow[i].ToString()));
                                                if (_lineData != null)
                                                {
                                                    _propInfo[j].SetValue(_newObject, _lineData, null);
                                                }
                                            }
                                        }

                                        #endregion V
                                        #region W

                                        #endregion W

                                        #endregion Import Excel For Object

                                    }
                                }
                            }
                            _currSession.Save(_newObject);
                            _currSession.CommitTransaction();
                        }

                    }
                    //_currSession.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
        }

        private PropertyInfo[] GetPropInfo(ObjectList _objList)
        {
            PropertyInfo[] _result = null;
            try
            {
                #region PropertyInfo A

                #endregion PropertyInfo A

                #region PropertyInfo B

                //if (_objList == ObjectList.BusinessPartner)
                //{
                //    _result = typeof(BusinessPartner).GetProperties();
                //}

                #endregion PropertyInfo B

                #region PropertyInfo C
                if (_objList == ObjectList.Company)
                {
                    _result = typeof(Company).GetProperties();
                }

                //crewing
                if (_objList == ObjectList.Crew)
                {
                    _result = typeof(Crew).GetProperties();
                }

                if (_objList == ObjectList.CrewCertificateOfCompetence)
                {
                    _result = typeof(CrewCertificateOfCompetence).GetProperties();
                }

                if (_objList == ObjectList.CrewClassList)
                {
                    _result = typeof(CrewClassList).GetProperties();
                }

                if (_objList == ObjectList.CrewContract)
                {
                    _result = typeof(CrewContract).GetProperties();
                }

                if (_objList == ObjectList.CrewDocument)
                {
                    _result = typeof(CrewDocument).GetProperties();
                }

                if (_objList == ObjectList.CrewFamily)
                {
                    _result = typeof(CrewFamily).GetProperties();
                }

                if (_objList == ObjectList.CrewSafemanning)
                {
                    _result = typeof(CrewSafemanning).GetProperties();
                }

                #endregion PropertyInfo C

                #region PropertyInfo D

                else if (_objList == ObjectList.Dock)
                {
                    _result = typeof(DocumentShipping).GetProperties();
                }

                else if (_objList == ObjectList.DocumentShipping)
                {
                    _result = typeof(DocumentShipping).GetProperties();
                }

                else if (_objList == ObjectList.DocumentShippingAgency)
                {
                    _result = typeof(DocumentShippingAgency).GetProperties();
                }

                #endregion PropertyInfo D

                #region PropertyInfo E
                else if (_objList == ObjectList.Employee)
                {
                    _result = typeof(Employee).GetProperties();
                }
                #endregion PropertyInfo E

                #region PropertyInfo F

                #endregion PropertyInfo F

                #region PropertyInfo G

                #endregion PropertyInfo G

                #region PropertyInfo H

                #endregion PropertyInfo H

                #region PropertyInfo I


                #endregion PropertyInfo I

                #region PropertyInfo J                
                else if (_objList == ObjectList.JobPosition)
                {
                    _result = typeof(JobPosition).GetProperties();
                }
                #endregion PropertyInfo J

                #region PropertyInfo K
                else if (_objList == ObjectList.KpiRole)
                {
                    _result = typeof(KpiRole).GetProperties();
                }
                else if (_objList == ObjectList.KpiRoleLine)
                {
                    _result = typeof(KpiRoleLine).GetProperties();
                }
                else if (_objList == ObjectList.KpiRoleGrading)
                {
                    _result = typeof(KpiRoleGrading).GetProperties();
                }
                else if (_objList == ObjectList.KpiRoleCascadingLine)
                {
                    _result = typeof(KpiRoleCascadingLine).GetProperties();
                }
                else if (_objList == ObjectList.KpiYear)
                {
                    _result = typeof(KpiYear).GetProperties();
                }
                #endregion PropertyInfo K

                #region PropertyInfo L


                #endregion PropertyInfo L

                #region PropertyInfo M

                #endregion PropertyInfo M

                #region PropertyInfo N

                #endregion  PropertyInfo N

                #region PropertyInfo O

                else if (_objList == ObjectList.OrganizationDimension1)
                {
                    _result = typeof(OrganizationDimension1).GetProperties();
                }

                #endregion PropertyInfo O

                #region PropertyInfo P


                #endregion PropertyInfo P

                #region PropertyInfo Q

                else if (_objList == ObjectList.QuotationCustomer)
                {
                    _result = typeof(QuotationCustomer).GetProperties();
                }

                #endregion PropertyInfo Q

                #region PropertyInfo R

                #endregion PropertyInfo R

                #region PropertyInfo S

                else if (_objList == ObjectList.ShippingHeadInspection)
                {
                    _result = typeof(ShippingHeadInspection).GetProperties();
                }

                else if (_objList == ObjectList.ShippingHeadInspectionLine)
                {
                    _result = typeof(ShippingHeadInspectionLine).GetProperties();
                }

                else if (_objList == ObjectList.ShippingMasterCargoContract)
                {
                    _result = typeof(ShippingMasterCargoContract).GetProperties();
                }

                else if (_objList == ObjectList.ShippingMasterChecklistHsse)
                {
                    _result = typeof(ShippingMasterChecklistHsse).GetProperties();
                }

                if (_objList == ObjectList.ShippingMasterCustomer)
                {
                    _result = typeof(ShippingMasterCustomer).GetProperties();
                }

                if (_objList == ObjectList.ShippingMasterDocRenewAbout)
                {
                    _result = typeof(ShippingMasterDocRenewAbout).GetProperties();
                }

                else if (_objList == ObjectList.ShippingMasterInspection)
                {
                    _result = typeof(ShippingMasterInspection).GetProperties();
                }

                else if (_objList == ObjectList.ShippingMasterInstitusi)
                {
                    _result = typeof(ShippingMasterInstitusi).GetProperties();
                }

                else if (_objList == ObjectList.ShippingMasterLocation)
                {
                    _result = typeof(ShippingMasterLocation).GetProperties();
                }

                if (_objList == ObjectList.ShippingMasterPartVesselCategory)
                {
                    _result = typeof(ShippingMasterPartVesselCategory).GetProperties();
                }

                if (_objList == ObjectList.ShippingMasterPartVesselSubCategory)
                {
                    _result = typeof(ShippingMasterPartVesselSubCategory).GetProperties();
                }

                if (_objList == ObjectList.ShippingMasterPartVesselUnit)
                {
                    _result = typeof(ShippingMasterPartVesselUnit).GetProperties();
                }

                if (_objList == ObjectList.ShippingMasterPaymentRequestAccount)
                {
                    _result = typeof(ShippingMasterPaymentRequestAccount).GetProperties();
                }

                if (_objList == ObjectList.ShippingMasterPaymentRequestProject)
                {
                    _result = typeof(ShippingMasterPaymentRequestProject).GetProperties();
                }

                else if (_objList == ObjectList.ShippingMasterPort)
                {
                    _result = typeof(ShippingMasterPort).GetProperties();
                }

                else if (_objList == ObjectList.ShippingPaymentRequest)
                {
                    _result = typeof(ShippingMasterPort).GetProperties();
                }

                else if (_objList == ObjectList.ShippingVesselDailySetHead)
                {
                    _result = typeof(ShippingVesselDailySetHead).GetProperties();
                }

                else if (_objList == ObjectList.ShippingVesselDailySetLineDeck)
                {
                    _result = typeof(ShippingVesselDailySetLineDeck).GetProperties();
                }

                else if (_objList == ObjectList.ShippingVesselDailySetLineEngine)
                {
                    _result = typeof(ShippingVesselDailySetLineEngine).GetProperties();
                }

                #endregion PropertyInfo S

                #region PropertyInfo T

                #endregion PropertyInfo T

                #region PropertyInfo U

                #endregion PropertyInfo U

                #region PropertyInfo V 

                else if (_objList == ObjectList.Vessel)
                {
                    _result = typeof(Vessel).GetProperties();
                }

                else if (_objList == ObjectList.VesselDamageReport)
                {
                    _result = typeof(VesselDamageReport).GetProperties();
                }

                else if (_objList == ObjectList.VesselDamageReportSpare)
                {
                    _result = typeof(VesselDamageReportSpare).GetProperties();
                }

                else if (_objList == ObjectList.VesselDocking)
                {
                    _result = typeof(VesselDocking).GetProperties();
                }

                else if (_objList == ObjectList.VesselDockingLineActual)
                {
                    _result = typeof(VesselDockingLineActual).GetProperties();
                }

                else if (_objList == ObjectList.VesselDockingLinePlan)
                {
                    _result = typeof(VesselDockingLinePlan).GetProperties();
                }

                else if (_objList == ObjectList.VesselDocument)
                {
                    _result = typeof(VesselDocument).GetProperties();
                }

                else if (_objList == ObjectList.VesselDocumentFigure)
                {
                    _result = typeof(VesselDocumentFigure).GetProperties();
                }

                else if (_objList == ObjectList.VesselInspection)
                {
                    _result = typeof(VesselInspection).GetProperties();
                }

                else if (_objList == ObjectList.VesselInspectionLine)
                {
                    _result = typeof(VesselInspectionLine).GetProperties();
                }

                else if (_objList == ObjectList.VesselPart)
                {
                    _result = typeof(VesselPart).GetProperties();
                }

                else if (_objList == ObjectList.VesselRepairList)
                {
                    _result = typeof(VesselRepairList).GetProperties();
                }

                else if (_objList == ObjectList.VesselRepairListLine)
                {
                    _result = typeof(VesselRepairListLine).GetProperties();
                }

                else if (_objList == ObjectList.VesselSpk)
                {
                    _result = typeof(VesselSpk).GetProperties();
                }

                else if (_objList == ObjectList.VesselSpkLine)
                {
                    _result = typeof(VesselSpkLine).GetProperties();
                }

                else if (_objList == ObjectList.VesselSpkLineProgress)
                {
                    _result = typeof(VesselSpkLineProgress).GetProperties();
                }

                else if (_objList == ObjectList.Voyage)
                {
                    _result = typeof(Voyage).GetProperties();
                }

                else if (_objList == ObjectList.VoyageBunker)
                {
                    _result = typeof(VoyageBunker).GetProperties();
                }

                else if (_objList == ObjectList.VoyageCargo)
                {
                    _result = typeof(VoyageCargo).GetProperties();
                }

                else if (_objList == ObjectList.VoyageCycleTime)
                {
                    _result = typeof(VoyageCycleTime).GetProperties();
                }

                else if (_objList == ObjectList.VoyageRoute)
                {
                    _result = typeof(VoyageRoute).GetProperties();
                }

                #endregion PropertyInfo V

                #region PropertyInfo W

                #endregion PropertyInfo W

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        private object GetBusinessObject(Session _currSession, ObjectList _objList)
        {
            object _result = null;
            try
            {
                #region GetBusinessObject A

                #endregion GetBusinessObject A

                #region GetBusinessObject B

                //if (_objList == ObjectList.BusinessPartner)
                //{
                //    _result = (BusinessPartner)Activator.CreateInstance(typeof(BusinessPartner), _currSession);
                //}

                #endregion GetBusinessObject B

                #region GetBusinessObject C
                if (_objList == ObjectList.Company)
                {
                    _result = (Company)Activator.CreateInstance(typeof(Company), _currSession);
                }

                // Crewing
                if (_objList == ObjectList.Crew)
                {
                    _result = (Crew)Activator.CreateInstance(typeof(Crew), _currSession);
                }

                if (_objList == ObjectList.CrewCertificateOfCompetence)
                {
                    _result = (CrewCertificateOfCompetence)Activator.CreateInstance(typeof(CrewCertificateOfCompetence), _currSession);
                }

                if (_objList == ObjectList.CrewClassList)
                {
                    _result = (CrewClassList)Activator.CreateInstance(typeof(CrewClassList), _currSession);
                }

                if (_objList == ObjectList.CrewContract)
                {
                    _result = (CrewContract)Activator.CreateInstance(typeof(CrewContract), _currSession);
                }

                if (_objList == ObjectList.CrewDocument)
                {
                    _result = (CrewDocument)Activator.CreateInstance(typeof(CrewDocument), _currSession);
                }

                if (_objList == ObjectList.CrewFamily)
                {
                    _result = (CrewFamily)Activator.CreateInstance(typeof(CrewFamily), _currSession);
                }

                if (_objList == ObjectList.CrewSafemanning)
                {
                    _result = (CrewSafemanning)Activator.CreateInstance(typeof(CrewSafemanning), _currSession);
                }

                #endregion GetBusinessObject C

                #region GetBusinessObject D

                else if (_objList == ObjectList.DocumentShipping)
                {
                    _result = (DocumentShipping)Activator.CreateInstance(typeof(DocumentShipping), _currSession);
                }

                else if (_objList == ObjectList.DocumentShippingAgency)
                {
                    _result = (DocumentShippingAgency)Activator.CreateInstance(typeof(DocumentShippingAgency), _currSession);
                }

                #endregion GetBusinessObject D

                #region GetBusinessObject E
                else if (_objList == ObjectList.Employee)
                {
                    _result = (Employee)Activator.CreateInstance(typeof(Employee), _currSession);
                }
                #endregion GetBusinessObject E

                #region GetBusinessObject F

                #endregion GetBusinessObject F

                #region GetBusinessObject G

                #endregion GetBusinessObject G

                #region GetBusinessObject H

                #endregion GetBusinessObject H

                #region GetBusinessObject I

                #endregion GetBusinessObject I

                #region GetBusinessObject J                
                else if (_objList == ObjectList.JobPosition)
                {
                    _result = (JobPosition)Activator.CreateInstance(typeof(JobPosition), _currSession);
                }
                #endregion GetBusinessObject J

                #region GetBusinessObject K
                else if (_objList == ObjectList.KpiRole)
                {
                    _result = (KpiRole)Activator.CreateInstance(typeof(KpiRole), _currSession);
                }
                else if (_objList == ObjectList.KpiRoleLine)
                {
                    _result = (KpiRoleLine)Activator.CreateInstance(typeof(KpiRoleLine), _currSession);
                }
                else if (_objList == ObjectList.KpiRoleGrading)
                {
                    _result = (KpiRoleGrading)Activator.CreateInstance(typeof(KpiRoleGrading), _currSession);
                }
                else if (_objList == ObjectList.KpiRoleCascadingLine)
                {
                    _result = (KpiRoleCascadingLine)Activator.CreateInstance(typeof(KpiRoleCascadingLine), _currSession);
                }
                else if (_objList == ObjectList.KpiYear)
                {
                    _result = (KpiYear)Activator.CreateInstance(typeof(KpiYear), _currSession);
                }


                #endregion GetBusinessObject K

                #region GetBusinessObject L

                #endregion GetBusinessObject L

                #region GetBusinessObject M
                #endregion GetBusinessObject M

                #region GetBusinessObject N

                //else if (_objList == ObjectList.NumberingHeader)
                //{
                //    _result = (NumberingHeader)Activator.CreateInstance(typeof(NumberingHeader), _currSession);
                //}
                //else if (_objList == ObjectList.NumberingLine)
                //{
                //    _result = (NumberingLine)Activator.CreateInstance(typeof(NumberingLine), _currSession);
                //}
                #endregion GetBusinessObject N

                #region GetBusinessObject O

                else if (_objList == ObjectList.OrganizationDimension1)
                {
                    _result = (OrganizationDimension1)Activator.CreateInstance(typeof(OrganizationDimension1), _currSession);
                }

                #endregion GetBusinessObject O

                #region GetBusinessObject P


                #endregion GetBusinessObject P

                #region GetBusinessObject Q

                else if (_objList == ObjectList.QuotationCustomer)
                {
                    _result = (QuotationCustomer)Activator.CreateInstance(typeof(QuotationCustomer), _currSession);
                }

                #endregion GetBusinessObject Q

                #region GetBusinessObject R

                #endregion GetBusinessObject R

                #region GetBusinessObject S

                else if (_objList == ObjectList.ShippingHeadInspection)
                {
                    _result = (ShippingHeadInspection)Activator.CreateInstance(typeof(ShippingHeadInspection), _currSession);
                }

                else if (_objList == ObjectList.ShippingHeadInspectionLine)
                {
                    _result = (ShippingHeadInspectionLine)Activator.CreateInstance(typeof(ShippingHeadInspectionLine), _currSession);
                }

                else if (_objList == ObjectList.ShippingMasterCargoContract)
                {
                    _result = (ShippingMasterCargoContract)Activator.CreateInstance(typeof(ShippingMasterCargoContract), _currSession);
                }

                else if (_objList == ObjectList.ShippingMasterChecklistHsse)
                {
                    _result = (ShippingMasterChecklistHsse)Activator.CreateInstance(typeof(ShippingMasterChecklistHsse), _currSession);
                }

                else if (_objList == ObjectList.ShippingMasterCustomer)
                {
                    _result = (ShippingMasterCustomer)Activator.CreateInstance(typeof(ShippingMasterCustomer), _currSession);
                }

                else if (_objList == ObjectList.ShippingMasterDocRenewAbout)
                {
                    _result = (ShippingMasterDocRenewAbout)Activator.CreateInstance(typeof(ShippingMasterDocRenewAbout), _currSession);
                }

                else if (_objList == ObjectList.ShippingMasterInspection)
                {
                    _result = (ShippingMasterInspection)Activator.CreateInstance(typeof(ShippingMasterInspection), _currSession);
                }

                else if (_objList == ObjectList.ShippingMasterInstitusi)
                {
                    _result = (ShippingMasterInstitusi)Activator.CreateInstance(typeof(ShippingMasterInstitusi), _currSession);
                }

                else if (_objList == ObjectList.ShippingMasterLocation)
                {
                    _result = (ShippingMasterLocation)Activator.CreateInstance(typeof(ShippingMasterLocation), _currSession);
                }

                if (_objList == ObjectList.ShippingMasterPartVesselCategory)
                {
                    _result = (ShippingMasterPartVesselCategory)Activator.CreateInstance(typeof(ShippingMasterPartVesselCategory), _currSession);
                }

                if (_objList == ObjectList.ShippingMasterPartVesselSubCategory)
                {
                    _result = (ShippingMasterPartVesselSubCategory)Activator.CreateInstance(typeof(ShippingMasterPartVesselSubCategory), _currSession);
                }

                if (_objList == ObjectList.ShippingMasterPartVesselUnit)
                {
                    _result = (ShippingMasterPartVesselUnit)Activator.CreateInstance(typeof(ShippingMasterPartVesselUnit), _currSession);
                }

                else if (_objList == ObjectList.ShippingMasterPaymentRequestAccount)
                {
                    _result = (ShippingMasterPaymentRequestAccount)Activator.CreateInstance(typeof(ShippingMasterPaymentRequestAccount), _currSession);
                }

                else if (_objList == ObjectList.ShippingMasterPaymentRequestProject)
                {
                    _result = (ShippingMasterPaymentRequestProject)Activator.CreateInstance(typeof(ShippingMasterPaymentRequestProject), _currSession);
                }

                else if (_objList == ObjectList.ShippingMasterPort)
                {
                    _result = (ShippingMasterPort)Activator.CreateInstance(typeof(ShippingMasterPort), _currSession);
                }

                else if (_objList == ObjectList.ShippingPaymentRequest)
                {
                    _result = (ShippingPaymentRequest)Activator.CreateInstance(typeof(ShippingPaymentRequest), _currSession);
                }

                if (_objList == ObjectList.ShippingVesselDailySetHead)
                {
                    _result = (ShippingVesselDailySetHead)Activator.CreateInstance(typeof(ShippingVesselDailySetHead), _currSession);
                }

                if (_objList == ObjectList.ShippingVesselDailySetLineDeck)
                {
                    _result = (ShippingVesselDailySetLineDeck)Activator.CreateInstance(typeof(ShippingVesselDailySetLineDeck), _currSession);
                }

                if (_objList == ObjectList.ShippingVesselDailySetLineEngine)
                {
                    _result = (ShippingVesselDailySetLineEngine)Activator.CreateInstance(typeof(ShippingVesselDailySetLineEngine), _currSession);
                }

                #endregion GetBusinessObject S

                #region GetBusinessObject T

                #endregion GetBusinessObject T

                #region GetBusinessObject U

                #endregion GetBusinessObject U

                #region GetBusinessObject V

                else if (_objList == ObjectList.Vessel)
                {
                    _result = (Vessel)Activator.CreateInstance(typeof(Vessel), _currSession);
                }

                else if (_objList == ObjectList.VesselDamageReport)
                {
                    _result = (VesselDamageReport)Activator.CreateInstance(typeof(VesselDamageReport), _currSession);
                }

                else if (_objList == ObjectList.VesselDamageReportSpare)
                {
                    _result = (VesselDamageReport)Activator.CreateInstance(typeof(VesselDamageReportSpare), _currSession);
                }

                else if (_objList == ObjectList.VesselDocking)
                {
                    _result = (VesselDocking)Activator.CreateInstance(typeof(VesselDocking), _currSession);
                }

                else if (_objList == ObjectList.VesselDockingLineActual)
                {
                    _result = (VesselDockingLineActual)Activator.CreateInstance(typeof(VesselDockingLineActual), _currSession);
                }

                else if (_objList == ObjectList.VesselDockingLinePlan)
                {
                    _result = (VesselDockingLinePlan)Activator.CreateInstance(typeof(VesselDockingLinePlan), _currSession);
                }

                else if (_objList == ObjectList.VesselDocument)
                {
                    _result = (VesselDocument)Activator.CreateInstance(typeof(VesselDocument), _currSession);
                }

                else if (_objList == ObjectList.VesselDocumentFigure)
                {
                    _result = (VesselDocumentFigure)Activator.CreateInstance(typeof(VesselDocumentFigure), _currSession);
                }

                else if (_objList == ObjectList.VesselInspection)
                {
                    _result = (VesselInspection)Activator.CreateInstance(typeof(VesselInspection), _currSession);
                }

                else if (_objList == ObjectList.VesselInspectionLine)
                {
                    _result = (VesselInspectionLine)Activator.CreateInstance(typeof(VesselInspectionLine), _currSession);
                }

                else if (_objList == ObjectList.VesselPart)
                {
                    _result = (VesselPart)Activator.CreateInstance(typeof(VesselPart), _currSession);
                }

                else if (_objList == ObjectList.VesselRepairList)
                {
                    _result = (VesselRepairList)Activator.CreateInstance(typeof(VesselRepairList), _currSession);
                }

                else if (_objList == ObjectList.VesselRepairListLine)
                {
                    _result = (VesselRepairListLine)Activator.CreateInstance(typeof(VesselRepairListLine), _currSession);
                }

                else if (_objList == ObjectList.VesselSpk)
                {
                    _result = (VesselSpk)Activator.CreateInstance(typeof(VesselSpk), _currSession);
                }

                else if (_objList == ObjectList.VesselSpkLine)
                {
                    _result = (VesselSpkLine)Activator.CreateInstance(typeof(VesselSpkLine), _currSession);
                }

                else if (_objList == ObjectList.VesselSpkLineProgress)
                {
                    _result = (VesselSpkLineProgress)Activator.CreateInstance(typeof(VesselSpkLineProgress), _currSession);
                }

                else if (_objList == ObjectList.Voyage)
                {
                    _result = (Voyage)Activator.CreateInstance(typeof(Voyage), _currSession);
                }

                else if (_objList == ObjectList.VoyageBunker)
                {
                    _result = (VoyageBunker)Activator.CreateInstance(typeof(VoyageBunker), _currSession);
                }

                else if (_objList == ObjectList.VoyageCargo)
                {
                    _result = (VoyageCargo)Activator.CreateInstance(typeof(VoyageCargo), _currSession);
                }

                else if (_objList == ObjectList.VoyageCycleTime)
                {
                    _result = (VoyageCycleTime)Activator.CreateInstance(typeof(VoyageCycleTime), _currSession);
                }

                else if (_objList == ObjectList.VoyageRoute)
                {
                    _result = (VoyageRoute)Activator.CreateInstance(typeof(VoyageRoute), _currSession);
                }

                #endregion GetBusinessObject V

                #region GetBusinessObject W

                #endregion GetBusinessObject W

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public XPCollection<ApplicationApprovalSetting> GetApplicationApprovalSettingByApprovalLevel(Session currentSession, UserAccess _locUserAccess,
                         ObjectList _locObjectList, Company _locCompany, OrganizationDimension1 _locOrgDim1, OrganizationDimension2 _locOrgDim2, OrganizationDimension3 _locOrgDim3)
        {
            XPCollection<ApplicationApprovalSetting> _result = null;
            Company _localCompany = null;
            OrganizationDimension1 _localOrgDim1 = null;
            OrganizationDimension2 _localOrgDim2 = null;
            OrganizationDimension3 _localOrgDim3 = null;

            try
            {
                if (_locCompany != null) { _localCompany = _locCompany; }
                if (_locOrgDim1 != null) { _localOrgDim1 = _locOrgDim1; }
                if (_locOrgDim2 != null) { _localOrgDim2 = _locOrgDim2; }
                if (_locOrgDim3 != null) { _localOrgDim3 = _locOrgDim3; }


                ApplicationSetting _appSetting = currentSession.FindObject<ApplicationSetting>(new GroupOperator(GroupOperatorType.And,
                                               new BinaryOperator("Active", true)));
                if (_appSetting != null)
                {
                    //Cari dulu Application Setup Detail
                    XPCollection<ApplicationActionSetting> _locApplicationActionSetting1s = new XPCollection<ApplicationActionSetting>
                                                                        (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locCompany),
                                                                        new BinaryOperator("ApplicationSetting", _appSetting),
                                                                        new BinaryOperator("ApplicationActionList", ApplicationActionList.Approval),
                                                                        new BinaryOperator("ObjectList", _locObjectList),
                                                                        new BinaryOperator("Active", true)));

                    if (_locApplicationActionSetting1s != null && _locApplicationActionSetting1s.Count() > 0)
                    {
                        foreach (ApplicationActionSetting _locApplicationActionSetting1 in _locApplicationActionSetting1s)
                        {

                            #region ByCompany
                            if (_locApplicationActionSetting1.FilterListSetup == FilterListSetup.ByCompany)
                            {
                                XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                         (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                          new BinaryOperator("Company", _localCompany),
                                                                                          new BinaryOperator("ObjectList", _locObjectList),
                                                                                          new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                          new BinaryOperator("UserAccess", _locUserAccess),
                                                                                          new BinaryOperator("Active", true)));
                                if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                {
                                    _result = _locAppApprovalSettings;
                                }

                            }
                            #endregion ByCompany


                            #region ByCompanyAndWorkplace
                            if (_locApplicationActionSetting1.FilterListSetup == FilterListSetup.ByCompanyAndWorkplace)
                            {
                                XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                         (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                          new BinaryOperator("Company", _localCompany),
                                                                                          new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                          new BinaryOperator("ObjectList", _locObjectList),
                                                                                          new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                          new BinaryOperator("UserAccess", _locUserAccess),
                                                                                          new BinaryOperator("Active", true)));
                                if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                {
                                    _result = _locAppApprovalSettings;
                                }

                            }
                            #endregion ByCompanyAndWorkplace


                            #region ByCompanyAndWorkplaceAndDepartment
                            if (_locApplicationActionSetting1.FilterListSetup == FilterListSetup.ByCompanyAndWorkplaceAndDepartment)
                            {
                                XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                         (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                          new BinaryOperator("Company", _localCompany),
                                                                                          new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                          new BinaryOperator("OrgDim2", _localOrgDim2),
                                                                                          new BinaryOperator("ObjectList", _locObjectList),
                                                                                          new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                          new BinaryOperator("UserAccess", _locUserAccess),
                                                                                          new BinaryOperator("Active", true)));
                                if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                {
                                    _result = _locAppApprovalSettings;
                                }

                            }
                            #endregion ByCompanyAndWorkplaceAndDepartment



                            #region ByCompanyAndWorkplaceAndDepartmentAndDivision
                            if (_locApplicationActionSetting1.FilterListSetup == FilterListSetup.ByCompanyAndWorkplaceAndDepartmentAndDivision)
                            {
                                XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                         (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                          new BinaryOperator("Company", _localCompany),
                                                                                          new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                          new BinaryOperator("OrgDim2", _localOrgDim2),
                                                                                          new BinaryOperator("OrgDim3", _localOrgDim3),
                                                                                          new BinaryOperator("ObjectList", _locObjectList),
                                                                                          new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                          new BinaryOperator("UserAccess", _locUserAccess),
                                                                                          new BinaryOperator("Active", true)));
                                if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                {
                                    _result = _locAppApprovalSettings;
                                }

                            }
                            #endregion ByCompanyAndWorkplaceAndDepartmentAndDivision
                        }
                    }
                    else
                    {
                        XPCollection<ApplicationActionSetting> _locApplicationActionSetting2s = new XPCollection<ApplicationActionSetting>
                                                                        (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _locCompany),
                                                                        new BinaryOperator("ApplicationSetting", _appSetting),
                                                                        new BinaryOperator("ApplicationActionList", ApplicationActionList.Approval),
                                                                        new BinaryOperator("ObjectList", ObjectList.None),
                                                                        new BinaryOperator("Active", true)));

                        if (_locApplicationActionSetting2s != null && _locApplicationActionSetting2s.Count() > 0)
                        {
                            foreach (ApplicationActionSetting _locApplicationActionSetting2 in _locApplicationActionSetting2s)
                            {
                                #region ByCompany
                                if (_locApplicationActionSetting2.FilterListSetup == FilterListSetup.ByCompany)
                                {
                                    XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                             (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                              new BinaryOperator("Company", _localCompany),
                                                                                              new BinaryOperator("ObjectList", _locObjectList),
                                                                                              new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                              new BinaryOperator("UserAccess", _locUserAccess),
                                                                                              new BinaryOperator("Active", true)));
                                    if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                    {
                                        _result = _locAppApprovalSettings;
                                    }

                                }
                                #endregion ByCompany


                                #region ByCompanyAndWorkplace
                                if (_locApplicationActionSetting2.FilterListSetup == FilterListSetup.ByCompanyAndWorkplace)
                                {
                                    XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                             (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                              new BinaryOperator("Company", _localCompany),
                                                                                              new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                              new BinaryOperator("ObjectList", _locObjectList),
                                                                                              new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                              new BinaryOperator("UserAccess", _locUserAccess),
                                                                                              new BinaryOperator("Active", true)));
                                    if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                    {
                                        _result = _locAppApprovalSettings;
                                    }

                                }
                                #endregion ByCompanyAndWorkplace



                                #region ByCompanyAndWorkplaceAndDepartment
                                if (_locApplicationActionSetting2.FilterListSetup == FilterListSetup.ByCompanyAndWorkplaceAndDepartment)
                                {
                                    XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                             (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                              new BinaryOperator("Company", _localCompany),
                                                                                              new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                              new BinaryOperator("OrgDim2", _localOrgDim2),
                                                                                              new BinaryOperator("ObjectList", _locObjectList),
                                                                                              new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                              new BinaryOperator("UserAccess", _locUserAccess),
                                                                                              new BinaryOperator("Active", true)));
                                    if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                    {
                                        _result = _locAppApprovalSettings;
                                    }

                                }
                                #endregion ByCompanyAndWorkplaceAndDepartment


                                #region ByCompanyAndWorkplaceAndDepartmentAndDivision
                                if (_locApplicationActionSetting2.FilterListSetup == FilterListSetup.ByCompanyAndWorkplaceAndDepartmentAndDivision)
                                {
                                    XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                             (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                              new BinaryOperator("Company", _localCompany),
                                                                                              new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                              new BinaryOperator("OrgDim2", _localOrgDim2),
                                                                                              new BinaryOperator("OrgDim3", _localOrgDim3),
                                                                                              new BinaryOperator("ObjectList", _locObjectList),
                                                                                              new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                              new BinaryOperator("UserAccess", _locUserAccess),
                                                                                              new BinaryOperator("Active", true)));
                                    if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                    {
                                        _result = _locAppApprovalSettings;
                                    }

                                }
                                #endregion ByCompanyAndWorkplaceAndDepartmentAndDivision
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject =  GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public XPCollection<ApplicationApprovalSetting> GetApplicationApprovalSettingByApprovalLevelNonUserAccess(Session currentSession, ApprovalLevel _locApprovalLevel,
                        ObjectList _locObjectList, Company _locCompany, OrganizationDimension1 _locOrgDim1, OrganizationDimension2 _locOrgDim2, OrganizationDimension3 _locOrgDim3
                        )
        {
            XPCollection<ApplicationApprovalSetting> _result = null;
            Company _localCompany = null;
            OrganizationDimension1 _localOrgDim1 = null;
            OrganizationDimension2 _localOrgDim2 = null;
            OrganizationDimension3 _localOrgDim3 = null;

            try
            {
                if (_locCompany != null) { _localCompany = _locCompany; }
                if (_locOrgDim1 != null) { _localOrgDim1 = _locOrgDim1; }
                if (_locOrgDim2 != null) { _localOrgDim2 = _locOrgDim2; }
                if (_locOrgDim3 != null) { _localOrgDim3 = _locOrgDim3; }

                ApplicationSetting _appSetting = currentSession.FindObject<ApplicationSetting>(new GroupOperator(GroupOperatorType.And,
                                               new BinaryOperator("Active", true)));
                if (_appSetting != null)
                {
                    //Cari dulu Application Setup Detail
                    XPCollection<ApplicationActionSetting> _locApplicationActionSetting1s = new XPCollection<ApplicationActionSetting>
                                                                        (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _localCompany),
                                                                        new BinaryOperator("ApplicationSetting", _appSetting),
                                                                        new BinaryOperator("ApplicationActionList", ApplicationActionList.Approval),
                                                                        new BinaryOperator("ObjectList", _locObjectList),
                                                                        new BinaryOperator("Active", true)));

                    if (_locApplicationActionSetting1s != null && _locApplicationActionSetting1s.Count() > 0)
                    {
                        foreach (ApplicationActionSetting _locApplicationActionSetting1 in _locApplicationActionSetting1s)
                        {

                            #region ByCompany
                            if (_locApplicationActionSetting1.FilterListSetup == FilterListSetup.ByCompany)
                            {
                                XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                         (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                          new BinaryOperator("Company", _localCompany),
                                                                                          new BinaryOperator("ObjectList", _locObjectList),
                                                                                          new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                          new BinaryOperator("ApprovalLevel", _locApprovalLevel),
                                                                                          new BinaryOperator("Active", true)));
                                if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                {
                                    _result = _locAppApprovalSettings;
                                }
                            }
                            #endregion ByCompany


                            #region ByCompanyAndWorkplace
                            if (_locApplicationActionSetting1.FilterListSetup == FilterListSetup.ByCompanyAndWorkplace)
                            {
                                XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                         (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                          new BinaryOperator("Company", _localCompany),
                                                                                          new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                          new BinaryOperator("ObjectList", _locObjectList),
                                                                                          new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                          new BinaryOperator("ApprovalLevel", _locApprovalLevel),
                                                                                          new BinaryOperator("Active", true)));
                                if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                {
                                    _result = _locAppApprovalSettings;
                                }

                            }
                            #endregion ByCompanyAndWorkplace


                            #region ByCompanyAndWorkplaceAndDepartment
                            if (_locApplicationActionSetting1.FilterListSetup == FilterListSetup.ByCompanyAndWorkplaceAndDepartment)
                            {
                                XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                         (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                          new BinaryOperator("Company", _localCompany),
                                                                                          new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                          new BinaryOperator("OrgDim2", _localOrgDim2),
                                                                                          new BinaryOperator("ObjectList", _locObjectList),
                                                                                          new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                          new BinaryOperator("ApprovalLevel", _locApprovalLevel),
                                                                                          new BinaryOperator("Active", true)));
                                if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                {
                                    _result = _locAppApprovalSettings;
                                }


                            }
                            #endregion ByCompanyAndWorkplaceAndDepartment


                            #region ByCompanyAndWorkplaceAndDepartmentAndDivision
                            if (_locApplicationActionSetting1.FilterListSetup == FilterListSetup.ByCompanyAndWorkplaceAndDepartmentAndDivision)
                            {
                                XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                         (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                          new BinaryOperator("Company", _localCompany),
                                                                                          new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                          new BinaryOperator("OrgDim2", _localOrgDim2),
                                                                                          new BinaryOperator("OrgDim3", _localOrgDim3),
                                                                                          new BinaryOperator("ObjectList", _locObjectList),
                                                                                          new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                          new BinaryOperator("ApprovalLevel", _locApprovalLevel),
                                                                                          new BinaryOperator("Active", true)));
                                if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                {
                                    _result = _locAppApprovalSettings;
                                }

                            }
                            #endregion ByCompanyAndWorkplaceAndDepartmentAndDivision
                        }
                    }
                    else
                    {
                        XPCollection<ApplicationActionSetting> _locApplicationActionSetting2s = new XPCollection<ApplicationActionSetting>
                                                                        (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Company", _localCompany),
                                                                        new BinaryOperator("ApplicationSetting", _appSetting),
                                                                        new BinaryOperator("ApplicationActionList", ApplicationActionList.Approval),
                                                                        new BinaryOperator("ObjectList", ObjectList.None),
                                                                        new BinaryOperator("Active", true)));

                        if (_locApplicationActionSetting2s != null && _locApplicationActionSetting2s.Count() > 0)
                        {
                            foreach (ApplicationActionSetting _locApplicationActionSetting2 in _locApplicationActionSetting2s)
                            {

                                #region ByCompany
                                if (_locApplicationActionSetting2.FilterListSetup == FilterListSetup.ByCompany)
                                {
                                    XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                             (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                              new BinaryOperator("Company", _localCompany),
                                                                                              new BinaryOperator("ObjectList", _locObjectList),
                                                                                              new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                              new BinaryOperator("ApprovalLevel", _locApprovalLevel),
                                                                                              new BinaryOperator("Active", true)));
                                    if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                    {
                                        _result = _locAppApprovalSettings;
                                    }
                                }
                                #endregion ByCompany


                                #region ByCompanyAndWorkplace
                                if (_locApplicationActionSetting2.FilterListSetup == FilterListSetup.ByCompanyAndWorkplace)
                                {
                                    XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                             (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                              new BinaryOperator("Company", _localCompany),
                                                                                              new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                              new BinaryOperator("ObjectList", _locObjectList),
                                                                                              new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                              new BinaryOperator("ApprovalLevel", _locApprovalLevel),
                                                                                              new BinaryOperator("Active", true)));
                                    if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                    {
                                        _result = _locAppApprovalSettings;
                                    }

                                }
                                #endregion ByCompanyAndWorkplace


                                #region ByCompanyAndWorkplaceAndDepartment
                                if (_locApplicationActionSetting2.FilterListSetup == FilterListSetup.ByCompanyAndWorkplaceAndDepartment)
                                {
                                    XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                             (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                              new BinaryOperator("Company", _localCompany),
                                                                                              new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                              new BinaryOperator("OrgDim2", _localOrgDim2),
                                                                                              new BinaryOperator("ObjectList", _locObjectList),
                                                                                              new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                              new BinaryOperator("ApprovalLevel", _locApprovalLevel),
                                                                                              new BinaryOperator("Active", true)));
                                    if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                    {
                                        _result = _locAppApprovalSettings;
                                    }


                                }
                                #endregion ByCompanyAndWorkplaceAndDepartment


                                #region ByCompanyAndWorkplaceAndDepartmentAndDivision
                                if (_locApplicationActionSetting2.FilterListSetup == FilterListSetup.ByCompanyAndWorkplaceAndDepartmentAndDivision)
                                {
                                    XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = new XPCollection<ApplicationApprovalSetting>
                                                                                             (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("ApplicationSetting", _appSetting),
                                                                                              new BinaryOperator("Company", _localCompany),
                                                                                              new BinaryOperator("OrgDim1", _localOrgDim1),
                                                                                              new BinaryOperator("OrgDim2", _localOrgDim2),
                                                                                              new BinaryOperator("OrgDim3", _localOrgDim3),
                                                                                              new BinaryOperator("ObjectList", _locObjectList),
                                                                                              new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                              new BinaryOperator("ApprovalLevel", _locApprovalLevel),
                                                                                              new BinaryOperator("Active", true)));
                                    if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                    {
                                        _result = _locAppApprovalSettings;
                                    }

                                }
                                #endregion ByCompanyAndWorkplaceAndDepartmentAndDivision
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject =  GlobalFunction " + ex.ToString());
            }
            return _result;
        }


        #region Get Import Excel For Enum

        public int GetStatus(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Open".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Progress".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Cancel".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Close".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Approved".ToLower())
                    {
                        _result = 5;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetNumberingType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Documents".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Objects".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Employee".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "LotNumber".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Item".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Other".ToLower())
                    {
                        _result = 10;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetObjectList(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Company".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "OrganizationDimension1".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "OrganizationDimension2".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "OrganizationDimension3".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "OrganizationDimension4".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "JobPosition".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "Employee".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "EmployeePositionLine".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "KpiYear".ToLower())
                    {
                        _result = 9;
                    }
                    else if (_locObjectName == "KpiRole".ToLower())
                    {
                        _result = 10;
                    }
                    else if (_locObjectName == "KpiRoleLine".ToLower())
                    {
                        _result = 11;
                    }
                    else if (_locObjectName == "KpiRoleGrading".ToLower())
                    {
                        _result = 12;
                    }
                    else if (_locObjectName == "KpiSubmit".ToLower())
                    {
                        _result = 13;
                    }  
                    else if (_locObjectName == "Vessel".ToLower())
                    {
                        _result = 22;
                    } 
                    else if (_locObjectName == "QuotationCustomer".ToLower())
                    {
                        _result = 33;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetKpiPeriod(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Monthly".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Quarter".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Semester".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Yearly".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetKpiFormat(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Persen".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Amount".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Scale".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Date".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Month".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Day".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "Hour".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "Liter".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "Rupiah".ToLower())
                    {
                        _result = 9;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetKpiValue(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Maximize".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Minimize".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetKpiGroupType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Cascading".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Personal".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetKpiYtdType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Average".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "SumAll".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetKpiBscType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "FP".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "IPP".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "LGP".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "CP".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #region Get Enum Shipping Nih Bos

        public int GetAgama(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "_".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "Islam".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Kristen".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Katolik".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Hindu".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Buddha".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Konghucu".ToLower())
                    {
                        _result = 6;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetGoldar(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "_".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "A".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "B".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "AB".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "O".ToLower())
                    {
                        _result = 4;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetGender(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Male".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "Female".ToLower())
                    {
                        _result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetItemPosition(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Engine".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "Deck".ToLower())
                    {
                        _result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }
        
        public int GetTypeForCrewClass(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "OilTanker".ToLower())
                    {
                        _result = 0;
                    }
                    //else if (_locObjectName == "Endorsement".ToLower())
                    //{
                    //    _result = 1;
                    //}
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetStatusForCrew(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Offboard".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Onboard".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Blacklist".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDocumentType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "SeaVessel".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "RiverVessel".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "DocumentCrew".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Travel".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "CertificatesOfCompetence".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "CertificateOfProficiency".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "HealthCrew".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "EndorsementCrew".ToLower())
                    {
                        _result = 8;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDocumentRenewalType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Regular".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "Endorsement".ToLower())
                    {
                        _result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetVesselType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "OilBarge".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "TugBoat".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "SelfPropelledOilBarges".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "MotorTanker".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "LandingCraftTank".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "KapalMotorPenumpang".ToLower())
                    {
                        _result = 6;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetVesselHull(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "SingleBottom".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "DoubleBottom".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "SingleHull".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "DoubleHull".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetStatusOwnershipVessel(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Shipping".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "Carter".ToLower())
                    {
                        _result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetMarital(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "_".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "Single".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Married".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Married".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Divorce".ToLower())
                    {
                        _result = 4;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPtkp(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "_".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "TK0".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "TK1".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "TK2".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "TK3".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "K0".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "K1".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "K2".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "K3".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "KI0".ToLower())
                    {
                        _result = 9;
                    }
                    else if (_locObjectName == "KI1".ToLower())
                    {
                        _result = 10;
                    }
                    else if (_locObjectName == "KI2".ToLower())
                    {
                        _result = 11;
                    }
                    else if (_locObjectName == "KI3".ToLower())
                    {
                        _result = 12;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetFamilyStatus(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Suami".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "Istri".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Anak".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Ayah".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Ibu".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Adik".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Kakak".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "Lainnya".ToLower())
                    {
                        _result = 7;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetStatusCrewOn(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "None".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "NewHire".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Rejoin".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Mutasi".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Promosi".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Demosi".ToLower())
                    {
                        _result = 5;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }
        public int GetStatusCrewOff(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "None".ToLower())
                    {
                        _result = 0;
                    }
                    else if (_locObjectName == "Mutasi".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Demosi".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Promosi".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "FinishContract".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Resign".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Terminated".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "AWOL".ToLower())
                    {
                        _result = 7;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion Get Enum Shipping Nih Bos

        #endregion Get Import Excel For Enum

        #endregion ImportData

        #region Email

        public ApplicationMailSetting GetApplicationMailSetup(Session currentSession, Company _locCompany)
        {
            ApplicationMailSetting result = null;
            try
            {
                ApplicationMailSetting _locApplicationMailSetting = currentSession.FindObject<ApplicationMailSetting>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Company", _locCompany),
                                                                new BinaryOperator("Active", true)));
                if (_locApplicationMailSetting != null)
                {
                    result = _locApplicationMailSetting;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = GlobalFunction " + ex.ToString());
            }
            return result;
        }


        public string BackgroundBodyMailKpiProgress(Employee _locEmployee,JobPosition _locEmployeeJob, OrganizationDimension2 _locEmployeeDepartement, Month _locEmployeeKpiSubmitMonth, KpiYear _locEmployeeKpiSubmitYear)
        {
            string body = string.Empty;
            string _locStringEmployee = string.Empty;
            string _locStringEmployeeJob = string.Empty;
            string _locStringEmployeeDepartement = string.Empty;
            string _locStringEmployeeKpiSubmitMonth = string.Empty;
            string _locStringEmployeeKpiSubmitYear = string.Empty;


            string baseDirectory = (string)AppDomain.CurrentDomain.GetData("ContentRootPath");
            string filePath = Path.Combine(baseDirectory+ "\\Templete\\" + "mailprogress.html");
            try
            {
                if (File.Exists(filePath))
                {
                    body = File.ReadAllText(filePath);
                }
                else
                {
                    Console.WriteLine("File does not exist at: " + filePath);
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("File not found: " + ex.Message);
                throw; 
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: " + ex.Message);
            }

            if (body != null && body != "")
            {

                #region Employee
                if (_locEmployee != null)
                {
                    _locStringEmployee = _locEmployee.ToString();
                    body = body.Replace("{Employee}", _locStringEmployee);
                }
                #endregion Employee    
                
                #region Job Position
                if (_locEmployeeJob != null)
                {
                    _locStringEmployeeJob = _locEmployeeJob.ToString();
                    body = body.Replace("{JobPosition}", _locStringEmployeeJob);
                }
                #endregion Job Position                
                
                #region Departement
                if ( _locEmployeeDepartement != null )
                {
                    _locStringEmployeeDepartement = _locEmployeeDepartement.ToString();
                    body = body.Replace("{Departement}", _locStringEmployeeDepartement);
                }
                #endregion Departement

                #region Month
                if (_locEmployeeKpiSubmitMonth != 0)
                {
                    _locStringEmployeeKpiSubmitMonth = _locEmployeeKpiSubmitMonth.ToString();
                    body = body.Replace("{MonthKpi}", _locStringEmployeeKpiSubmitMonth);
                }
                #endregion Month                
                
                #region Year
                if (_locEmployeeKpiSubmitYear != null)
                {
                    _locStringEmployeeKpiSubmitYear = _locEmployeeKpiSubmitYear.ToString();
                    body = body.Replace("{YearKpi}", _locStringEmployeeKpiSubmitYear);
                }
                #endregion Year
            }

            return body;
        }        
        
        public string BackgroundBodyMailKpiReject(Employee _locEmployee, JobPosition _locEmployeeJob, OrganizationDimension2 _locEmployeeDepartement, Month _locEmployeeKpiSubmitMonth, KpiYear _locEmployeeKpiSubmitYear)
        {
            string body = string.Empty;
            string _locStringEmployee = string.Empty;
            string _locStringEmployeeJob = string.Empty;
            string _locStringEmployeeDepartement = string.Empty;
            string _locStringEmployeeKpiSubmitMonth = string.Empty;
            string _locStringEmployeeKpiSubmitYear = string.Empty;

            string baseDirectory = (string)AppDomain.CurrentDomain.GetData("ContentRootPath");
            string filePath = Path.Combine(baseDirectory+ "\\Templete\\" + "mailreject.html");
            try
            {
                if (File.Exists(filePath))
                {
                    body = File.ReadAllText(filePath);
                }
                else
                {
                    Console.WriteLine("File does not exist at: " + filePath);
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("File not found: " + ex.Message);
                throw; 
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: " + ex.Message);
            }

            if (body != null && body != "")
            {

                #region Employee
                if (_locEmployee != null)
                {
                    _locStringEmployee = _locEmployee.ToString();
                    body = body.Replace("{Employee}", _locStringEmployee);
                }
                #endregion Employee    

                #region Job Position
                if (_locEmployeeJob != null)
                {
                    _locStringEmployeeJob = _locEmployeeJob.ToString();
                    body = body.Replace("{JobPosition}", _locStringEmployeeJob);
                }
                #endregion Job Position                

                #region Departement
                if (_locEmployeeDepartement != null)
                {
                    _locStringEmployeeDepartement = _locEmployeeDepartement.ToString();
                    body = body.Replace("{Departement}", _locStringEmployeeDepartement);
                }
                #endregion Departement

                #region Month
                if (_locEmployeeKpiSubmitMonth != 0)
                {
                    _locStringEmployeeKpiSubmitMonth = _locEmployeeKpiSubmitMonth.ToString();
                    body = body.Replace("{MonthKpi}", _locStringEmployeeKpiSubmitMonth);
                }
                #endregion Month                

                #region Year
                if (_locEmployeeKpiSubmitYear != null)
                {
                    _locStringEmployeeKpiSubmitYear = _locEmployeeKpiSubmitYear.ToString();
                    body = body.Replace("{YearKpi}", _locStringEmployeeKpiSubmitYear);
                }
                #endregion Year
            }

            return body;
        }

        public string BackgroundBodyMailKpiApprove()
        {
            string body = string.Empty;
            string baseDirectory = (string)AppDomain.CurrentDomain.GetData("ContentRootPath");
            string filePath = Path.Combine(baseDirectory + "\\Templete\\" + "mailapprove.html");
            try
            {
                if (File.Exists(filePath))
                {
                    body = File.ReadAllText(filePath);
                    return body;
                }
                else
                {
                    Console.WriteLine("File does not exist at: " + filePath);
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("File not found: " + ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: " + ex.Message);
            }

            return body;
        }

        public void SetAndSendMail(string _locSmtpHost, int _locSmtpPort, string _locMailFrom, string _locMailFromPassword, string _locMailTo, string _locSubjectMail, string _locBody)
        {
            try
            {
                if (_locMailTo != null)
                {
                    foreach (string _locRowMailTo in _locMailTo.Replace(" ", "").Split(';'))
                    {
                        if (!string.IsNullOrEmpty(_locRowMailTo))
                        {
                            SmtpClient client = new SmtpClient(_locSmtpHost, _locSmtpPort);
                            client.TargetName = "None";
                            //client.UseDefaultCredentials = false;
                            client.Credentials = new System.Net.NetworkCredential(_locMailFrom, _locMailFromPassword);
                            MailAddress from = new MailAddress(_locMailFrom, String.Empty, System.Text.Encoding.UTF8);
                            MailAddress to = new MailAddress(_locRowMailTo);
                            MailMessage message = new MailMessage(from, to);
                            message.Body = _locBody;
                            message.IsBodyHtml = true;
                            message.BodyEncoding = System.Text.Encoding.UTF8;
                            message.Subject = _locSubjectMail;
                            message.SubjectEncoding = System.Text.Encoding.UTF8;
                            client.EnableSsl = true;
                            client.Send(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = GlobalFunction " + ex.ToString());
            }
        }

        #endregion Email
    
    }
}
