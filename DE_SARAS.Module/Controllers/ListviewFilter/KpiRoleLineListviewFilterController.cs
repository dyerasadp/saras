﻿using DE_SARAS.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ListviewFilter
{

    public partial class KpiRoleLineListviewFilterController : ViewController
    {

        public KpiRoleLineListviewFilterController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewId = "KpiRoleLine_ListView";
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region FilterBaseOnOrganization

            XPCollection<ApplicationOrganizationSetting> _locAppOrganizationSetting1s = null;
            XPCollection<ApplicationOrganizationSetting> _locAppOrganizationSettings = null;
            var User = SecuritySystem.CurrentUserName;
            //Company
            string _beginString1 = null;
            string _endString1 = null;
            string _fullString1 = null;
            //Workplace
            string _beginString2 = null;
            string _endString2 = null;
            string _fullString2 = null;
            //Department
            string _beginString3 = null;
            string _endString3 = null;
            string _fullString3 = null;
            //Division
            string _beginString4 = null;
            string _endString4 = null;
            string _fullString4 = null;
            //Section
            string _beginString5 = null;
            string _endString5 = null;
            string _fullString5 = null;
            //Employee
            string _beginString6 = null;
            string _endString6 = null;
            string _fullString6 = null;
            string _fullString = null;
            var ListView = View as ListView;
            Session currentSession = null;

            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", User));
                if (_locUserAccess.Session != null)
                {
                    currentSession = _locUserAccess.Session;
                }
                if (_locUserAccess != null && currentSession != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        List<string> _stringCompany = new List<string>();
                        List<string> _stringOrgDim1 = new List<string>();
                        List<string> _stringOrgDim2 = new List<string>();
                        List<string> _stringOrgDim3 = new List<string>();
                        List<string> _stringOrgDim4 = new List<string>();

                        _locAppOrganizationSetting1s = new XPCollection<ApplicationOrganizationSetting>
                                            (currentSession, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("UserAccess", _locUserAccess),
                                            new BinaryOperator("PageType", CustomProcess.PageType.ListView),
                                            new BinaryOperator("ObjectListGroup", CustomProcess.ObjectListGroup.KpiRole),
                                            new BinaryOperator("Active", true)));
                        if (_locAppOrganizationSetting1s != null && _locAppOrganizationSetting1s.Count() > 0)
                        {
                            _locAppOrganizationSettings = _locAppOrganizationSetting1s;
                        }

                        #region TakeDataFromApplicationOrganizationSetup
                        if (_locAppOrganizationSettings != null && _locAppOrganizationSettings.Count > 0)
                        {
                            foreach (ApplicationOrganizationSetting _locAppOrganizationSetup in _locAppOrganizationSettings)
                            {
                                if (_locAppOrganizationSetup.Company != null)
                                {
                                    if (_locAppOrganizationSetup.Company.Code != null) { _stringCompany.Add(_locAppOrganizationSetup.Company.Code); }
                                }
                                if (_locAppOrganizationSetup.OrgDim1 != null)
                                {
                                    if (_locAppOrganizationSetup.OrgDim1.Code != null) { _stringOrgDim1.Add(_locAppOrganizationSetup.OrgDim1.Code); }
                                }
                                if (_locAppOrganizationSetup.OrgDim2 != null)
                                {
                                    if (_locAppOrganizationSetup.OrgDim2.Code != null) { _stringOrgDim2.Add(_locAppOrganizationSetup.OrgDim2.Code); }
                                }
                                if (_locAppOrganizationSetup.OrgDim3 != null)
                                {
                                    if (_locAppOrganizationSetup.OrgDim3.Code != null) { _stringOrgDim3.Add(_locAppOrganizationSetup.OrgDim3.Code); }
                                }
                                if (_locAppOrganizationSetup.OrgDim4 != null)
                                {
                                    if (_locAppOrganizationSetup.OrgDim4.Code != null) { _stringOrgDim4.Add(_locAppOrganizationSetup.OrgDim4.Code); }
                                }

                            }
                        }
                        #endregion TakeDataFromApplicationOrganizationSetup
                        #region TakeDataDirectFromUser
                        else
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                if (_locUserAccess.Employee.Company.Code != null) { _stringCompany.Add(_locUserAccess.Employee.Company.Code); }
                            }
                            if (_locUserAccess.Employee.OrganizationDimension1 != null)
                            {
                                if (_locUserAccess.Employee.OrganizationDimension1.Code != null) { _stringOrgDim1.Add(_locUserAccess.Employee.OrganizationDimension1.Code); }
                            }
                            if (_locUserAccess.Employee.OrganizationDimension2 != null)
                            {
                                if (_locUserAccess.Employee.OrganizationDimension2.Code != null) { _stringOrgDim2.Add(_locUserAccess.Employee.OrganizationDimension2.Code); }
                            }
                            if (_locUserAccess.Employee.OrganizationDimension3 != null)
                            {
                                if (_locUserAccess.Employee.OrganizationDimension3.Code != null) { _stringOrgDim3.Add(_locUserAccess.Employee.OrganizationDimension3.Code); }
                            }
                            if (_locUserAccess.Employee.OrganizationDimension4 != null)
                            {
                                if (_locUserAccess.Employee.OrganizationDimension4.Code != null) { _stringOrgDim4.Add(_locUserAccess.Employee.OrganizationDimension4.Code); }
                            }

                        }
                        #endregion TakeDataDirectFromUserv

                        #region Company
                        IEnumerable<string> _stringArrayCompanyDistinct = _stringCompany.Distinct();
                        string[] _stringArrayCompanyList = _stringArrayCompanyDistinct.ToArray();
                        if (_stringArrayCompanyList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                            {
                                Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                                if (_locCompany != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayCompanyList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                            {
                                Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                                if (_locCompany != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                    else
                                    {
                                        _endString1 = _endString1 + " OR [Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString1 = _beginString1 + _endString1;

                        #endregion Company

                        #region OrganizationDimension1
                        IEnumerable<string> _stringArrayOrgDim1Distinct = _stringOrgDim1.Distinct();
                        string[] _stringArrayOrgDim1List = _stringArrayOrgDim1Distinct.ToArray();
                        if (_stringArrayOrgDim1List.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayOrgDim1List.Length; i++)
                            {
                                OrganizationDimension1 _locOrgDim1 = currentSession.FindObject<OrganizationDimension1>(new BinaryOperator("Code", _stringArrayOrgDim1List[i]));
                                if (_locOrgDim1 != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString2 = "[OrgDim1.Code]=='" + _locOrgDim1.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayOrgDim1List.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayOrgDim1List.Length; i++)
                            {
                                OrganizationDimension1 _locOrgDim1 = currentSession.FindObject<OrganizationDimension1>(new BinaryOperator("Code", _stringArrayOrgDim1List[i]));
                                if (_locOrgDim1 != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString2 = "[OrgDim1.Code]=='" + _locOrgDim1.Code + "'";
                                    }
                                    else
                                    {
                                        _endString2 = _endString2 + " OR [OrgDim1.Code]=='" + _locOrgDim1.Code + "'";
                                    }
                                }

                            }
                        }
                        _fullString2 = _beginString2 + _endString2;
                        #endregion OrganizationDimension1

                        #region OrganizationDimension2
                        IEnumerable<string> _stringArrayOrgDim2Distinct = _stringOrgDim2.Distinct();
                        string[] _stringArrayOrgDim2List = _stringArrayOrgDim2Distinct.ToArray();
                        if (_stringArrayOrgDim2List.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayOrgDim2List.Length; i++)
                            {
                                OrganizationDimension2 _locOrgDim2 = currentSession.FindObject<OrganizationDimension2>(new BinaryOperator("Code", _stringArrayOrgDim2List[i]));
                                if (_locOrgDim2 != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString3 = "[OrgDim2.Code]=='" + _locOrgDim2.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayOrgDim2List.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayOrgDim2List.Length; i++)
                            {
                                OrganizationDimension2 _locOrgDim2 = currentSession.FindObject<OrganizationDimension2>(new BinaryOperator("Code", _stringArrayOrgDim2List[i]));
                                if (_locOrgDim2 != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString3 = "[OrgDim2.Code]=='" + _locOrgDim2.Code + "'";
                                    }
                                    else
                                    {
                                        _endString3 = _endString3 + " OR [OrgDim2.Code]=='" + _locOrgDim2.Code + "'";
                                    }
                                }

                            }
                        }
                        _fullString3 = _beginString3 + _endString3;

                        #endregion OrganizationDimension2

                        #region OrganizationDimension3
                        IEnumerable<string> _stringArrayOrgDim3Distinct = _stringOrgDim3.Distinct();
                        string[] _stringArrayOrgDim3List = _stringArrayOrgDim3Distinct.ToArray();
                        if (_stringArrayOrgDim3List.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayOrgDim3List.Length; i++)
                            {
                                OrganizationDimension3 _locOrgDim3 = currentSession.FindObject<OrganizationDimension3>(new BinaryOperator("Code", _stringArrayOrgDim3List[i]));
                                if (_locOrgDim3 != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString4 = "[OrgDim3.Code]=='" + _locOrgDim3.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayOrgDim3List.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayOrgDim3List.Length; i++)
                            {
                                OrganizationDimension3 _locOrgDim3 = currentSession.FindObject<OrganizationDimension3>(new BinaryOperator("Code", _stringArrayOrgDim3List[i]));
                                if (_locOrgDim3 != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString4 = "[OrgDim3.Code]=='" + _locOrgDim3.Code + "'";
                                    }
                                    else
                                    {
                                        _endString4 = _endString4 + " OR [OrgDim3.Code]=='" + _locOrgDim3.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString4 = _beginString4 + _endString4;

                        #endregion OrganizationDimension3

                        #region OrganizationDimension4
                        IEnumerable<string> _stringArrayOrgDim4Distinct = _stringOrgDim4.Distinct();
                        string[] _stringArrayOrgDim4List = _stringArrayOrgDim4Distinct.ToArray();
                        if (_stringArrayOrgDim4List.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayOrgDim4List.Length; i++)
                            {
                                OrganizationDimension4 _locOrgDim4 = currentSession.FindObject<OrganizationDimension4>(new BinaryOperator("Code", _stringArrayOrgDim4List[i]));
                                if (_locOrgDim4 != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString5 = "[OrgDim4.Code]=='" + _locOrgDim4.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayOrgDim4List.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayOrgDim4List.Length; i++)
                            {
                                OrganizationDimension4 _locOrgDim4 = currentSession.FindObject<OrganizationDimension4>(new BinaryOperator("Code", _stringArrayOrgDim4List[i]));
                                if (_locOrgDim4 != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString5 = "[OrgDim4.Code]=='" + _locOrgDim4.Code + "'";
                                    }
                                    else
                                    {
                                        _endString5 = _endString5 + " OR [OrgDim4.Code]=='" + _locOrgDim4.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString5 = _beginString5 + _endString5;

                        #endregion OrganizationDimension4


                        #region FilterStringArray
                        //Part1
                        if (_stringArrayCompanyList.Length >= 1 && _stringArrayOrgDim1List.Length >= 1 && _stringArrayOrgDim2List.Length >= 1 && _stringArrayOrgDim3List.Length >= 1 && _stringArrayOrgDim4List.Length >= 1)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " ) AND ( " + _fullString4 + " ) AND ( " + _fullString5 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayOrgDim1List.Length >= 1 && _stringArrayOrgDim2List.Length >= 1 && _stringArrayOrgDim3List.Length >= 1 && _stringArrayOrgDim4List.Length >= 1)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " ) AND ( " + _fullString4 + " ) AND ( " + _fullString5 + " ) ";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayOrgDim1List.Length >= 1 && _stringArrayOrgDim2List.Length >= 1 && _stringArrayOrgDim3List.Length >= 1 && _stringArrayOrgDim4List.Length == 0)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " ) AND ( " + _fullString4 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayOrgDim1List.Length >= 1 && _stringArrayOrgDim2List.Length >= 1 && _stringArrayOrgDim3List.Length == 0 && _stringArrayOrgDim4List.Length == 0)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayOrgDim1List.Length >= 1 && _stringArrayOrgDim2List.Length == 0 && _stringArrayOrgDim3List.Length == 0 && _stringArrayOrgDim4List.Length == 0)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayOrgDim1List.Length == 0 && _stringArrayOrgDim2List.Length == 0 && _stringArrayOrgDim3List.Length == 0 && _stringArrayOrgDim4List.Length == 0)
                        {
                            _fullString = _fullString1;
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayOrgDim1List.Length >= 1 && _stringArrayOrgDim2List.Length >= 1 && _stringArrayOrgDim3List.Length >= 1 && _stringArrayOrgDim4List.Length == 0)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " ) AND ( " + _fullString4 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayOrgDim1List.Length >= 1 && _stringArrayOrgDim2List.Length >= 1 && _stringArrayOrgDim3List.Length == 0 && _stringArrayOrgDim4List.Length == 0)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " ) AND ( " + _fullString3 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayOrgDim1List.Length >= 1 && _stringArrayOrgDim2List.Length == 0 && _stringArrayOrgDim3List.Length == 0 && _stringArrayOrgDim4List.Length == 0)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString2 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayOrgDim1List.Length == 0 && _stringArrayOrgDim2List.Length == 0 && _stringArrayOrgDim3List.Length == 0 && _stringArrayOrgDim4List.Length == 0)
                        {
                            _fullString = "( " + _fullString1 + " )";
                        }
                        #endregion FilterStringArray

                        if (_fullString != null)
                        {
                            ListView.CollectionSource.Criteria["KpiRoleFilter"] = CriteriaOperator.Parse(_fullString);
                        }
                    }
                }
            }

            #endregion FilterBaseOnOrganization
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
