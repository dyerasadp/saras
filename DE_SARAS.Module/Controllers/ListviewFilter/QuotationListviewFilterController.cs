﻿using DE_SARAS.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ListviewFilter
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class QuotationListviewFilterController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public QuotationListviewFilterController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewId = "Quotation_ListView";
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region FilterBaseOnOrganization

            XPCollection<ApplicationOrganizationSetting> _locAppOrganizationSetting1s = null;
            XPCollection<ApplicationOrganizationSetting> _locAppOrganizationSettings = null;
            var User = SecuritySystem.CurrentUserName;
            //Company
            string _beginString1 = null;
            string _endString1 = null;
            string _fullString1 = null;

            //Employee
            string _beginString6 = null;
            string _endString6 = null;
            string _fullString6 = null;
            string _fullString = null;
            var ListView = View as ListView;
            Session currentSession = null;

            if (this.ObjectSpace != null)
            {
                UserAccess _locUserAccess = this.ObjectSpace.FindObject<UserAccess>(new BinaryOperator("UserName", User));
                if (_locUserAccess.Session != null)
                {
                    currentSession = _locUserAccess.Session;
                }
                if (_locUserAccess != null && currentSession != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        List<string> _stringCompany = new List<string>();
                        List<string> _stringEmployee = new List<string>();

                        _locAppOrganizationSetting1s = new XPCollection<ApplicationOrganizationSetting>
                                            (currentSession, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("UserAccess", _locUserAccess),
                                            new BinaryOperator("PageType", CustomProcess.PageType.ListView),
                                            new BinaryOperator("ObjectListGroup", CustomProcess.ObjectListGroup.Quotation),
                                            new BinaryOperator("Active", true)));
                        if (_locAppOrganizationSetting1s != null && _locAppOrganizationSetting1s.Count() > 0)
                        {
                            _locAppOrganizationSettings = _locAppOrganizationSetting1s;
                        }

                        #region TakeDataFromApplicationOrganizationSetup
                        if (_locAppOrganizationSettings != null && _locAppOrganizationSettings.Count > 0)
                        {
                            foreach (ApplicationOrganizationSetting _locAppOrganizationSetup in _locAppOrganizationSettings)
                            {
                                if (_locAppOrganizationSetup.Company != null)
                                {
                                    if (_locAppOrganizationSetup.Company.Code != null) { _stringCompany.Add(_locAppOrganizationSetup.Company.Code); }
                                }                    
                                if (_locAppOrganizationSetup.Employee != null)
                                {
                                    if (_locAppOrganizationSetup.Employee.Code != null) { _stringEmployee.Add(_locAppOrganizationSetup.Employee.Code); }
                                }
                            }
                        }
                        #endregion TakeDataFromApplicationOrganizationSetup

                        #region TakeDataDirectFromUser
                        else
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                if (_locUserAccess.Employee.Company.Code != null) { _stringCompany.Add(_locUserAccess.Employee.Company.Code); }
                            }
                            if (_locUserAccess.Employee.Code != null)
                            {
                                _stringEmployee.Add(_locUserAccess.Employee.Code);
                            }
                        }
                        #endregion TakeDataDirectFromUserv

                        #region Company
                        IEnumerable<string> _stringArrayCompanyDistinct = _stringCompany.Distinct();
                        string[] _stringArrayCompanyList = _stringArrayCompanyDistinct.ToArray();
                        if (_stringArrayCompanyList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                            {
                                Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                                if (_locCompany != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayCompanyList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                            {
                                Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                                if (_locCompany != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                    else
                                    {
                                        _endString1 = _endString1 + " OR [Company.Code]=='" + _locCompany.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString1 = _beginString1 + _endString1;

                        #endregion Company

                        #region Employee
                        IEnumerable<string> _stringArrayEmployeeDistinct = _stringEmployee.Distinct();
                        string[] _stringArrayEmployeeList = _stringArrayEmployeeDistinct.ToArray();
                        if (_stringArrayEmployeeList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayEmployeeList.Length; i++)
                            {
                                Employee _locEmployee = currentSession.FindObject<Employee>(new BinaryOperator("Code", _stringArrayEmployeeList[i]));
                                if (_locEmployee != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString6 = "[Employee.Code]=='" + _locEmployee.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayEmployeeList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayEmployeeList.Length; i++)
                            {
                                Employee _locEmployee = currentSession.FindObject<Employee>(new BinaryOperator("Code", _stringArrayEmployeeList[i]));
                                if (_locEmployee != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString6 = "[Employee.Code]=='" + _locEmployee.Code + "'";
                                    }
                                    else
                                    {
                                        _endString6 = _endString6 + " OR [Employee.Code]=='" + _locEmployee.Code + "'";
                                    }
                                }

                            }
                        }
                        _fullString6 = _beginString6 + _endString6;

                        #endregion Employee

                        #region FilterStringArray
                        //Part1
                        if (_stringArrayCompanyList.Length >= 1 && _stringArrayEmployeeList.Length >= 1)
                        {
                            _fullString = "( " + _fullString1 + " ) AND ( " + _fullString6 + " )";
                        }
                        else if (_stringArrayCompanyList.Length >= 1 && _stringArrayEmployeeList.Length == 0)
                        {
                            _fullString = _fullString1;
                        }
                        else if (_stringArrayCompanyList.Length == 0 && _stringArrayEmployeeList.Length >= 1)
                        {
                            _fullString = _fullString6;
                        }
                        #endregion FilterStringArray

                        if (_fullString != null)
                        {
                            ListView.CollectionSource.Criteria["QuotationFilter"] = CriteriaOperator.Parse(_fullString);
                        }
                    }
                }
            }

            #endregion FilterBaseOnOrganization
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
