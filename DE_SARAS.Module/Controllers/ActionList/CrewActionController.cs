﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CrewActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/

        private ChoiceActionItem _selectionListviewStatusFilter;

        public CrewActionController()
        {
            InitializeComponent();
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(Crew);

            #region FilterStatus

            SingleChoiceAction CrewListviewFilterSelectionAction = new SingleChoiceAction(this, "CrewListviewFilterSelectionAction", PredefinedCategory.Edit)
            {
                Caption = "Filter",
                //Specify the display mode for the Action's items. Here the items are operations that you perform against selected records.
                ItemType = SingleChoiceActionItemType.ItemIsOperation,
                //Set the Action to become available in the Task List View when a user selects one or more objects.
                SelectionDependencyType = SelectionDependencyType.RequireMultipleObjects
            };
            CrewListviewFilterSelectionAction.Execute += CrewListviewFilterSelectionAction_Execute;

            CrewListviewFilterSelectionAction.Items.Clear();
            foreach (object _onboardCrew in Enum.GetValues(typeof(CustomProcess.StatusForCrew)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.StatusForCrew));
                _selectionListviewStatusFilter = new ChoiceActionItem(_ed.GetCaption(_onboardCrew), _onboardCrew);
                CrewListviewFilterSelectionAction.Items.Add(_selectionListviewStatusFilter);
            }
            #endregion FilterStatus

            #region LookupCvAction 

            //SimpleAction LookupCvAction = new SimpleAction(this, "LookupCvAction", PredefinedCategory.View)
            //{
            //    Caption = "Find CV",
            //    ImageName = "Redo"
            //};
            //LookupCvAction.Execute += LookupCvAction_Execute;

            #endregion LookupCvAction
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CrewListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(Crew)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((StatusForCrew)e.SelectedChoiceActionItem.Data == StatusForCrew.Offboard)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("StatusCrew", StatusForCrew.Offboard, BinaryOperatorType.Equal);
                    }
                    else if ((StatusForCrew)e.SelectedChoiceActionItem.Data == StatusForCrew.Onboard)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("StatusCrew", StatusForCrew.Onboard, BinaryOperatorType.Equal);
                    }
                    else if ((StatusForCrew)e.SelectedChoiceActionItem.Data == StatusForCrew.Blacklist)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("StatusCrew", StatusForCrew.Blacklist, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Crew " + ex.ToString());
            }
        }

        //void LookupCvAction_Execute(Object sender, SimpleActionExecuteEventArgs e)
        //{
        //    IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(Crew));
        //    string listViewId = Application.FindListViewId(typeof(Crew));

        //    CollectionSourceBase collectionSource = Application.CreateCollectionSource(
        //        objectSpace,
        //        typeof(Crew),
        //        listViewId
        //    );

        //    // Filtering criteria to display only the "CV" status crew
        //    collectionSource.Criteria["StatusCrew"] = CriteriaOperator.Parse("StatusCrew == '0'");

        //    e.ShowViewParameters.CreatedView = Application.CreateListView(
        //       listViewId,
        //       collectionSource,
        //       true
        //    );

        //    e.ShowViewParameters.TargetWindow = TargetWindow.NewWindow;
        //    e.ShowViewParameters.Context = TemplateContext.PopupWindow;
        //    e.ShowViewParameters.Controllers.Add(Application.CreateController<DialogController>());
        //}
    }
}
