﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PicaActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public PicaActionController()
        {
            InitializeComponent();
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(Pica);
            // Target required Views (via the TargetXXX properties) and create their Actions.

            SimpleAction closePicaAction = new SimpleAction(this, "ClosePicaAction", PredefinedCategory.View)
            {
                Caption = "Close Pica",
                ConfirmationMessage = "Are you sure you want to Close the data? If You want to Close this data, please upload your attachment.",
                ImageName = "Actions_Delete"
            };
            closePicaAction.Execute += ClosePicaAction_Execute;
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region ClosePicaAction_Execute
        private void ClosePicaAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                JobPosition _currObjectJobPosition = null;
                Employee _locEmployee = null;

                #region get Behaviour

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        Pica _locPicaOS = (Pica)_objectSpace.GetObject(obj);


                        if (_locPicaOS != null)
                        {
                            if (_locPicaOS.Session != null)
                            {
                                _currSession = _locPicaOS.Session;
                            }

                            if (_locPicaOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locPicaOS.Code;
                                _currObjectJobPosition = _locPicaOS.JobPosition;

                                Pica _locPicaXPO = _currSession.FindObject<Pica>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new BinaryOperator("JobPosition", _currObjectJobPosition)
                                                                    ));

                                if (_locPicaXPO != null)
                                {
                                    if( _locPicaXPO.StatusPica == Status.Open)
                                    {
                                        if(_locPicaXPO.Attachment != null)
                                        {
                                            _locPicaXPO.StatusPica = Status.Close;
                                            _locPicaXPO.Save();
                                            _locPicaXPO.Session.CommitTransaction();

                                            SuccsessMessageShow("Closing Data has successfully");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("You must add your Attachment");
                                        }

                                    }
                                    else
                                    {
                                        ErrorMessageShow("This data is not OPEN");
                                    }

                                }
                            }
                            else
                            {
                                ErrorMessageShow("Can not Closing PICA");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion get Behaviour


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = KpiSubmit " + ex.ToString());
            }
        }

        #endregion ClosePicaAction_Execute

        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
