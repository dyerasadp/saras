﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Map.Kml.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.XtraGauges.Core.Model;
using Google.Protobuf.WellKnownTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    public partial class KpiSubmitActionController : ViewController
    {
        private SimpleAction progressKpiAction;
        public KpiSubmitActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(KpiSubmit);

            #region GetKpiAction 

            SimpleAction getKpiAction = new SimpleAction(this, "GetKpiAction", PredefinedCategory.View)
            {
                Caption = "Get Data",
                ConfirmationMessage = "Are you sure you want to get the data?",
                ImageName = "BO_Security_Permission_Action"
            };
            getKpiAction.Execute += GetKpiAction_Execute;

            #endregion GetKpiAction

            #region ProgressKpiAction

            SimpleAction progressKpiAction = new SimpleAction(this, "ProgressKpiAction", PredefinedCategory.View)
            {
                Caption = "Process Kpi",
                ConfirmationMessage = "Are you sure you want to Process this kpi?",
                ImageName = "BO_Security_Permission_Action"
            };
            progressKpiAction.Execute += ProgressKpiAction_Execute;

            #endregion ProgressKpiAction

            #region RejectKpiAction

            SimpleAction rejectKpiAction = new SimpleAction(this, "RejectKpiAction", PredefinedCategory.View)
            {
                Caption = "Reject Kpi",
                ConfirmationMessage = "Are you sure you want to Reject the data?",
                ImageName = "Actions_Delete"
            };
            rejectKpiAction.Execute += RejectKpiAction_Execute;

            #endregion RejectKpiAction

            #region CancelKpiAction

            SimpleAction cancelKpiAction = new SimpleAction(this, "CancelKpiAction", PredefinedCategory.View)
            {
                Caption = "Cancel Kpi",
                ConfirmationMessage = "Are you sure you want to Cancel the data?",
                ImageName = "BO_Audit_ChangeHistory"
            };
            cancelKpiAction.Execute += CancelKpiAction_Execute;

            #endregion CancelKpiAction

            #region ProcessBehaviourAction

            SimpleAction processBehaviourAction = new SimpleAction(this, "ProcessBehaviourAction", PredefinedCategory.View)
            {
                Caption = "Process Behaviour",
                ConfirmationMessage = "Are you sure you want to Process the data?",
                ImageName = "BO_Security_Permission_Action"
            };
            processBehaviourAction.Execute += ProcessBehaviourAction_Execute;

            #endregion ProcessBehaviourAction

            #region ApprovalAction 

            SimpleAction kpiSubmitApprovalAction = new SimpleAction(this, "KpiSubmitApprovalAction", PredefinedCategory.View)
            {
                Caption = "ApprovedAction",
                ConfirmationMessage = "Are you sure you want to Approved the data?",
                ImageName = "BO_Security_Permission_Action"
            };
            kpiSubmitApprovalAction.Execute += kpiSubmitApprovalAction_Execute;

            #endregion ApprovalAction

        }
        protected override void OnActivated()
        {
            base.OnActivated();
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region GetKpiAction_Execute
        private void GetKpiAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                JobPosition _currObjectJobPosition = null;
                Employee _locEmployee = null;
                double _pencapaianProgressCascadingSubmitLine = 0;
                double _totalScoreCascading = 0;

                #region get KPI

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        KpiSubmit _locKpiSubmitOS = (KpiSubmit)_objectSpace.GetObject(obj);

                        if (_locKpiSubmitOS != null)
                        {
                            if (_locKpiSubmitOS.Session != null)
                            {
                                _currSession = _locKpiSubmitOS.Session;
                            }

                            if (_locKpiSubmitOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locKpiSubmitOS.Code;
                                _currObjectJobPosition = _locKpiSubmitOS.JobPosition;

                                KpiSubmit _locKpiSubmitXPO = _currSession.FindObject<KpiSubmit>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new BinaryOperator("JobPosition", _currObjectJobPosition)
                                                                    ));

                                if (_locKpiSubmitXPO != null)
                                {
                                    if( _locKpiSubmitXPO.Name != null)
                                    {
                                        _locEmployee = _locKpiSubmitXPO.Name;
                                    }

                                    XPCollection<KpiSubmitLine> _locKpiSubmitLineCheckings = new XPCollection<KpiSubmitLine>
                                                                     (_currSession, new BinaryOperator("KpiSubmit", _locKpiSubmitXPO));
                                    if (_locKpiSubmitLineCheckings != null && _locKpiSubmitLineCheckings.Count() > 0)
                                    {
                                        //Message Information
                                        InformationMessageShow("Data is Ready");
                                    }
                                    else
                                    {
                                        //xp collection jobpositionline
                                        XPCollection<EmployeePositionLine> _locEmployeePositionLines = new XPCollection<EmployeePositionLine>
                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Employee", _locEmployee),
                                                                         new BinaryOperator("Active", true)
                                                                         ));
                                        if (_locEmployeePositionLines != null && _locEmployeePositionLines.Count() > 0)
                                        {
                                            foreach (EmployeePositionLine _locEmployeePositionLine in _locEmployeePositionLines)
                                            {
                                                if (_locEmployeePositionLine.JobPosition != null)
                                                {
                                                    XPCollection<KpiRoleLine> _locKpiRoleLines = new XPCollection<KpiRoleLine>
                                                                      (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("DefaultPosition", _locEmployeePositionLine.JobPosition),
                                                                       new BinaryOperator("Active", true),
                                                                       new GroupOperator(GroupOperatorType.Or,
                                                                       new BinaryOperator("KpiType", KpiType.All),
                                                                       new BinaryOperator("Employee", _locEmployee)
                                                                       )));

                                                    if (_locKpiRoleLines != null && _locKpiRoleLines.Count() > 0)
                                                    {
                                                        foreach (KpiRoleLine _locKpiRoleLine in _locKpiRoleLines)
                                                        {

                                                            KpiSubmitLine _saveKpiSubmitLine = new KpiSubmitLine(_currSession)
                                                            {
                                                                KpiYear = _locKpiRoleLine.KpiYear,
                                                                Name = _locKpiRoleLine,
                                                                Weight = _locKpiRoleLine.Weight,
                                                                Proportion = _locKpiRoleLine.Proportion,
                                                                Ytdtarget = _locKpiRoleLine.Ytdtarget,
                                                                KpiPeriod = _locKpiRoleLine.KpiPeriod,
                                                                KpiFormat = _locKpiRoleLine.KpiFormat,
                                                                KpiTarget = _locKpiRoleLine.KpiTarget,
                                                                KpiValue = _locKpiRoleLine.KpiValue,
                                                                KpiGroupType = _locKpiRoleLine.KpiGroupType,
                                                                KpiYtdType = _locKpiRoleLine.KpiYtdType,
                                                                KpiSubmitLineStatus = Status.Open,
                                                                KpiStatusPencapaian = KpiStatusPencapaian.None,
                                                                KpiSubmit = _locKpiSubmitXPO,
                                                                Company = _locKpiRoleLine.Company,
                                                                OrgDim1 = _locKpiRoleLine.OrgDim1,
                                                                OrgDim2 = _locKpiRoleLine.OrgDim2,
                                                                OrgDim3 = _locKpiRoleLine.OrgDim3,
                                                                OrgDim4 = _locKpiRoleLine.OrgDim4,
                                                                DefaultPosition = _locKpiRoleLine.DefaultPosition,
                                                                Employee = _locEmployee,
                                                            };
                                                            _saveKpiSubmitLine.Save();
                                                            _saveKpiSubmitLine.Session.CommitTransaction();


                                                            if (_locKpiRoleLine.KpiGroupType == KpiGroupType.Cascading)
                                                            {

                                                                //get kpi submit line ketika submitline sudah terisi 
                                                                XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                      (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear),
                                                                       new BinaryOperator("MonthKpi", _locKpiSubmitXPO.KpiSubmitMonth),
                                                                       new BinaryOperator("Employee", _locKpiSubmitXPO.Name),
                                                                       new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                       new BinaryOperator("KpiGroupType", KpiGroupType.Cascading),
                                                                       new GroupOperator(GroupOperatorType.Or,
                                                                       new BinaryOperator("KpiSubmitLineStatus", Status.Open),
                                                                       new BinaryOperator("KpiSubmitLineStatus", Status.Progress),
                                                                       new BinaryOperator("KpiSubmitLineStatus", Status.Approved)
                                                                       )));

                                                                if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                                                                {
                                                                    foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                                                                    {
                                                                        XPCollection<KpiRoleCascadingLine> _locKpiRoleCascadingLines = new XPCollection<KpiRoleCascadingLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("KpiRoleLine", _locKpiRoleLine),
                                                                                            new BinaryOperator("Active", true)
                                                                                            ));
                                                                        if (_locKpiRoleCascadingLines != null && _locKpiRoleCascadingLines.Count() > 0)
                                                                        {
                                                                            foreach (KpiRoleCascadingLine _locKpiRoleCascadingLine in _locKpiRoleCascadingLines)
                                                                            {
                                                                                //kalo bawahannya udah ngisi pencapaian atau udah progress
                                                                                //cek di submit line dengan employee dan kpiroleline 1/1 = _locKpiRoleCascadingLine
                                                                                KpiSubmitLine _locKpiSubmitLineXPO = _currSession.FindObject<KpiSubmitLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Name", _locKpiRoleCascadingLine.KpiRoleLineSelect),
                                                                                                            new BinaryOperator("Employee", _locKpiRoleCascadingLine.Employee),
                                                                                                            new BinaryOperator("Leader", _locKpiSubmitXPO.Name),
                                                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                                                            new BinaryOperator("KpiSubmitLineStatus", Status.Open),
                                                                                                            new BinaryOperator("KpiSubmitLineStatus", Status.Progress),
                                                                                                            new BinaryOperator("KpiSubmitLineStatus", Status.Approved)
                                                                                                            )));
                                                                                if (_locKpiSubmitLineXPO != null)
                                                                                {
                                                                                    //update pencapaiannya dan hitungannya kaya di update sebelumnya
                                                                                    _pencapaianProgressCascadingSubmitLine = _locKpiSubmitLineXPO.ResultValue;
                                                                                    KpiSubmitLineCascading _updateKpiSubmitLineCascading = new KpiSubmitLineCascading(_currSession)
                                                                                    {
                                                                                        KpiRoleCascadingLine = _locKpiRoleCascadingLine,
                                                                                        Weight = _locKpiRoleCascadingLine.Weight,
                                                                                        Pencapaian = _pencapaianProgressCascadingSubmitLine,
                                                                                        Score = (_locKpiRoleCascadingLine.Weight / 100) * _pencapaianProgressCascadingSubmitLine,
                                                                                        Employee = _locKpiRoleCascadingLine.Employee,
                                                                                        KpiSubmitLine = _locKpiSubmitLine,
                                                                                    };
                                                                                    _updateKpiSubmitLineCascading.Save();
                                                                                    _updateKpiSubmitLineCascading.Session.CommitTransaction();
                                                                                    _totalScoreCascading = _totalScoreCascading + _updateKpiSubmitLineCascading.Score;
                                                                                }
                                                                                else
                                                                                {
                                                                                    //biasa kalo bawahan belum ada yg submit pencapaian dan belum progress
                                                                                    KpiSubmitLineCascading _saveKpiSubmitLineCascading = new KpiSubmitLineCascading(_currSession)
                                                                                    {
                                                                                        KpiRoleCascadingLine = _locKpiRoleCascadingLine,
                                                                                        Weight = _locKpiRoleCascadingLine.Weight,
                                                                                        Employee = _locKpiRoleCascadingLine.Employee,
                                                                                        KpiSubmitLine = _locKpiSubmitLine,
                                                                                    };
                                                                                    _saveKpiSubmitLineCascading.Save();
                                                                                    _saveKpiSubmitLineCascading.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                        _locKpiSubmitLine.Pencapaian = _locKpiSubmitLine.Pencapaian + _totalScoreCascading;
                                                                        _locKpiSubmitLine.ResultValue = _locKpiSubmitLine.ResultValue + _totalScoreCascading;
                                                                        _locKpiSubmitLine.Save();
                                                                        _locKpiSubmitLine.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                            _saveKpiSubmitLine.Save();
                                                            _saveKpiSubmitLine.Session.CommitTransaction();

                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        _locKpiSubmitXPO.Save();
                                        _locKpiSubmitXPO.Session.CommitTransaction();

                                        //Message
                                        SuccsessMessageShow("Get Data has successfully");
                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion get KPI

                #region get Behaviour

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        KpiSubmit _locKpiSubmitOS = (KpiSubmit)_objectSpace.GetObject(obj);


                        if (_locKpiSubmitOS != null)
                        {
                            if (_locKpiSubmitOS.Session != null)
                            {
                                _currSession = _locKpiSubmitOS.Session;
                            }

                            if (_locKpiSubmitOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locKpiSubmitOS.Code;
                                _currObjectJobPosition = _locKpiSubmitOS.JobPosition;

                                KpiSubmit _locKpiSubmitXPO = _currSession.FindObject<KpiSubmit>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new BinaryOperator("JobPosition", _currObjectJobPosition)
                                                                    ));

                                if (_locKpiSubmitXPO != null)
                                {
                                    if (_locKpiSubmitXPO.Name != null)
                                    {
                                        _locEmployee = _locKpiSubmitXPO.Name;
                                    }

                                    XPCollection<BehaviourSubmitLine> _locBehaviourRoleLineCheckings = new XPCollection<BehaviourSubmitLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("KpiSubmit", _locKpiSubmitXPO)
                                                               ));
                                    if(_locBehaviourRoleLineCheckings != null && _locBehaviourRoleLineCheckings.Count() > 0)
                                    {
                                        InformationMessageShow("Data is Ready");
                                    }
                                    else
                                    {

                                        #region Behaviour bawahannya
                                        //get Behaviour bawahannnya
                                        XPCollection<Employee> _locEmployees = new XPCollection<Employee>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                   new BinaryOperator("Leader", _locEmployee),
                                                                   new BinaryOperator("Active", true)
                                                                   ));
                                        if (_locEmployees != null && _locEmployees.Count() > 0)
                                        {
                                            foreach (Employee _locEmploye in _locEmployees)
                                            {
                                                XPCollection<BehaviourRoleLine> _locBehaviourRoleLines = new XPCollection<BehaviourRoleLine>
                                                                  (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Active", true)
                                                                  ));
                                                if (_locBehaviourRoleLines != null && _locBehaviourRoleLines.Count() > 0)
                                                {
                                                    foreach (BehaviourRoleLine _locBehaviourRoleLine in _locBehaviourRoleLines)
                                                    {
                                                        BehaviourSubmitLine _saveBehaviourSubmitLine = new BehaviourSubmitLine(_currSession)
                                                        {
                                                            Behaviour = _locBehaviourRoleLine,
                                                            BehaviourPeriod = _locBehaviourRoleLine.BehaviourPeriod,
                                                            BehaviourType = _locBehaviourRoleLine.BehaviourType,
                                                            BehaviourCategory = _locBehaviourRoleLine.BehaviourCategory,
                                                            KpiSubmit = _locKpiSubmitXPO,
                                                            Company = _locKpiSubmitXPO.Company,
                                                            OrgDim1 = _locKpiSubmitXPO.OrgDim1,
                                                            OrgDim2 = _locKpiSubmitXPO.OrgDim2,
                                                            OrgDim3 = _locKpiSubmitXPO.OrgDim3,
                                                            OrgDim4 = _locKpiSubmitXPO.OrgDim4,
                                                            DefaultPosition = _currObjectJobPosition.ToString(),
                                                            Employee = _locEmployee,
                                                            Staff = _locEmploye
                                                        };
                                                        _saveBehaviourSubmitLine.Save();
                                                        _saveBehaviourSubmitLine.Session.CommitTransaction();
                                                    }

                                                }
                                            }

                                        }

                                        #endregion Behaviour Bawahan

                                        #region Behaviour sendiri
                                        //xp collection behaviour role line ini yg biasa tanpa bawahan
                                        XPCollection<BehaviourRoleLine> _locBehaviourRoleLine2s = new XPCollection<BehaviourRoleLine>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                   new BinaryOperator("Active", true)
                                                                   ));
                                        if (_locBehaviourRoleLine2s != null && _locBehaviourRoleLine2s.Count() > 0)
                                        {
                                            foreach (BehaviourRoleLine _locBehaviourRoleLine2 in _locBehaviourRoleLine2s)
                                            {
                                                BehaviourSubmitLine _saveBehaviourSubmitLine2 = new BehaviourSubmitLine(_currSession)
                                                {
                                                    Behaviour = _locBehaviourRoleLine2,
                                                    BehaviourPeriod = _locBehaviourRoleLine2.BehaviourPeriod,
                                                    BehaviourType = _locBehaviourRoleLine2.BehaviourType,
                                                    BehaviourCategory = _locBehaviourRoleLine2.BehaviourCategory,
                                                    KpiSubmit = _locKpiSubmitXPO,
                                                    Company = _locKpiSubmitXPO.Company,
                                                    OrgDim1 = _locKpiSubmitXPO.OrgDim1,
                                                    OrgDim2 = _locKpiSubmitXPO.OrgDim2,
                                                    OrgDim3 = _locKpiSubmitXPO.OrgDim3,
                                                    OrgDim4 = _locKpiSubmitXPO.OrgDim4,
                                                    DefaultPosition = _currObjectJobPosition.ToString(),
                                                    Employee = _locEmployee,
                                                };
                                                _saveBehaviourSubmitLine2.Save();
                                                _saveBehaviourSubmitLine2.Session.CommitTransaction();
                                            }

                                        }
                                        _locKpiSubmitXPO.Save();
                                        _locKpiSubmitXPO.Session.CommitTransaction();

                                        //Message
                                        SuccsessMessageShow("Get Data Behaviour has successfully");

                                        #endregion Behaviour sendiri
                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion get Behaviour


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = KpiSubmit " + ex.ToString());
            }
        }

        #endregion GetKpiAction_Execute

        #region ProgressKpiAction_Execute

        private void ProgressKpiAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Company _locCompany = null;
                Employee _locEmployee = null;
                JobPosition _locEmployeeJob = null;
                OrganizationDimension2 _locEmployeeDepartement = null;
                Month _locEmployeeKpiSubmitMonth = 0;
                KpiYear _locEmployeeKpiSubmitYear = null;
                string _locSubjectMail = null;
                string _locBody = null;
                string _locMailTo = null;
                int _kpiStatusPica = 0;
                double _pencapaianProgressCascadingSubmitLine = 0;
                double _totalScoreCascading = 0;
                double _locWeight = 0;
                string _locLeader = null;
                bool isKpiActionEnabled = true;

                #region Progress KPI
                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        KpiSubmit _locKpiSubmitOS = (KpiSubmit)_objectSpace.GetObject(obj);

                        if (_locKpiSubmitOS != null)
                        {
                            if (_locKpiSubmitOS.Session != null)
                            {
                                _currSession = _locKpiSubmitOS.Session;
                            }

                            if (_locKpiSubmitOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locKpiSubmitOS.Code;
                                _locLeader = _locKpiSubmitOS.Leader.Code;

                                KpiSubmit _locKpiSubmitXPO = _currSession.FindObject<KpiSubmit>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Open),
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Reject),
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Cancel),
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Progress)
                                                                    )));
                                if (_locKpiSubmitXPO != null)
                                {
                                    if (_locKpiSubmitXPO.Company != null) { _locCompany = _locKpiSubmitXPO.Company; }
                                    if (_locKpiSubmitXPO.Name != null) { _locEmployee = _locKpiSubmitXPO.Name; }
                                    if (_locKpiSubmitXPO.JobPosition != null) { _locEmployeeJob = _locKpiSubmitXPO.JobPosition; }
                                    if (_locKpiSubmitXPO.OrgDim2 != null) { _locEmployeeDepartement = _locKpiSubmitXPO.OrgDim2; }
                                    if (_locKpiSubmitXPO.KpiSubmitMonth != 0) { _locEmployeeKpiSubmitMonth = _locKpiSubmitXPO.KpiSubmitMonth; }
                                    if (_locKpiSubmitXPO.KpiSubmitYear != null) { _locEmployeeKpiSubmitYear = _locKpiSubmitXPO.KpiSubmitYear; }

                                    XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("KpiSubmitLineStatus", Status.Open),
                                                                    new BinaryOperator("KpiSubmitLineStatus", Status.Reject),
                                                                    new BinaryOperator("KpiSubmitLineStatus", Status.Cancel),
                                                                    new BinaryOperator("KpiSubmitLineStatus", Status.Progress)
                                                                    )));
                                    if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                                    {
                                        #region untuk update data kpi sabmit cascading di atasan
                                        foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                                        {
                                            if (_locKpiSubmitLine.KpiStatusPencapaian == KpiStatusPencapaian.TidakTercapai)
                                            {
                                                XPCollection<Pica> _locPicas = new XPCollection<Pica>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("KpiSubmitLine", _locKpiSubmitLine)
                                                                            ));
                                                if (_locPicas != null && _locPicas.Count() < 1)
                                                {
                                                    _kpiStatusPica = _kpiStatusPica + 1;
                                                }
                                                if (_locPicas == null && _locPicas.Count() < 1)
                                                {
                                                    _kpiStatusPica = _kpiStatusPica + 1;
                                                }

                                            }
                                            if (_kpiStatusPica < 1)
                                            {
                                                if (_locKpiSubmitLine.KpiGroupType == KpiGroupType.Cascading)
                                                {
                                                    #region Cek KPI Submit Line yang sesuai leadernya yang type nya cascading

                                                    #region Atasan
                                                    KpiSubmitLine _locKpiSubmitLineProgress = _currSession.FindObject<KpiSubmitLine>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Employee", _locKpiSubmitXPO.Leader),
                                                        new BinaryOperator("KpiGroupType", KpiGroupType.Cascading),
                                                        new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear),
                                                        new BinaryOperator("MonthKpi", _locKpiSubmitXPO.KpiSubmitMonth),
                                                        new BinaryOperator("Company", _locKpiSubmitXPO.Company),
                                                        new BinaryOperator("OrgDim1", _locKpiSubmitXPO.OrgDim1),
                                                        new BinaryOperator("OrgDim2", _locKpiSubmitXPO.OrgDim2),
                                                        new GroupOperator(GroupOperatorType.Or,
                                                        new BinaryOperator("KpiSubmitLineStatus", Status.Open),
                                                        new BinaryOperator("KpiSubmitLineStatus", Status.Reject),
                                                        new BinaryOperator("KpiSubmitLineStatus", Status.Cancel),
                                                        new BinaryOperator("KpiSubmitLineStatus", Status.Progress)
                                                       )));

                                                    if (_locKpiSubmitLineProgress != null)
                                                    {

                                                        #region Find KPI Submit Cascading yang namanya sesuai nama staff/bawahan yang login di Cascading atasannya

                                                        XPCollection<KpiSubmitLineCascading> _locKpiSubmitLineCascadingProgress = new XPCollection<KpiSubmitLineCascading>
                                                                              (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("KpiSubmitLine", _locKpiSubmitLineProgress),
                                                                              new BinaryOperator("Employee", _locKpiSubmitXPO.Name),
                                                                              new GroupOperator(GroupOperatorType.Or,
                                                                              new BinaryOperator("Status", Status.Open),
                                                                              new BinaryOperator("Status", Status.Reject),
                                                                              new BinaryOperator("Status", Status.Cancel),
                                                                              new BinaryOperator("Status", Status.Progress)
                                                                              )));

                                                        if (_locKpiSubmitLineCascadingProgress != null && _locKpiSubmitLineCascadingProgress.Count() > 0)
                                                        {
                                                            foreach (KpiSubmitLineCascading _locKpiSubmitLineCascadingProgres in _locKpiSubmitLineCascadingProgress)
                                                            {
                                                                #region update status Kpi Submit Line Cascading punya atasan dari bawahan

                                                                _locWeight = _locKpiSubmitLineCascadingProgres.Weight;

                                                                _pencapaianProgressCascadingSubmitLine = _locKpiSubmitLine.ResultValue;

                                                                _locKpiSubmitLineCascadingProgres.Pencapaian = _pencapaianProgressCascadingSubmitLine;
                                                                _locKpiSubmitLineCascadingProgres.Score = (_locKpiSubmitLineCascadingProgres.Weight / 100) * _pencapaianProgressCascadingSubmitLine;
                                                                _locKpiSubmitLineCascadingProgres.Save();
                                                                _locKpiSubmitLineCascadingProgres.Session.CommitTransaction();

                                                                #endregion update status Kpi Submit Line Cascading  atasan dari bawahan
                                                            }

                                                        }

                                                        #endregion Find KPI Submit Cascading yang namanya sesuai nama staff/bawahan yang login di Cascading atasannya
                                                    }

                                                    #endregion Atasan

                                                    #region Atasan2
                                                    KpiSubmitLine _locKpiSubmitLineProgres2 = _currSession.FindObject<KpiSubmitLine>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Employee", _locKpiSubmitXPO.Leader),
                                                        new BinaryOperator("KpiGroupType", KpiGroupType.Cascading),
                                                        new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear),
                                                        new BinaryOperator("MonthKpi", _locKpiSubmitXPO.KpiSubmitMonth),
                                                        new BinaryOperator("Company", _locKpiSubmitXPO.Company),
                                                        new BinaryOperator("OrgDim1", _locKpiSubmitXPO.OrgDim1),
                                                        new BinaryOperator("OrgDim2", _locKpiSubmitXPO.OrgDim2),
                                                        new GroupOperator(GroupOperatorType.Or,
                                                        new BinaryOperator("KpiSubmitLineStatus", Status.Open),
                                                        new BinaryOperator("KpiSubmitLineStatus", Status.Reject),
                                                        new BinaryOperator("KpiSubmitLineStatus", Status.Cancel),
                                                        new BinaryOperator("KpiSubmitLineStatus", Status.Progress)
                                                       )));

                                                    if (_locKpiSubmitLineProgres2 != null)
                                                    {
                                                        XPCollection<KpiSubmitLineCascading> _locKpiSubmitLineCascadingProgress = new XPCollection<KpiSubmitLineCascading>
                                                                              (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("KpiSubmitLine", _locKpiSubmitLineProgres2),
                                                                              new GroupOperator(GroupOperatorType.Or,
                                                                              new BinaryOperator("Status", Status.Open),
                                                                              new BinaryOperator("Status", Status.Reject),
                                                                              new BinaryOperator("Status", Status.Cancel),
                                                                              new BinaryOperator("Status", Status.Progress)
                                                                              )));
                                                        if (_locKpiSubmitLineCascadingProgress != null && _locKpiSubmitLineCascadingProgress.Count() > 0)
                                                        {
                                                            foreach (KpiSubmitLineCascading _locKpiSubmitLineCascadingProgres in _locKpiSubmitLineCascadingProgress)
                                                            {
                                                                _totalScoreCascading = _totalScoreCascading + _locKpiSubmitLineCascadingProgres.Score;
                                                            }
                                                        }
                                                        _locKpiSubmitLineProgres2.Pencapaian = _totalScoreCascading;
                                                        _locKpiSubmitLineProgres2.Save();
                                                        _locKpiSubmitLineProgres2.Session.CommitTransaction();

                                                    }

                                                    #endregion Atasan2

                                                    #endregion Cek KPI Submit Line yang sesuai leadernya yang type nya cascading

                                                    #region update status KPI Submit Line user yang login/diri sendiri (Personal)

                                                    if (_locKpiSubmitLine.KpiStatusPencapaian == KpiStatusPencapaian.TidakTercapai)
                                                    {
                                                        XPCollection<Pica> _locPicas = new XPCollection<Pica>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("KpiSubmitLine", _locKpiSubmitLine)
                                                                                    ));
                                                        if (_locPicas != null && _locPicas.Count() < 1)
                                                        {
                                                            _kpiStatusPica = _kpiStatusPica + 1;
                                                        }
                                                        if (_locPicas == null && _locPicas.Count() < 1)
                                                        {
                                                            _kpiStatusPica = _kpiStatusPica + 1;
                                                        }

                                                    }
                                                    if (_kpiStatusPica < 1)
                                                    {
                                                        #region update status Kpi Submit Line

                                                        if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Open)
                                                        {
                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Progress;
                                                            _locKpiSubmitLine.Save();
                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                        }
                                                        else if(_locKpiSubmitLine.KpiSubmitLineStatus == Status.Cancel)
                                                        {
                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Progress;
                                                            _locKpiSubmitLine.Save();
                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                        }
                                                        else if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Reject)
                                                        {
                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Progress;
                                                            _locKpiSubmitLine.Save();
                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                        }

                                                        #endregion update status Kpi Submit Line

                                                        #region Update status Kpi Submit
                                                        //Update status kpi submit di akun user yg login
                                                        _locKpiSubmitXPO.KpiSubmitStatus = Status.Progress;
                                                        _locKpiSubmitXPO.Save();
                                                        _locKpiSubmitXPO.Session.CommitTransaction();

                                                        #endregion Update status Kpi Submit
                                                    }
                                                    if (_kpiStatusPica > 0)
                                                    {
                                                        ErrorMessageShow("CAN NOT CONTINUE TO PROGRESS, YOUR PICA NOT AVAILABLE");
                                                    }

                                                    #endregion update status KPI Submit Line user yang login/diri sendiri (Personal)
                                                }
                                          
                                                else if (_locKpiSubmitLine.KpiGroupType == KpiGroupType.Personal)
                                                {
                                                    #region update status KPI Submit Line user yang login/diri sendiri (Personal)

                                                    if (_locKpiSubmitLine.KpiStatusPencapaian == KpiStatusPencapaian.TidakTercapai)
                                                    {
                                                        XPCollection<Pica> _locPicas = new XPCollection<Pica>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("KpiSubmitLine", _locKpiSubmitLine)
                                                                                    ));
                                                        if (_locPicas != null && _locPicas.Count() < 1)
                                                        {
                                                            _kpiStatusPica = _kpiStatusPica + 1;
                                                        }
                                                        if (_locPicas == null && _locPicas.Count() < 1)
                                                        {
                                                            _kpiStatusPica = _kpiStatusPica + 1;
                                                        }

                                                    }
                                                    if (_kpiStatusPica < 1)
                                                    {
                                                        #region update status Kpi Submit Line

                                                        if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Open)
                                                        {
                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Progress;
                                                            _locKpiSubmitLine.Save();
                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                        }
                                                        else if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Cancel)
                                                        {
                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Progress;
                                                            _locKpiSubmitLine.Save();
                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                        }
                                                        else if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Reject)
                                                        {
                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Progress;
                                                            _locKpiSubmitLine.Save();
                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                        }


                                                        #endregion update status Kpi Submit Line

                                                        #region Update status Kpi Submit
                                                        //Update status kpi submit di akun user yg login
                                                        _locKpiSubmitXPO.KpiSubmitStatus = Status.Progress;
                                                        _locKpiSubmitXPO.Save();
                                                        _locKpiSubmitXPO.Session.CommitTransaction();

                                                        #endregion Update status Kpi Submit

                                                        //Message
                                                        SuccsessMessageShow("Kpi Submit has successfully updates to progress");
                                                    }
                                                    if (_kpiStatusPica > 0)
                                                    {
                                                        ErrorMessageShow("PICA Not Available");

                                                        #region update status Kpi Submit Line

                                                        if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Open)
                                                        {
                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Open;
                                                            _locKpiSubmitLine.Save();
                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                        }
                                                        else if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Cancel)
                                                        {
                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Open;
                                                            _locKpiSubmitLine.Save();
                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                        }
                                                        else if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Reject)
                                                        {
                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Open;
                                                            _locKpiSubmitLine.Save();
                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                        }


                                                        #endregion update status Kpi Submit Line

                                                        #region Update status Kpi Submit
                                                        //Update status kpi submit di akun user yg login
                                                        _locKpiSubmitXPO.KpiSubmitStatus = Status.Open;
                                                        _locKpiSubmitXPO.Save();
                                                        _locKpiSubmitXPO.Session.CommitTransaction();

                                                        #endregion Update status Kpi Submit
                                                    }

                                                    #endregion update status KPI Submit Line user yang login/diri sendiri (Personal)
                                                }
                                            }
                                            if (_kpiStatusPica > 0)
                                            {
                                                ErrorMessageShow("CAN NOT CONTINUE TO PROGRESS, YOUR PICA NOT AVAILABLE");
                                            }

                                        }

                                        #endregion untuk update data kpi saubmit cascading di atasan

                                    }

                                    Employee _locEmployeeLeader = _currSession.FindObject<Employee>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _locLeader),
                                                                        new BinaryOperator("Active", true)
                                                                        ));
                                    if(_locEmployeeLeader != null) 
                                    {
                                        
                                        #region Email
                                        ApplicationMailSetting _locApplicationMailSetting = _globFunc.GetApplicationMailSetup(_currSession, _locCompany);
                                        if (_locApplicationMailSetting != null)
                                        {
                                            _locSubjectMail = "Permohonan Approve Kpi ";
                                            _locMailTo = _locEmployeeLeader.Email;
                                            _locBody = _globFunc.BackgroundBodyMailKpiProgress(_locEmployee, _locEmployeeJob, _locEmployeeDepartement,_locEmployeeKpiSubmitMonth,_locEmployeeKpiSubmitYear);
                                            _globFunc.SetAndSendMail(_locApplicationMailSetting.SmtpHost, _locApplicationMailSetting.SmtpPort, _locApplicationMailSetting.MailFrom, _locApplicationMailSetting.MailFromPassword, _locMailTo, _locSubjectMail, _locBody);
                                        }
                                        #endregion Email

                                    }

                                    SetKpiSubmitTaskBaseByApprovalInformationLevelForKpiSubmit(_currSession, _locKpiSubmitXPO, ObjectList.KpiSubmit, ApprovalLevel.Level1);
                                   
                                }
                                else
                                {
                                    //Message error
                                    ErrorMessageShow("Data Kpi Submit Not Available");
                                }

                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Kpi Submit Not Available");
                            }

                            if (!isKpiActionEnabled)
                            {
                                // Your logic for when the action is disabled
                                Console.WriteLine("Action is disabled. Cannot proceed.");
                                return; // Optionally, you can return early or perform other actions.
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion Progress KPI

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        #endregion ProgressKpiAction_Execute

        #region RejectKpiAction_Execute

        private void RejectKpiAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Company _locCompany = null;
                Employee _locEmployee = null;
                JobPosition _locEmployeeJob = null;
                OrganizationDimension2 _locEmployeeDepartement = null;
                Month _locEmployeeKpiSubmitMonth = 0;
                KpiYear _locEmployeeKpiSubmitYear = null;
                string _locSubjectMail = null;
                string _locBody = null;
                string _locMailTo = null;
                string _locCodeEmployee = null;
                GlobalFunction _globFunc = new GlobalFunction();

                #region Reject KPI
                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        KpiSubmit _locKpiSubmitOS = (KpiSubmit)_objectSpace.GetObject(obj);

                        if (_locKpiSubmitOS != null)
                        {
                            if (_locKpiSubmitOS.Session != null)
                            {
                                _currSession = _locKpiSubmitOS.Session;
                            }

                            if (_locKpiSubmitOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locKpiSubmitOS.Code;
                                _locCodeEmployee = _locKpiSubmitOS.Name.Code;

                                KpiSubmit _locKpiSubmitXPO = _currSession.FindObject<KpiSubmit>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Progress),
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Reject)
                                                                    )));
                                if (_locKpiSubmitXPO != null)
                                {
                                    if (_locKpiSubmitXPO.Company != null) { _locCompany = _locKpiSubmitXPO.Company; }
                                    if (_locKpiSubmitXPO.Name != null) { _locEmployee = _locKpiSubmitXPO.Name; }
                                    if (_locKpiSubmitXPO.JobPosition != null) { _locEmployeeJob = _locKpiSubmitXPO.JobPosition; }
                                    if (_locKpiSubmitXPO.OrgDim2 != null) { _locEmployeeDepartement = _locKpiSubmitXPO.OrgDim2; }
                                    if (_locKpiSubmitXPO.KpiSubmitMonth != 0) { _locEmployeeKpiSubmitMonth = _locKpiSubmitXPO.KpiSubmitMonth; }
                                    if (_locKpiSubmitXPO.KpiSubmitYear != null) { _locEmployeeKpiSubmitYear = _locKpiSubmitXPO.KpiSubmitYear; }

                                    XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("KpiSubmitLineStatus", Status.Progress),
                                                                    new BinaryOperator("KpiSubmitLineStatus", Status.Reject)
                                                                    )));
                                    if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                                    {
                                        
                                        foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                                        {
                                            #region Update Status Kpi Submit Line

                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Reject;
                                            _locKpiSubmitLine.Save();
                                            _locKpiSubmitLine.Session.CommitTransaction();

                                            #endregion Update Status Kpi Submit Line
                                        }

                                    }

                                    #region Update status Kpi Submit

                                    _locKpiSubmitXPO.KpiSubmitStatus = Status.Reject;
                                    _locKpiSubmitXPO.Save();
                                    _locKpiSubmitXPO.Session.CommitTransaction();

                                    #endregion Update status Kpi Submit

                                    Employee _locEmployeeKpiSubmit = _currSession.FindObject<Employee>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locCodeEmployee),
                                                                     new BinaryOperator("Active", true)
                                                                     ));
                                    if (_locEmployeeKpiSubmit != null) 
                                    {
                                        #region Email
                                        ApplicationMailSetting _locApplicationMailSetting = _globFunc.GetApplicationMailSetup(_currSession, _locCompany);
                                        if (_locApplicationMailSetting != null)
                                        {
                                            _locSubjectMail = "Penolakan Pengajuan Kpi ";
                                            _locMailTo = _locEmployee.Email;
                                            _locBody = _globFunc.BackgroundBodyMailKpiReject(_locEmployee, _locEmployeeJob, _locEmployeeDepartement, _locEmployeeKpiSubmitMonth, _locEmployeeKpiSubmitYear);
                                            _globFunc.SetAndSendMail(_locApplicationMailSetting.SmtpHost, _locApplicationMailSetting.SmtpPort, _locApplicationMailSetting.MailFrom, _locApplicationMailSetting.MailFromPassword, _locMailTo, _locSubjectMail, _locBody);
                                        }
                                        #endregion Email
                                    }
                                    //Message error
                                    ErrorMessageShow("Data Kpi Submit Status Is Rejected");

                                }
                                else
                                {
                                    //Message error
                                    ErrorMessageShow("Data Kpi Submit Not Available");
                                }

                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Kpi Submit Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion Reject KPI

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = KpiSubmit " + ex.ToString());
            }
        }

        #endregion RejectKpiAction_Execute #region RejectKpiAction_Execute

        #region Kpi Approval
        private void kpiSubmitApprovalAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                XPCollection<ApplicationApprovalSetting> _locAppApprovalSettings = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;
                Company _locCompany = null;
                OrganizationDimension1 _locOrgDim1 = null;
                OrganizationDimension2 _locOrgDim2 = null;
                OrganizationDimension3 _locOrgDim3 = null;
                Employee _locEmployee = null;
                string _locMailTo = null;
                string _locSubjectMail = null;
                string _locBody = null;
                string _locCodeEmployee = null;



                foreach (Object obj in _objectsToProcess)
                {
                    KpiSubmit _objInNewObjectSpace = (KpiSubmit)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                        KpiSubmit _locKpiSubmitXPO = _currentSession.FindObject<KpiSubmit>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Progress),
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Approved)
                                                                    )));

                        if (_locKpiSubmitXPO != null)
                        {

                            if (_locKpiSubmitXPO.Company != null) { _locCompany = _locKpiSubmitXPO.Company; }
                            if (_locKpiSubmitXPO.Name != null) { _locCodeEmployee = _locKpiSubmitXPO.Name.Code; }
                            if (_locKpiSubmitXPO.OrgDim1 != null) { _locOrgDim1 = _locKpiSubmitXPO.OrgDim1; }
                            if (_locKpiSubmitXPO.OrgDim2 != null) { _locOrgDim2 = _locKpiSubmitXPO.OrgDim2; }
                            if (_locKpiSubmitXPO.OrgDim3 != null) { _locOrgDim3 = _locKpiSubmitXPO.OrgDim3; }

                            if (_locKpiSubmitXPO.Name != null)
                            {
                                if (_locKpiSubmitXPO.Name.Email != null) { _locMailTo = _locKpiSubmitXPO.Name.Email; }
                            }

                            KpiSubmitApproval _locKpiSubmitApproval = _currentSession.FindObject<KpiSubmitApproval>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                            if (_locKpiSubmitApproval == null)
                            {

                                _locAppApprovalSettings = _globFunc.GetApplicationApprovalSettingByApprovalLevel(_currentSession, _locUserAccess, ObjectList.KpiSubmit,
                                                                                                                _locCompany, _locOrgDim1, _locOrgDim2, _locOrgDim3);

                                if (_locAppApprovalSettings != null && _locAppApprovalSettings.Count() > 0)
                                {
                                    foreach (ApplicationApprovalSetting _locAppApprovalSetting in _locAppApprovalSettings)
                                    {
                                        if (_locAppApprovalSetting.Employee != null)
                                        {
                                            _locEmployee = _locAppApprovalSetting.Employee;
                                        }

                                        #region Approval Level 1
                                        if (_locAppApprovalSetting.ApprovalLevel == ApprovalLevel.Level1)
                                        {
                                            bool _locActivationPosting = false;
                                            bool _locLockApproval = false;

                                            KpiSubmitApproval _locKpiSubmitApprovalXPO = _currentSession.FindObject<KpiSubmitApproval>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locKpiSubmitApprovalXPO == null)
                                            {
                                                if (_locKpiSubmitXPO.ActivationPosting == false)
                                                {
                                                    if (_locAppApprovalSetting.Lock == true)
                                                    {
                                                        _locActivationPosting = true;
                                                        _locLockApproval = true;
                                                    }
                                                }
                                                else
                                                {
                                                    _locActivationPosting = _locKpiSubmitXPO.ActivationPosting;
                                                }

                                                SetKpiSubmitTaskBaseByApprovalInformationLevelForKpiSubmit(_currentSession, _locKpiSubmitXPO, ObjectList.KpiSubmit, ApprovalLevel.Level2);
                                                SetActivationPostingKpiSubmitLine(_currentSession, _locKpiSubmitXPO, _locActivationPosting);

                                                #region SaveKpiSubmitApproval

                                                if (_locAppApprovalSetting.EndApproval == true)
                                                {
                                                    KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        LockApproval = _locLockApproval,
                                                        EndApproval = true,
                                                        KpiSubmit = _locKpiSubmitXPO,
                                                        ApprovedBy = _locEmployee,
                                                    };
                                                    _saveDataKSA.Save();
                                                    _saveDataKSA.Session.CommitTransaction();

                                                    #region Save to Kpi Monitoring

                                                    KpiSubmit _locKpiSubmitOS = _locKpiSubmitXPO;

                                                    Session _currSession = null;
                                                    double _locJanuary = 0;
                                                    double _locFebruary = 0;
                                                    double _locMarch = 0;
                                                    double _locApril = 0;
                                                    double _locMay = 0;
                                                    double _locJune = 0;
                                                    double _locJuly = 0;
                                                    double _locAugust = 0;
                                                    double _locSeptember = 0;
                                                    double _locOctober = 0;
                                                    double _locNovember = 0;
                                                    double _locDecember = 0;
                                                    double _locytd = 0;
                                                    double _locScoreYtd = 0;


                                                    if (_locKpiSubmitOS != null)
                                                    {
                                                        if (_locKpiSubmitOS.Session != null)
                                                        {
                                                            _currSession = _locKpiSubmitOS.Session;
                                                        }

                                                        if (_locKpiSubmitOS.Code != null && _currSession != null)
                                                        {
                                                            _currObjectId = _locKpiSubmitOS.Code;
                                                            KpiSubmit _locKpiSubmitXPO2 = _currSession.FindObject<KpiSubmit>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Code", _currObjectId),
                                                                                                new BinaryOperator("KpiSubmitStatus", Status.Progress)
                                                                                                ));
                                                            if (_locKpiSubmitXPO2 != null)
                                                            {
                                                                XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("KpiSubmit", _locKpiSubmitXPO2),
                                                                                                new BinaryOperator("KpiSubmitLineStatus", Status.Progress)
                                                                                                ));
                                                                if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                                                                {
                                                                    foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                                                                    {
                                                                        #region Perhitungan berdasarkan KpiTypePeriod

                                                                        XPCollection<KpiSubmitLine> _locKpiSubmitLines2 = new XPCollection<KpiSubmitLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Name", _locKpiSubmitLine.Name),
                                                                                            new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear)
                                                                                            ));
                                                                        if (_locKpiSubmitLines2 != null && _locKpiSubmitLines2.Count() > 0)
                                                                        {
                                                                            foreach (KpiSubmitLine _locKpiSubmitLine2 in _locKpiSubmitLines2)
                                                                            {

                                                                                #region Loc Pencapaian Per Bulan

                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.January)
                                                                                {
                                                                                    _locJanuary = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.February)
                                                                                {
                                                                                    _locFebruary = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.March)
                                                                                {
                                                                                    _locMarch = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.April)
                                                                                {
                                                                                    _locApril = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.May)
                                                                                {
                                                                                    _locMay = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.June)
                                                                                {
                                                                                    _locJune = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.July)
                                                                                {
                                                                                    _locJuly = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.August)
                                                                                {
                                                                                    _locAugust = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.September)
                                                                                {
                                                                                    _locSeptember = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.October)
                                                                                {
                                                                                    _locOctober = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.November)
                                                                                {
                                                                                    _locNovember = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.December)
                                                                                {
                                                                                    _locDecember = _locKpiSubmitLine2.Pencapaian;
                                                                                }

                                                                                #endregion Pencapaian Per Bulan

                                                                                #region Ytd Kpi Monitoring Type Sum All
                                                                                if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.SumAll)
                                                                                {
                                                                                    _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember);
                                                                                }
                                                                                #endregion Ytd Kpi Monitoring Type Sum All

                                                                                #region SEND DATA KE KPI Monitoring
                                                                                KpiMonitoring _locKpiMonitoring = _currSession.FindObject<KpiMonitoring>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                                        new BinaryOperator("Name", _locKpiSubmitLine2.Name),
                                                                                                                                                        new BinaryOperator("KpiYear", _locKpiSubmitLine2.KpiYear),
                                                                                                                                                        new BinaryOperator("DefaultPosition", _locKpiSubmitLine2.DefaultPosition),
                                                                                                                                                        new BinaryOperator("Employee", _locKpiSubmitLine2.Employee)
                                                                                                                                                        ));
                                                                                if (_locKpiMonitoring != null)
                                                                                {
                                                                                    #region Ytd Kpi Monitoring Type Sum All
                                                                                    if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.SumAll)
                                                                                    {
                                                                                        _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember);
                                                                                    }
                                                                                    #endregion Ytd Kpi Monitoring Type Sum All

                                                                                    #region Ytd Kpi Monitoring Type Average
                                                                                    if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.Average)
                                                                                    {
                                                                                        int a = 0;
                                                                                        double b = 0;
                                                                                        double average = 0;

                                                                                        if (_locJanuary > 0) { a = a + 1; b = b + _locJanuary; }
                                                                                        if (_locFebruary > 0) { a = a + 1; b = b + _locFebruary; }
                                                                                        if (_locMarch > 0) { a = a + 1; b = b + _locMarch; }
                                                                                        if (_locApril > 0) { a = a + 1; b = b + _locApril; }
                                                                                        if (_locMay > 0) { a = a + 1; b = b + _locMay; }
                                                                                        if (_locJune > 0) { a = a + 1; b = b + _locJune; }
                                                                                        if (_locJuly > 0) { a = a + 1; b = b + _locJuly; }
                                                                                        if (_locAugust > 0) { a = a + 1; b = b + _locAugust; }
                                                                                        if (_locSeptember > 0) { a = a + 1; b = b + _locSeptember; }
                                                                                        if (_locOctober > 0) { a = a + 1; b = b + _locOctober; }
                                                                                        if (_locNovember > 0) { a = a + 1; b = b + _locNovember; }
                                                                                        if (_locDecember > 0) { a = a + 1; b = b + _locDecember; }

                                                                                        average = b / a;
                                                                                        _locytd = average;
                                                                                        a = 0;
                                                                                        b = 0;
                                                                                    }
                                                                                    #endregion Ytd Kpi Monitoring Type Average

                                                                                    #region Value Kpi Monitoring

                                                                                    double _locScoreGrading = 0;

                                                                                    XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                                                                   new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                                   new BinaryOperator("Active", true)),
                                                                                                                                   new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                                   );
                                                                                    if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                                    {

                                                                                        foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                                        {

                                                                                            if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                            {
                                                                                                if (_locytd > _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                            {
                                                                                                if (_locytd <= _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                            {
                                                                                                if (_locytd < _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                            {
                                                                                                if (_locytd == _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    #endregion Value Kpi Monitoring

                                                                                    #region Score Kpi Monitoring

                                                                                    _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine2.Proportion / 100);

                                                                                    #endregion Score Kpi Monitoring

                                                                                    #region Update Field

                                                                                    _locKpiMonitoring.January = _locJanuary;
                                                                                    _locKpiMonitoring.February = _locFebruary;
                                                                                    _locKpiMonitoring.March = _locMarch;
                                                                                    _locKpiMonitoring.April = _locApril;
                                                                                    _locKpiMonitoring.May = _locMay;
                                                                                    _locKpiMonitoring.June = _locJune;
                                                                                    _locKpiMonitoring.July = _locJuly;
                                                                                    _locKpiMonitoring.August = _locAugust;
                                                                                    _locKpiMonitoring.September = _locSeptember;
                                                                                    _locKpiMonitoring.October = _locOctober;
                                                                                    _locKpiMonitoring.November = _locNovember;
                                                                                    _locKpiMonitoring.December = _locDecember;
                                                                                    _locKpiMonitoring.Ytd = _locytd;
                                                                                    _locKpiMonitoring.Value = _locScoreGrading;
                                                                                    _locKpiMonitoring.ScoreYtd = _locScoreYtd;
                                                                                    _locKpiMonitoring.Save();
                                                                                    _locKpiMonitoring.Session.CommitTransaction();

                                                                                    #endregion Update Field

                                                                                    #region Save to KPI Final Score

                                                                                    XPCollection<KpiMonitoring> _locKpiMonitorings = new XPCollection<KpiMonitoring>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("Employee", _locKpiSubmitXPO.Name),
                                                                                           new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear)
                                                                                           ));
                                                                                    if (_locKpiMonitorings != null & _locKpiMonitorings.Count() > 0)
                                                                                    {
                                                                                        double score = 0;
                                                                                        double totalScore = 0;

                                                                                        foreach (KpiMonitoring _locKpiMonitoring2 in _locKpiMonitorings)
                                                                                        {
                                                                                            score = _locKpiMonitoring2.ScoreYtd;
                                                                                        }


                                                                                        totalScore = score + score;

                                                                                        KpiFinalScore _locKpiFinalScore = _currSession.FindObject<KpiFinalScore>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                                       new BinaryOperator("Employee", _locKpiSubmitXPO.Name)));
                                                                                        if (_locKpiFinalScore != null)
                                                                                        {
                                                                                            _locKpiFinalScore.TotalScore = totalScore;
                                                                                            _locKpiFinalScore.TotalFinalTarget = totalScore;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            KpiFinalScore _saveKpiFinalScoreUpdate = new KpiFinalScore(_currSession)
                                                                                            {
                                                                                                Code = "FS-",
                                                                                                TotalScore = totalScore,
                                                                                                TotalFinalTarget = totalScore,
                                                                                                Employee = _locKpiSubmitLine.Employee,
                                                                                            };
                                                                                            _saveKpiFinalScoreUpdate.Save();
                                                                                            _saveKpiFinalScoreUpdate.Session.CommitTransaction();
                                                                                        }

                                                                                    }

                                                                                    #endregion Save to KPI Final Score
                                                                                }
                                                                                else
                                                                                {
                                                                                    #region Ytd from New Data

                                                                                    if(_locKpiSubmitLine2.KpiPeriod == KpiPeriod.Monthly)
                                                                                    {
                                                                                        _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember) / 1;
                                                                                    }
                                                                                 

                                                                                    #endregion Ytd from New Data

                                                                                    #region Value Kpi Monitoring Awal Penginputan Data

                                                                                    double _locScoreGrading = 0;

                                                                                    XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                                                                   new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                                   new BinaryOperator("Active", true)),
                                                                                                                                   new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                                   );
                                                                                    if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                                    {

                                                                                        foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                                        {

                                                                                            if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian > _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian <= _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian < _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian == _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    #endregion Value Kpi Monitoring Awal Penginputan Data

                                                                                    #region Score Kpi Monitoring Awal Penginputan Data

                                                                                    _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine.Proportion / 100);

                                                                                    #endregion Score Kpi Monitoring Awal Penginputan Data

                                                                                    #region Add New Value in Field Kpi Monitoring

                                                                                    KpiMonitoring _saveKpiMonitoring = new KpiMonitoring(_currSession)
                                                                                    {
                                                                                        Code = _locKpiSubmitLine.Code,
                                                                                        KpiYear = _locKpiSubmitLine.KpiYear,
                                                                                        Name = _locKpiSubmitLine.Name,
                                                                                        ProportionKpi = _locKpiSubmitLine.Proportion,
                                                                                        TargetKpi = _locKpiSubmitLine.KpiTarget,
                                                                                        January = _locJanuary,
                                                                                        February = _locFebruary,
                                                                                        March = _locMarch,
                                                                                        April = _locApril,
                                                                                        May = _locMay,
                                                                                        June = _locJune,
                                                                                        July = _locJuly,
                                                                                        August = _locAugust,
                                                                                        September = _locSeptember,
                                                                                        October = _locOctober,
                                                                                        November = _locNovember,
                                                                                        December = _locDecember,
                                                                                        Ytd = _locytd,
                                                                                        Value = _locScoreGrading,
                                                                                        ScoreYtd = _locScoreYtd,
                                                                                        KpiYtdType = _locKpiSubmitLine.KpiYtdType,
                                                                                        Company = _locKpiSubmitLine.Company,
                                                                                        OrgDim1 = _locKpiSubmitLine.OrgDim1,
                                                                                        OrgDim2 = _locKpiSubmitLine.OrgDim2,
                                                                                        OrgDim3 = _locKpiSubmitLine.OrgDim3,
                                                                                        OrgDim4 = _locKpiSubmitLine.OrgDim4,
                                                                                        DefaultPosition = _locKpiSubmitLine.DefaultPosition,
                                                                                        Employee = _locKpiSubmitLine.Employee,
                                                                                    };
                                                                                    _saveKpiMonitoring.Save();
                                                                                    _saveKpiMonitoring.Session.CommitTransaction();

                                                                                    #endregion Add New Value in Field Kpi Monitoring

                                                                                    #region Save to KPI Final Score

                                                                                    XPCollection<KpiMonitoring> _locKpiMonitorings = new XPCollection<KpiMonitoring>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("Employee", _locKpiSubmitXPO.Name),
                                                                                           new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear)
                                                                                           ));
                                                                                    if (_locKpiMonitorings != null & _locKpiMonitorings.Count() > 0)
                                                                                    {
                                                                                        double score = 0;
                                                                                        double totalScore = 0;

                                                                                        foreach (KpiMonitoring _locKpiMonitoring2 in _locKpiMonitorings)
                                                                                        {
                                                                                            score = _locKpiMonitoring2.ScoreYtd;
                                                                                        }


                                                                                        totalScore = score + score;

                                                                                        KpiFinalScore _locKpiFinalScore = _currSession.FindObject<KpiFinalScore>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                                       new BinaryOperator("Employee", _locKpiSubmitXPO.Name)));
                                                                                        if (_locKpiFinalScore != null)
                                                                                        {
                                                                                            _locKpiFinalScore.TotalScore = totalScore;
                                                                                            _locKpiFinalScore.TotalFinalTarget = totalScore;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            KpiFinalScore _saveKpiFinalScore = new KpiFinalScore(_currSession)
                                                                                            {
                                                                                                Code = "FS-",
                                                                                                TotalScore = totalScore,
                                                                                                TotalFinalTarget = totalScore,
                                                                                                Employee = _locKpiSubmitLine.Employee,
                                                                                            };
                                                                                            _saveKpiFinalScore.Save();
                                                                                            _saveKpiFinalScore.Session.CommitTransaction();
                                                                                        }

                                                                                    }

                                                                                    #endregion Save to KPI Final Score
                                                                                }

                                                                                #endregion SEND DATA KE KPI Monitoring
                                                                            }
                                                                        }

                                                                        #endregion  Perhitungan berdasarkan KpiTypePeriod

                                                                        #region update status Kpi Submit Line

                                                                        if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Progress)
                                                                        {
                                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Approved;
                                                                            _locKpiSubmitLine.Save();
                                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                                        }

                                                                        #endregion update status Kpi Submit Line
                                                                    }
                                                                }

                                                                #region SEND DATA KE FINAL GRADING

                                                                Employee _employeeKpiSubmit = _locKpiSubmitXPO.Name;
                                                                Company _company = _locKpiSubmitXPO.Company;

                                                                KpiGrading _locKpiGrading = _currSession.FindObject<KpiGrading>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("Employee", _employeeKpiSubmit)
                                                                                                                                ));
                                                                if (_locKpiGrading != null)
                                                                {
                                                                    double TotNilaiAkhir = 0;
                                                                    string _locScoreGrading = null;
                                                                    KpiFinalScore _locKpiFinalScoreCheck = _currSession.FindObject<KpiFinalScore>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                      new BinaryOperator("JobPosition", _locKpiSubmitXPO.JobPosition),
                                                                                                                                      new BinaryOperator("Employee", _locKpiSubmitXPO.Name)
                                                                                                                                      ));
                                                                    if (_locKpiFinalScoreCheck != null)
                                                                    {
                                                                        TotNilaiAkhir = _locKpiFinalScoreCheck.TotalFinalTarget;
                                                                    }

                                                                    _locKpiGrading.TotalNilaiAkhir = TotNilaiAkhir;
                                                                    _locKpiGrading.Save();
                                                                    _locKpiGrading.Session.CommitTransaction();

                                                                    XPCollection<KpiFinalGradingSetting> _availableKpiFinalGradingSettings = new XPCollection<KpiFinalGradingSetting>(_currSession,
                                                                                                          new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Company", _locKpiGrading.Company),
                                                                                                          new BinaryOperator("Active", true)),
                                                                                                          new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                          );
                                                                    if (_availableKpiFinalGradingSettings != null && _availableKpiFinalGradingSettings.Count() > 0)
                                                                    {
                                                                        foreach (KpiFinalGradingSetting _availableKpiFinalGradingSetting in _availableKpiFinalGradingSettings)
                                                                        {
                                                                            if (_availableKpiFinalGradingSetting.Operation == ">")
                                                                            {
                                                                                if (_locKpiGrading.TotalNilaiAkhir > _availableKpiFinalGradingSetting.Value)
                                                                                {
                                                                                    _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                                                                                }
                                                                            }
                                                                            if (_availableKpiFinalGradingSetting.Operation == "<=")
                                                                            {
                                                                                if (_locKpiGrading.TotalNilaiAkhir <= _availableKpiFinalGradingSetting.Value)
                                                                                {
                                                                                    _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                                                                                }
                                                                            }
                                                                            if (_availableKpiFinalGradingSetting.Operation == "<")
                                                                            {
                                                                                if (_locKpiGrading.TotalNilaiAkhir < _availableKpiFinalGradingSetting.Value)
                                                                                {
                                                                                    _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                                                                                }
                                                                            }
                                                                            if (_availableKpiFinalGradingSetting.Operation == "=")
                                                                            {
                                                                                if (_locKpiGrading.TotalNilaiAkhir == _availableKpiFinalGradingSetting.Value)
                                                                                {
                                                                                    _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                                                                                }
                                                                            }
                                                                            if (_availableKpiFinalGradingSetting.Operation == ">=")
                                                                            {
                                                                                if (_locKpiGrading.TotalNilaiAkhir >= _availableKpiFinalGradingSetting.Value)
                                                                                {
                                                                                    _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                                                                                }
                                                                            }
                                                                        }
                                                                        _locKpiGrading.FinalResult = _locScoreGrading;
                                                                        _locScoreGrading = null;
                                                                        _locKpiGrading.Save();
                                                                        _locKpiGrading.Session.CommitTransaction();

                                                                        if(_locKpiGrading.FinalResult != null)
                                                                        {
                                                                            string _result = _locKpiGrading.FinalResult;
                                                                            KpiFinalGradingSetting _locKpiFinalPredicateCheck = _currSession.FindObject<KpiFinalGradingSetting>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                     new BinaryOperator("Result", _result)));
                                                                            if(_locKpiFinalPredicateCheck != null )
                                                                            {
                                                                                _locKpiGrading.FinalPredicate = _locKpiFinalPredicateCheck.Predicate;
                                                                                _locKpiGrading.Save();
                                                                                _locKpiGrading.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    string _locScoreGrading2 = null;
                                                                    string _locFinalPredicate2 = null;


                                                                    KpiFinalScore _locKpiFinalScoreCheck = _currSession.FindObject<KpiFinalScore>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                      new BinaryOperator("JobPosition", _locKpiSubmitXPO.JobPosition),
                                                                                                                                      new BinaryOperator("Employee", _locKpiSubmitXPO.Name)
                                                                                                                                      ));
                                                                    if (_locKpiFinalScoreCheck != null)
                                                                    {

                                                                        XPCollection<KpiFinalGradingSetting> _availableKpiFinalGradingSetting2s = new XPCollection<KpiFinalGradingSetting>(_currSession,
                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("Company", _locKpiSubmitXPO.Company),
                                                                                                         new BinaryOperator("Active", true)),
                                                                                                         new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                         );
                                                                        if (_availableKpiFinalGradingSetting2s != null && _availableKpiFinalGradingSetting2s.Count() > 0)
                                                                        {
                                                                            foreach (KpiFinalGradingSetting _availableKpiFinalGradingSetting2 in _availableKpiFinalGradingSetting2s)
                                                                            {
                                                                                if (_availableKpiFinalGradingSetting2.Operation == ">")
                                                                                {
                                                                                    if (_locKpiFinalScoreCheck.TotalFinalTarget > _availableKpiFinalGradingSetting2.Value)
                                                                                    {
                                                                                        _locScoreGrading2 = _availableKpiFinalGradingSetting2.Result;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiFinalGradingSetting2.Operation == "<=")
                                                                                {
                                                                                    if (_locKpiFinalScoreCheck.TotalFinalTarget <= _availableKpiFinalGradingSetting2.Value)
                                                                                    {
                                                                                        _locScoreGrading2 = _availableKpiFinalGradingSetting2.Result;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiFinalGradingSetting2.Operation == "<")
                                                                                {
                                                                                    if (_locKpiFinalScoreCheck.TotalFinalTarget < _availableKpiFinalGradingSetting2.Value)
                                                                                    {
                                                                                        _locScoreGrading2 = _availableKpiFinalGradingSetting2.Result;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiFinalGradingSetting2.Operation == "=")
                                                                                {
                                                                                    if (_locKpiFinalScoreCheck.TotalFinalTarget == _availableKpiFinalGradingSetting2.Value)
                                                                                    {
                                                                                        _locScoreGrading2 = _availableKpiFinalGradingSetting2.Result;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiFinalGradingSetting2.Operation == ">=")
                                                                                {
                                                                                    if (_locKpiFinalScoreCheck.TotalFinalTarget >= _availableKpiFinalGradingSetting2.Value)
                                                                                    {
                                                                                        _locScoreGrading2 = _availableKpiFinalGradingSetting2.Result;
                                                                                    }
                                                                                }
                                                                            }

                                                                            if (_locScoreGrading2 != null)
                                                                            {
                                                                                string _result = _locScoreGrading2;
                                                                                KpiFinalGradingSetting _locKpiFinalPredicateCheck2 = _currSession.FindObject<KpiFinalGradingSetting>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                     new BinaryOperator("Result", _result)));
                                                                                if (_locKpiFinalPredicateCheck2 != null)
                                                                                {
                                                                                    _locFinalPredicate2 = _locKpiFinalPredicateCheck2.Predicate;
                                                                                }
                                                                            }
                                                                        }

                                                                        KpiGrading _saveKpiGrading = new KpiGrading(_currSession)
                                                                        {
                                                                            Code = "FG-",
                                                                            TotalNilaiAkhir = _locKpiFinalScoreCheck.TotalFinalTarget,
                                                                            FinalResult = _locScoreGrading2,
                                                                            FinalPredicate = _locFinalPredicate2,
                                                                            Employee = _locKpiSubmitXPO.Name,
                                                                            Company = _locKpiSubmitXPO.Company,
                                                                            OrgDim1 = _locKpiSubmitXPO.OrgDim1,
                                                                            OrgDim2 = _locKpiSubmitXPO.OrgDim2,
                                                                            OrgDim3 = _locKpiSubmitXPO.OrgDim3,
                                                                            OrgDim4 = _locKpiSubmitXPO.OrgDim4,
                                                                            JobPosition = _locKpiSubmitXPO.JobPosition,
                                                                        };
                                                                        _locScoreGrading2 = null;
                                                                        _saveKpiGrading.Save();
                                                                        _saveKpiGrading.Session.CommitTransaction();
                                                                    }

                                                                   


                                                                }

                                                                #endregion SEND DATA KE FINAL GRADING

                                                                _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                                                _locKpiSubmitXPO.Save();
                                                                _locKpiSubmitXPO.Session.CommitTransaction();




                                                                //Message
                                                                SuccsessMessageShow("Kpi Submit has successfully updates to Approved");
                                                            }
                                                            else
                                                            {
                                                                //Message error
                                                                ErrorMessageShow("Data Kpi Submit Not Available");
                                                            }

                                                        }
                                                        else
                                                        {
                                                            //Message Error
                                                            ErrorMessageShow("Data Kpi Submit Order Not Available");
                                                        }

                                                    }


                                                    #endregion Save To Kpi Monitoring

                                                    Employee _locEmployeeMail = _currSession.FindObject<Employee>
                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Code", _locCodeEmployee),
                                                                             new BinaryOperator("Active", true)
                                                                             ));
                                                    if (_locEmployeeMail != null)
                                                    {
                                                        #region Email
                                                        ApplicationMailSetting _locApplicationMailSetting = _globFunc.GetApplicationMailSetup(_currSession, _locCompany);
                                                        if (_locApplicationMailSetting != null)
                                                        {
                                                            _locSubjectMail = "Persetujuan Appprove Kpi";
                                                            _locMailTo = _locEmployeeMail.Email;
                                                            _locBody = _globFunc.BackgroundBodyMailKpiApprove();
                                                            _globFunc.SetAndSendMail(_locApplicationMailSetting.SmtpHost, _locApplicationMailSetting.SmtpPort, _locApplicationMailSetting.MailFrom, _locApplicationMailSetting.MailFromPassword, _locMailTo, _locSubjectMail, _locBody);
                                                        }
                                                        #endregion Email
                                                    }

                                                }
                                                else
                                                {
                                                    KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        LockApproval = _locLockApproval,
                                                        KpiSubmit = _locKpiSubmitXPO,
                                                        ApprovedBy = _locEmployee,
                                                    };
                                                    _saveDataKSA.Save();
                                                    _saveDataKSA.Session.CommitTransaction();

                                                }

                                                #endregion SaveKpiSubmitApproval

                                                _locKpiSubmitXPO.ActivationPosting = _locActivationPosting;
                                                _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                                _locKpiSubmitXPO.StatusDate = now;
                                                _locKpiSubmitXPO.Save();
                                                _locKpiSubmitXPO.Session.CommitTransaction();


                                                if (_locAppApprovalSetting.Posting == true)
                                                {
                                                    if (_locKpiSubmitXPO.ActivationPosting == true && _locKpiSubmitXPO.KpiSubmitStatus == Status.Approved)
                                                    {
                                                        //SetPostingProcessPO(_currentSession, _locPurchaseOrderXPO);
                                                    }
                                                }

                                                DeleteKpiSubmitTaskBase(_currentSession, _locKpiSubmitXPO);
                                                SuccsessMessageShow("Your KPI has successfully Approve");
                                            }
                                        }
                                        #endregion Approval Level 1

                                        #region Approval Level 2
                                        if (_locAppApprovalSetting.ApprovalLevel == ApprovalLevel.Level2)
                                        {
                                            bool _locActivationPosting = false;
                                            bool _locLockApproval = false;

                                            KpiSubmitApproval _locKpiSubmitApprovalXPO = _currentSession.FindObject<KpiSubmitApproval>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locKpiSubmitApprovalXPO == null)
                                            {
                                                if (_locKpiSubmitXPO.ActivationPosting == false)
                                                {
                                                    if (_locAppApprovalSetting.Lock == true)
                                                    {
                                                        _locActivationPosting = true;
                                                        _locLockApproval = true;
                                                    }
                                                }
                                                else
                                                {
                                                    _locActivationPosting = _locKpiSubmitXPO.ActivationPosting;
                                                }

                                                SetKpiSubmitTaskBaseByApprovalInformationLevelForKpiSubmit(_currentSession, _locKpiSubmitXPO, ObjectList.KpiSubmit, ApprovalLevel.Level3);
                                                SetActivationPostingKpiSubmitLine(_currentSession, _locKpiSubmitXPO, _locActivationPosting);


                                                #region SaveKpiSubmitApproval

                                                if (_locAppApprovalSetting.EndApproval == true)
                                                {
                                                    KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        LockApproval = _locLockApproval,
                                                        EndApproval = true,
                                                        KpiSubmit = _locKpiSubmitXPO,
                                                        ApprovedBy = _locEmployee,
                                                    };
                                                    _saveDataKSA.Save();
                                                    _saveDataKSA.Session.CommitTransaction();

                                                    #region Save to Kpi Monitoring

                                                    KpiSubmit _locKpiSubmitOS = _locKpiSubmitXPO;

                                                    Session _currSession = null;
                                                    double _locJanuary = 0;
                                                    double _locFebruary = 0;
                                                    double _locMarch = 0;
                                                    double _locApril = 0;
                                                    double _locMay = 0;
                                                    double _locJune = 0;
                                                    double _locJuly = 0;
                                                    double _locAugust = 0;
                                                    double _locSeptember = 0;
                                                    double _locOctober = 0;
                                                    double _locNovember = 0;
                                                    double _locDecember = 0;
                                                    double _locytd = 0;
                                                    double _locScoreYtd = 0;


                                                    if (_locKpiSubmitOS != null)
                                                    {
                                                        if (_locKpiSubmitOS.Session != null)
                                                        {
                                                            _currSession = _locKpiSubmitOS.Session;
                                                        }

                                                        if (_locKpiSubmitOS.Code != null && _currSession != null)
                                                        {
                                                            _currObjectId = _locKpiSubmitOS.Code;
                                                            KpiSubmit _locKpiSubmitXPO2 = _currSession.FindObject<KpiSubmit>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Code", _currObjectId),
                                                                                                new BinaryOperator("KpiSubmitStatus", Status.Progress)
                                                                                                ));
                                                            if (_locKpiSubmitXPO2 != null)
                                                            {
                                                                XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("KpiSubmit", _locKpiSubmitXPO2),
                                                                                                new BinaryOperator("KpiSubmitLineStatus", Status.Progress)
                                                                                                ));
                                                                if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                                                                {
                                                                    foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                                                                    {
                                                                        #region update status Kpi Submit Line

                                                                        if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Progress)
                                                                        {
                                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Approved;
                                                                            _locKpiSubmitLine.Save();
                                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                                        }

                                                                        #endregion update status Kpi Submit Line

                                                                        #region Perhitungan berdasarkan KpiTypePeriod
                                                                        XPCollection<KpiSubmitLine> _locKpiSubmitLines2 = new XPCollection<KpiSubmitLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Name", _locKpiSubmitLine.Name),
                                                                                            new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear)
                                                                                            ));
                                                                        if (_locKpiSubmitLines2 != null && _locKpiSubmitLines2.Count() > 0)
                                                                        {
                                                                            foreach (KpiSubmitLine _locKpiSubmitLine2 in _locKpiSubmitLines2)
                                                                            {

                                                                                #region Loc Pencapaian Per Bulan

                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.January)
                                                                                {
                                                                                    _locJanuary = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.February)
                                                                                {
                                                                                    _locFebruary = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.March)
                                                                                {
                                                                                    _locMarch = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.April)
                                                                                {
                                                                                    _locApril = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.May)
                                                                                {
                                                                                    _locMay = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.June)
                                                                                {
                                                                                    _locJune = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.July)
                                                                                {
                                                                                    _locJuly = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.August)
                                                                                {
                                                                                    _locAugust = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.September)
                                                                                {
                                                                                    _locSeptember = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.October)
                                                                                {
                                                                                    _locOctober = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.November)
                                                                                {
                                                                                    _locNovember = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.December)
                                                                                {
                                                                                    _locDecember = _locKpiSubmitLine2.Pencapaian;
                                                                                }

                                                                                #endregion Pencapaian Per Bulan

                                                                                #region Ytd Kpi Monitoring Type Sum All
                                                                                if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.SumAll)
                                                                                {
                                                                                    _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember);
                                                                                }
                                                                                #endregion Ytd Kpi Monitoring Type Sum All

                                                                                KpiMonitoring _locKpiMonitoring = _currSession.FindObject<KpiMonitoring>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                                        new BinaryOperator("Name", _locKpiSubmitLine2.Name),
                                                                                                                                                        new BinaryOperator("KpiYear", _locKpiSubmitLine2.KpiYear),
                                                                                                                                                        new BinaryOperator("DefaultPosition", _locKpiSubmitLine2.DefaultPosition),
                                                                                                                                                        new BinaryOperator("Employee", _locKpiSubmitLine2.Employee)
                                                                                                                                                        ));
                                                                                if (_locKpiMonitoring != null)
                                                                                {
                                                                                    #region Ytd Kpi Monitoring Type Sum All
                                                                                    if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.SumAll)
                                                                                    {
                                                                                        _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember);
                                                                                    }
                                                                                    #endregion Ytd Kpi Monitoring Type Sum All


                                                                                    #region Ytd Kpi Monitoring Type Average
                                                                                    if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.Average)
                                                                                    {
                                                                                        int a = 0;
                                                                                        double b = 0;
                                                                                        double average = 0;

                                                                                        if (_locJanuary > 0) { a = a + 1; b = b + _locJanuary; }
                                                                                        if (_locFebruary > 0) { a = a + 1; b = b + _locFebruary; }
                                                                                        if (_locMarch > 0) { a = a + 1; b = b + _locMarch; }
                                                                                        if (_locApril > 0) { a = a + 1; b = b + _locApril; }
                                                                                        if (_locMay > 0) { a = a + 1; b = b + _locMay; }
                                                                                        if (_locJune > 0) { a = a + 1; b = b + _locJune; }
                                                                                        if (_locJuly > 0) { a = a + 1; b = b + _locJuly; }
                                                                                        if (_locAugust > 0) { a = a + 1; b = b + _locAugust; }
                                                                                        if (_locSeptember > 0) { a = a + 1; b = b + _locSeptember; }
                                                                                        if (_locOctober > 0) { a = a + 1; b = b + _locOctober; }
                                                                                        if (_locNovember > 0) { a = a + 1; b = b + _locNovember; }
                                                                                        if (_locDecember > 0) { a = a + 1; b = b + _locDecember; }

                                                                                        average = b / a;
                                                                                        _locytd = average;
                                                                                        a = 0;
                                                                                        b = 0;
                                                                                    }
                                                                                    #endregion Ytd Kpi Monitoring Type Average

                                                                                    #region Value Kpi Monitoring

                                                                                    double _locScoreGrading = 0;

                                                                                    XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                                                                   new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                                   new BinaryOperator("Active", true)),
                                                                                                                                   new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                                   );
                                                                                    if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                                    {

                                                                                        foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                                        {

                                                                                            if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                            {
                                                                                                if (_locytd > _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                            {
                                                                                                if (_locytd <= _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                            {
                                                                                                if (_locytd < _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                            {
                                                                                                if (_locytd == _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    #endregion Value Kpi Monitoring

                                                                                    #region Score Kpi Monitoring

                                                                                    _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine2.KpiTarget / 100);

                                                                                    #endregion Score Kpi Monitoring

                                                                                    #region Update Field

                                                                                    _locKpiMonitoring.January = _locJanuary;
                                                                                    _locKpiMonitoring.February = _locFebruary;
                                                                                    _locKpiMonitoring.March = _locMarch;
                                                                                    _locKpiMonitoring.April = _locApril;
                                                                                    _locKpiMonitoring.May = _locMay;
                                                                                    _locKpiMonitoring.June = _locJune;
                                                                                    _locKpiMonitoring.July = _locJuly;
                                                                                    _locKpiMonitoring.August = _locAugust;
                                                                                    _locKpiMonitoring.September = _locSeptember;
                                                                                    _locKpiMonitoring.October = _locOctober;
                                                                                    _locKpiMonitoring.November = _locNovember;
                                                                                    _locKpiMonitoring.December = _locDecember;
                                                                                    _locKpiMonitoring.Ytd = _locytd;
                                                                                    _locKpiMonitoring.Value = _locScoreGrading;
                                                                                    _locKpiMonitoring.ScoreYtd = _locScoreYtd;
                                                                                    _locKpiMonitoring.Save();
                                                                                    _locKpiMonitoring.Session.CommitTransaction();

                                                                                    #endregion Update Field
                                                                                }
                                                                                else
                                                                                {
                                                                                    #region Ytd from New Data
                                                                                    _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember) / 1;

                                                                                    #endregion Ytd from New Data

                                                                                    #region Value Kpi Monitoring Awal Penginputan Data

                                                                                    double _locScoreGrading = 0;

                                                                                    XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                                                                   new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                                   new BinaryOperator("Active", true)),
                                                                                                                                   new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                                   );
                                                                                    if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                                    {

                                                                                        foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                                        {

                                                                                            if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian > _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian <= _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian < _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian == _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    #endregion Value Kpi Monitoring Awal Penginputan Data

                                                                                    #region Score Kpi Monitoring Awal Penginputan Data

                                                                                    _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine.KpiTarget / 100);

                                                                                    #endregion Score Kpi Monitoring Awal Penginputan Data

                                                                                    #region Add New Value in Field Kpi Monitoring

                                                                                    KpiMonitoring _saveKpiMonitoring = new KpiMonitoring(_currSession)
                                                                                    {
                                                                                        Code = _locKpiSubmitLine.Code,
                                                                                        KpiYear = _locKpiSubmitLine.KpiYear,
                                                                                        Name = _locKpiSubmitLine.Name,
                                                                                        ProportionKpi = _locKpiSubmitLine.Proportion,
                                                                                        TargetKpi = _locKpiSubmitLine.KpiTarget,
                                                                                        January = _locJanuary,
                                                                                        February = _locFebruary,
                                                                                        March = _locMarch,
                                                                                        April = _locApril,
                                                                                        May = _locMay,
                                                                                        June = _locJune,
                                                                                        July = _locJuly,
                                                                                        August = _locAugust,
                                                                                        September = _locSeptember,
                                                                                        October = _locOctober,
                                                                                        November = _locNovember,
                                                                                        December = _locDecember,
                                                                                        Ytd = _locytd,
                                                                                        Value = _locScoreGrading,
                                                                                        ScoreYtd = _locScoreYtd,
                                                                                        KpiYtdType = _locKpiSubmitLine.KpiYtdType,
                                                                                        Company = _locKpiSubmitLine.Company,
                                                                                        OrgDim1 = _locKpiSubmitLine.OrgDim1,
                                                                                        OrgDim2 = _locKpiSubmitLine.OrgDim2,
                                                                                        OrgDim3 = _locKpiSubmitLine.OrgDim3,
                                                                                        OrgDim4 = _locKpiSubmitLine.OrgDim4,
                                                                                        DefaultPosition = _locKpiSubmitLine.DefaultPosition,
                                                                                        Employee = _locKpiSubmitLine.Employee,
                                                                                    };
                                                                                    _saveKpiMonitoring.Save();
                                                                                    _saveKpiMonitoring.Session.CommitTransaction();

                                                                                    #endregion Add New Value in Field Kpi Monitoring
                                                                                }
                                                                            }


                                                                        }

                                                                        #endregion  Perhitungan berdasarkan KpiTypePeriod
                                                                    }
                                                                }
                                                                _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                                                _locKpiSubmitXPO.Save();
                                                                _locKpiSubmitXPO.Session.CommitTransaction();


                                                                //Message
                                                                SuccsessMessageShow("Kpi Submit has successfully updates to Approved");
                                                            }
                                                            else
                                                            {
                                                                //Message error
                                                                ErrorMessageShow("Data Kpi Submit Not Available");
                                                            }

                                                        }
                                                        else
                                                        {
                                                            //Message Error
                                                            ErrorMessageShow("Data Kpi Submit Order Not Available");
                                                        }

                                                    }


                                                    #endregion Save To Kpi Monitoring

                                                }
                                                else
                                                {
                                                    KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        LockApproval = _locLockApproval,
                                                        KpiSubmit = _locKpiSubmitXPO,
                                                        ApprovedBy = _locEmployee,
                                                    };
                                                    _saveDataKSA.Save();
                                                    _saveDataKSA.Session.CommitTransaction();

                                                    SetKpiSubmitApproval(_currentSession, _locKpiSubmitXPO, ApprovalLevel.Level1);

                                                }

                                                #endregion SaveKpiSubmitApproval

                                                _locKpiSubmitXPO.ActivationPosting = _locActivationPosting;
                                                _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                                _locKpiSubmitXPO.StatusDate = now;
                                                _locKpiSubmitXPO.Save();
                                                _locKpiSubmitXPO.Session.CommitTransaction();

                                                if (_locAppApprovalSetting.Posting == true)
                                                {
                                                    if (_locKpiSubmitXPO.ActivationPosting == true && _locKpiSubmitXPO.KpiSubmitStatus == Status.Approved)
                                                    {
                                                        //SetPostingProcessPO(_currentSession, _locPurchaseOrderXPO);
                                                    }
                                                }

                                                DeleteKpiSubmitTaskBase(_currentSession, _locKpiSubmitXPO);
                                                SuccsessMessageShow("Purchase Order has successfully Approve");
                                            }
                                        }
                                        #endregion Approval Level 2

                                        #region Approval Level 3
                                        if (_locAppApprovalSetting.ApprovalLevel == ApprovalLevel.Level3)
                                        {
                                            bool _locActivationPosting = false;
                                            bool _locLockApproval = false;

                                            KpiSubmitApproval _locKpiSubmitApprovalXPO = _currentSession.FindObject<KpiSubmitApproval>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locKpiSubmitApprovalXPO == null)
                                            {
                                                if (_locKpiSubmitXPO.ActivationPosting == false)
                                                {
                                                    if (_locAppApprovalSetting.Lock == true)
                                                    {
                                                        _locActivationPosting = true;
                                                        _locLockApproval = true;
                                                    }
                                                }
                                                else
                                                {
                                                    _locActivationPosting = _locKpiSubmitXPO.ActivationPosting;
                                                }

                                                SetActivationPostingKpiSubmitLine(_currentSession, _locKpiSubmitXPO, _locActivationPosting);



                                                #region SaveKpiSubmitApproval

                                                if (_locAppApprovalSetting.EndApproval == true)
                                                {
                                                    KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        LockApproval = _locLockApproval,
                                                        EndApproval = true,
                                                        KpiSubmit = _locKpiSubmitXPO,
                                                        ApprovedBy = _locEmployee,
                                                    };
                                                    _saveDataKSA.Save();
                                                    _saveDataKSA.Session.CommitTransaction();

                                                    #region Save to Kpi Monitoring

                                                    KpiSubmit _locKpiSubmitOS = _locKpiSubmitXPO;

                                                    Session _currSession = null;
                                                    double _locJanuary = 0;
                                                    double _locFebruary = 0;
                                                    double _locMarch = 0;
                                                    double _locApril = 0;
                                                    double _locMay = 0;
                                                    double _locJune = 0;
                                                    double _locJuly = 0;
                                                    double _locAugust = 0;
                                                    double _locSeptember = 0;
                                                    double _locOctober = 0;
                                                    double _locNovember = 0;
                                                    double _locDecember = 0;
                                                    double _locytd = 0;
                                                    double _locScoreYtd = 0;


                                                    if (_locKpiSubmitOS != null)
                                                    {
                                                        if (_locKpiSubmitOS.Session != null)
                                                        {
                                                            _currSession = _locKpiSubmitOS.Session;
                                                        }

                                                        if (_locKpiSubmitOS.Code != null && _currSession != null)
                                                        {
                                                            _currObjectId = _locKpiSubmitOS.Code;
                                                            KpiSubmit _locKpiSubmitXPO2 = _currSession.FindObject<KpiSubmit>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Code", _currObjectId),
                                                                                                new BinaryOperator("KpiSubmitStatus", Status.Progress)
                                                                                                ));
                                                            if (_locKpiSubmitXPO2 != null)
                                                            {
                                                                XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("KpiSubmit", _locKpiSubmitXPO2),
                                                                                                new BinaryOperator("KpiSubmitLineStatus", Status.Progress)
                                                                                                ));
                                                                if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                                                                {
                                                                    foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                                                                    {
                                                                        #region update status Kpi Submit Line

                                                                        if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Progress)
                                                                        {
                                                                            _locKpiSubmitLine.KpiSubmitLineStatus = Status.Approved;
                                                                            _locKpiSubmitLine.Save();
                                                                            _locKpiSubmitLine.Session.CommitTransaction();
                                                                        }

                                                                        #endregion update status Kpi Submit Line

                                                                        #region Perhitungan berdasarkan KpiTypePeriod
                                                                        XPCollection<KpiSubmitLine> _locKpiSubmitLines2 = new XPCollection<KpiSubmitLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Name", _locKpiSubmitLine.Name),
                                                                                            new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear)
                                                                                            ));
                                                                        if (_locKpiSubmitLines2 != null && _locKpiSubmitLines2.Count() > 0)
                                                                        {
                                                                            foreach (KpiSubmitLine _locKpiSubmitLine2 in _locKpiSubmitLines2)
                                                                            {

                                                                                #region Loc Pencapaian Per Bulan

                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.January)
                                                                                {
                                                                                    _locJanuary = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.February)
                                                                                {
                                                                                    _locFebruary = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.March)
                                                                                {
                                                                                    _locMarch = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.April)
                                                                                {
                                                                                    _locApril = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.May)
                                                                                {
                                                                                    _locMay = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.June)
                                                                                {
                                                                                    _locJune = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.July)
                                                                                {
                                                                                    _locJuly = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.August)
                                                                                {
                                                                                    _locAugust = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.September)
                                                                                {
                                                                                    _locSeptember = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.October)
                                                                                {
                                                                                    _locOctober = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.November)
                                                                                {
                                                                                    _locNovember = _locKpiSubmitLine2.Pencapaian;
                                                                                }
                                                                                if (_locKpiSubmitLine2.MonthKpi == Month.December)
                                                                                {
                                                                                    _locDecember = _locKpiSubmitLine2.Pencapaian;
                                                                                }

                                                                                #endregion Pencapaian Per Bulan

                                                                                #region Ytd Kpi Monitoring Type Sum All
                                                                                if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.SumAll)
                                                                                {
                                                                                    _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember);
                                                                                }
                                                                                #endregion Ytd Kpi Monitoring Type Sum All

                                                                                KpiMonitoring _locKpiMonitoring = _currSession.FindObject<KpiMonitoring>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                                        new BinaryOperator("Name", _locKpiSubmitLine2.Name),
                                                                                                                                                        new BinaryOperator("KpiYear", _locKpiSubmitLine2.KpiYear),
                                                                                                                                                        new BinaryOperator("DefaultPosition", _locKpiSubmitLine2.DefaultPosition),
                                                                                                                                                        new BinaryOperator("Employee", _locKpiSubmitLine2.Employee)
                                                                                                                                                        ));
                                                                                if (_locKpiMonitoring != null)
                                                                                {
                                                                                    #region Ytd Kpi Monitoring Type Sum All
                                                                                    if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.SumAll)
                                                                                    {
                                                                                        _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember);
                                                                                    }
                                                                                    #endregion Ytd Kpi Monitoring Type Sum All


                                                                                    #region Ytd Kpi Monitoring Type Average
                                                                                    if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.Average)
                                                                                    {
                                                                                        int a = 0;
                                                                                        double b = 0;
                                                                                        double average = 0;

                                                                                        if (_locJanuary > 0) { a = a + 1; b = b + _locJanuary; }
                                                                                        if (_locFebruary > 0) { a = a + 1; b = b + _locFebruary; }
                                                                                        if (_locMarch > 0) { a = a + 1; b = b + _locMarch; }
                                                                                        if (_locApril > 0) { a = a + 1; b = b + _locApril; }
                                                                                        if (_locMay > 0) { a = a + 1; b = b + _locMay; }
                                                                                        if (_locJune > 0) { a = a + 1; b = b + _locJune; }
                                                                                        if (_locJuly > 0) { a = a + 1; b = b + _locJuly; }
                                                                                        if (_locAugust > 0) { a = a + 1; b = b + _locAugust; }
                                                                                        if (_locSeptember > 0) { a = a + 1; b = b + _locSeptember; }
                                                                                        if (_locOctober > 0) { a = a + 1; b = b + _locOctober; }
                                                                                        if (_locNovember > 0) { a = a + 1; b = b + _locNovember; }
                                                                                        if (_locDecember > 0) { a = a + 1; b = b + _locDecember; }

                                                                                        average = b / a;
                                                                                        _locytd = average;
                                                                                        a = 0;
                                                                                        b = 0;
                                                                                    }
                                                                                    #endregion Ytd Kpi Monitoring Type Average

                                                                                    #region Value Kpi Monitoring

                                                                                    double _locScoreGrading = 0;

                                                                                    XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                                                                   new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                                   new BinaryOperator("Active", true)),
                                                                                                                                   new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                                   );
                                                                                    if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                                    {

                                                                                        foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                                        {

                                                                                            if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                            {
                                                                                                if (_locytd > _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                            {
                                                                                                if (_locytd <= _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                            {
                                                                                                if (_locytd < _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                            {
                                                                                                if (_locytd == _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    #endregion Value Kpi Monitoring

                                                                                    #region Score Kpi Monitoring

                                                                                    _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine2.KpiTarget / 100);

                                                                                    #endregion Score Kpi Monitoring

                                                                                    #region Update Field

                                                                                    _locKpiMonitoring.January = _locJanuary;
                                                                                    _locKpiMonitoring.February = _locFebruary;
                                                                                    _locKpiMonitoring.March = _locMarch;
                                                                                    _locKpiMonitoring.April = _locApril;
                                                                                    _locKpiMonitoring.May = _locMay;
                                                                                    _locKpiMonitoring.June = _locJune;
                                                                                    _locKpiMonitoring.July = _locJuly;
                                                                                    _locKpiMonitoring.August = _locAugust;
                                                                                    _locKpiMonitoring.September = _locSeptember;
                                                                                    _locKpiMonitoring.October = _locOctober;
                                                                                    _locKpiMonitoring.November = _locNovember;
                                                                                    _locKpiMonitoring.December = _locDecember;
                                                                                    _locKpiMonitoring.Ytd = _locytd;
                                                                                    _locKpiMonitoring.Value = _locScoreGrading;
                                                                                    _locKpiMonitoring.ScoreYtd = _locScoreYtd;
                                                                                    _locKpiMonitoring.Save();
                                                                                    _locKpiMonitoring.Session.CommitTransaction();

                                                                                    #endregion Update Field
                                                                                }
                                                                                else
                                                                                {
                                                                                    #region Ytd from New Data
                                                                                    _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember) / 1;

                                                                                    #endregion Ytd from New Data

                                                                                    #region Value Kpi Monitoring Awal Penginputan Data

                                                                                    double _locScoreGrading = 0;

                                                                                    XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                                                                   new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                                   new BinaryOperator("Active", true)),
                                                                                                                                   new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                                   );
                                                                                    if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                                    {

                                                                                        foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                                        {

                                                                                            if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian > _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian <= _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian < _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                            if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                            {
                                                                                                if (_locKpiSubmitLine2.Pencapaian == _availableKpiRoleGrading.ValueGrading)
                                                                                                {
                                                                                                    _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    #endregion Value Kpi Monitoring Awal Penginputan Data

                                                                                    #region Score Kpi Monitoring Awal Penginputan Data

                                                                                    _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine.KpiTarget / 100);

                                                                                    #endregion Score Kpi Monitoring Awal Penginputan Data

                                                                                    #region Add New Value in Field Kpi Monitoring

                                                                                    KpiMonitoring _saveKpiMonitoring = new KpiMonitoring(_currSession)
                                                                                    {
                                                                                        Code = _locKpiSubmitLine.Code,
                                                                                        KpiYear = _locKpiSubmitLine.KpiYear,
                                                                                        Name = _locKpiSubmitLine.Name,
                                                                                        ProportionKpi = _locKpiSubmitLine.Proportion,
                                                                                        TargetKpi = _locKpiSubmitLine.KpiTarget,
                                                                                        January = _locJanuary,
                                                                                        February = _locFebruary,
                                                                                        March = _locMarch,
                                                                                        April = _locApril,
                                                                                        May = _locMay,
                                                                                        June = _locJune,
                                                                                        July = _locJuly,
                                                                                        August = _locAugust,
                                                                                        September = _locSeptember,
                                                                                        October = _locOctober,
                                                                                        November = _locNovember,
                                                                                        December = _locDecember,
                                                                                        Ytd = _locytd,
                                                                                        Value = _locScoreGrading,
                                                                                        ScoreYtd = _locScoreYtd,
                                                                                        KpiYtdType = _locKpiSubmitLine.KpiYtdType,
                                                                                        Company = _locKpiSubmitLine.Company,
                                                                                        OrgDim1 = _locKpiSubmitLine.OrgDim1,
                                                                                        OrgDim2 = _locKpiSubmitLine.OrgDim2,
                                                                                        OrgDim3 = _locKpiSubmitLine.OrgDim3,
                                                                                        OrgDim4 = _locKpiSubmitLine.OrgDim4,
                                                                                        DefaultPosition = _locKpiSubmitLine.DefaultPosition,
                                                                                        Employee = _locKpiSubmitLine.Employee,
                                                                                    };
                                                                                    _saveKpiMonitoring.Save();
                                                                                    _saveKpiMonitoring.Session.CommitTransaction();

                                                                                    #endregion Add New Value in Field Kpi Monitoring
                                                                                }
                                                                            }


                                                                        }

                                                                        #endregion  Perhitungan berdasarkan KpiTypePeriod
                                                                    }
                                                                }
                                                                _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                                                _locKpiSubmitXPO.Save();
                                                                _locKpiSubmitXPO.Session.CommitTransaction();


                                                                //Message
                                                                SuccsessMessageShow("Kpi Submit has successfully updates to Approved");
                                                            }
                                                            else
                                                            {
                                                                //Message error
                                                                ErrorMessageShow("Data Kpi Submit Not Available");
                                                            }

                                                        }
                                                        else
                                                        {
                                                            //Message Error
                                                            ErrorMessageShow("Data Kpi Submit Order Not Available");
                                                        }

                                                    }


                                                    #endregion Save To Kpi Monitoring

                                                }
                                                else
                                                {
                                                    KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        LockApproval = _locLockApproval,
                                                        KpiSubmit = _locKpiSubmitXPO,
                                                        ApprovedBy = _locEmployee,
                                                    };
                                                    _saveDataKSA.Save();
                                                    _saveDataKSA.Session.CommitTransaction();

                                                    SetKpiSubmitApproval(_currentSession, _locKpiSubmitXPO, ApprovalLevel.Level2);
                                                    SetKpiSubmitApproval(_currentSession, _locKpiSubmitXPO, ApprovalLevel.Level1);


                                                }

                                                #endregion SaveKpiSubmitApproval

                                                _locKpiSubmitXPO.ActivationPosting = _locActivationPosting;
                                                _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                                _locKpiSubmitXPO.StatusDate = now;
                                                _locKpiSubmitXPO.Save();
                                                _locKpiSubmitXPO.Session.CommitTransaction();


                                                if (_locAppApprovalSetting.Posting == true)
                                                {
                                                    if (_locKpiSubmitXPO.ActivationPosting == true && _locKpiSubmitXPO.KpiSubmitStatus == Status.Approved)
                                                    {
                                                        //SetPostingProcessPO(_currentSession, _locPurchaseOrderXPO);
                                                    }
                                                }

                                                DeleteKpiSubmitTaskBase(_currentSession, _locKpiSubmitXPO);
                                                SuccsessMessageShow("Purchase Order has successfully Approve");
                                            }
                                        }
                                        #endregion Approval Level 3
                                    }
                                }
                            }
                        }
                        else
                        {
                            ErrorMessageShow("KPI Submit Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #endregion Kpi Approval

        #region Cancel Kpi
        private void CancelKpiAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Employee _locEmployeeLeader = null;
                Month _locMonthKpi = 0;
                KpiYear _locYearKpi = null;



                #region Cancel KPI
                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        KpiSubmit _locKpiSubmitOS = (KpiSubmit)_objectSpace.GetObject(obj);

                        if (_locKpiSubmitOS != null)
                        {
                            if (_locKpiSubmitOS.Session != null)
                            {
                                _currSession = _locKpiSubmitOS.Session;
                            }

                            if (_locKpiSubmitOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locKpiSubmitOS.Code;
                               

                                KpiSubmit _locKpiSubmitXPO = _currSession.FindObject<KpiSubmit>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Approved)
                                                                    )));
                                if (_locKpiSubmitXPO != null)
                                {
                                    _locEmployeeLeader = _locKpiSubmitXPO.Leader;
                                    _locMonthKpi = _locKpiSubmitXPO.KpiSubmitMonth;
                                    _locYearKpi = _locKpiSubmitXPO.KpiSubmitYear;

                                    XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("KpiSubmitLineStatus", Status.Approved)
                                                                    )));
                                    if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                                    {

                                        foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                                        {
                                            if(_locKpiSubmitLine.KpiGroupType == KpiGroupType.Cascading) 
                                            {
                                                #region update cancel di cascading diri sendiri 
                                                XPCollection<KpiSubmitLineCascading> _locKpiSubmitLineCascadings = new XPCollection<KpiSubmitLineCascading>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                   new BinaryOperator("KpiSubmitLine", _locKpiSubmitLine),
                                                                   new GroupOperator(GroupOperatorType.Or,
                                                                   new BinaryOperator("Status", Status.Approved)
                                                                   )));
                                                if(_locKpiSubmitLineCascadings != null && _locKpiSubmitLineCascadings.Count() > 0)
                                                {
                                                    foreach(KpiSubmitLineCascading _locKpiSubmitLineCascading in _locKpiSubmitLineCascadings)
                                                    {
                                                        if(_locKpiSubmitLineCascading.Status == Status.Approved)
                                                        {
                                                            _locKpiSubmitLineCascading.Status = Status.Cancel;
                                                            _locKpiSubmitLineCascading.Save();
                                                            _locKpiSubmitLineCascading.Session.CommitTransaction();
                                                        }
                                                    }
                                                }

                                                #region Update Status Kpi Submit Line

                                                _locKpiSubmitLine.KpiSubmitLineStatus = Status.Cancel;
                                                _locKpiSubmitLine.Save();
                                                _locKpiSubmitLine.Session.CommitTransaction();

                                                #endregion Update Status Kpi Submit Line

                                                #endregion update cancel di cascading diri sendiri 

                                                #region update status di atasannya karena ada data cascading 

                                                KpiSubmit _locKpiSubmitLeaderXPO = _currSession.FindObject<KpiSubmit>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Name", _locEmployeeLeader),
                                                                                    new BinaryOperator("KpiSubmitMonth", _locMonthKpi),
                                                                                    new BinaryOperator("KpiSubmitYear", _locYearKpi),
                                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                                    new BinaryOperator("KpiSubmitStatus", Status.Approved),
                                                                                    new BinaryOperator("KpiSubmitStatus", Status.Open),
                                                                                    new BinaryOperator("KpiSubmitStatus", Status.Progress)
                                                                                    )));
                                                if (_locKpiSubmitLeaderXPO != null)
                                                {
                                                    XPCollection<KpiSubmitLine> _locKpiSubmitLineLeaders = new XPCollection<KpiSubmitLine>
                                                                              (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("KpiSubmit", _locKpiSubmitLeaderXPO),
                                                                              new GroupOperator(GroupOperatorType.Or,
                                                                              new BinaryOperator("KpiSubmitLineStatus", Status.Approved),
                                                                              new BinaryOperator("KpiSubmitLineStatus", Status.Open),
                                                                              new BinaryOperator("KpiSubmitStatus", Status.Progress)
                                                                              )));
                                                    if (_locKpiSubmitLineLeaders != null && _locKpiSubmitLineLeaders.Count() > 0)
                                                    {
                                                        foreach (KpiSubmitLine _locKpiSubmitLineLeader in _locKpiSubmitLineLeaders)
                                                        {
                                                            if (_locKpiSubmitLineLeader.KpiGroupType == KpiGroupType.Cascading)
                                                            {
                                                                XPCollection<KpiSubmitLineCascading> _locKpiSubmitLineCascadingLeaders = new XPCollection<KpiSubmitLineCascading>
                                                                              (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("KpiSubmitLine", _locKpiSubmitLineLeader),
                                                                              new GroupOperator(GroupOperatorType.Or,
                                                                              new BinaryOperator("Status", Status.Approved),
                                                                              new BinaryOperator("Status", Status.Open),
                                                                              new BinaryOperator("Status", Status.Progress)
                                                                              )));
                                                                if (_locKpiSubmitLineCascadingLeaders != null && _locKpiSubmitLineCascadingLeaders.Count() > 0)
                                                                {
                                                                    foreach (KpiSubmitLineCascading _locKpiSubmitLineCascadingLeader in _locKpiSubmitLineCascadingLeaders)
                                                                    {
                                                                        _locKpiSubmitLineCascadingLeader.Status = Status.Cancel;
                                                                        _locKpiSubmitLineCascadingLeader.Save();
                                                                        _locKpiSubmitLineCascadingLeader.Session.CommitTransaction();
                                                                    }
                                                                }

                                                            }
                                                            else
                                                            {
                                                                _locKpiSubmitLineLeader.KpiSubmitLineStatus = Status.Cancel;
                                                                _locKpiSubmitLineLeader.Save();
                                                                _locKpiSubmitLineLeader.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }

                                                    _locKpiSubmitLeaderXPO.KpiSubmitStatus = Status.Cancel;
                                                    _locKpiSubmitLeaderXPO.Save();
                                                    _locKpiSubmitLeaderXPO.Session.CommitTransaction();
                                                }

                                                #endregion Update status cancel di atasannya

                                            }
                                            else
                                            {
                                                #region Update Status Kpi Submit Line

                                                _locKpiSubmitLine.KpiSubmitLineStatus = Status.Cancel;
                                                _locKpiSubmitLine.Save();
                                                _locKpiSubmitLine.Session.CommitTransaction();

                                                #endregion Update Status Kpi Submit Line
                                            }
                                        }
                                    }

                                    #region Update status Kpi Submit

                                    #region Delete Approval data
                                    KpiSubmitApproval _locKpiSubmitApprovalXPO = _currSession.FindObject<KpiSubmitApproval>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("KpiSubmit", _locKpiSubmitXPO))
                                                                        );

                                    if (_locKpiSubmitApprovalXPO != null)
                                    {
                                        if (_locKpiSubmitApprovalXPO.KpiSubmit != null)
                                        {
                                            DeleteKpiSubmitApprovalKpiSubmit(_currSession, _locKpiSubmitApprovalXPO);
                                        }
                                    }
                                    #endregion Delete Approval Data

                                    _locKpiSubmitXPO.KpiSubmitStatus = Status.Cancel;
                                    _locKpiSubmitXPO.Save();
                                    _locKpiSubmitXPO.Session.CommitTransaction();

                                    #endregion Update status Kpi Submit

                                    //Message error
                                    ErrorMessageShow("Data Kpi Submit Status Is Cancel");

                                }
                                else
                                {
                                    //Message error
                                    ErrorMessageShow("Data Kpi Submit Not Available");
                                }

                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Kpi Submit Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion Cancel KPI

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = KpiSubmit " + ex.ToString());
            }
        }

        #endregion Cancel Kpi

        #region ProcessBehaviourAction_Execute

        private void ProcessBehaviourAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Employee _locEmployee = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        KpiSubmit _locKpiSubmitOS = (KpiSubmit)_objectSpace.GetObject(obj);

                        if (_locKpiSubmitOS != null)
                        {
                            if (_locKpiSubmitOS.Session != null)
                            {
                                _currSession = _locKpiSubmitOS.Session;
                            }

                            if (_locKpiSubmitOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locKpiSubmitOS.Code;
                                _locEmployee = _locKpiSubmitOS.Name;


                                KpiSubmit _locKpiSubmitXPO = _currSession.FindObject<KpiSubmit>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Open),
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Progress),
                                                                    new BinaryOperator("KpiSubmitStatus", Status.Approved)
                                                                    )));
                                if (_locKpiSubmitXPO != null)
                                {
                                    double _locNilai = 0;
                                    double _scoreBhv = 0;

                                    double _sumAllSinergi = 0;
                                    double _sumSinergiUser = 0;
                                    double _sumSinergiAtasan = 0;

                                    double _sumAllAkuntabel = 0;
                                    double _sumAkuntabelUser = 0;
                                    double _sumAkuntabelAtasan = 0;

                                    double _sumAllDinamis = 0;
                                    double _sumDinamisUser = 0;
                                    double _sumDinamisAtasan = 0;

                                    double _sumAllProfesional = 0;
                                    double _sumProfesionalUser = 0;
                                    double _sumProfesionalAtasan = 0;

                                    double _sumTotSinergi = 0;
                                    double _sumTotAkuntabel = 0;
                                    double _sumTotDinamis = 0;
                                    double _sumTotProfesional = 0;


                                    XPCollection<BehaviourSubmitLine> _locBehaviourSubmitLines = new XPCollection<BehaviourSubmitLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("KpiSubmit", _locKpiSubmitXPO)
                                                                    ));
                                    if (_locBehaviourSubmitLines != null && _locBehaviourSubmitLines.Count() > 0)
                                    {
                                        //xp collection behaviour role line
                                        XPCollection<BehaviourRole> _locBehaviourRoles = new XPCollection<BehaviourRole>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                   new BinaryOperator("Active", true),
                                                                   new BinaryOperator("Company", _locKpiSubmitXPO.Company),
                                                                   new BinaryOperator("YearBehaviour", _locKpiSubmitXPO.KpiSubmitYear)
                                                                   ));
                                        if (_locBehaviourRoles != null && _locBehaviourRoles.Count() > 0)
                                        {
                                            foreach (BehaviourRole _locBehaviourRole in _locBehaviourRoles)
                                            {
                                                // cek kondisi di Behaviour Monitoring
                                                XPCollection<BehaviourMonitoring> _locBehaviourMonitorings = new XPCollection<BehaviourMonitoring>
                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("BehaviourRole", _locBehaviourRole),
                                                                        new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear),
                                                                        new BinaryOperator("Employee", _locEmployee)
                                                                        ));
                                                if (_locBehaviourMonitorings != null && _locBehaviourMonitorings.Count() > 0)
                                                {
                                                    //update biasa data sendiri
                                                    foreach (BehaviourMonitoring _locBehaviourMonitoring in _locBehaviourMonitorings)
                                                    {
                                                        foreach (BehaviourSubmitLine _locBehaviourSubmitLine in _locBehaviourSubmitLines)
                                                        {
                                                            #region Sinergi

                                                            if (_locBehaviourSubmitLine.BehaviourCategory == BehaviourCategory.Sinergi)
                                                            {

                                                                _sumTotSinergi = _sumTotSinergi + _locBehaviourSubmitLine.ValueHide;

                                                                // Mencari bobot 
                                                                if (_locBehaviourRole.Category == BehaviourCategory.Sinergi)
                                                                {
                                                                    if (_locBehaviourSubmitLine.Staff != null)
                                                                    {
                                                                        _sumSinergiUser = _sumTotSinergi * (_locBehaviourRole.BobotAtasan / 100);
                                                                    }
                                                                    else
                                                                    {
                                                                        _sumSinergiAtasan = _sumTotSinergi * (_locBehaviourRole.BobotUser / 100);
                                                                    }

                                                                    _sumAllSinergi = _sumSinergiUser + _sumSinergiAtasan;

                                                                }


                                                                //Mencari Score Grading Behaviour Sinergi
                                                                #region Nilai Behaviour Sinergi

                                                                XPCollection<BehaviourGradingLine> _availableBehaviourGradingLines = new XPCollection<BehaviourGradingLine>(_currSession,
                                                                                                               new GroupOperator(GroupOperatorType.And,
                                                                                                               new BinaryOperator("Active", true)),
                                                                                                               new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                               );

                                                                if (_availableBehaviourGradingLines != null && _availableBehaviourGradingLines.Count() > 0)
                                                                {

                                                                    foreach (BehaviourGradingLine _availableBehaviourGradingLine in _availableBehaviourGradingLines)
                                                                    {

                                                                        if (_availableBehaviourGradingLine.Operation == ">")
                                                                        {
                                                                            if (_sumAllSinergi > _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "<=")
                                                                        {
                                                                            if (_sumAllSinergi <= _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "<")
                                                                        {
                                                                            if (_sumAllSinergi < _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "=")
                                                                        {
                                                                            if (_sumAllSinergi == _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                #endregion Nilai Behaviour Sinergi

                                                                #region Score Sinergi Behaviour

                                                                _scoreBhv = (_locBehaviourRole.Bobot / 100) * _locNilai;

                                                                #endregion Score Sinergi Behaviour
                                                            }

                                                            #endregion Sinergi

                                                            #region Akuntabel

                                                            if (_locBehaviourSubmitLine.BehaviourCategory == BehaviourCategory.Akuntabel)
                                                            {
                                                                _sumTotAkuntabel = _sumTotAkuntabel + _locBehaviourSubmitLine.ValueHide;

                                                                // Mencari bobot 
                                                                if (_locBehaviourRole.Category == BehaviourCategory.Akuntabel)
                                                                {

                                                                    _sumAkuntabelUser = _sumTotAkuntabel * (_locBehaviourRole.BobotUser / 100);
                                                                    _sumAkuntabelAtasan = 0;

                                                                    _sumAllAkuntabel = _sumAkuntabelUser + _sumAkuntabelAtasan;
                                                                }


                                                                //Mencari Score Grading Behaviour Akuntabel
                                                                #region Nilai Behaviour Akuntabel

                                                                XPCollection<BehaviourGradingLine> _availableBehaviourGradingLines = new XPCollection<BehaviourGradingLine>(_currSession,
                                                                                                               new GroupOperator(GroupOperatorType.And,
                                                                                                               new BinaryOperator("Active", true)),
                                                                                                               new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                               );

                                                                if (_availableBehaviourGradingLines != null && _availableBehaviourGradingLines.Count() > 0)
                                                                {

                                                                    foreach (BehaviourGradingLine _availableBehaviourGradingLine in _availableBehaviourGradingLines)
                                                                    {

                                                                        if (_availableBehaviourGradingLine.Operation == ">")
                                                                        {
                                                                            if (_sumAllAkuntabel > _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "<=")
                                                                        {
                                                                            if (_sumAllAkuntabel <= _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "<")
                                                                        {
                                                                            if (_sumAllAkuntabel < _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "=")
                                                                        {
                                                                            if (_sumAllAkuntabel == _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                #endregion Nilai Behaviour Akuntabel

                                                                #region Score Akuntabel Behaviour

                                                                _scoreBhv = (_locBehaviourRole.Bobot / 100) * _locNilai;

                                                                #endregion Score Akuntabel Behaviour
                                                            }

                                                            #endregion Akuntabel

                                                            #region Dinamis

                                                            if (_locBehaviourSubmitLine.BehaviourCategory == BehaviourCategory.Dinamis)
                                                            {
                                                                _sumTotDinamis = _sumTotDinamis + _locBehaviourSubmitLine.ValueHide;

                                                                // Mencari bobot 

                                                                if (_locBehaviourRole.Category == BehaviourCategory.Dinamis)
                                                                {

                                                                    _sumDinamisUser = _sumTotAkuntabel * (_locBehaviourRole.BobotUser / 100);
                                                                    _sumDinamisAtasan = 0;

                                                                    _sumAllDinamis = _sumDinamisUser + _sumDinamisAtasan;
                                                                }


                                                                //Mencari Score Grading Behaviour Dinamis
                                                                #region Nilai Behaviour Dinamis

                                                                XPCollection<BehaviourGradingLine> _availableBehaviourGradingLines = new XPCollection<BehaviourGradingLine>(_currSession,
                                                                                                               new GroupOperator(GroupOperatorType.And,
                                                                                                               new BinaryOperator("Active", true)),
                                                                                                               new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                               );

                                                                if (_availableBehaviourGradingLines != null && _availableBehaviourGradingLines.Count() > 0)
                                                                {

                                                                    foreach (BehaviourGradingLine _availableBehaviourGradingLine in _availableBehaviourGradingLines)
                                                                    {

                                                                        if (_availableBehaviourGradingLine.Operation == ">")
                                                                        {
                                                                            if (_sumAllDinamis > _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "<=")
                                                                        {
                                                                            if (_sumAllDinamis <= _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "<")
                                                                        {
                                                                            if (_sumAllDinamis < _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "=")
                                                                        {
                                                                            if (_sumAllDinamis == _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                #endregion Nilai Behaviour Dinamis

                                                                #region Score Dinamis Behaviour

                                                                _scoreBhv = (_locBehaviourRole.Bobot / 100) * _locNilai;

                                                                #endregion Score Dinamis Behaviour
                                                            }

                                                            #endregion Dinamis

                                                            #region Profesional

                                                            if (_locBehaviourSubmitLine.BehaviourCategory == BehaviourCategory.Profesional)
                                                            {
                                                                _sumTotProfesional = _sumTotProfesional + _locBehaviourSubmitLine.ValueHide;

                                                                // Mencari bobot
                                                                if (_locBehaviourRole.Category == BehaviourCategory.Profesional)
                                                                {

                                                                    _sumProfesionalUser = _sumTotProfesional * (_locBehaviourRole.BobotUser / 100);
                                                                    _sumProfesionalAtasan = 0;

                                                                    _sumAllProfesional = _sumProfesionalUser + _sumProfesionalAtasan;
                                                                }


                                                                //Mencari Score Grading Behaviour Profesional
                                                                #region Nilai Behaviour Profesional

                                                                XPCollection<BehaviourGradingLine> _availableBehaviourGradingLines = new XPCollection<BehaviourGradingLine>(_currSession,
                                                                                                               new GroupOperator(GroupOperatorType.And,
                                                                                                               new BinaryOperator("Active", true)),
                                                                                                               new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                               );

                                                                if (_availableBehaviourGradingLines != null && _availableBehaviourGradingLines.Count() > 0)
                                                                {

                                                                    foreach (BehaviourGradingLine _availableBehaviourGradingLine in _availableBehaviourGradingLines)
                                                                    {

                                                                        if (_availableBehaviourGradingLine.Operation == ">")
                                                                        {
                                                                            if (_sumAllProfesional > _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "<=")
                                                                        {
                                                                            if (_sumAllProfesional <= _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "<")
                                                                        {
                                                                            if (_sumAllProfesional < _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                        if (_availableBehaviourGradingLine.Operation == "=")
                                                                        {
                                                                            if (_sumAllProfesional == _availableBehaviourGradingLine.Value)
                                                                            {
                                                                                _locNilai = _availableBehaviourGradingLine.Result;
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                #endregion Nilai Behaviour Profesional

                                                                #region Score Profesional Behaviour

                                                                _scoreBhv = (_locBehaviourRole.Bobot / 100) * _locNilai;

                                                                #endregion Score Profesional Behaviour
                                                            }

                                                            #endregion Profesional

                                                        }

                                                        #region Update Behaviour Monitoring Sendiri

                                                        _locBehaviourMonitoring.BehaviourRole = _locBehaviourRole;
                                                        _locBehaviourMonitoring.KpiYear = _locBehaviourRole.YearBehaviour;
                                                        _locBehaviourMonitoring.Bobot = _locBehaviourRole.Bobot;
                                                        _locBehaviourMonitoring.Value = _locBehaviourMonitoring.Value + _locNilai;
                                                        _locBehaviourMonitoring.ScoreBehaviour = _locBehaviourMonitoring.ScoreBehaviour + _scoreBhv;
                                                        _locBehaviourMonitoring.Company = _locKpiSubmitXPO.Company;
                                                        _locBehaviourMonitoring.OrgDim1 = _locKpiSubmitXPO.OrgDim1;

                                                        _locBehaviourMonitoring.Save();
                                                        _locBehaviourMonitoring.Session.CommitTransaction();

                                                        SuccsessMessageShow("Data  Behaviour has Successfully to Process");

                                                        #endregion Update Behaviour Monitoring Sendiri
                                                    }
                                                }
                                                else
                                                {
                                                    //new data
                                                    foreach (BehaviourSubmitLine _locBehaviourSubmitLine in _locBehaviourSubmitLines)
                                                    {
                                                        #region Sinergi

                                                        if (_locBehaviourSubmitLine.BehaviourCategory == BehaviourCategory.Sinergi)
                                                        {

                                                            _sumTotSinergi = _sumTotSinergi + _locBehaviourSubmitLine.ValueHide;

                                                            // Mencari bobot 
                                                            if (_locBehaviourRole.Category == BehaviourCategory.Sinergi)
                                                            {
                                                                if (_locBehaviourSubmitLine.Staff != null)
                                                                {
                                                                    _sumSinergiUser = _sumTotSinergi * (_locBehaviourRole.BobotAtasan / 100);
                                                                }
                                                                else
                                                                {
                                                                    _sumSinergiAtasan = _sumTotSinergi * (_locBehaviourRole.BobotUser / 100);
                                                                }

                                                                _sumAllSinergi = _sumSinergiUser + _sumSinergiAtasan;

                                                            }


                                                            //Mencari Score Grading Behaviour Sinergi
                                                            #region Nilai Behaviour Sinergi

                                                            XPCollection<BehaviourGradingLine> _availableBehaviourGradingLines = new XPCollection<BehaviourGradingLine>(_currSession,
                                                                                                           new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("Active", true)),
                                                                                                           new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                           );

                                                            if (_availableBehaviourGradingLines != null && _availableBehaviourGradingLines.Count() > 0)
                                                            {

                                                                foreach (BehaviourGradingLine _availableBehaviourGradingLine in _availableBehaviourGradingLines)
                                                                {

                                                                    if (_availableBehaviourGradingLine.Operation == ">")
                                                                    {
                                                                        if (_sumAllSinergi > _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "<=")
                                                                    {
                                                                        if (_sumAllSinergi <= _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "<")
                                                                    {
                                                                        if (_sumAllSinergi < _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "=")
                                                                    {
                                                                        if (_sumAllSinergi == _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            #endregion Nilai Behaviour Sinergi

                                                            #region Score Sinergi Behaviour

                                                            _scoreBhv = (_locBehaviourRole.Bobot / 100) * _locNilai;

                                                            #endregion Score Sinergi Behaviour

                                                        }

                                                        #endregion Sinergi

                                                        #region Akuntabel

                                                        if (_locBehaviourSubmitLine.BehaviourCategory == BehaviourCategory.Akuntabel)
                                                        {
                                                            _sumTotAkuntabel = _sumTotAkuntabel + _locBehaviourSubmitLine.ValueHide;

                                                            // Mencari bobot 
                                                            if (_locBehaviourRole.Category == BehaviourCategory.Akuntabel)
                                                            {

                                                                _sumAkuntabelUser = _sumTotAkuntabel * (_locBehaviourRole.BobotUser / 100);
                                                                _sumAkuntabelAtasan = 0;

                                                                _sumAllAkuntabel = _sumAkuntabelUser + _sumAkuntabelAtasan;
                                                            }


                                                            //Mencari Score Grading Behaviour Akuntabel
                                                            #region Nilai Behaviour Akuntabel

                                                            XPCollection<BehaviourGradingLine> _availableBehaviourGradingLines = new XPCollection<BehaviourGradingLine>(_currSession,
                                                                                                           new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("Active", true)),
                                                                                                           new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                           );

                                                            if (_availableBehaviourGradingLines != null && _availableBehaviourGradingLines.Count() > 0)
                                                            {

                                                                foreach (BehaviourGradingLine _availableBehaviourGradingLine in _availableBehaviourGradingLines)
                                                                {

                                                                    if (_availableBehaviourGradingLine.Operation == ">")
                                                                    {
                                                                        if (_sumAllAkuntabel > _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "<=")
                                                                    {
                                                                        if (_sumAllAkuntabel <= _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "<")
                                                                    {
                                                                        if (_sumAllAkuntabel < _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "=")
                                                                    {
                                                                        if (_sumAllAkuntabel == _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            #endregion Nilai Behaviour Akuntabel

                                                            #region Score Akuntabel Behaviour

                                                            _scoreBhv = (_locBehaviourRole.Bobot / 100) * _locNilai;

                                                            #endregion Score Akuntabel Behaviour
                                                        }

                                                        #endregion Akuntabel

                                                        #region Dinamis

                                                        if (_locBehaviourSubmitLine.BehaviourCategory == BehaviourCategory.Dinamis)
                                                        {
                                                            _sumTotDinamis = _sumTotDinamis + _locBehaviourSubmitLine.ValueHide;

                                                            // Mencari bobot 

                                                            if (_locBehaviourRole.Category == BehaviourCategory.Dinamis)
                                                            {

                                                                _sumDinamisUser = _sumTotAkuntabel * (_locBehaviourRole.BobotUser / 100);
                                                                _sumDinamisAtasan = 0;

                                                                _sumAllDinamis = _sumDinamisUser + _sumDinamisAtasan;
                                                            }


                                                            //Mencari Score Grading Behaviour Dinamis
                                                            #region Nilai Behaviour Dinamis

                                                            XPCollection<BehaviourGradingLine> _availableBehaviourGradingLines = new XPCollection<BehaviourGradingLine>(_currSession,
                                                                                                           new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("Active", true)),
                                                                                                           new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                           );

                                                            if (_availableBehaviourGradingLines != null && _availableBehaviourGradingLines.Count() > 0)
                                                            {

                                                                foreach (BehaviourGradingLine _availableBehaviourGradingLine in _availableBehaviourGradingLines)
                                                                {

                                                                    if (_availableBehaviourGradingLine.Operation == ">")
                                                                    {
                                                                        if (_sumAllDinamis > _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "<=")
                                                                    {
                                                                        if (_sumAllDinamis <= _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "<")
                                                                    {
                                                                        if (_sumAllDinamis < _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "=")
                                                                    {
                                                                        if (_sumAllDinamis == _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            #endregion Nilai Behaviour Dinamis

                                                            #region Score Dinamis Behaviour

                                                            _scoreBhv = (_locBehaviourRole.Bobot / 100) * _locNilai;

                                                            #endregion Score Dinamis Behaviour
                                                        }

                                                        #endregion Dinamis

                                                        #region Profesional

                                                        if (_locBehaviourSubmitLine.BehaviourCategory == BehaviourCategory.Profesional)
                                                        {
                                                            _sumTotProfesional = _sumTotProfesional + _locBehaviourSubmitLine.ValueHide;

                                                            // Mencari bobot
                                                            if (_locBehaviourRole.Category == BehaviourCategory.Profesional)
                                                            {

                                                                _sumProfesionalUser = _sumTotProfesional * (_locBehaviourRole.BobotUser / 100);
                                                                _sumProfesionalAtasan = 0;

                                                                _sumAllProfesional = _sumProfesionalUser + _sumProfesionalAtasan;
                                                            }


                                                            //Mencari Score Grading Behaviour Profesional
                                                            #region Nilai Behaviour Profesional

                                                            XPCollection<BehaviourGradingLine> _availableBehaviourGradingLines = new XPCollection<BehaviourGradingLine>(_currSession,
                                                                                                           new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("Active", true)),
                                                                                                           new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                           );

                                                            if (_availableBehaviourGradingLines != null && _availableBehaviourGradingLines.Count() > 0)
                                                            {

                                                                foreach (BehaviourGradingLine _availableBehaviourGradingLine in _availableBehaviourGradingLines)
                                                                {

                                                                    if (_availableBehaviourGradingLine.Operation == ">")
                                                                    {
                                                                        if (_sumAllProfesional > _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "<=")
                                                                    {
                                                                        if (_sumAllProfesional <= _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "<")
                                                                    {
                                                                        if (_sumAllProfesional < _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                    if (_availableBehaviourGradingLine.Operation == "=")
                                                                    {
                                                                        if (_sumAllProfesional == _availableBehaviourGradingLine.Value)
                                                                        {
                                                                            _locNilai = _availableBehaviourGradingLine.Result;
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            #endregion Nilai Behaviour Profesional

                                                            #region Score Profesional Behaviour

                                                            _scoreBhv = (_locBehaviourRole.Bobot / 100) * _locNilai;

                                                            #endregion Score Profesional Behaviour
                                                        }

                                                        #endregion Profesional

                                                    }

                                                    #region Save new Behaviour Bawahan ke Monitoring
                                                    //get Behaviour bawahannnya
                                                    XPCollection<Employee> _locEmployees = new XPCollection<Employee>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Leader", _locEmployee),
                                                                               new BinaryOperator("Active", true)
                                                                               ));
                                                    if (_locEmployees != null && _locEmployees.Count() > 0)
                                                    {
                                                        foreach (Employee _locEmploye in _locEmployees)
                                                        {
                                                            BehaviourMonitoring _locBehaviourStaff = _currSession.FindObject<BehaviourMonitoring>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("BehaviourRole", _locBehaviourRole),
                                                                     new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear),
                                                                     new BinaryOperator("Employee", _locEmploye)
                                                                    ));
                                                            if (_locBehaviourStaff != null)
                                                            {
                                                                _locBehaviourStaff.BehaviourRole = _locBehaviourRole;
                                                                _locBehaviourStaff.KpiYear = _locBehaviourRole.YearBehaviour;
                                                                _locBehaviourStaff.Bobot = _locBehaviourRole.Bobot;
                                                                _locBehaviourStaff.Value = _locBehaviourStaff.Value + _locNilai;
                                                                _locBehaviourStaff.ScoreBehaviour = _locBehaviourStaff.ScoreBehaviour + _scoreBhv;
                                                                _locBehaviourStaff.Company = _locKpiSubmitXPO.Company;
                                                                _locBehaviourStaff.OrgDim1 = _locKpiSubmitXPO.OrgDim1;

                                                                _locBehaviourStaff.Save();
                                                                _locBehaviourStaff.Session.CommitTransaction();
                                                            }
                                                            else
                                                            {
                                                                #region Save New Behaviour Monitoring
                                                                BehaviourMonitoring _saveBehaviourMonitoringStaff = new BehaviourMonitoring(_currSession)
                                                                {
                                                                    BehaviourRole = _locBehaviourRole,
                                                                    KpiYear = _locBehaviourRole.YearBehaviour,
                                                                    Bobot = _locBehaviourRole.Bobot,
                                                                    Value = _locNilai,
                                                                    ScoreBehaviour = _scoreBhv,
                                                                    Employee = _locEmploye,
                                                                    Company = _locKpiSubmitXPO.Company,
                                                                    OrgDim1 = _locKpiSubmitXPO.OrgDim1,
                                                                };
                                                                _saveBehaviourMonitoringStaff.Save();
                                                                _saveBehaviourMonitoringStaff.Session.CommitTransaction();

                                                                #endregion Save New Behaviour Monitoring
                                                            }

                                                            SuccsessMessageShow("Data  Behaviour has Successfully to Process");


                                                        }
                                                    }

                                                    #endregion Save new Behaviour Bawahan ke Monitoring
                                                    
                                                    #region Save New Behaviour Sendiri ke Monitoring
                                                    BehaviourMonitoring _saveBehaviourMonitoring = new BehaviourMonitoring(_currSession)
                                                    {
                                                        BehaviourRole = _locBehaviourRole,
                                                        KpiYear = _locBehaviourRole.YearBehaviour,
                                                        Bobot = _locBehaviourRole.Bobot,
                                                        Value = _locNilai,
                                                        ScoreBehaviour = _scoreBhv,
                                                        Employee = _locEmployee,
                                                        Company = _locKpiSubmitXPO.Company,
                                                        OrgDim1 = _locKpiSubmitXPO.OrgDim1,
                                                        OrgDim2 = _locKpiSubmitXPO.OrgDim2,
                                                        OrgDim3 = _locKpiSubmitXPO.OrgDim3,
                                                        OrgDim4 = _locKpiSubmitXPO.OrgDim4,
                                                    };
                                                    _saveBehaviourMonitoring.Save();
                                                    _saveBehaviourMonitoring.Session.CommitTransaction();

                                                    #endregion Save New Behaviour Monitoring

                                                    SuccsessMessageShow("Data  Behaviour has Successfully to Process");
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //Message error
                                    ErrorMessageShow("Data  Behaviour Not Available");
                                }

                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Kpi Submit Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }


        #endregion ProcessBehaviourAction_Execute

        #region Code
        public void SetKpiSubmitTaskBaseByApprovalInformationLevelForKpiSubmit(Session _currSession, KpiSubmit _locKpiSubmitXPO, ObjectList _locObjectList, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                XPCollection<ApplicationApprovalSetting> _locApplicationApprovalSettings = null;
                Company _localCompany = null;
                OrganizationDimension1 _localOrgDim1 = null;
                OrganizationDimension2 _localOrgDim2 = null;
                OrganizationDimension3 _localOrgDim3 = null;

                if (_locKpiSubmitXPO != null)
                {

                    if (_locKpiSubmitXPO.Company != null) { _localCompany = _locKpiSubmitXPO.Company; }
                    if (_locKpiSubmitXPO.OrgDim1 != null) { _localOrgDim1 = _locKpiSubmitXPO.OrgDim1; }
                    if (_locKpiSubmitXPO.OrgDim2 != null) { _localOrgDim2 = _locKpiSubmitXPO.OrgDim2; }
                    if (_locKpiSubmitXPO.OrgDim3 != null) { _localOrgDim3 = _locKpiSubmitXPO.OrgDim3; }


                    _locApplicationApprovalSettings = _globFunc.GetApplicationApprovalSettingByApprovalLevelNonUserAccess(_currSession, _locApprovalLevel, _locObjectList, _localCompany, _localOrgDim1, _localOrgDim2, _localOrgDim3);

                    if (_locApplicationApprovalSettings != null && _locApplicationApprovalSettings.Count() > 0)
                    {
                        foreach (ApplicationApprovalSetting _locApplicationApprovalSetting in _locApplicationApprovalSettings)
                        {
                            XPCollection<KpiSubmitTaskBase> _locKpiSubmitTaskBases = new XPCollection<KpiSubmitTaskBase>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("KpiSubmit", _locKpiSubmitXPO)
                                                                                     ));
                            
                            if (_locKpiSubmitTaskBases != null && _locKpiSubmitTaskBases.Count() > 0)
                            {
                                foreach(KpiSubmitTaskBase _locKpiSubmitTaskBase in _locKpiSubmitTaskBases)
                                {
                                    _locKpiSubmitTaskBase.ApprovalStatus = Status.Progress;
                                    _locKpiSubmitTaskBase.Save();
                                    _locKpiSubmitTaskBase.Session.CommitTransaction();
                                }

                            }
                            else
                            {
                                KpiSubmitTaskBase _saveKpiSubmitTaskBase = new KpiSubmitTaskBase(_currSession)
                                {
                                    KpiSubmit = _locKpiSubmitXPO,
                                    Company = _locKpiSubmitXPO.Company,
                                    OrgDim1 = _locKpiSubmitXPO.OrgDim1,
                                    OrgDim2 = _locKpiSubmitXPO.OrgDim2,
                                    OrgDim3 = _locKpiSubmitXPO.OrgDim3,
                                    OrgDim4 = _locKpiSubmitXPO.OrgDim4,
                                    Employee = _locKpiSubmitXPO.Name,
                                    ApprovalLevel = _locApprovalLevel,
                                    ApprovalStatus = Status.Progress,
                                    ApplicationApprovalSetting = _locApplicationApprovalSetting,
                                };

                                _saveKpiSubmitTaskBase.Save();
                                _saveKpiSubmitTaskBase.Session.CommitTransaction();
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = KpiSubmit " + ex.ToString());
            }
        }

        public void SetActivationPostingKpiSubmitLine(Session _currentSession, KpiSubmit _locKpiSubmitXPO, bool _locActivationPosting)
        {
            try
            {
                DateTime now = DateTime.Now;

                XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                        (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                         new BinaryOperator("Select", true),
                                                                         new GroupOperator(GroupOperatorType.Or,
                                                                         new BinaryOperator("Status", Status.Progress),
                                                                         new BinaryOperator("Status", Status.Approved))
                                                                         ));

                if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                {
                    foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                    {
                        _locKpiSubmitLine.KpiSubmitLineStatus = Status.Approved;
                        _locKpiSubmitLine.StatusDate = now;
                        _locKpiSubmitLine.ActivationPosting = _locActivationPosting;
                        _locKpiSubmitLine.Save();
                        _locKpiSubmitLine.Session.CommitTransaction();
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = KpiSubmit " + ex.ToString());
            }
        }

        public void SetKpiSubmitApproval(Session _currentSession, KpiSubmit _locKpiSubmitXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                KpiSubmitApproval _locKpiSubmitApprovalXPO = _currentSession.FindObject<KpiSubmitApproval>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locKpiSubmitApprovalXPO == null)
                {
                    KpiSubmitApproval _saveDataKSA2 = new KpiSubmitApproval(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        KpiSubmit = _locKpiSubmitXPO
                    };
                    _saveDataKSA2.Save();
                    _saveDataKSA2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = KpiSubmit " + ex.ToString());
            }
        }

        private void DeleteKpiSubmitTaskBase(Session _currentSession, KpiSubmit _locKpiSubmitXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                KpiSubmitTaskBase _locKpiSubmitTaskBaseXPO = _currentSession.FindObject<KpiSubmitTaskBase>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("KpiSubmit", _locKpiSubmitXPO)
                                                                        //new BinaryOperator("Employee", _locEmployeeXPO)
                                                                        ));
                if (_locKpiSubmitTaskBaseXPO != null)
                {
                    _locKpiSubmitTaskBaseXPO.Delete();
                    _locKpiSubmitTaskBaseXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = KpiSubmit " + ex.ToString());
            }
        }

        #endregion Code

        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void InformationMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Information";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void DeleteKpiSubmitApprovalKpiSubmit(Session _currSession, KpiSubmitApproval _locKpiSubmitApprovalXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locKpiSubmitApprovalXPO != null)
                {
                    _locKpiSubmitApprovalXPO.Delete();
                    _locKpiSubmitApprovalXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = KpiSubmitTaskBase " + ex.ToString());
            }
        }

        #endregion Global Method



    }
}
