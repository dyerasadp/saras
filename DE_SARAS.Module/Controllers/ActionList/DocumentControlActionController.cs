﻿using DE_SARAS.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DocumentControlActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public DocumentControlActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(DocumentControl);

            #region Set Document
            SimpleAction setDocumentAction = new SimpleAction(this, "SetDocumentAction", PredefinedCategory.View)
            {
                Caption = "Set Document",
                ConfirmationMessage = "Are you sure you want to set default the data?",
                ImageName = "BO_Security_Permission_Action"
            };
            setDocumentAction.Execute += SetDocumentAction_Execute;

            #endregion Set Document


        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }


        #region SetDocumentAction_Execute
        private void SetDocumentAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        DocumentControl _locDocumentOS = (DocumentControl)_objectSpace.GetObject(obj);

                        if (_locDocumentOS != null)
                        {
                            if (_locDocumentOS.Session != null)
                            {
                                _currSession = _locDocumentOS.Session;
                            }

                            if (_locDocumentOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locDocumentOS.Code;
                                DocumentControl _locDocumentXPO = _currSession.FindObject<DocumentControl>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new BinaryOperator("Active", true)
                                                                    ));
                                if (_locDocumentXPO != null)
                                {
                                    XPCollection<DocumentControlLine> _locDocumentControlLines = new XPCollection<DocumentControlLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("DocumentControl", _locDocumentXPO),
                                                                    new BinaryOperator("Active", true)
                                                                    ));

                                    if (_locDocumentControlLines != null && _locDocumentControlLines.Count() > 0)
                                    {
                                        foreach (DocumentControlLine _locDocumentControlLine in _locDocumentControlLines)
                                        {
                                            DocumentControl _locDocument2XPO = _currSession.FindObject<DocumentControl>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new BinaryOperator("Active", true)
                                                                    ));
                                            if (_locDocument2XPO != null)
                                            {
                                                _locDocument2XPO.Document = _locDocumentControlLine.Document;
                                                _locDocument2XPO.Attachment = _locDocumentControlLine.Attachment;
                                                _locDocument2XPO.StartDate = _locDocumentControlLine.StartDate;
                                                _locDocument2XPO.EndDate = _locDocumentControlLine.EndDate;
                                                _locDocument2XPO.Save();
                                                _locDocument2XPO.Session.CommitTransaction();


                                                //Message
                                                SuccsessMessageShow("Set Document has successfully updates");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //Message error
                                    ErrorMessageShow("Document Not Available");
                                }

                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = Employee " + ex.ToString());
            }
        }

        #endregion SetDocumentAction_Execute


        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
