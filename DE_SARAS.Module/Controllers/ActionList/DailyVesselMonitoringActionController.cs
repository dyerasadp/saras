﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DailyVesselMonitoringActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public DailyVesselMonitoringActionController()
        {
            InitializeComponent();
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(DailyVesselMonitoring);

            #region GetChecklist

            SimpleAction getChecklistVesselAction = new SimpleAction(this, "getChecklistVesselAction", PredefinedCategory.View)
            {
                Caption = "Get Checklist",
                ConfirmationMessage = "Are you sure?",
                ImageName = "BO_Security_Permission_Action"
            };
            getChecklistVesselAction.Execute += GetChecklistVesselAction_Execute;

            #endregion GetChecklist

            #region VerifDvmChecklist

            SimpleAction verifDvmAction = new SimpleAction(this, "verifDvmAction", PredefinedCategory.View)
            {
                Caption = "Verify",
                ConfirmationMessage = "Are you sure?",
                ImageName = "Check"
            };
            verifDvmAction.Execute += VerifDvmAction_Execute;

            #endregion VerifDvmChecklist
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region GetChecklistAction_Execute
        private void GetChecklistVesselAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                #region GetChecklist

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        DailyVesselMonitoring _locDVM = (DailyVesselMonitoring)_objectSpace.GetObject(obj);

                        if (_locDVM != null)
                        {
                            if (_locDVM.Session != null)
                            {
                                _currSession = _locDVM.Session;
                            }

                            if (_locDVM.Code != null && _currSession != null)
                            {
                                _currObjectId = _locDVM.Code;

                                DailyVesselMonitoring _locDvmXPO = _currSession.FindObject<DailyVesselMonitoring>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locDvmXPO != null)
                                {
                                    if (_locDvmXPO.Vessel != null && _locDvmXPO.ChecklistHead != null)
                                    {
                                        #region Get Checklist Item Deck

                                        XPCollection<ShippingVesselDailySetLineDeck> _locVesselSetDeckLines = new XPCollection<ShippingVesselDailySetLineDeck>
                                                (_currSession, new GroupOperator(GroupOperatorType.And, 
                                                new BinaryOperator("ChecklistHead", _locDvmXPO.ChecklistHead),
                                                new BinaryOperator("Active", true)
                                                ));

                                        if (_locVesselSetDeckLines != null && _locVesselSetDeckLines.Count() > 0)
                                        {
                                            foreach (ShippingVesselDailySetLineDeck _locVesselSetDeckLine in _locVesselSetDeckLines)
                                            {
                                                VesselPart _locVesselPartDeck = _currSession.FindObject<VesselPart>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _locVesselSetDeckLine.Code)
                                                                    ));

                                                DailyVesselMonitoringLineDeck _saveDvmLineDeck = new DailyVesselMonitoringLineDeck(_currSession)
                                                {
                                                    MonitoringHead = _locDvmXPO,
                                                    ItemChecklist = _locVesselSetDeckLine.Item,
                                                    Result = _locVesselPartDeck.Result,
                                                    Remarks = _locVesselPartDeck.Remarks,
                                                };
                                                _saveDvmLineDeck.Save();
                                                _saveDvmLineDeck.Session.CommitTransaction();
                                            }
                                        }

                                        #endregion Get Checklist Item 

                                        #region Get Checklist Item Engine

                                        XPCollection<ShippingVesselDailySetLineEngine> _locVesselSetEngineLines = new XPCollection<ShippingVesselDailySetLineEngine>
                                                (_currSession, new GroupOperator(GroupOperatorType.And, 
                                                new BinaryOperator("ChecklistHead", _locDvmXPO.ChecklistHead),
                                                new BinaryOperator("Active", true)
                                                ));

                                        if (_locVesselSetEngineLines != null && _locVesselSetEngineLines.Count() > 0)
                                        {
                                            foreach (ShippingVesselDailySetLineEngine _locVesselSetEngineLine in _locVesselSetEngineLines)
                                            {
                                                VesselPart _locVesselPartEngine = _currSession.FindObject<VesselPart>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _locVesselSetEngineLine.Code)
                                                                    ));

                                                DailyVesselMonitoringLineEngine _saveDvmLineEngine = new DailyVesselMonitoringLineEngine(_currSession)
                                                {
                                                    MonitoringHead = _locDvmXPO,
                                                    ItemChecklist = _locVesselSetEngineLine.Item,
                                                    Result = _locVesselPartEngine.Result,
                                                    Remarks = _locVesselPartEngine.Remarks,
                                                };
                                                _saveDvmLineEngine.Save();
                                                _saveDvmLineEngine.Session.CommitTransaction();
                                            }
                                        }

                                        #endregion Get Checklist Item Engine
                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion GetChecklist


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DailyVesselMonitoring " + ex.ToString());
            }
        }

        #endregion GetChecklistAction_Execute

        #region VerifDvmAction_Execute

        private void VerifDvmAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        DailyVesselMonitoring _locDVM = (DailyVesselMonitoring)_objectSpace.GetObject(obj);

                        if (_locDVM != null)
                        {
                            if (_locDVM.Session != null)
                            {
                                _currSession = _locDVM.Session;
                            }

                            if (_locDVM.Code != null && _currSession != null)
                            {
                                _currObjectId = _locDVM.Code;

                                DailyVesselMonitoring _locMonitoringXPO = _currSession.FindObject<DailyVesselMonitoring>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locMonitoringXPO != null)
                                {
                                    if (_locMonitoringXPO.Verify == VerifyStatus.NotVerified)
                                    {
                                        _locMonitoringXPO.VerifiedDate = now;
                                        _locMonitoringXPO.Verify = VerifyStatus.Verified;
                                        _locMonitoringXPO.Save();
                                        _locMonitoringXPO.Session.CommitTransaction();

                                        XPCollection<DailyVesselMonitoringLineDeck> _locMonitoringDecks = new XPCollection<DailyVesselMonitoringLineDeck>
                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("MonitoringHead", _locMonitoringXPO)
                                            ));

                                        if (_locMonitoringDecks != null && _locMonitoringDecks.Count() > 0)
                                        {
                                            foreach (DailyVesselMonitoringLineDeck _locMonitoringDeck in _locMonitoringDecks)
                                            {
                                                VesselPart _locVesselPartXPO = _currSession.FindObject<VesselPart>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _locMonitoringDeck.ItemChecklist.Code)
                                                                    ));

                                                _locVesselPartXPO.Result = _locMonitoringDeck.Result;
                                                _locVesselPartXPO.Remarks = _locMonitoringDeck.Remarks;
                                                _locVesselPartXPO.Save();
                                                _locVesselPartXPO.Session.CommitTransaction();
                                            }
                                        }

                                        XPCollection<DailyVesselMonitoringLineEngine> _locMonitoringEngines = new XPCollection<DailyVesselMonitoringLineEngine>
                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("MonitoringHead", _locMonitoringXPO)
                                            ));

                                        if (_locMonitoringEngines != null && _locMonitoringEngines.Count() > 0)
                                        {
                                            foreach (DailyVesselMonitoringLineEngine _locMonitoringEngine in _locMonitoringEngines)
                                            {
                                                VesselPart _locVesselPartXPO = _currSession.FindObject<VesselPart>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _locMonitoringEngine.ItemChecklist.Code)
                                                                    ));

                                                _locVesselPartXPO.Result = _locMonitoringEngine.Result;
                                                _locVesselPartXPO.Remarks = _locMonitoringEngine.Remarks;
                                                _locVesselPartXPO.Save();
                                                _locVesselPartXPO.Session.CommitTransaction();
                                            }
                                        }

                                        SuccsessMessageShow("The Selected Daily Vessel Monitoring Data Has Succesfuly Verified");
                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Daily Vessel Monitoring Data Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DailyVesselMonitoring " + ex.ToString());
            }
        }

        #endregion VerifDvmAction_Execute

        #region Global Method
            private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
