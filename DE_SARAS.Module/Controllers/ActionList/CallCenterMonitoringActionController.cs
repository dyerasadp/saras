﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CallCenterMonitoringActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public CallCenterMonitoringActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(CallCenterMonitoring);

            #region CloseAction 

            SimpleAction closeCallCenterAction = new SimpleAction(this, "CloseCallCenterAction", PredefinedCategory.View)
            {
                Caption = "Close Ticket",
                ConfirmationMessage = "Are you sure you want to Close the data?",
                ImageName = "Redo"
            };
            closeCallCenterAction.Execute += CloseCallCenterAction_Execute;

            #endregion CloseAction
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }


        #region CloseCallCenterAction_Execute
        private void CloseCallCenterAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                CallCenter _callCenter = null;
                CallCenter _callCentermon = null;
                string _callCenterCode = null;


                #region Closed

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CallCenterMonitoring _locCallCenterMonitoringActivityOS = (CallCenterMonitoring)_objectSpace.GetObject(obj);


                        if (_locCallCenterMonitoringActivityOS != null)
                        {
                            if (_locCallCenterMonitoringActivityOS.Session != null)
                            {
                                _currSession = _locCallCenterMonitoringActivityOS.Session;
                            }

                            if (_locCallCenterMonitoringActivityOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCallCenterMonitoringActivityOS.Code;

                                CallCenterMonitoring _locCallCenterMonitoringXPO = _currSession.FindObject<CallCenterMonitoring>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locCallCenterMonitoringXPO != null)
                                {
                                    if (_locCallCenterMonitoringXPO.StatusTicket == StatusTicket.Progress)
                                    {  
                                        
                                        //DateTime futureDate = new DateTime(2023, 11, 26, 20, 31, 29);
                                        _locCallCenterMonitoringXPO.ClosedDate = now; 
                                        _locCallCenterMonitoringXPO.Interval = _locCallCenterMonitoringXPO.ClosedDate - _locCallCenterMonitoringXPO.ProgressDate;
                                        _locCallCenterMonitoringXPO.StatusTicket = StatusTicket.Closed;
                                        _locCallCenterMonitoringXPO.ActivationPosting = true;
                                        _locCallCenterMonitoringXPO.Save(); 
                                        _locCallCenterMonitoringXPO.Session.CommitTransaction();

                                        #region Update Status on Call Center
                                        _callCenter = _locCallCenterMonitoringXPO.CallCenter;
                                        XPCollection<CallCenter> _locCallCenters = new XPCollection<CallCenter>
                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("StatusTicket", StatusTicket.Progress)
                                                                         ));
                                        if (_locCallCenters != null && _locCallCenters.Count() > 0)
                                        {
                                            foreach(CallCenter _locCallCenter in _locCallCenters)
                                            {
                                                _callCenterCode = _locCallCenter.Code;
                                                CallCenter _locCallCenterXPO = _currSession.FindObject<CallCenter>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _callCenterCode)
                                                                                ));
                                                if(_locCallCenterXPO != null)
                                                {
                                                    _locCallCenterXPO.StatusTicket = StatusTicket.Closed;
                                                    _locCallCenterXPO.ActivationPosting = true;
                                                    _locCallCenterXPO.Save();
                                                    _locCallCenterXPO.Session.CommitTransaction();


                                                    //Message
                                                    SuccsessMessageShow("The Ticket has been Successfully Closed");
                                                }

                                            }
                                        }

                                        #endregion Update Status on Call Center

                                        #region Update Status on Call Center Task Base

                                        XPCollection<CallCenterTaskBase> _locCallCenterTaskBases = new XPCollection<CallCenterTaskBase>
                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("StatusTicket", StatusTicket.Progress)
                                                                         ));
                                        if (_locCallCenterTaskBases != null && _locCallCenterTaskBases.Count() > 0)
                                        {
                                            foreach (CallCenterTaskBase _locCallCenterTaskBase in _locCallCenterTaskBases)
                                            {
                                                _callCentermon = _locCallCenterMonitoringXPO.CallCenter;
                                                CallCenterTaskBase _locCallCenterTbXPO = _currSession.FindObject<CallCenterTaskBase>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CallCenter", _callCentermon)
                                                                                ));
                                                if (_locCallCenterTbXPO != null)
                                                {
                                                    _locCallCenterTbXPO.ClosedDate = now;
                                                    _locCallCenterTbXPO.Interval = _locCallCenterTbXPO.ClosedDate - _locCallCenterMonitoringXPO.ProgressDate;
                                                    _locCallCenterTbXPO.StatusTicket = StatusTicket.Closed;
                                                    _locCallCenterTbXPO.ActivationPosting = true;
                                                    _locCallCenterTbXPO.Save();
                                                    _locCallCenterTbXPO.Session.CommitTransaction();


                                                    //Message
                                                    SuccsessMessageShow("The Ticket has been Successfully Closed");
                                                }

                                            }
                                        }

                                        #endregion Update Status on Call Center Task Base
                                    }
                                    else
                                    {
                                        //Message Error
                                        ErrorMessageShow("The Ticket has been Closed");
                                    }

                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Ticket Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion Closed
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CallCenter " + ex.ToString());
            }
        }

        #endregion CloseCallCenterAction_Execute


        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
