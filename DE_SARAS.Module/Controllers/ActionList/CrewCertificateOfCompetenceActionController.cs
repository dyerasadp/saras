﻿using DE_SARAS.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CrewCertificateOfCompetenceActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public CrewCertificateOfCompetenceActionController()
        {
            InitializeComponent();
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(CrewCertificateOfCompetence);

            #region Set Default Class Action

            SimpleAction defaultClassAction = new SimpleAction(this, "DefaultClassAction", PredefinedCategory.View)
            {
                Caption = "Set Default",
                ConfirmationMessage = "Are you sure to set this class to default?",
                ImageName = "Redo"
            };
            defaultClassAction.Execute += DefaultClassAction_Execute;

            #endregion Set Default Class Action
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }


        #region Set Default CLass Execute

        private void DefaultClassAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CrewCertificateOfCompetence _locCrewClassActivityOS = (CrewCertificateOfCompetence)_objectSpace.GetObject(obj);

                        if (_locCrewClassActivityOS != null)
                        {
                            if (_locCrewClassActivityOS.Session != null)
                            {
                                _currSession = _locCrewClassActivityOS.Session;
                            }

                            if (_locCrewClassActivityOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCrewClassActivityOS.Code;

                                CrewCertificateOfCompetence _locClassXPO = _currSession.FindObject<CrewCertificateOfCompetence>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locClassXPO != null)
                                {
                                    //cari semua data crew contract dari object crew nya, lalu default yang ada dimatikan
                                    XPCollection<CrewCertificateOfCompetence> _locCrewClasses = new XPCollection<CrewCertificateOfCompetence>
                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Crew", _locClassXPO.Crew)
                                            ));

                                    if (_locClassXPO.Default == false)
                                    {
                                        if (_locCrewClasses != null && _locCrewClasses.Count() > 0)
                                        {
                                            foreach (CrewCertificateOfCompetence _locCrewClass in _locCrewClasses)
                                            {
                                                if (_locCrewClass.Default == true)
                                                {
                                                    _locCrewClass.Default = false;
                                                }
                                            }
                                        }

                                        _locClassXPO.Default = true;
                                        _locClassXPO.Save();
                                        _locClassXPO.Session.CommitTransaction();
                                    }

                                    Crew _locCrew = _currSession.FindObject<Crew>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _locClassXPO.Crew.Code)
                                                                    ));

                                    CrewClassList _locClass = _currSession.FindObject<CrewClassList>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _locClassXPO.Class.Code)
                                                                    ));

                                    if (_locCrew != null)
                                    {
                                        _locCrew.Class = _locClass;
                                    }

                                    SuccsessMessageShow("The contract set to default");

                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Voyage Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CallCenter " + ex.ToString());
            }
        }

        #endregion Set Default Class Execute

        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
