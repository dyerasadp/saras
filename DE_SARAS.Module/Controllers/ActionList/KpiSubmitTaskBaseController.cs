﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class KpiSubmitTaskBaseController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public KpiSubmitTaskBaseController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(KpiSubmitTaskBase);

            #region ApprovalAction 

            SimpleAction kpiSubmitTaskBaseApprovalAction = new SimpleAction(this, "PurchaseTaskBaseApprovalAction", PredefinedCategory.View)
            {
                Caption = "ApprovedAction",
                ConfirmationMessage = "Are you sure you want to Approved the data?",
                ImageName = "BO_Security_Permission_Action"
            };
            kpiSubmitTaskBaseApprovalAction.Execute += KpiSubmitTaskBaseApprovalAction_Execute;

            #endregion ApprovalAction

            #region RejectKpiTaskBaseAction

            SimpleAction rejectKpiTaskBaseAction = new SimpleAction(this, "RejectKpiTaskBaseAction", PredefinedCategory.View)
            {
                Caption = "Reject Kpi",
                ConfirmationMessage = "Are you sure you want to Reject the data?",
                ImageName = "Actions_Delete"
            };
            rejectKpiTaskBaseAction.Execute += RejectKpiTaskBaseAction_Execute;

            #endregion RejectKpiTaskBaseAction
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region Approval
        private void KpiSubmitTaskBaseApprovalAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;
                ObjectList _locObjectList = ObjectList.None;
                KpiSubmit _locKpiSubmit = null;


                foreach (Object obj in _objectsToProcess)
                {
                    KpiSubmitTaskBase _objInNewObjectSpace = (KpiSubmitTaskBase)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                        if (_objInNewObjectSpace.Session != null)
                        {
                            _currentSession = _objInNewObjectSpace.Session;
                        }
                    }

                    if (_currObjectId != null && _currentSession != null)
                    {
                        UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                        KpiSubmitTaskBase _locKpiSubmitTaskBaseXPO = _currentSession.FindObject<KpiSubmitTaskBase>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)
                                                                         ));

                        if (_locKpiSubmitTaskBaseXPO != null)
                        {

                            #region KpiSubmit
                            if (_locKpiSubmitTaskBaseXPO.KpiSubmit != null)
                            {
                                _locObjectList = ObjectList.KpiSubmit;
                                _locKpiSubmit = _locKpiSubmitTaskBaseXPO.KpiSubmit;

                                if (_locKpiSubmitTaskBaseXPO.ApplicationApprovalSetting != null)
                                {
                                    SetKpiSubmitTaskBaseByApprovalForKpiSubmit(_currentSession, _locKpiSubmit, _locKpiSubmitTaskBaseXPO.ApplicationApprovalSetting, _locUserAccess, _locObjectList);
                                    //SetKpiSubmitTaskBaseMonitoring(_currentSession, _locKpiSubmitTaskBaseXPO);
                                    DeleteKpiSubmitTaskBase(_currentSession, _locKpiSubmitTaskBaseXPO);
                                }
                                else
                                {
                                    ErrorMessageShow("Data Kpi Submit Not Available");
                                }

                            }
                            #endregion KpiSubmit

                        }
                        else
                        {
                            ErrorMessageShow("Kpi Submit Task Base Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseTaskBase " + ex.ToString());
            }
        }

        #region Code KpiSubmit

        private void SetKpiSubmitTaskBaseByApprovalForKpiSubmit(Session _currentSession, KpiSubmit _locKpiSubmitXPO,ApplicationApprovalSetting _locApplicationApprovalXPO, UserAccess _locUserAccess, ObjectList _locObjectList)
        {
            try
            {
                KpiSubmitActionController _globFuncKS = new KpiSubmitActionController();
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                Company _locCompany = null;
                Employee _locEmployee = null;
                string _locEmployeeCode = null;
                string _locSubjectMail = null;
                string _locBody = null;
                string _locApprovalLevel = null;
                string _locMailTo = null;

                if (_locKpiSubmitXPO != null)
                {
                    if (_locKpiSubmitXPO.Company != null) { _locCompany = _locKpiSubmitXPO.Company; }
                    KpiSubmitApproval _locKpiSubmitApproval = _currentSession.FindObject<KpiSubmitApproval>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                     new BinaryOperator("EndApproval", true)));
                    if (_locKpiSubmitApproval == null)
                    {
                        if (_locApplicationApprovalXPO != null)
                        {
                            if (_locApplicationApprovalXPO.Employee != null)
                            {
                                _locEmployee = _locApplicationApprovalXPO.Employee;
                            }

                            #region Approval Level 1
                            if (_locApplicationApprovalXPO.ApprovalLevel == ApprovalLevel.Level1)
                            {
                                bool _locActivationPosting = false;
                                bool _locLockApproval = false;

                                KpiSubmitApproval _locKpiSubmitApprovalXPO = _currentSession.FindObject<KpiSubmitApproval>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                     new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                if (_locKpiSubmitApprovalXPO == null)
                                {
                                    if (_locKpiSubmitXPO.ActivationPosting == false)
                                    {
                                        if (_locApplicationApprovalXPO.Lock == true)
                                        {
                                            _locActivationPosting = true;
                                            _locLockApproval = true;
                                        }
                                    }
                                    else
                                    {
                                        _locActivationPosting = _locKpiSubmitXPO.ActivationPosting;
                                    }

                                    _globFuncKS.SetKpiSubmitTaskBaseByApprovalInformationLevelForKpiSubmit(_currentSession, _locKpiSubmitXPO, _locObjectList, ApprovalLevel.Level2);
                                    _globFuncKS.SetActivationPostingKpiSubmitLine(_currentSession, _locKpiSubmitXPO, _locActivationPosting);


                                    #region SaveKpiSubmitApproval

                                    if (_locApplicationApprovalXPO.EndApproval == true)
                                    {

                                        KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                        {
                                            ApprovalDate = now,
                                            ApprovalStatus = Status.Approved,
                                            ApprovalLevel = ApprovalLevel.Level1,
                                            LockApproval = _locLockApproval,
                                            EndApproval = true,
                                            KpiSubmit = _locKpiSubmitXPO,
                                            ApprovedBy = _locEmployee,
                                        };
                                        _saveDataKSA.Save();
                                        _saveDataKSA.Session.CommitTransaction();

                                        #region Save to Kpi Monitoring

                                        KpiSubmit _locKpiSubmitOS = _locKpiSubmitXPO;
                                        Session _currSession = null;
                                        string _currObjectId = null;
                                        double _locJanuary = 0;
                                        double _locFebruary = 0;
                                        double _locMarch = 0;
                                        double _locApril = 0;
                                        double _locMay = 0;
                                        double _locJune = 0;
                                        double _locJuly = 0;
                                        double _locAugust = 0;
                                        double _locSeptember = 0;
                                        double _locOctober = 0;
                                        double _locNovember = 0;
                                        double _locDecember = 0;
                                        double _locytd = 0;
                                        double _locScoreYtd = 0;


                                        if (_locKpiSubmitOS != null)
                                        {
                                            if (_locKpiSubmitOS.Session != null)
                                            {
                                                _currSession = _locKpiSubmitOS.Session;
                                            }

                                            if (_locKpiSubmitOS.Code != null && _currSession != null)
                                            {
                                                _currObjectId = _locKpiSubmitOS.Code;
                                                KpiSubmit _locKpiSubmitXPO2 = _currSession.FindObject<KpiSubmit>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Code", _currObjectId),
                                                                                    new BinaryOperator("KpiSubmitStatus", Status.Progress)
                                                                                    ));
                                                if (_locKpiSubmitXPO2 != null)
                                                {
                                                    XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("KpiSubmit", _locKpiSubmitXPO2),
                                                                                    new BinaryOperator("KpiSubmitLineStatus", Status.Progress)
                                                                                    ));
                                                    if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                                                    {
                                                        foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                                                        {
                                                            #region update status Kpi Submit Line

                                                            if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Progress)
                                                            {
                                                                _locKpiSubmitLine.KpiSubmitLineStatus = Status.Approved;
                                                                _locKpiSubmitLine.Save();
                                                                _locKpiSubmitLine.Session.CommitTransaction();
                                                            }

                                                            #endregion update status Kpi Submit Line

                                                            #region Perhitungan berdasarkan KpiTypePeriod
                                                            XPCollection<KpiSubmitLine> _locKpiSubmitLines2 = new XPCollection<KpiSubmitLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Name", _locKpiSubmitLine.Name),
                                                                                new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear)
                                                                                ));
                                                            if (_locKpiSubmitLines2 != null && _locKpiSubmitLines2.Count() > 0)
                                                            {
                                                                foreach (KpiSubmitLine _locKpiSubmitLine2 in _locKpiSubmitLines2)
                                                                {

                                                                    #region Loc Pencapaian Per Bulan

                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.January)
                                                                    {
                                                                        _locJanuary = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.February)
                                                                    {
                                                                        _locFebruary = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.March)
                                                                    {
                                                                        _locMarch = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.April)
                                                                    {
                                                                        _locApril = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.May)
                                                                    {
                                                                        _locMay = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.June)
                                                                    {
                                                                        _locJune = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.July)
                                                                    {
                                                                        _locJuly = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.August)
                                                                    {
                                                                        _locAugust = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.September)
                                                                    {
                                                                        _locSeptember = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.October)
                                                                    {
                                                                        _locOctober = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.November)
                                                                    {
                                                                        _locNovember = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.December)
                                                                    {
                                                                        _locDecember = _locKpiSubmitLine2.Pencapaian;
                                                                    }

                                                                    #endregion Pencapaian Per Bulan

                                                                    #region Ytd Kpi Monitoring Type Sum All
                                                                    if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.SumAll)
                                                                    {
                                                                        _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember);
                                                                    }
                                                                    #endregion Ytd Kpi Monitoring Type Sum All

                                                                    KpiMonitoring _locKpiMonitoring = _currSession.FindObject<KpiMonitoring>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                            new BinaryOperator("Name", _locKpiSubmitLine2.Name),
                                                                                                                                            new BinaryOperator("KpiYear", _locKpiSubmitLine2.KpiYear),
                                                                                                                                            new BinaryOperator("DefaultPosition", _locKpiSubmitLine2.DefaultPosition),
                                                                                                                                            new BinaryOperator("Employee", _locKpiSubmitLine2.Employee)
                                                                                                                                            ));
                                                                    if (_locKpiMonitoring != null)
                                                                    {
                                                                        #region Ytd Kpi Monitoring Type Average
                                                                        int a = 0;
                                                                        double b = 0;
                                                                        double average = 0;

                                                                        if (_locJanuary > 0) { a = a + 1; b = b + _locJanuary; }
                                                                        if (_locFebruary > 0) { a = a + 1; b = b + _locFebruary; }
                                                                        if (_locMarch > 0) { a = a + 1; b = b + _locMarch; }
                                                                        if (_locApril > 0) { a = a + 1; b = b + _locApril; }
                                                                        if (_locMay > 0) { a = a + 1; b = b + _locMay; }
                                                                        if (_locJune > 0) { a = a + 1; b = b + _locJune; }
                                                                        if (_locJuly > 0) { a = a + 1; b = b + _locJuly; }
                                                                        if (_locAugust > 0) { a = a + 1; b = b + _locAugust; }
                                                                        if (_locSeptember > 0) { a = a + 1; b = b + _locSeptember; }
                                                                        if (_locOctober > 0) { a = a + 1; b = b + _locOctober; }
                                                                        if (_locNovember > 0) { a = a + 1; b = b + _locNovember; }
                                                                        if (_locDecember > 0) { a = a + 1; b = b + _locDecember; }

                                                                        average = b / a;
                                                                        _locytd = average;
                                                                        a = 0;
                                                                        b = 0;
                                                                        #endregion Ytd Kpi Monitoring Type Average

                                                                        #region Value Kpi Monitoring

                                                                        double _locScoreGrading = 0;

                                                                        XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                       new GroupOperator(GroupOperatorType.And,
                                                                                                                       new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                       new BinaryOperator("Active", true)),
                                                                                                                       new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                       );
                                                                        if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                        {

                                                                            foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                            {

                                                                                if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                {
                                                                                    if (_locytd > _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                {
                                                                                    if (_locytd <= _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                {
                                                                                    if (_locytd < _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                {
                                                                                    if (_locytd == _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                        #endregion Value Kpi Monitoring

                                                                        #region Score Kpi Monitoring

                                                                        _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine2.Proportion / 100);

                                                                        #endregion Score Kpi Monitoring

                                                                        #region Update Field

                                                                        _locKpiMonitoring.January = _locJanuary;
                                                                        _locKpiMonitoring.February = _locFebruary;
                                                                        _locKpiMonitoring.March = _locMarch;
                                                                        _locKpiMonitoring.April = _locApril;
                                                                        _locKpiMonitoring.May = _locMay;
                                                                        _locKpiMonitoring.June = _locJune;
                                                                        _locKpiMonitoring.July = _locJuly;
                                                                        _locKpiMonitoring.August = _locAugust;
                                                                        _locKpiMonitoring.September = _locSeptember;
                                                                        _locKpiMonitoring.October = _locOctober;
                                                                        _locKpiMonitoring.November = _locNovember;
                                                                        _locKpiMonitoring.December = _locDecember;
                                                                        _locKpiMonitoring.Ytd = _locytd;
                                                                        _locKpiMonitoring.Value = _locScoreGrading;
                                                                        _locKpiMonitoring.ScoreYtd = _locScoreYtd;
                                                                        _locKpiMonitoring.Save();
                                                                        _locKpiMonitoring.Session.CommitTransaction();

                                                                        #endregion Update Field

                                                                        #region Save to KPI Final Score

                                                                        XPCollection<KpiMonitoring> _locKpiMonitorings = new XPCollection<KpiMonitoring>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Employee", _locKpiSubmitXPO.Name),
                                                                               new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear)
                                                                               ));
                                                                        if (_locKpiMonitorings != null & _locKpiMonitorings.Count() > 0)
                                                                        {
                                                                            double score = 0;
                                                                            double totalScore = 0;

                                                                            foreach (KpiMonitoring _locKpiMonitoring2 in _locKpiMonitorings)
                                                                            {
                                                                                score = _locKpiMonitoring2.ScoreYtd;
                                                                            }


                                                                            totalScore = score + score;

                                                                            KpiFinalScore _locKpiFinalScore = _currSession.FindObject<KpiFinalScore>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                           new BinaryOperator("Employee", _locKpiSubmitXPO.Name)));
                                                                            if (_locKpiFinalScore != null)
                                                                            {
                                                                                _locKpiFinalScore.TotalScore = totalScore;
                                                                                _locKpiFinalScore.TotalFinalTarget = totalScore;
                                                                            }
                                                                            else
                                                                            {
                                                                                KpiFinalScore _saveKpiFinalScoreUpdate = new KpiFinalScore(_currSession)
                                                                                {
                                                                                    Code = "FS-",
                                                                                    TotalScore = totalScore,
                                                                                    TotalFinalTarget = totalScore,
                                                                                    Employee = _locKpiSubmitLine.Employee,
                                                                                };
                                                                                _saveKpiFinalScoreUpdate.Save();
                                                                                _saveKpiFinalScoreUpdate.Session.CommitTransaction();
                                                                            }

                                                                        }

                                                                        #endregion Save to KPI Final Score
                                                                    }
                                                                    else
                                                                    {
                                                                        #region Ytd from New Data
                                                                        _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember) / 1;

                                                                        #endregion Ytd from New Data

                                                                        #region Value Kpi Monitoring Awal Penginputan Data

                                                                        double _locScoreGrading = 0;

                                                                        XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                       new GroupOperator(GroupOperatorType.And,
                                                                                                                       new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                       new BinaryOperator("Active", true)),
                                                                                                                       new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                       );
                                                                        if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                        {

                                                                            foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                            {

                                                                                if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian > _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian <= _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian < _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian == _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                        #endregion Value Kpi Monitoring Awal Penginputan Data

                                                                        #region Score Kpi Monitoring Awal Penginputan Data

                                                                        _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine.Proportion / 100);

                                                                        #endregion Score Kpi Monitoring Awal Penginputan Data

                                                                        #region Add New Value in Field Kpi Monitoring

                                                                        KpiMonitoring _saveKpiMonitoring = new KpiMonitoring(_currSession)
                                                                        {
                                                                            Code = _locKpiSubmitLine.Code,
                                                                            KpiYear = _locKpiSubmitLine.KpiYear,
                                                                            Name = _locKpiSubmitLine.Name,
                                                                            ProportionKpi = _locKpiSubmitLine.Proportion,
                                                                            TargetKpi = _locKpiSubmitLine.KpiTarget,
                                                                            January = _locJanuary,
                                                                            February = _locFebruary,
                                                                            March = _locMarch,
                                                                            April = _locApril,
                                                                            May = _locMay,
                                                                            June = _locJune,
                                                                            July = _locJuly,
                                                                            August = _locAugust,
                                                                            September = _locSeptember,
                                                                            October = _locOctober,
                                                                            November = _locNovember,
                                                                            December = _locDecember,
                                                                            Ytd = _locytd,
                                                                            Value = _locScoreGrading,
                                                                            ScoreYtd = _locScoreYtd,
                                                                            KpiYtdType = _locKpiSubmitLine.KpiYtdType,
                                                                            Company = _locKpiSubmitLine.Company,
                                                                            OrgDim1 = _locKpiSubmitLine.OrgDim1,
                                                                            OrgDim2 = _locKpiSubmitLine.OrgDim2,
                                                                            OrgDim3 = _locKpiSubmitLine.OrgDim3,
                                                                            OrgDim4 = _locKpiSubmitLine.OrgDim4,
                                                                            DefaultPosition = _locKpiSubmitLine.DefaultPosition,
                                                                            Employee = _locKpiSubmitLine.Employee,
                                                                        };
                                                                        _saveKpiMonitoring.Save();
                                                                        _saveKpiMonitoring.Session.CommitTransaction();

                                                                        #endregion Add New Value in Field Kpi Monitoring

                                                                        #region Save to KPI Final Score

                                                                        XPCollection<KpiMonitoring> _locKpiMonitorings = new XPCollection<KpiMonitoring>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Employee", _locKpiSubmitXPO.Name),
                                                                               new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear)
                                                                               ));
                                                                        if(_locKpiMonitorings != null & _locKpiMonitorings.Count() > 0)
                                                                        {
                                                                            double score = 0;
                                                                            double totalScore = 0;

                                                                            foreach (KpiMonitoring _locKpiMonitoring2 in _locKpiMonitorings)
                                                                            {
                                                                                score = _locKpiMonitoring2.ScoreYtd;
                                                                            }


                                                                            totalScore = score + score;

                                                                            KpiFinalScore _locKpiFinalScore = _currSession.FindObject<KpiFinalScore>(new GroupOperator(GroupOperatorType.And,
                                                                                                              new BinaryOperator("Employee", _locKpiSubmitXPO.Name)));
                                                                            if(_locKpiFinalScore != null)
                                                                            {
                                                                                _locKpiFinalScore.TotalScore = totalScore;
                                                                                _locKpiFinalScore.TotalFinalTarget = totalScore;
                                                                            }
                                                                            else
                                                                            {
                                                                                KpiFinalScore _saveKpiFinalScore = new KpiFinalScore(_currSession)
                                                                                {
                                                                                    Code = "FS-",
                                                                                    TotalScore = totalScore,
                                                                                    TotalFinalTarget = totalScore,
                                                                                    Employee = _locKpiSubmitLine.Employee,
                                                                                };
                                                                                _saveKpiFinalScore.Save();
                                                                                _saveKpiFinalScore.Session.CommitTransaction();
                                                                            }
                                                                            
                                                                        }

                                                                        #endregion Save to KPI Final Score
                                                                    }
                                                                }


                                                            }

                                                            #endregion  Perhitungan berdasarkan KpiTypePeriod
                                                        }
                                                    }

                                                    #region SEND DATA KE FINAL GRADING

                                                    Employee _employeeKpiSubmit = _locKpiSubmitXPO.Name;
                                                    Company _company = _locKpiSubmitXPO.Company;

                                                    KpiGrading _locKpiGrading = _currSession.FindObject<KpiGrading>(new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("Employee", _employeeKpiSubmit)
                                                                                                                    ));
                                                    if (_locKpiGrading != null)
                                                    {
                                                        double TotNilaiAkhir = 0;
                                                        string _locScoreGrading = null;
                                                        KpiFinalScore _locKpiFinalScoreCheck = _currSession.FindObject<KpiFinalScore>(new GroupOperator(GroupOperatorType.And,
                                                                                                                          new BinaryOperator("JobPosition", _locKpiSubmitXPO.JobPosition),
                                                                                                                          new BinaryOperator("Employee", _locKpiSubmitXPO.Name)
                                                                                                                          ));
                                                        if (_locKpiFinalScoreCheck != null)
                                                        {
                                                            TotNilaiAkhir = _locKpiFinalScoreCheck.TotalFinalTarget;
                                                        }

                                                        _locKpiGrading.TotalNilaiAkhir = TotNilaiAkhir;
                                                        _locKpiGrading.Save();
                                                        _locKpiGrading.Session.CommitTransaction();

                                                        XPCollection<KpiFinalGradingSetting> _availableKpiFinalGradingSettings = new XPCollection<KpiFinalGradingSetting>(_currSession,
                                                                                              new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("Company", _locKpiGrading.Company),
                                                                                              new BinaryOperator("Active", true)),
                                                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                              );
                                                        if (_availableKpiFinalGradingSettings != null && _availableKpiFinalGradingSettings.Count() > 0)
                                                        {
                                                            foreach (KpiFinalGradingSetting _availableKpiFinalGradingSetting in _availableKpiFinalGradingSettings)
                                                            {
                                                                if (_availableKpiFinalGradingSetting.Operation == ">")
                                                                {
                                                                    if (_locKpiGrading.TotalNilaiAkhir > _availableKpiFinalGradingSetting.Value)
                                                                    {
                                                                        _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                                                                    }
                                                                }
                                                                if (_availableKpiFinalGradingSetting.Operation == "<=")
                                                                {
                                                                    if (_locKpiGrading.TotalNilaiAkhir <= _availableKpiFinalGradingSetting.Value)
                                                                    {
                                                                        _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                                                                    }
                                                                }
                                                                if (_availableKpiFinalGradingSetting.Operation == "<")
                                                                {
                                                                    if (_locKpiGrading.TotalNilaiAkhir < _availableKpiFinalGradingSetting.Value)
                                                                    {
                                                                        _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                                                                    }
                                                                }
                                                                if (_availableKpiFinalGradingSetting.Operation == "=")
                                                                {
                                                                    if (_locKpiGrading.TotalNilaiAkhir == _availableKpiFinalGradingSetting.Value)
                                                                    {
                                                                        _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                                                                    }
                                                                }
                                                                if (_availableKpiFinalGradingSetting.Operation == ">=")
                                                                {
                                                                    if (_locKpiGrading.TotalNilaiAkhir >= _availableKpiFinalGradingSetting.Value)
                                                                    {
                                                                        _locScoreGrading = _availableKpiFinalGradingSetting.Result;
                                                                    }
                                                                }
                                                            }
                                                            _locKpiGrading.FinalResult = _locScoreGrading;
                                                            _locScoreGrading = null;
                                                            _locKpiGrading.Save();
                                                            _locKpiGrading.Session.CommitTransaction();

                                                            if (_locKpiGrading.FinalResult != null)
                                                            {
                                                                string _result = _locKpiGrading.FinalResult;
                                                                KpiFinalGradingSetting _locKpiFinalPredicateCheck = _currSession.FindObject<KpiFinalGradingSetting>(new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("Result", _result)));
                                                                if (_locKpiFinalPredicateCheck != null)
                                                                {
                                                                    _locKpiGrading.FinalPredicate = _locKpiFinalPredicateCheck.Predicate;
                                                                    _locKpiGrading.Save();
                                                                    _locKpiGrading.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        string _locScoreGrading2 = null;
                                                        string _locFinalPredicate2 = null;


                                                        KpiFinalScore _locKpiFinalScoreCheck = _currSession.FindObject<KpiFinalScore>(new GroupOperator(GroupOperatorType.And,
                                                                                                                          new BinaryOperator("JobPosition", _locKpiSubmitXPO.JobPosition),
                                                                                                                          new BinaryOperator("Employee", _locKpiSubmitXPO.Name)
                                                                                                                          ));
                                                        if (_locKpiFinalScoreCheck != null)
                                                        {

                                                            XPCollection<KpiFinalGradingSetting> _availableKpiFinalGradingSetting2s = new XPCollection<KpiFinalGradingSetting>(_currSession,
                                                                                             new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Company", _locKpiSubmitXPO.Company),
                                                                                             new BinaryOperator("Active", true)),
                                                                                             new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                             );
                                                            if (_availableKpiFinalGradingSetting2s != null && _availableKpiFinalGradingSetting2s.Count() > 0)
                                                            {
                                                                foreach (KpiFinalGradingSetting _availableKpiFinalGradingSetting2 in _availableKpiFinalGradingSetting2s)
                                                                {
                                                                    if (_availableKpiFinalGradingSetting2.Operation == ">")
                                                                    {
                                                                        if (_locKpiFinalScoreCheck.TotalFinalTarget > _availableKpiFinalGradingSetting2.Value)
                                                                        {
                                                                            _locScoreGrading2 = _availableKpiFinalGradingSetting2.Result;
                                                                        }
                                                                    }
                                                                    if (_availableKpiFinalGradingSetting2.Operation == "<=")
                                                                    {
                                                                        if (_locKpiFinalScoreCheck.TotalFinalTarget <= _availableKpiFinalGradingSetting2.Value)
                                                                        {
                                                                            _locScoreGrading2 = _availableKpiFinalGradingSetting2.Result;
                                                                        }
                                                                    }
                                                                    if (_availableKpiFinalGradingSetting2.Operation == "<")
                                                                    {
                                                                        if (_locKpiFinalScoreCheck.TotalFinalTarget < _availableKpiFinalGradingSetting2.Value)
                                                                        {
                                                                            _locScoreGrading2 = _availableKpiFinalGradingSetting2.Result;
                                                                        }
                                                                    }
                                                                    if (_availableKpiFinalGradingSetting2.Operation == "=")
                                                                    {
                                                                        if (_locKpiFinalScoreCheck.TotalFinalTarget == _availableKpiFinalGradingSetting2.Value)
                                                                        {
                                                                            _locScoreGrading2 = _availableKpiFinalGradingSetting2.Result;
                                                                        }
                                                                    }
                                                                    if (_availableKpiFinalGradingSetting2.Operation == ">=")
                                                                    {
                                                                        if (_locKpiFinalScoreCheck.TotalFinalTarget >= _availableKpiFinalGradingSetting2.Value)
                                                                        {
                                                                            _locScoreGrading2 = _availableKpiFinalGradingSetting2.Result;
                                                                        }
                                                                    }
                                                                }

                                                                if (_locScoreGrading2 != null)
                                                                {
                                                                    string _result = _locScoreGrading2;
                                                                    KpiFinalGradingSetting _locKpiFinalPredicateCheck2 = _currSession.FindObject<KpiFinalGradingSetting>(new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("Result", _result)));
                                                                    if (_locKpiFinalPredicateCheck2 != null)
                                                                    {
                                                                        _locFinalPredicate2 = _locKpiFinalPredicateCheck2.Predicate;
                                                                    }
                                                                }
                                                            }

                                                            KpiGrading _saveKpiGrading = new KpiGrading(_currSession)
                                                            {
                                                                Code = "FG-",
                                                                TotalNilaiAkhir = _locKpiFinalScoreCheck.TotalFinalTarget,
                                                                FinalResult = _locScoreGrading2,
                                                                FinalPredicate = _locFinalPredicate2,
                                                                Employee = _locKpiSubmitXPO.Name,
                                                                Company = _locKpiSubmitXPO.Company,
                                                                OrgDim1 = _locKpiSubmitXPO.OrgDim1,
                                                                OrgDim2 = _locKpiSubmitXPO.OrgDim2,
                                                                OrgDim3 = _locKpiSubmitXPO.OrgDim3,
                                                                OrgDim4 = _locKpiSubmitXPO.OrgDim4,
                                                                JobPosition = _locKpiSubmitXPO.JobPosition,
                                                            };
                                                            _locScoreGrading2 = null;
                                                            _saveKpiGrading.Save();
                                                            _saveKpiGrading.Session.CommitTransaction();
                                                        }




                                                    }

                                                    #endregion SEND DATA KE FINAL GRADING

                                                    _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                                    _locKpiSubmitXPO.Save();
                                                    _locKpiSubmitXPO.Session.CommitTransaction();


                                                    //Message
                                                    SuccessMessageShow("Kpi Submit has successfully updates to Approved");
                                                }
                                                else
                                                {
                                                    //Message error
                                                    ErrorMessageShow("Data Kpi Submit Not Available");
                                                }

                                            }
                                            else
                                            {
                                                //Message Error
                                                ErrorMessageShow("Data Kpi Submit Order Not Available");
                                            }

                                        }


                                        #endregion Save To Kpi Monitoring

                                        #region Sending Mail
                                        _locEmployeeCode = _locKpiSubmitXPO.Name.Code;
                                        Employee _locEmployeeMail = _currSession.FindObject<Employee>
                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("Code", _locEmployeeCode),
                                                                 new BinaryOperator("Active", true)
                                                                 ));
                                        if (_locEmployeeMail != null)
                                        {
                                            #region Email
                                            ApplicationMailSetting _locApplicationMailSetting = _globFunc.GetApplicationMailSetup(_currSession, _locCompany);
                                            if (_locApplicationMailSetting != null)
                                            {
                                                _locSubjectMail = "Persetujuan Pengajuan Kpi ";
                                                _locMailTo = _locEmployeeMail.Email;
                                                _locBody = _globFunc.BackgroundBodyMailKpiApprove();
                                                _globFunc.SetAndSendMail(_locApplicationMailSetting.SmtpHost, _locApplicationMailSetting.SmtpPort, _locApplicationMailSetting.MailFrom, _locApplicationMailSetting.MailFromPassword, _locMailTo, _locSubjectMail, _locBody);
                                            }
                                            #endregion Email
                                        }
                                        #endregion Sending Mail

                                    }
                                    else
                                    {
                                        KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                        {
                                            ApprovalDate = now,
                                            ApprovalStatus = Status.Approved,
                                            ApprovalLevel = ApprovalLevel.Level1,
                                            LockApproval = _locLockApproval,
                                            KpiSubmit = _locKpiSubmitXPO,
                                            ApprovedBy = _locEmployee,
                                        };
                                        _saveDataKSA.Save();
                                        _saveDataKSA.Session.CommitTransaction();

                                    }

                                    #endregion SaveKpiSubmitApproval

                                    _locKpiSubmitXPO.ActivationPosting = _locActivationPosting;
                                    _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                    _locKpiSubmitXPO.StatusDate = now;
                                    _locKpiSubmitXPO.Save();
                                    _locKpiSubmitXPO.Session.CommitTransaction();

                                    if (_locApplicationApprovalXPO.Posting == true)
                                    {
                                        if (_locKpiSubmitXPO.ActivationPosting == true && _locKpiSubmitXPO.KpiSubmitStatus == Status.Approved)
                                        {
                                            //_globFuncPO.SetPostingProcessPO(_currentSession, _locPurchaseOrderXPO);
                                        }
                                    }

                                    SuccessMessageShow("KPI has successfully Approve");
                                }
                            }
                            #endregion Approval Level 1

                            #region Approval Level 2
                            if (_locApplicationApprovalXPO.ApprovalLevel == ApprovalLevel.Level2)
                            {
                                bool _locActivationPosting = false;
                                bool _locLockApproval = false;


                                KpiSubmitApproval _locKpiSubmitApprovalXPO = _currentSession.FindObject<KpiSubmitApproval>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                     new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                if (_locKpiSubmitApprovalXPO == null)
                                {
                                    if (_locKpiSubmitXPO.ActivationPosting == false)
                                    {
                                        if (_locApplicationApprovalXPO.Lock == true)
                                        {
                                            _locActivationPosting = true;
                                            _locLockApproval = true;
                                        }
                                    }
                                    else
                                    {
                                        _locActivationPosting = _locKpiSubmitXPO.ActivationPosting;
                                    }

                                    _globFuncKS.SetKpiSubmitTaskBaseByApprovalInformationLevelForKpiSubmit(_currentSession, _locKpiSubmitXPO, _locObjectList, ApprovalLevel.Level3);
                                    _globFuncKS.SetActivationPostingKpiSubmitLine(_currentSession, _locKpiSubmitXPO, _locActivationPosting);

                                    #region SaveKpiSubmitApproval

                                    if (_locApplicationApprovalXPO.EndApproval == true)
                                    {
                                        KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                        {
                                            ApprovalDate = now,
                                            ApprovalStatus = Status.Approved,
                                            ApprovalLevel = ApprovalLevel.Level2,
                                            LockApproval = _locLockApproval,
                                            EndApproval = true,
                                            KpiSubmit = _locKpiSubmitXPO,
                                            ApprovedBy = _locEmployee,
                                        };
                                        _saveDataKSA.Save();
                                        _saveDataKSA.Session.CommitTransaction();


                                        #region Save to Kpi Monitoring

                                        KpiSubmit _locKpiSubmitOS = _locKpiSubmitXPO;

                                        Session _currSession = null;
                                        string _currObjectId = null;
                                        double _locJanuary = 0;
                                        double _locFebruary = 0;
                                        double _locMarch = 0;
                                        double _locApril = 0;
                                        double _locMay = 0;
                                        double _locJune = 0;
                                        double _locJuly = 0;
                                        double _locAugust = 0;
                                        double _locSeptember = 0;
                                        double _locOctober = 0;
                                        double _locNovember = 0;
                                        double _locDecember = 0;
                                        double _locytd = 0;
                                        double _locScoreYtd = 0;


                                        if (_locKpiSubmitOS != null)
                                        {
                                            if (_locKpiSubmitOS.Session != null)
                                            {
                                                _currSession = _locKpiSubmitOS.Session;
                                            }

                                            if (_locKpiSubmitOS.Code != null && _currSession != null)
                                            {
                                                _currObjectId = _locKpiSubmitOS.Code;
                                                KpiSubmit _locKpiSubmitXPO2 = _currSession.FindObject<KpiSubmit>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Code", _currObjectId),
                                                                                    new BinaryOperator("KpiSubmitStatus", Status.Progress)
                                                                                    ));
                                                if (_locKpiSubmitXPO2 != null)
                                                {
                                                    XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("KpiSubmit", _locKpiSubmitXPO2),
                                                                                    new BinaryOperator("KpiSubmitLineStatus", Status.Progress)
                                                                                    ));
                                                    if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                                                    {
                                                        foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                                                        {
                                                            #region update status Kpi Submit Line

                                                            if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Progress)
                                                            {
                                                                _locKpiSubmitLine.KpiSubmitLineStatus = Status.Approved;
                                                                _locKpiSubmitLine.Save();
                                                                _locKpiSubmitLine.Session.CommitTransaction();
                                                            }

                                                            #endregion update status Kpi Submit Line

                                                            #region Perhitungan berdasarkan KpiTypePeriod
                                                            XPCollection<KpiSubmitLine> _locKpiSubmitLines2 = new XPCollection<KpiSubmitLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Name", _locKpiSubmitLine.Name),
                                                                                new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear)
                                                                                ));
                                                            if (_locKpiSubmitLines2 != null && _locKpiSubmitLines2.Count() > 0)
                                                            {
                                                                foreach (KpiSubmitLine _locKpiSubmitLine2 in _locKpiSubmitLines2)
                                                                {

                                                                    #region Loc Pencapaian Per Bulan

                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.January)
                                                                    {
                                                                        _locJanuary = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.February)
                                                                    {
                                                                        _locFebruary = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.March)
                                                                    {
                                                                        _locMarch = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.April)
                                                                    {
                                                                        _locApril = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.May)
                                                                    {
                                                                        _locMay = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.June)
                                                                    {
                                                                        _locJune = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.July)
                                                                    {
                                                                        _locJuly = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.August)
                                                                    {
                                                                        _locAugust = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.September)
                                                                    {
                                                                        _locSeptember = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.October)
                                                                    {
                                                                        _locOctober = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.November)
                                                                    {
                                                                        _locNovember = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.December)
                                                                    {
                                                                        _locDecember = _locKpiSubmitLine2.Pencapaian;
                                                                    }

                                                                    #endregion Pencapaian Per Bulan

                                                                    #region Ytd Kpi Monitoring Type Sum All
                                                                    if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.SumAll)
                                                                    {
                                                                        _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember);
                                                                    }
                                                                    #endregion Ytd Kpi Monitoring Type Sum All

                                                                    KpiMonitoring _locKpiMonitoring = _currSession.FindObject<KpiMonitoring>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                            new BinaryOperator("Name", _locKpiSubmitLine2.Name),
                                                                                                                                            new BinaryOperator("KpiYear", _locKpiSubmitLine2.KpiYear),
                                                                                                                                            new BinaryOperator("DefaultPosition", _locKpiSubmitLine2.DefaultPosition),
                                                                                                                                            new BinaryOperator("Employee", _locKpiSubmitLine2.Employee)
                                                                                                                                            ));
                                                                    if (_locKpiMonitoring != null)
                                                                    {
                                                                        #region Ytd Kpi Monitoring Type Average
                                                                        int a = 0;
                                                                        double b = 0;
                                                                        double average = 0;

                                                                        if (_locJanuary > 0) { a = a + 1; b = b + _locJanuary; }
                                                                        if (_locFebruary > 0) { a = a + 1; b = b + _locFebruary; }
                                                                        if (_locMarch > 0) { a = a + 1; b = b + _locMarch; }
                                                                        if (_locApril > 0) { a = a + 1; b = b + _locApril; }
                                                                        if (_locMay > 0) { a = a + 1; b = b + _locMay; }
                                                                        if (_locJune > 0) { a = a + 1; b = b + _locJune; }
                                                                        if (_locJuly > 0) { a = a + 1; b = b + _locJuly; }
                                                                        if (_locAugust > 0) { a = a + 1; b = b + _locAugust; }
                                                                        if (_locSeptember > 0) { a = a + 1; b = b + _locSeptember; }
                                                                        if (_locOctober > 0) { a = a + 1; b = b + _locOctober; }
                                                                        if (_locNovember > 0) { a = a + 1; b = b + _locNovember; }
                                                                        if (_locDecember > 0) { a = a + 1; b = b + _locDecember; }

                                                                        average = b / a;
                                                                        _locytd = average;
                                                                        a = 0;
                                                                        b = 0;
                                                                        #endregion Ytd Kpi Monitoring Type Average

                                                                        #region Value Kpi Monitoring

                                                                        double _locScoreGrading = 0;

                                                                        XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                       new GroupOperator(GroupOperatorType.And,
                                                                                                                       new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                       new BinaryOperator("Active", true)),
                                                                                                                       new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                       );
                                                                        if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                        {

                                                                            foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                            {

                                                                                if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                {
                                                                                    if (_locytd > _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                {
                                                                                    if (_locytd <= _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                {
                                                                                    if (_locytd < _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                {
                                                                                    if (_locytd == _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                        #endregion Value Kpi Monitoring

                                                                        #region Score Kpi Monitoring

                                                                        _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine2.KpiTarget / 100);

                                                                        #endregion Score Kpi Monitoring

                                                                        #region Update Field

                                                                        _locKpiMonitoring.January = _locJanuary;
                                                                        _locKpiMonitoring.February = _locFebruary;
                                                                        _locKpiMonitoring.March = _locMarch;
                                                                        _locKpiMonitoring.April = _locApril;
                                                                        _locKpiMonitoring.May = _locMay;
                                                                        _locKpiMonitoring.June = _locJune;
                                                                        _locKpiMonitoring.July = _locJuly;
                                                                        _locKpiMonitoring.August = _locAugust;
                                                                        _locKpiMonitoring.September = _locSeptember;
                                                                        _locKpiMonitoring.October = _locOctober;
                                                                        _locKpiMonitoring.November = _locNovember;
                                                                        _locKpiMonitoring.December = _locDecember;
                                                                        _locKpiMonitoring.Ytd = _locytd;
                                                                        _locKpiMonitoring.Value = _locScoreGrading;
                                                                        _locKpiMonitoring.ScoreYtd = _locScoreYtd;
                                                                        _locKpiMonitoring.Save();
                                                                        _locKpiMonitoring.Session.CommitTransaction();

                                                                        #endregion Update Field
                                                                    }
                                                                    else
                                                                    {
                                                                        #region Ytd from New Data
                                                                        _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember) / 1;

                                                                        #endregion Ytd from New Data

                                                                        #region Value Kpi Monitoring Awal Penginputan Data

                                                                        double _locScoreGrading = 0;

                                                                        XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                       new GroupOperator(GroupOperatorType.And,
                                                                                                                       new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                       new BinaryOperator("Active", true)),
                                                                                                                       new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                       );
                                                                        if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                        {

                                                                            foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                            {

                                                                                if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian > _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian <= _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian < _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian == _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                        #endregion Value Kpi Monitoring Awal Penginputan Data

                                                                        #region Score Kpi Monitoring Awal Penginputan Data

                                                                        _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine.KpiTarget / 100);

                                                                        #endregion Score Kpi Monitoring Awal Penginputan Data

                                                                        #region Add New Value in Field Kpi Monitoring

                                                                        KpiMonitoring _saveKpiMonitoring = new KpiMonitoring(_currSession)
                                                                        {
                                                                            Code = _locKpiSubmitLine.Code,
                                                                            KpiYear = _locKpiSubmitLine.KpiYear,
                                                                            Name = _locKpiSubmitLine.Name,
                                                                            ProportionKpi = _locKpiSubmitLine.Proportion,
                                                                            TargetKpi = _locKpiSubmitLine.KpiTarget,
                                                                            January = _locJanuary,
                                                                            February = _locFebruary,
                                                                            March = _locMarch,
                                                                            April = _locApril,
                                                                            May = _locMay,
                                                                            June = _locJune,
                                                                            July = _locJuly,
                                                                            August = _locAugust,
                                                                            September = _locSeptember,
                                                                            October = _locOctober,
                                                                            November = _locNovember,
                                                                            December = _locDecember,
                                                                            Ytd = _locytd,
                                                                            Value = _locScoreGrading,
                                                                            ScoreYtd = _locScoreYtd,
                                                                            KpiYtdType = _locKpiSubmitLine.KpiYtdType,
                                                                            Company = _locKpiSubmitLine.Company,
                                                                            OrgDim1 = _locKpiSubmitLine.OrgDim1,
                                                                            OrgDim2 = _locKpiSubmitLine.OrgDim2,
                                                                            OrgDim3 = _locKpiSubmitLine.OrgDim3,
                                                                            OrgDim4 = _locKpiSubmitLine.OrgDim4,
                                                                            DefaultPosition = _locKpiSubmitLine.DefaultPosition,
                                                                            Employee = _locKpiSubmitLine.Employee,
                                                                        };
                                                                        _saveKpiMonitoring.Save();
                                                                        _saveKpiMonitoring.Session.CommitTransaction();

                                                                        #endregion Add New Value in Field Kpi Monitoring
                                                                    }
                                                                }


                                                            }

                                                            #endregion  Perhitungan berdasarkan KpiTypePeriod
                                                        }
                                                    }
                                                    _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                                    _locKpiSubmitXPO.Save();
                                                    _locKpiSubmitXPO.Session.CommitTransaction();


                                                    //Message
                                                    SuccessMessageShow("Kpi Submit has successfully updates to Approved");
                                                }
                                                else
                                                {
                                                    //Message error
                                                    ErrorMessageShow("Data Kpi Submit Not Available");
                                                }

                                            }
                                            else
                                            {
                                                //Message Error
                                                ErrorMessageShow("Data Kpi Submit Order Not Available");
                                            }

                                        }


                                        #endregion Save To Kpi Monitoring
                                    }
                                    else
                                    {
                                        KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                        {
                                            ApprovalDate = now,
                                            ApprovalStatus = Status.Approved,
                                            ApprovalLevel = ApprovalLevel.Level2,
                                            LockApproval = _locLockApproval,
                                            KpiSubmit = _locKpiSubmitXPO,
                                            ApprovedBy = _locEmployee,
                                        };
                                        _saveDataKSA.Save();
                                        _saveDataKSA.Session.CommitTransaction();

                                        _globFuncKS.SetKpiSubmitApproval(_currentSession, _locKpiSubmitXPO, ApprovalLevel.Level1);


                                    }

                                    #endregion SaveKpiSubmitApproval

                                    _locKpiSubmitXPO.ActivationPosting = _locActivationPosting;
                                    _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                    _locKpiSubmitXPO.StatusDate = now;
                                    _locKpiSubmitXPO.Save();
                                    _locKpiSubmitXPO.Session.CommitTransaction();

                                    if (_locApplicationApprovalXPO.Posting == true)
                                    {
                                        if (_locKpiSubmitXPO.ActivationPosting == true && _locKpiSubmitXPO.KpiSubmitStatus == Status.Approved)
                                        {
                                            //_globFuncPO.SetPostingProcessPO(_currentSession, _locPurchaseOrderXPO);
                                        }
                                    }

                                    SuccessMessageShow("Purchase Order has successfully Approve");
                                }
                            }
                            #endregion Approval Level 2

                            #region Approval Level 3
                            if (_locApplicationApprovalXPO.ApprovalLevel == ApprovalLevel.Level3)
                            {
                                bool _locActivationPosting = false;
                                bool _locLockApproval = false;
                                bool _locActiveApproved3 = false;


                                KpiSubmitApproval _locKpiSubmitApprovalXPO = _currentSession.FindObject<KpiSubmitApproval>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("KpiSubmit", _locKpiSubmitXPO),
                                                                     new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                if (_locKpiSubmitApprovalXPO == null)
                                {
                                    if (_locKpiSubmitXPO.ActivationPosting == false)
                                    {
                                        if (_locApplicationApprovalXPO.Lock == true)
                                        {
                                            _locActivationPosting = true;
                                        }
                                    }
                                    else
                                    {
                                        _locActivationPosting = _locKpiSubmitXPO.ActivationPosting;
                                    }

                                    _globFuncKS.SetActivationPostingKpiSubmitLine(_currentSession, _locKpiSubmitXPO, _locActivationPosting);


                                    #region SaveKpiSubmitApproval

                                    if (_locApplicationApprovalXPO.EndApproval == true)
                                    {
                                        KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                        {
                                            ApprovalDate = now,
                                            ApprovalStatus = Status.Approved,
                                            ApprovalLevel = ApprovalLevel.Level3,
                                            LockApproval = _locLockApproval,
                                            EndApproval = true,
                                            KpiSubmit = _locKpiSubmitXPO,
                                            ApprovedBy = _locEmployee,
                                        };
                                        _saveDataKSA.Save();
                                        _saveDataKSA.Session.CommitTransaction();

                                        #region Save to Kpi Monitoring

                                        KpiSubmit _locKpiSubmitOS = _locKpiSubmitXPO;

                                        Session _currSession = null;
                                        string _currObjectId = null;
                                        double _locJanuary = 0;
                                        double _locFebruary = 0;
                                        double _locMarch = 0;
                                        double _locApril = 0;
                                        double _locMay = 0;
                                        double _locJune = 0;
                                        double _locJuly = 0;
                                        double _locAugust = 0;
                                        double _locSeptember = 0;
                                        double _locOctober = 0;
                                        double _locNovember = 0;
                                        double _locDecember = 0;
                                        double _locytd = 0;
                                        double _locScoreYtd = 0;


                                        if (_locKpiSubmitOS != null)
                                        {
                                            if (_locKpiSubmitOS.Session != null)
                                            {
                                                _currSession = _locKpiSubmitOS.Session;
                                            }

                                            if (_locKpiSubmitOS.Code != null && _currSession != null)
                                            {
                                                _currObjectId = _locKpiSubmitOS.Code;
                                                KpiSubmit _locKpiSubmitXPO2 = _currSession.FindObject<KpiSubmit>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Code", _currObjectId),
                                                                                    new BinaryOperator("KpiSubmitStatus", Status.Progress)
                                                                                    ));
                                                if (_locKpiSubmitXPO2 != null)
                                                {
                                                    XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("KpiSubmit", _locKpiSubmitXPO2),
                                                                                    new BinaryOperator("KpiSubmitLineStatus", Status.Progress)
                                                                                    ));
                                                    if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                                                    {
                                                        foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                                                        {
                                                            #region update status Kpi Submit Line

                                                            if (_locKpiSubmitLine.KpiSubmitLineStatus == Status.Progress)
                                                            {
                                                                _locKpiSubmitLine.KpiSubmitLineStatus = Status.Approved;
                                                                _locKpiSubmitLine.Save();
                                                                _locKpiSubmitLine.Session.CommitTransaction();
                                                            }

                                                            #endregion update status Kpi Submit Line

                                                            #region Perhitungan berdasarkan KpiTypePeriod
                                                            XPCollection<KpiSubmitLine> _locKpiSubmitLines2 = new XPCollection<KpiSubmitLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Name", _locKpiSubmitLine.Name),
                                                                                new BinaryOperator("KpiYear", _locKpiSubmitXPO.KpiSubmitYear)
                                                                                ));
                                                            if (_locKpiSubmitLines2 != null && _locKpiSubmitLines2.Count() > 0)
                                                            {
                                                                foreach (KpiSubmitLine _locKpiSubmitLine2 in _locKpiSubmitLines2)
                                                                {

                                                                    #region Loc Pencapaian Per Bulan

                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.January)
                                                                    {
                                                                        _locJanuary = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.February)
                                                                    {
                                                                        _locFebruary = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.March)
                                                                    {
                                                                        _locMarch = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.April)
                                                                    {
                                                                        _locApril = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.May)
                                                                    {
                                                                        _locMay = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.June)
                                                                    {
                                                                        _locJune = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.July)
                                                                    {
                                                                        _locJuly = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.August)
                                                                    {
                                                                        _locAugust = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.September)
                                                                    {
                                                                        _locSeptember = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.October)
                                                                    {
                                                                        _locOctober = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.November)
                                                                    {
                                                                        _locNovember = _locKpiSubmitLine2.Pencapaian;
                                                                    }
                                                                    if (_locKpiSubmitLine2.MonthKpi == Month.December)
                                                                    {
                                                                        _locDecember = _locKpiSubmitLine2.Pencapaian;
                                                                    }

                                                                    #endregion Pencapaian Per Bulan

                                                                    #region Ytd Kpi Monitoring Type Sum All
                                                                    if (_locKpiSubmitLine2.KpiYtdType == KpiYtdType.SumAll)
                                                                    {
                                                                        _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember);
                                                                    }
                                                                    #endregion Ytd Kpi Monitoring Type Sum All

                                                                    KpiMonitoring _locKpiMonitoring = _currSession.FindObject<KpiMonitoring>(new GroupOperator(GroupOperatorType.And,
                                                                                                                                            new BinaryOperator("Name", _locKpiSubmitLine2.Name),
                                                                                                                                            new BinaryOperator("KpiYear", _locKpiSubmitLine2.KpiYear),
                                                                                                                                            new BinaryOperator("DefaultPosition", _locKpiSubmitLine2.DefaultPosition),
                                                                                                                                            new BinaryOperator("Employee", _locKpiSubmitLine2.Employee)
                                                                                                                                            ));
                                                                    if (_locKpiMonitoring != null)
                                                                    {
                                                                        #region Ytd Kpi Monitoring Type Average
                                                                        int a = 0;
                                                                        double b = 0;
                                                                        double average = 0;

                                                                        if (_locJanuary > 0) { a = a + 1; b = b + _locJanuary; }
                                                                        if (_locFebruary > 0) { a = a + 1; b = b + _locFebruary; }
                                                                        if (_locMarch > 0) { a = a + 1; b = b + _locMarch; }
                                                                        if (_locApril > 0) { a = a + 1; b = b + _locApril; }
                                                                        if (_locMay > 0) { a = a + 1; b = b + _locMay; }
                                                                        if (_locJune > 0) { a = a + 1; b = b + _locJune; }
                                                                        if (_locJuly > 0) { a = a + 1; b = b + _locJuly; }
                                                                        if (_locAugust > 0) { a = a + 1; b = b + _locAugust; }
                                                                        if (_locSeptember > 0) { a = a + 1; b = b + _locSeptember; }
                                                                        if (_locOctober > 0) { a = a + 1; b = b + _locOctober; }
                                                                        if (_locNovember > 0) { a = a + 1; b = b + _locNovember; }
                                                                        if (_locDecember > 0) { a = a + 1; b = b + _locDecember; }

                                                                        average = b / a;
                                                                        _locytd = average;
                                                                        a = 0;
                                                                        b = 0;
                                                                        #endregion Ytd Kpi Monitoring Type Average

                                                                        #region Value Kpi Monitoring

                                                                        double _locScoreGrading = 0;

                                                                        XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                       new GroupOperator(GroupOperatorType.And,
                                                                                                                       new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                       new BinaryOperator("Active", true)),
                                                                                                                       new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                       );
                                                                        if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                        {

                                                                            foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                            {

                                                                                if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                {
                                                                                    if (_locytd > _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                {
                                                                                    if (_locytd <= _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                {
                                                                                    if (_locytd < _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                {
                                                                                    if (_locytd == _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                        #endregion Value Kpi Monitoring

                                                                        #region Score Kpi Monitoring

                                                                        _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine2.KpiTarget / 100);

                                                                        #endregion Score Kpi Monitoring

                                                                        #region Update Field

                                                                        _locKpiMonitoring.January = _locJanuary;
                                                                        _locKpiMonitoring.February = _locFebruary;
                                                                        _locKpiMonitoring.March = _locMarch;
                                                                        _locKpiMonitoring.April = _locApril;
                                                                        _locKpiMonitoring.May = _locMay;
                                                                        _locKpiMonitoring.June = _locJune;
                                                                        _locKpiMonitoring.July = _locJuly;
                                                                        _locKpiMonitoring.August = _locAugust;
                                                                        _locKpiMonitoring.September = _locSeptember;
                                                                        _locKpiMonitoring.October = _locOctober;
                                                                        _locKpiMonitoring.November = _locNovember;
                                                                        _locKpiMonitoring.December = _locDecember;
                                                                        _locKpiMonitoring.Ytd = _locytd;
                                                                        _locKpiMonitoring.Value = _locScoreGrading;
                                                                        _locKpiMonitoring.ScoreYtd = _locScoreYtd;
                                                                        _locKpiMonitoring.Save();
                                                                        _locKpiMonitoring.Session.CommitTransaction();

                                                                        #endregion Update Field
                                                                    }
                                                                    else
                                                                    {
                                                                        #region Ytd from New Data
                                                                        _locytd = (_locJanuary + _locFebruary + _locMarch + _locApril + _locMay + _locJune + _locJuly + _locAugust + _locSeptember + _locOctober + _locNovember + _locDecember) / 1;

                                                                        #endregion Ytd from New Data

                                                                        #region Value Kpi Monitoring Awal Penginputan Data

                                                                        double _locScoreGrading = 0;

                                                                        XPCollection<KpiRoleGrading> _availableKpiRoleGradings = new XPCollection<KpiRoleGrading>(_currSession,
                                                                                                                       new GroupOperator(GroupOperatorType.And,
                                                                                                                       new BinaryOperator("KpiRoleLine", _locKpiSubmitLine2.Name),
                                                                                                                       new BinaryOperator("Active", true)),
                                                                                                                       new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending)
                                                                                                                       );
                                                                        if (_availableKpiRoleGradings != null && _availableKpiRoleGradings.Count() > 0)
                                                                        {

                                                                            foreach (KpiRoleGrading _availableKpiRoleGrading in _availableKpiRoleGradings)
                                                                            {

                                                                                if (_availableKpiRoleGrading.OperationGrading == ">")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian > _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<=")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian <= _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "<")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian < _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                                if (_availableKpiRoleGrading.OperationGrading == "=")
                                                                                {
                                                                                    if (_locKpiSubmitLine2.Pencapaian == _availableKpiRoleGrading.ValueGrading)
                                                                                    {
                                                                                        _locScoreGrading = _availableKpiRoleGrading.ScoreGrading;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                        #endregion Value Kpi Monitoring Awal Penginputan Data

                                                                        #region Score Kpi Monitoring Awal Penginputan Data

                                                                        _locScoreYtd = _locScoreGrading * (_locKpiSubmitLine.KpiTarget / 100);

                                                                        #endregion Score Kpi Monitoring Awal Penginputan Data

                                                                        #region Add New Value in Field Kpi Monitoring

                                                                        KpiMonitoring _saveKpiMonitoring = new KpiMonitoring(_currSession)
                                                                        {
                                                                            Code = _locKpiSubmitLine.Code,
                                                                            KpiYear = _locKpiSubmitLine.KpiYear,
                                                                            Name = _locKpiSubmitLine.Name,
                                                                            ProportionKpi = _locKpiSubmitLine.Proportion,
                                                                            TargetKpi = _locKpiSubmitLine.KpiTarget,
                                                                            January = _locJanuary,
                                                                            February = _locFebruary,
                                                                            March = _locMarch,
                                                                            April = _locApril,
                                                                            May = _locMay,
                                                                            June = _locJune,
                                                                            July = _locJuly,
                                                                            August = _locAugust,
                                                                            September = _locSeptember,
                                                                            October = _locOctober,
                                                                            November = _locNovember,
                                                                            December = _locDecember,
                                                                            Ytd = _locytd,
                                                                            Value = _locScoreGrading,
                                                                            ScoreYtd = _locScoreYtd,
                                                                            KpiYtdType = _locKpiSubmitLine.KpiYtdType,
                                                                            Company = _locKpiSubmitLine.Company,
                                                                            OrgDim1 = _locKpiSubmitLine.OrgDim1,
                                                                            OrgDim2 = _locKpiSubmitLine.OrgDim2,
                                                                            OrgDim3 = _locKpiSubmitLine.OrgDim3,
                                                                            OrgDim4 = _locKpiSubmitLine.OrgDim4,
                                                                            DefaultPosition = _locKpiSubmitLine.DefaultPosition,
                                                                            Employee = _locKpiSubmitLine.Employee,
                                                                        };
                                                                        _saveKpiMonitoring.Save();
                                                                        _saveKpiMonitoring.Session.CommitTransaction();

                                                                        #endregion Add New Value in Field Kpi Monitoring
                                                                    }
                                                                }


                                                            }

                                                            #endregion  Perhitungan berdasarkan KpiTypePeriod
                                                        }
                                                    }
                                                    _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                                    _locKpiSubmitXPO.Save();
                                                    _locKpiSubmitXPO.Session.CommitTransaction();


                                                    //Message
                                                    SuccessMessageShow("Kpi Submit has successfully updates to Approved");
                                                }
                                                else
                                                {
                                                    //Message error
                                                    ErrorMessageShow("Data Kpi Submit Not Available");
                                                }

                                            }
                                            else
                                            {
                                                //Message Error
                                                ErrorMessageShow("Data Kpi Submit Order Not Available");
                                            }

                                        }


                                        #endregion Save To Kpi Monitoring
                                    }
                                    else
                                    {
                                        KpiSubmitApproval _saveDataKSA = new KpiSubmitApproval(_currentSession)
                                        {
                                            ApprovalDate = now,
                                            ApprovalStatus = Status.Approved,
                                            ApprovalLevel = ApprovalLevel.Level3,
                                            LockApproval = _locLockApproval,
                                            KpiSubmit = _locKpiSubmitXPO,
                                            ApprovedBy = _locEmployee,
                                        };
                                        _saveDataKSA.Save();
                                        _saveDataKSA.Session.CommitTransaction();

                                        _globFuncKS.SetKpiSubmitApproval(_currentSession, _locKpiSubmitXPO, ApprovalLevel.Level2);
                                        _globFuncKS.SetKpiSubmitApproval(_currentSession, _locKpiSubmitXPO, ApprovalLevel.Level1);

                                        _locActiveApproved3 = true;
                                    }

                                    #endregion SavePurchaseOrderApproval

                                    _locKpiSubmitXPO.ActivationPosting = _locActivationPosting;
                                    _locKpiSubmitXPO.KpiSubmitStatus = Status.Approved;
                                    _locKpiSubmitXPO.StatusDate = now;
                                    _locKpiSubmitXPO.Save();
                                    _locKpiSubmitXPO.Session.CommitTransaction();

                                    if (_locApplicationApprovalXPO.Posting == true)
                                    {
                                        if (_locKpiSubmitXPO.ActivationPosting == true && _locKpiSubmitXPO.KpiSubmitStatus == Status.Approved)
                                        {
                                            //_globFuncPO.SetPostingProcessPO(_currentSession, _locPurchaseOrderXPO);
                                        }
                                    }

                                    SuccessMessageShow("Data KPI Submit has successfully Approve");
                                }
                            }
                            #endregion Approval Level 3
                        }

                    }
                    else
                    {
                        ErrorMessageShow("Data KPI Submit Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data KPI Submit Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = KpiSubmitTaskBase " + ex.ToString());
            }
        }

        #endregion Code KpiSubmit

        #endregion Approval

        #region RejectKpiTaskBaseAction

        private void RejectKpiTaskBaseAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                string _objectCodeKpiSubmit = null;



                #region Progress KPI
                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        KpiSubmitTaskBase _locKpiSubmitTaskBaseOS = (KpiSubmitTaskBase)_objectSpace.GetObject(obj);

                        if (_locKpiSubmitTaskBaseOS != null)
                        {
                            if (_locKpiSubmitTaskBaseOS.Session != null)
                            {
                                _currSession = _locKpiSubmitTaskBaseOS.Session;
                            }

                            if (_locKpiSubmitTaskBaseOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locKpiSubmitTaskBaseOS.Code;

                                KpiSubmitTaskBase _locKpiSubmitTaskBaseXPO = _currSession.FindObject<KpiSubmitTaskBase>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("ApprovalStatus", Status.None),
                                                                    new BinaryOperator("ApprovalStatus", Status.Progress)
                                                                    )));
                                if (_locKpiSubmitTaskBaseXPO != null)
                                {

                                    _objectCodeKpiSubmit = _locKpiSubmitTaskBaseXPO.KpiSubmit.Code;

                                    XPCollection<KpiSubmit> _locKpiSubmits = new XPCollection<KpiSubmit>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                   new BinaryOperator("Code", _objectCodeKpiSubmit),
                                                                   new GroupOperator(GroupOperatorType.Or,
                                                                   new BinaryOperator("KpiSubmitStatus", Status.Progress)
                                                                   )));
                                    if(_locKpiSubmits != null && _locKpiSubmits.Count > 0)
                                    {
                                        foreach(KpiSubmit _locKpisubmit in _locKpiSubmits) 
                                        {
                                            #region Update Status Kpi Submit 

                                            _locKpisubmit.KpiSubmitStatus = Status.Reject;
                                            _locKpisubmit.Save();
                                            _locKpisubmit.Session.CommitTransaction();

                                            #endregion Update Status Kpi Submit 

                                            XPCollection<KpiSubmitLine> _locKpiSubmitLines = new XPCollection<KpiSubmitLine>
                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                   new BinaryOperator("KpiSubmit", _locKpisubmit),
                                                                   new GroupOperator(GroupOperatorType.Or,
                                                                   new BinaryOperator("KpiSubmitLineStatus", Status.Progress)
                                                                   )));
                                            if (_locKpiSubmitLines != null && _locKpiSubmitLines.Count() > 0)
                                            {

                                                foreach (KpiSubmitLine _locKpiSubmitLine in _locKpiSubmitLines)
                                                {
                                                    #region Update Status Kpi Submit Line

                                                    _locKpiSubmitLine.KpiSubmitLineStatus = Status.Reject;
                                                    _locKpiSubmitLine.Save();
                                                    _locKpiSubmitLine.Session.CommitTransaction();

                                                    #endregion Update Status Kpi Submit Line

                                                }

                                            }

                                        }
                                    }

                                    #region Update status Kpi Submit

                                    _locKpiSubmitTaskBaseXPO.ApprovalStatus = Status.Reject;
                                    _locKpiSubmitTaskBaseXPO.Save();
                                    _locKpiSubmitTaskBaseXPO.Session.CommitTransaction();

                                    #endregion Update status Kpi Submit

                                    //Message error
                                    ErrorMessageShow("Data Kpi Submit Status Is Rejected");

                                }
                                else
                                {
                                    //Message error
                                    ErrorMessageShow("Data Kpi Submit Not Available");
                                }

                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Kpi Submit Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion Progress KPI

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = KpiSubmit " + ex.ToString());
            }
        }

        #endregion RejectKpiTaskBaseAction

        #region Global Method

        private void SetKpiSubmitTaskBaseMonitoring(Session _currentSession, KpiSubmitTaskBase _locKpiSubmitTaskBaseXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locKpiSubmitTaskBaseXPO != null)
                {
                    //PurchaseTaskBaseMonitoring _savePurchaseTaskBaseMonitoring = new PurchaseTaskBaseMonitoring(_currentSession)
                    //{
                    //    PurchaseTaskBase = _locPurchaseTaskBaseXPO,
                    //    Company = _locPurchaseTaskBaseXPO.Company,
                    //    Workplace = _locPurchaseTaskBaseXPO.Workplace,
                    //    Department = _locPurchaseTaskBaseXPO.Department,
                    //    Division = _locPurchaseTaskBaseXPO.Division,
                    //    Section = _locPurchaseTaskBaseXPO.Section,
                    //    Employee = _locPurchaseTaskBaseXPO.Employee,
                    //    TransactionPeriod = _locPurchaseTaskBaseXPO.TransactionPeriod,
                    //    ApprovalLevel = _locPurchaseTaskBaseXPO.ApprovalLevel,
                    //    PurchaseRequisition = _locPurchaseTaskBaseXPO.PurchaseRequisition,
                    //    PurchaseOrder = _locPurchaseTaskBaseXPO.PurchaseOrder,
                    //    InventoryTransferIn = _locPurchaseTaskBaseXPO.InventoryTransferIn,
                    //    AssetTransferIn = _locPurchaseTaskBaseXPO.AssetTransferIn,
                    //    ServiceTransferIn = _locPurchaseTaskBaseXPO.ServiceTransferIn,
                    //    PurchaseInvoice = _locPurchaseTaskBaseXPO.PurchaseInvoice,
                    //    PayableTransaction = _locPurchaseTaskBaseXPO.PayableTransaction,
                    //    Remark = _locPurchaseTaskBaseXPO.Remark,
                    //    ApplicationApprovalSetup = _locPurchaseTaskBaseXPO.ApplicationApprovalSetup
                    //};
                    //_savePurchaseTaskBaseMonitoring.Save();
                    //_savePurchaseTaskBaseMonitoring.Session.CommitTransaction();

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = KpiSubmitTaskBase " + ex.ToString());
            }
        }

        private void DeleteKpiSubmitTaskBase(Session _currentSession, KpiSubmitTaskBase _locKpiSubmitTaskBase)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locKpiSubmitTaskBase != null)
                {
                    _locKpiSubmitTaskBase.Delete();
                    _locKpiSubmitTaskBase.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = KpiSubmitTaskBase " + ex.ToString());
            }
        }

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
