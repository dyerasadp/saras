﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    public partial class QuotationActionController : ViewController
    {
        public QuotationActionController()
        {
            InitializeComponent();
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(Quotation);

            #region RenewelAction 

            SimpleAction renewelQuotationAction = new SimpleAction(this, "RenewelQuotationAction", PredefinedCategory.View)
            {
                Caption = "Renewel",
                ConfirmationMessage = "Are you sure you want to Renewel the data?",
                ImageName = "Copy"
            };
            renewelQuotationAction.Execute += RenewelQuotationAction_Execute;

            #endregion RenewelAction
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region RenewelQuotationAction_Execute
        private void RenewelQuotationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                #region Renewel Quotation

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        Quotation _locQuotationOS = (Quotation)_objectSpace.GetObject(obj);


                        if (_locQuotationOS != null)
                        {
                            if (_locQuotationOS.Session != null)
                            {
                                _currSession = _locQuotationOS.Session;
                            }

                            if (_locQuotationOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locQuotationOS.Code;

                                Quotation _locQuotationXPO = _currSession.FindObject<Quotation>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));
                                if (_locQuotationXPO != null)
                                {
                                    GlobalFunction _globFunc = new GlobalFunction();
                                    Company _locCompany = null;
                                    QuotationPeriode _locPeriode = null;
                                    string DocNumber = null;

                                    _locCompany = _locQuotationXPO.Company;
                                    _locPeriode = _locQuotationXPO.Periode;

                                    //DocNumber = _globFunc.GetNumberingUnlockOptimisticRecord2(_currSession.DataLayer, ObjectList.Quotation, _locCompany, _locPeriode);

                                    #region Save Data Renewel 
                                    Quotation _saveNewQuotation = new Quotation(_currSession)
                                    {
                                        Name = _locQuotationXPO.Name,
                                        Type = _locQuotationXPO.Type,
                                        Mops = _locQuotationXPO.Mops,
                                        AreaJual = _locQuotationXPO.AreaJual,
                                        Noted = _locQuotationXPO.Noted,
                                        Status = QuotationStatus.RENEWEL,
                                        RenewelDate = DateTime.Now,
                                        Periode = _locQuotationXPO.Periode,
                                        DocNo = DocNumber,
                                    };
                                    _saveNewQuotation.Save();
                                    _saveNewQuotation.Session.CommitTransaction();

                                    #endregion Save Data Renewel

                                    #region Save List Customer Line
                                    XPCollection<QuotationListCustomerLine> _locQuotationListCustomerLines = new XPCollection<QuotationListCustomerLine>
                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Quotation", _locQuotationXPO)
                                                                ));
                                    if(_locQuotationListCustomerLines != null && _locQuotationListCustomerLines.Count() > 0)
                                    {
                                        foreach(QuotationListCustomerLine _locQuotationListCustomerLine in _locQuotationListCustomerLines)
                                        {
                                            QuotationListCustomerLine _saveNewQuotationListCustomerLine = new QuotationListCustomerLine(_currSession)
                                            {
                                                Name = _locQuotationListCustomerLine.Name,
                                                Email = _locQuotationListCustomerLine.Email,
                                                Quotation = _saveNewQuotation,
                                                
                                            };
                                            _saveNewQuotationListCustomerLine.Save();
                                            _saveNewQuotationListCustomerLine.Session.CommitTransaction();
                                        }
                                    }

                                    #endregion Save List Customer Line

                                    #region Mechanism Line

                                    XPCollection<QuotationMechanismLine> _locQuotationMechanismLines = new XPCollection<QuotationMechanismLine>
                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Quotation", _locQuotationXPO)
                                                                ));
                                    if (_locQuotationMechanismLines != null && _locQuotationMechanismLines.Count() > 0)
                                    {
                                        foreach (QuotationMechanismLine _locQuotationMechanismLine in _locQuotationMechanismLines)
                                        {
                                            QuotationMechanismLine _saveNewQuotationMechanismLine = new QuotationMechanismLine(_currSession)
                                            {
                                                Information = _locQuotationMechanismLine.Information,
                                                DetailInformation = _locQuotationMechanismLine.DetailInformation,
                                                Group = _locQuotationMechanismLine.Group,
                                                Quotation = _saveNewQuotation,

                                            };
                                            _saveNewQuotationMechanismLine.Save();
                                            _saveNewQuotationMechanismLine.Session.CommitTransaction();
                                        }
                                    }

                                    #endregion Mechanism Line

                                    #region Bank Line

                                    XPCollection<QuotationBankLine> _locQuotationBankLines = new XPCollection<QuotationBankLine>
                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Quotation", _locQuotationXPO)
                                                                ));
                                    if (_locQuotationBankLines != null && _locQuotationBankLines.Count() > 0)
                                    {
                                        foreach (QuotationBankLine _locQuotationBankLine in _locQuotationBankLines)
                                        {
                                            QuotationBankLine _saveNewQuotationBankLine = new QuotationBankLine(_currSession)
                                            {
                                                QuotationBank = _locQuotationBankLine.QuotationBank,
                                                AccountBank = _locQuotationBankLine.AccountBank,
                                                Branch = _locQuotationBankLine.Branch,
                                                Company = _locQuotationBankLine.Company,
                                                Quotation = _saveNewQuotation,

                                            };
                                            _saveNewQuotationBankLine.Save();
                                            _saveNewQuotationBankLine.Session.CommitTransaction();
                                        }
                                    }

                                    #endregion Bank Line

                                    #region Price Line

                                    XPCollection<QuotationPriceLine> _locQuotationPriceLines = new XPCollection<QuotationPriceLine>
                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Quotation", _locQuotationXPO)
                                                                ));
                                    if (_locQuotationPriceLines != null && _locQuotationPriceLines.Count() > 0)
                                    {
                                        foreach (QuotationPriceLine _locQuotationPriceLine in _locQuotationPriceLines)
                                        {
                                            QuotationPriceLine _saveNewQuotationPriceLine = new QuotationPriceLine(_currSession)
                                            {
                                                Name = _locQuotationPriceLine.Name,
                                                ItemType = _locQuotationPriceLine.ItemType,
                                                BasePrice = _locQuotationPriceLine.BasePrice,
                                                TotalPrice = _locQuotationPriceLine.TotalPrice,
                                                Quotation = _saveNewQuotation,
                                            };
                                            _saveNewQuotationPriceLine.Save();
                                            _saveNewQuotationPriceLine.Session.CommitTransaction();


                                            #region Tax Price Line

                                            XPCollection<QuotationTaxPriceLine> _locQuotationTaxPriceLines = new XPCollection<QuotationTaxPriceLine>
                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("QuotationPriceLine", _locQuotationPriceLine)
                                                                        ));
                                            if (_locQuotationTaxPriceLines != null && _locQuotationTaxPriceLines.Count() > 0)
                                            {
                                                foreach (QuotationTaxPriceLine _locQuotationTaxPriceLine in _locQuotationTaxPriceLines)
                                                {
                                                    QuotationTaxPriceLine _saveNewQuotationTaxPriceLine = new QuotationTaxPriceLine(_currSession)
                                                    {
                                                        Tax = _locQuotationTaxPriceLine.Tax,
                                                        Value = _locQuotationTaxPriceLine.Value,
                                                        BasePrice = _saveNewQuotationPriceLine.BasePrice,
                                                        QuotationPriceLine = _saveNewQuotationPriceLine,
                                                    };
                                                    _saveNewQuotationPriceLine.Save();
                                                    _saveNewQuotationPriceLine.Session.CommitTransaction();
                                                }
                                            }

                                            #endregion Tax Price Line
                                        }
                                    }



                                    #endregion Price Line
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Ticket Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion Renewel Quotation
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CallCenter " + ex.ToString());
            }
        }

        #endregion RenewelQuotationAction_Execute


        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
