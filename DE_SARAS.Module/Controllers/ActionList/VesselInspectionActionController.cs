﻿using DE_SARAS.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class VesselInspectionActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public VesselInspectionActionController()
        {
            InitializeComponent();
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(VesselInspection);

            #region GetChecklist

            SimpleAction getChecklistInspection = new SimpleAction(this, "getChecklistInspection", PredefinedCategory.View)
            {
                Caption = "Get Checklist",
                ConfirmationMessage = "Are you sure?",
                ImageName = "BO_Security_Permission_Action"
            };
            getChecklistInspection.Execute += getChecklistInspection_Execute;

            #endregion GetChecklist
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region getChecklistInspection_Execute
        private void getChecklistInspection_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                #region GetChecklist

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        VesselInspection _locInspection = (VesselInspection)_objectSpace.GetObject(obj);

                        if (_locInspection != null)
                        {
                            if (_locInspection.Session != null)
                            {
                                _currSession = _locInspection.Session;
                            }

                            if (_locInspection.Code != null && _currSession != null)
                            {
                                _currObjectId = _locInspection.Code;

                                VesselInspection _locInspectionXPO = _currSession.FindObject<VesselInspection>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locInspectionXPO != null)
                                {
                                    if (_locInspectionXPO.TemplateChecklist != null)
                                    {
                                        #region Get Checklist For Vessel Inspection

                                        XPCollection<ShippingHeadInspectionLine> LocInspectionChecklist = new XPCollection<ShippingHeadInspectionLine>
                                                (_currSession, new GroupOperator(GroupOperatorType.And, 
                                                new BinaryOperator("StatusHead", _locInspectionXPO.TemplateChecklist),
                                                new BinaryOperator("Active",true)
                                                ));

                                        if (LocInspectionChecklist != null && LocInspectionChecklist.Count() > 0)
                                        {
                                            foreach (ShippingHeadInspectionLine _locChecklist in LocInspectionChecklist)
                                            {
                                                VesselInspectionLine _saveChecklist = new VesselInspectionLine(_currSession)
                                                {
                                                    Item = _locChecklist,
                                                    VesselInspection = _locInspectionXPO
                                                };
                                                _saveChecklist.Save();
                                                _saveChecklist.Session.CommitTransaction();
                                            }
                                        }

                                        #endregion Get Checklist For Vessel Inspection
                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion GetChecklist


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = KpiSubmit " + ex.ToString());
            }
        }

        #endregion getChecklistInspection_Execute

        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
