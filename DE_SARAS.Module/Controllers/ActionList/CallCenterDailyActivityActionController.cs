﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CallCenterDailyActivityActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public CallCenterDailyActivityActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(CallCenterDailyActivity);

            #region CloseAction 

            SimpleAction closeCallCenterDailyAction = new SimpleAction(this, "CloseCallCenterDailyAction", PredefinedCategory.View)
            {
                Caption = "Close Ticket",
                ConfirmationMessage = "Are you sure you want to Process the data?",
                ImageName = "Redo"
            };
            closeCallCenterDailyAction.Execute += CloseCallCenterDailyAction_Execute;

            #endregion CloseAction
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }


        #region CloseCallCenterAction_Execute
        private void CloseCallCenterDailyAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                TicketHelpType _dailyActivity = null;


                #region SEND

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CallCenterDailyActivity _locCallCenterDailyActivityOS = (CallCenterDailyActivity)_objectSpace.GetObject(obj);


                        if (_locCallCenterDailyActivityOS != null)
                        {
                            if (_locCallCenterDailyActivityOS.Session != null)
                            {
                                _currSession = _locCallCenterDailyActivityOS.Session;
                            }

                            if (_locCallCenterDailyActivityOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCallCenterDailyActivityOS.Code;

                                CallCenterDailyActivity _locCallCenterDailyActivityXPO = _currSession.FindObject<CallCenterDailyActivity>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locCallCenterDailyActivityXPO != null)
                                {
                                    if(_locCallCenterDailyActivityXPO.StatusTicket == StatusTicket.Progress)
                                    {
                                        _locCallCenterDailyActivityXPO.ClosedDate = DateTime.Now;
                                        _locCallCenterDailyActivityXPO.Interval = _locCallCenterDailyActivityXPO.ClosedDate - _locCallCenterDailyActivityXPO.ProgressDate;
                                        _locCallCenterDailyActivityXPO.StatusTicket = StatusTicket.Closed;
                                        _locCallCenterDailyActivityXPO.Save();
                                        _locCallCenterDailyActivityXPO.Session.CommitTransaction();

                                        //Message
                                        SuccsessMessageShow("The Ticket has been Successfully Closed");
                                    }
                                    else
                                    {
                                        //Message Error
                                        ErrorMessageShow("The Ticket has been Closed");
                                    }
                                   
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Ticket Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion SEND
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CallCenter " + ex.ToString());
            }
        }

        #endregion CloseCallCenterAction_Execute


        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
