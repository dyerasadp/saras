﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{

    public partial class VoyageActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public VoyageActionController()
        {
            InitializeComponent();
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(Voyage);

            #region ProgressAction 

            SimpleAction progressVoyageAction = new SimpleAction(this, "ProgressVoyageAction", PredefinedCategory.View)
            {
                Caption = "Progress",
                ConfirmationMessage = "Are you sure to change Voyage Status to In Progress?",
                ImageName = "Redo"
            };
            progressVoyageAction.Execute += ProgressVoyageAction_Execute;

            #endregion ProgressAction

            #region CloseAction 

            SimpleAction closeVoyageAction = new SimpleAction(this, "CloseVoyageAction", PredefinedCategory.View)
            {
                Caption = "Close Voyage",
                ConfirmationMessage = "Are you sure to Close the Voyage?",
                ImageName = "Close"
            };
            closeVoyageAction.Execute += CloseVoyageAction_Execute;

            #endregion CloseAction
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region Progress

        private void ProgressVoyageAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        Voyage _locVoyageActivityOS = (Voyage)_objectSpace.GetObject(obj);

                        if (_locVoyageActivityOS != null)
                        {
                            if (_locVoyageActivityOS.Session != null)
                            {
                                _currSession = _locVoyageActivityOS.Session;
                            }

                            if (_locVoyageActivityOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locVoyageActivityOS.Code;

                                Voyage _locVoyageXPO = _currSession.FindObject<Voyage>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locVoyageXPO != null)
                                {
                                    if (_locVoyageXPO.StatusVoyage == Status.Open)
                                    {
                                        _locVoyageXPO.ProgressDate = now;
                                        _locVoyageXPO.StatusVoyage = Status.Progress;
                                        _locVoyageXPO.Save();
                                        _locVoyageXPO.Session.CommitTransaction();

                                        XPCollection<VoyageRoute> _locVoyageRoutes = new XPCollection<VoyageRoute>
                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Voyage", _locVoyageXPO)
                                            ));

                                        if(_locVoyageRoutes != null && _locVoyageRoutes.Count()>0) {
                                            foreach (VoyageRoute _locVoyageRoute in _locVoyageRoutes)
                                            {
                                                _locVoyageRoute.Status = Status.Progress;
                                                _locVoyageRoute.Save();
                                                _locVoyageRoute.Session.CommitTransaction();
                                            }
                                        }

                                        SuccsessMessageShow("The Voyage Status has been Successfully Changed to Progress");
                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Voyage Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CallCenter " + ex.ToString());
            }
        }

        #endregion Progress

        #region Close

        private void CloseVoyageAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToClose = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToClose != null)
                {
                    foreach (Object obj in _objectToClose)
                    {
                        Voyage _locVoyageActivityOS = (Voyage)_objectSpace.GetObject(obj);

                        if (_locVoyageActivityOS != null)
                        {
                            if (_locVoyageActivityOS.Session != null)
                            {
                                _currSession = _locVoyageActivityOS.Session;
                            }

                            if (_locVoyageActivityOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locVoyageActivityOS.Code;

                                Voyage _locVoyageXPO = _currSession.FindObject<Voyage>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locVoyageXPO != null)
                                {
                                    if (_locVoyageXPO.StatusVoyage == Status.Progress)
                                    {
                                        _locVoyageXPO.CloseDate = now;
                                        _locVoyageXPO.StatusVoyage = Status.Close;
                                        _locVoyageXPO.Save();
                                        _locVoyageXPO.Session.CommitTransaction();

                                        //find all voyage route from current voyage
                                        XPCollection<VoyageRoute> _routeLines = new XPCollection<VoyageRoute>
                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("Voyage", _locVoyageXPO)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                                        //find last route from current voyage
                                        VoyageRoute _latestRoute = _routeLines.LastOrDefault();

                                        //find bunker data from last route
                                        VoyageBunker _latestBunker = _currSession.FindObject<VoyageBunker>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("VoyageRoute", _latestRoute)
                                                                     ));

                                        //find cargo data from last route
                                        VoyageCargo _latestCargo = _currSession.FindObject<VoyageCargo>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("VoyageRoute", _latestRoute)
                                                                     ));

                                        //find vessel remains from current vessel
                                        VesselRemains _locVesselRemains = _currSession.FindObject<VesselRemains>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Vessel", _locVoyageXPO.Vessel)
                                                                     ));

                                        if (_routeLines != null && _routeLines.Count() > 0)
                                        {
                                            foreach (VoyageRoute _routeLine in _routeLines)
                                            {
                                                _routeLine.Status = Status.Close;
                                                _routeLine.Save();
                                                _routeLine.Session.CommitTransaction();
                                            }
                                            if(_locVesselRemains != null)
                                            {
                                                if (_latestBunker != null)
                                                {
                                                    _locVesselRemains.QtyBunkerLeft = _latestBunker.LastBunkerQty;
                                                }
                                                if (_latestCargo != null)
                                                {
                                                    _locVesselRemains.QtyCargoLeft = _latestCargo.RemainingOnBoard;
                                                }
                                                _locVesselRemains.Save();
                                                _locVesselRemains.Session.CommitTransaction();
                                            }
                                        }
                                        
                                        SuccsessMessageShow("The Voyage Status has been Successfully Changed to Close");
                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Voyage Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CallCenter " + ex.ToString());
            }
        }

        #endregion Close


        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
