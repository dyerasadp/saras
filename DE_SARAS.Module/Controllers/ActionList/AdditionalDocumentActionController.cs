﻿using DE_SARAS.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.UI;
using DevExpress.XtraEditors.Controls;
using DevExpress.DataAccess.Native.Web;


namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class AdditionalDocumentActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/

        public AdditionalDocumentActionController()
        {
            InitializeComponent();
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(AdditionalDocument);

            //#region Play Video
            //SimpleAction playVideoAction = new SimpleAction(this, "PlayVideoAction", PredefinedCategory.View)
            //{
            //    Caption = "Play Video",
            //    ImageName = "BO_Security_Permission_Action"
            //};
            //playVideoAction.Execute += PlayVideoAction_Execute;

            //#endregion Play Video
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
           
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region PlayVideoAction_Execute

        private void PlayVideoAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            AdditionalDocument obj = View.SelectedObjects[0] as AdditionalDocument;
            if (obj != null && !string.IsNullOrEmpty(obj.UrlLink))
            {
                string message = $"To view the video, please right-click the link and open it in a new tab:\n\n{obj.UrlLink}";
                ErrorMessageShow(message);
            }
        }



        #endregion PlayVideoAction_Execute

        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string link)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 6000;

            // Provide instructions with a link in the message
            options.Message = link;
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "View Video";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
