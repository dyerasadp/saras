﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class VesselDocumentRenewalLineActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public VesselDocumentRenewalLineActionController()
        {
            InitializeComponent();
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(VesselDocumentRenewalLine);

            #region CloseAction

            SimpleAction CloseRenewalDocumentAction = new SimpleAction(this, "CloseRenewalDocumentAction", PredefinedCategory.View)
            {
                Caption = "End Process",
                ConfirmationMessage = "Are you sure to Close this Renewal Progress?",
                ImageName = "Close"
            };
            CloseRenewalDocumentAction.Execute += CloseRenewalDocumentAction_Execute;

            #endregion CloseAction
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region Close

        private void CloseRenewalDocumentAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToClose = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToClose != null)
                {
                    foreach (Object obj in _objectToClose)
                    {
                        VesselDocumentRenewalLine _locDocumentRenewalActivityOS = (VesselDocumentRenewalLine)_objectSpace.GetObject(obj);

                        if (_locDocumentRenewalActivityOS != null)
                        {
                            if (_locDocumentRenewalActivityOS.Session != null)
                            {
                                _currSession = _locDocumentRenewalActivityOS.Session;
                            }

                            if (_locDocumentRenewalActivityOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locDocumentRenewalActivityOS.Code;

                                VesselDocumentRenewalLine _locDocumentRenewal = _currSession.FindObject<VesselDocumentRenewalLine>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locDocumentRenewal != null)
                                {

                                    //mengubah data document di object vessel document
                                    VesselDocument _locDocument = _currSession.FindObject<VesselDocument>
                                        (new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Code", _locDocumentRenewal.Document.Code)
                                        ));

                                    if (_locDocument != null)
                                    {
                                        _locDocument.EndorseDate = _locDocumentRenewal.EndorseDateAfter;
                                        _locDocument.ExpiryDate = _locDocumentRenewal.ExpiredDateAfter;
                                        _locDocumentRenewal.Save();
                                        _locDocumentRenewal.Session.CommitTransaction();
                                    }

                                    //mengubah status di document renewal
                                    if (_locDocumentRenewal.Status == Status.Progress)
                                    {
                                        _locDocumentRenewal.Status = Status.Close;
                                        _locDocumentRenewal.Save();
                                        _locDocumentRenewal.Session.CommitTransaction();

                                        // tinggal cari vessel renewal linenya apa lalu update.
                                        SuccsessMessageShow("The Document Renewal Status has been Successfully Changed to Close");
                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Voyage Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CallCenter " + ex.ToString());
            }
        }

        #endregion Close

        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
