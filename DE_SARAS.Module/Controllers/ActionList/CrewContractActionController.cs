﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CrewContractActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public CrewContractActionController()
        {
            InitializeComponent();
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(CrewContract);

            #region Set Default Contract Action

            SimpleAction defaultContractAction = new SimpleAction(this, "DefaultContractAction", PredefinedCategory.View)
            {
                Caption = "Set Default",
                ConfirmationMessage = "Are you sure to set this contract to default?",
                ImageName = "Redo"
            };
            defaultContractAction.Execute += DefaultContractAction_Execute;

            #endregion Set Default Contract Action
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region Set Default Contract Execute

        private void DefaultContractAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CrewContract _locCrewContractActivityOS = (CrewContract)_objectSpace.GetObject(obj);

                        if (_locCrewContractActivityOS != null)
                        {
                            if (_locCrewContractActivityOS.Session != null)
                            {
                                _currSession = _locCrewContractActivityOS.Session;
                            }

                            if (_locCrewContractActivityOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCrewContractActivityOS.Code;

                                CrewContract _locContractXPO = _currSession.FindObject<CrewContract>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locContractXPO != null)
                                {
                                    //cari semua data crew contract dari object crew nya, lalu default yang ada dimatikan
                                    XPCollection<CrewContract> _locCrewContracts = new XPCollection<CrewContract>
                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Crew", _locContractXPO.Crew)
                                            ));

                                    if (_locContractXPO.Default == false)
                                    {
                                        if (_locCrewContracts != null && _locCrewContracts.Count() > 0)
                                        {
                                            foreach (CrewContract _locCrewContract in _locCrewContracts)
                                            {
                                                if (_locCrewContract.Default == true)
                                                {
                                                    _locCrewContract.Default = false;
                                                }
                                            }
                                        }

                                        _locContractXPO.Default = true;
                                        _locContractXPO.Save();
                                        _locContractXPO.Session.CommitTransaction();
                                    }

                                    Crew _locCrew = _currSession.FindObject<Crew>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _locContractXPO.Crew.Code)
                                                                    ));
                                    
                                    if (_locCrew != null)
                                    {
                                        _locCrew.DefaultPosition = _locContractXPO.JobPosition;
                                        _locCrew.DefaultVessel = _locContractXPO.Vessel;
                                    }

                                    SuccsessMessageShow("The contract set to default");

                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Voyage Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CallCenter " + ex.ToString());
            }
        }

        #endregion Set Default Contract Execute


        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
