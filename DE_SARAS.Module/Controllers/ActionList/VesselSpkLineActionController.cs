﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class VesselSpkLineActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public VesselSpkLineActionController()
        {
            InitializeComponent();
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(VesselSpkLine);

            #region GetChecklist

            SimpleAction closeVesselSpkLineAction = new SimpleAction(this, "closeVesselSpkLineAction", PredefinedCategory.View)
            {
                Caption = "Close Progress",
                ConfirmationMessage = "Are you sure?",
                ImageName = "Actions_CheckCircled"
            };
            closeVesselSpkLineAction.Execute += closeVesselSpkLineAction_Execute;

            #endregion GetChecklist
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region close progress spk

        private void closeVesselSpkLineAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        VesselSpkLine _locSPKLine = (VesselSpkLine)_objectSpace.GetObject(obj);

                        if (_locSPKLine != null)
                        {
                            if (_locSPKLine.Session != null)
                            {
                                _currSession = _locSPKLine.Session;
                            }

                            if (_locSPKLine.Code != null && _currSession != null)
                            {
                                _currObjectId = _locSPKLine.Code;

                                VesselSpkLine _locSPKLineXPO = _currSession.FindObject<VesselSpkLine>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locSPKLineXPO != null)
                                {

                                    XPCollection<VesselSpkLineProgress> _locSPKLineProgs = new XPCollection<VesselSpkLineProgress>
                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                           new BinaryOperator("VesselSpkLine", _locSPKLine)
                                           ));

                                    VesselSpkLineProgress _locSPKLineProg = _locSPKLineProgs.OrderByDescending(x => x.Code).FirstOrDefault();

                                    VesselPart _locVesselPart = _currSession.FindObject<VesselPart>
                                        (new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Code", _locSPKLineXPO.VesselPart.Code)
                                        ));

                                    if (_locVesselPart != null)
                                    {
                                        if (_locSPKLineProg.Status == RepairStatus.Done)
                                        {
                                            _locVesselPart.Result = ChecklistOption.Normal;
                                            _locVesselPart.Remarks = "";
                                        } else
                                        {
                                            _locVesselPart.Result = ChecklistOption.Abnormal;
                                        }
                                        _locVesselPart.Remarks = "";
                                        _locVesselPart.Save();
                                        _locVesselPart.Session.CommitTransaction();
                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = VesselSpkLine " + ex.ToString());
            }
        }

        #endregion close progress spk

        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
