﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class VesselDockingActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public VesselDockingActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(VesselDocking);

            #region Set Docking Status Progress

            SimpleAction ProgressStatusAction = new SimpleAction(this, "ProgressStatusAction", PredefinedCategory.View)
            {
                Caption = "Set Progress",
                ConfirmationMessage = "Are you sure to change Docking Status to In Progress?",
                ImageName = "Redo"
            };
            ProgressStatusAction.Execute += ProgressStatusAction_Execute;

            #endregion Set Docking Status Progress

            #region Set Docking Status Closed

            SimpleAction CloseStatusAction = new SimpleAction(this, "CloseStatusAction", PredefinedCategory.View)
            {
                Caption = "Set Close",
                ConfirmationMessage = "Are you sure to change Docking Status to In Progress?",
                ImageName = "Redo"
            };
            CloseStatusAction.Execute += CloseStatusAction_Execute;

            #endregion Set Docking Status Closed
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region Progress

        private void ProgressStatusAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        VesselDocking _locDockingActivityOS = (VesselDocking)_objectSpace.GetObject(obj);

                        if (_locDockingActivityOS != null)
                        {
                            if (_locDockingActivityOS.Session != null)
                            {
                                _currSession = _locDockingActivityOS.Session;
                            }

                            if (_locDockingActivityOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locDockingActivityOS.Code;

                                VesselDocking _locDockingXPO = _currSession.FindObject<VesselDocking>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                #region change status docking to progress

                                if (_locDockingXPO != null)
                                {
                                    if (_locDockingXPO.Status == Status.Open)
                                    {
                                        //_locDockingXPO.ProgressDate = now;
                                        _locDockingXPO.Status = Status.Progress;
                                        _locDockingXPO.Save();
                                        _locDockingXPO.Session.CommitTransaction();

                                        #region move all data from plan to actual

                                        XPCollection<VesselDockingLinePlan> _locDockingPlans = new XPCollection<VesselDockingLinePlan>
                                                (_currSession, new BinaryOperator("DockingProject", _locDockingXPO));

                                        if (_locDockingPlans != null && _locDockingPlans.Count() > 0)
                                        {
                                            foreach (VesselDockingLinePlan _locDockingPlan in _locDockingPlans)
                                            {
                                                VesselDockingLineActual _saveDockingActual = new VesselDockingLineActual(_currSession)
                                                {
                                                    DockingProject = _locDockingXPO,
                                                    ItemPart = _locDockingPlan.ItemPart,
                                                    WorkItem = _locDockingPlan.WorkItem,
                                                    WorkDescription = _locDockingPlan.WorkDescription,
                                                    WorkWeight = _locDockingPlan.WorkWeight,
                                                    WorkPercentage = _locDockingPlan.WorkPercentage,
                                                    //WorkStart = _locDockingPlan.WorkStart,
                                                    //WorkEnd = _locDockingPlan.WorkEnd,
                                                    //Duration = _locDockingPlan.Duration,
                                                    DocDate = DateTime.Now
                                                };
                                                _saveDockingActual.Save();
                                                _saveDockingActual.Session.CommitTransaction();
                                            }
                                        }

                                        #endregion move all data from plan to actual

                                        SuccsessMessageShow("The Docking Status has been Successfully Changed to Progress");
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Docking already in progress");
                                    }
                                }

                                #endregion change status docking to progress
                            }
                            else
                            {
                                ErrorMessageShow("Docking Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = VesselDocking " + ex.ToString());
            }
        }

        #endregion Progress

        #region Close

        private void CloseStatusAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToClose = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (_objectToClose != null)
                {
                    foreach (Object obj in _objectToClose)
                    {
                        VesselDocking _locDockingActivityOS = (VesselDocking)_objectSpace.GetObject(obj);

                        if (_locDockingActivityOS != null)
                        {
                            if (_locDockingActivityOS.Session != null)
                            {
                                _currSession = _locDockingActivityOS.Session;
                            }

                            if (_locDockingActivityOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locDockingActivityOS.Code;

                                VesselDocking _locDockingXPO = _currSession.FindObject<VesselDocking>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locDockingXPO != null)
                                {
                                    if (_locDockingXPO.Status == Status.Progress)
                                    {
                                        //_locDockingXPO.CloseDate = now;
                                        _locDockingXPO.Status = Status.Close;
                                        _locDockingXPO.Save();
                                        _locDockingXPO.Session.CommitTransaction();

                                        SuccsessMessageShow("The Docking Status has been Successfully Changed to Close");
                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Docking Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = VesselDocking " + ex.ToString());
            }
        }

        #endregion Close

        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
