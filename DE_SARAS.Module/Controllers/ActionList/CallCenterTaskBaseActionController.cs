﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CallCenterTaskBaseActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public CallCenterTaskBaseActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(CallCenterTaskBase);

            #region SendAction 

            SimpleAction progressCallCenterAction = new SimpleAction(this, "ProgressCallCenterAction", PredefinedCategory.View)
            {
                Caption = "Progress",
                ConfirmationMessage = "Are you sure you want to Progress the data?",
                ImageName = "Redo"
            };
            progressCallCenterAction.Execute += ProgressCallCenterAction_Execute;

            #endregion SendAction

            #region CloseAction 

            SimpleAction closeCallCenterTbAction = new SimpleAction(this, "CloseCallCenterTbAction", PredefinedCategory.View)
            {
                Caption = "Close Ticket",
                ConfirmationMessage = "Are you sure you want to Close the data?",
                ImageName = "Redo"
            };
            closeCallCenterTbAction.Execute += CloseCallCenterTbAction_Execute;

            #endregion CloseAction
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region ProgressCallCenterAction_Execute
        private void ProgressCallCenterAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                CallCenter _callCenter = null;
                


                #region PROGRESS

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CallCenterTaskBase _locCallCenterTaskBaseOS = (CallCenterTaskBase)_objectSpace.GetObject(obj);


                        if (_locCallCenterTaskBaseOS != null)
                        {
                            if (_locCallCenterTaskBaseOS.Session != null)
                            {
                                _currSession = _locCallCenterTaskBaseOS.Session;
                            }

                            if (_locCallCenterTaskBaseOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCallCenterTaskBaseOS.Code;

                                CallCenterTaskBase _locCallCenterTaskBaseXPO = _currSession.FindObject<CallCenterTaskBase>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locCallCenterTaskBaseXPO != null)
                                {
                                    if(_locCallCenterTaskBaseXPO.PicCallCenter != null)
                                    {
                                        if(_locCallCenterTaskBaseXPO.StatusTicket == CustomProcess.StatusTicket.Open)
                                        {
                                            _callCenter = _locCallCenterTaskBaseXPO.CallCenter;

                                            #region Sent To Call Center Monitoring
                                            CallCenterMonitoring _saveCallCenterMonitoring = new CallCenterMonitoring(_currSession)
                                            {
                                                CallCenter = _locCallCenterTaskBaseXPO.CallCenter,
                                                HelpType = _locCallCenterTaskBaseXPO.HelpType,
                                                TopicType = _locCallCenterTaskBaseXPO.TopicType,
                                                RequestCallCenter = _locCallCenterTaskBaseXPO.RequestCallCenter,
                                                PicCallCenter = _locCallCenterTaskBaseXPO.PicCallCenter,
                                                StatusTicket = CustomProcess.StatusTicket.Progress,
                                                TicketSubject = _locCallCenterTaskBaseXPO.TicketSubject,
                                                TicketDescription = _locCallCenterTaskBaseXPO.TicketDescription,
                                            };
                                            _saveCallCenterMonitoring.Save();
                                            _saveCallCenterMonitoring.Session.CommitTransaction();

                                            #endregion Sent To Call Center Monitoring

                                            #region Update Status di Task Base
                                            _locCallCenterTaskBaseXPO.StatusTicket = CustomProcess.StatusTicket.Progress;
                                            _locCallCenterTaskBaseXPO.ProgressDate = DateTime.Now;
                                            _locCallCenterTaskBaseXPO.Save();
                                            _locCallCenterTaskBaseXPO.Session.CommitTransaction();
                                            #endregion Update Status di Task Base

                                            #region Update Status di Call Center
                                            XPCollection<CallCenter> _locCallCenters = new XPCollection<CallCenter>
                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("StatusTicket", StatusTicket.Open)
                                                                         ));
                                            if(_locCallCenters != null && _locCallCenters.Count() > 0)
                                            {
                                                foreach(CallCenter _loccallCenter in _locCallCenters) 
                                                {
                                                    if(_loccallCenter == _callCenter)
                                                    {
                                                        _loccallCenter.StatusTicket = StatusTicket.Progress;
                                                        _loccallCenter.Save();
                                                        _loccallCenter.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                            #endregion Update Status di Call Center

                                            //Message
                                            SuccsessMessageShow("This Ticket is has been updated to Progress");
                                        }
                                    }
                                    else
                                    {
                                        //Message Error
                                        ErrorMessageShow("CAN'T PROGRESS THIS TICKET, PIC NOT SELECTED");
                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Ticket Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion PROGRESS
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CallCenterTaskBase " + ex.ToString());
            }
        }

        #endregion ProgressCallCenterAction_Execute

        #region CloseCallCenterTbAction_Execute
        private void CloseCallCenterTbAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                CallCenter _callCenter = null;
                CallCenter _callCentertb = null;
                string _callCenterCode = null;


                #region Closed

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CallCenterTaskBase _locCallCenterTaskBaseOS = (CallCenterTaskBase)_objectSpace.GetObject(obj);


                        if (_locCallCenterTaskBaseOS != null)
                        {
                            if (_locCallCenterTaskBaseOS.Session != null)
                            {
                                _currSession = _locCallCenterTaskBaseOS.Session;
                            }

                            if (_locCallCenterTaskBaseOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCallCenterTaskBaseOS.Code;

                                CallCenterTaskBase _locCallCenterTaskBaseXPO = _currSession.FindObject<CallCenterTaskBase>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locCallCenterTaskBaseXPO != null)
                                {
                                    if(_locCallCenterTaskBaseXPO.StatusTicket == StatusTicket.Open)
                                    {
                                        if (_locCallCenterTaskBaseXPO.PicCallCenter != null)
                                        {
                                            //Message Error
                                            ErrorMessageShow("Progress the ticket before this action");
                                        }
                                        else
                                        {
                                            //Message Error
                                            ErrorMessageShow("Please input PIC for the ticket,and then Progress");
                                        }

                                    }
                                    if (_locCallCenterTaskBaseXPO.StatusTicket == StatusTicket.Progress)
                                    {

                                        //DateTime futureDate = new DateTime(2023, 11, 26, 20, 31, 29);
                                        _locCallCenterTaskBaseXPO.ClosedDate = now;
                                        _locCallCenterTaskBaseXPO.StatusTicket = StatusTicket.Closed;
                                        _locCallCenterTaskBaseXPO.ActivationPosting = true;
                                        _locCallCenterTaskBaseXPO.Save();
                                        _locCallCenterTaskBaseXPO.Session.CommitTransaction();

                                        #region Update Status on Call Center
                                        _callCenter = _locCallCenterTaskBaseXPO.CallCenter;
                                        XPCollection<CallCenter> _locCallCenters = new XPCollection<CallCenter>
                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("StatusTicket", StatusTicket.Progress)
                                                                         ));
                                        if (_locCallCenters != null && _locCallCenters.Count() > 0)
                                        {
                                            foreach (CallCenter _locCallCenter in _locCallCenters)
                                            {
                                                _callCenterCode = _locCallCenter.Code;
                                                CallCenter _locCallCenterXPO = _currSession.FindObject<CallCenter>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _callCenterCode)
                                                                                ));
                                                if (_locCallCenterXPO != null)
                                                {
                                                    _locCallCenterXPO.StatusTicket = StatusTicket.Closed;
                                                    _locCallCenterXPO.ActivationPosting = true;
                                                    _locCallCenterXPO.Save();
                                                    _locCallCenterXPO.Session.CommitTransaction();


                                                    //Message
                                                    SuccsessMessageShow("The Ticket has been Successfully Closed");
                                                }

                                            }
                                        }

                                        #endregion Update Status on Call Center

                                        #region Update Status on Call Monitoring

                                        XPCollection<CallCenterMonitoring> _locCallCenterMonitorings = new XPCollection<CallCenterMonitoring>
                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("StatusTicket", StatusTicket.Progress)
                                                                         ));
                                        if (_locCallCenterMonitorings != null && _locCallCenterMonitorings.Count() > 0)
                                        {
                                            foreach (CallCenterMonitoring _locCallCenterMonitoring in _locCallCenterMonitorings)
                                            {
                                                _callCentertb = _locCallCenterTaskBaseXPO.CallCenter;
                                                CallCenterMonitoring _locCallCenterMonitoringXPO = _currSession.FindObject<CallCenterMonitoring>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CallCenter", _callCentertb)
                                                                                ));
                                                if (_locCallCenterMonitoringXPO != null)
                                                {
                                                    _locCallCenterMonitoringXPO.ClosedDate = now;
                                                    _locCallCenterMonitoringXPO.Interval = _locCallCenterMonitoringXPO.ClosedDate - _locCallCenterMonitoringXPO.ProgressDate;
                                                    _locCallCenterMonitoringXPO.StatusTicket = StatusTicket.Closed;
                                                    _locCallCenterMonitoringXPO.ActivationPosting = true;
                                                    _locCallCenterMonitoringXPO.Save();
                                                    _locCallCenterMonitoringXPO.Session.CommitTransaction();


                                                    //Message
                                                    SuccsessMessageShow("The Ticket has been Successfully Closed");
                                                }

                                            }
                                        }

                                        #endregion Update Status on Call Center Task Base
                                    }
                                    else
                                    {
                                        //Message Error
                                        ErrorMessageShow("The Ticket has been Closed");
                                    }

                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Ticket Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion Closed
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CallCenter " + ex.ToString());
            }
        }

        #endregion CloseCallCenterTbAction_Execute

        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
