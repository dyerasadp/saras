﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers
{
    public partial class EmployeeController : ViewController
    {
        public EmployeeController()
        {
            InitializeComponent();

            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(Employee);

            #region SetDeafaultPosition 

            SimpleAction setDefaultPositionAction = new SimpleAction(this, "SetDefaultPositionAction", PredefinedCategory.View)
            {
                Caption = "Set Default Position",
                ConfirmationMessage = "Are you sure you want to set default the data?",
                ImageName = "BO_Security_Permission_Action"
            };
            setDefaultPositionAction.Execute += SetDefaultPositionAction_Execute;

            #endregion SetDeafaultPosition

        }
        protected override void OnActivated()
        {
            base.OnActivated();
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();

        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
        }

        #region SetDefaultPositionAction_Execute
        private void SetDefaultPositionAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                string _locDefaultPosition = null;


                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        Employee _locEmployeeOS = (Employee)_objectSpace.GetObject(obj);

                        if (_locEmployeeOS != null)
                        {
                            if (_locEmployeeOS.Session != null)
                            {
                                _currSession = _locEmployeeOS.Session;
                            }

                            if (_locEmployeeOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locEmployeeOS.Code;
                                Employee _locEmployeeXPO = _currSession.FindObject<Employee>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Active",true)
                                                                    )));
                                if (_locEmployeeXPO != null)
                                {
                                    XPCollection<EmployeePositionLine> _locEmployeePositionLines = new XPCollection<EmployeePositionLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Employee", _locEmployeeXPO),
                                                                    new GroupOperator(GroupOperatorType.Or,
                                                                    new BinaryOperator("Default", true)

                                                                    )));
                                    if (_locEmployeePositionLines != null && _locEmployeePositionLines.Count() > 0)
                                    {
                                        foreach (EmployeePositionLine EmployeePositionLine in _locEmployeePositionLines)
                                        {
                                            _locDefaultPosition = _locDefaultPosition + EmployeePositionLine.JobPosition;
                                            EmployeePositionLine.Save();
                                            EmployeePositionLine.Session.CommitTransaction();
                                        }
                                    }
                                    //_locEmployeeXPO.DefaultPosition = _locDefaultPosition; 
                                    _locEmployeeXPO.Save();
                                    _locEmployeeXPO.Session.CommitTransaction();

                                    //Message
                                    SuccsessMessageShow("Set Default Position has successfully updates");
                                }
                                else
                                {
                                    //Message error
                                    ErrorMessageShow("Data Default Not Available");
                                }

                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Data Default Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = Employee " + ex.ToString());
            }
        }

        #endregion SetDefaultPositionAction_Execute

        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}

