﻿using DE_SARAS.Module.BusinessObjects;
using DE_SARAS.Module.CustomProcess;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DE_SARAS.Module.Controllers.ActionList
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CallCenterActionController : ViewController
    {
        // Use CodeRush to create Controllers and Actions with a few keystrokes.
        // https://docs.devexpress.com/CodeRushForRoslyn/403133/
        public CallCenterActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewType = ViewType.Any;
            TargetObjectType = typeof(CallCenter);

            #region SendAction 

            SimpleAction sendCallCenterAction = new SimpleAction(this, "SendCallCenterAction", PredefinedCategory.View)
            {
                Caption = "Send",
                ConfirmationMessage = "Are you sure you want to Process the data?",
                ImageName = "Redo"
            };
            sendCallCenterAction.Execute += SendCallCenterAction_Execute;

            #endregion SendAction
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region SendCallCenterAction_Execute
        private void SendCallCenterAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                // Untuk mendapatkan ObjectSpace =>Session
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);

                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                TicketHelpType _dailyActivity = null;


                #region SEND

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CallCenter _locCallCenterOS = (CallCenter)_objectSpace.GetObject(obj);


                        if (_locCallCenterOS != null)
                        {
                            if (_locCallCenterOS.Session != null)
                            {
                                _currSession = _locCallCenterOS.Session;
                            }

                            if (_locCallCenterOS.Code != null && _currSession != null)
                            {
                                _currObjectId = _locCallCenterOS.Code;

                                CallCenter _locCallCenterXPO = _currSession.FindObject<CallCenter>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)
                                                                    ));

                                if (_locCallCenterXPO != null)
                                {
                                    XPCollection<CallCenterTaskBase> _locCallCenterTaskBases = new XPCollection<CallCenterTaskBase>
                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("CallCenter", _locCallCenterXPO)
                                                                         ));
                                    if (_locCallCenterTaskBases != null && _locCallCenterTaskBases.Count() > 0)
                                    {
                                        //Message Error
                                        ErrorMessageShow("YOUR TICKET HAS BEEN SENT");
                                    }
                                    else
                                    {
                                        if(_locCallCenterXPO.StatusTicket == StatusTicket.Open)
                                        {
                                            TicketHelpType _locTicketHelpType = _currSession.FindObject<TicketHelpType>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Active", true),
                                                                                new BinaryOperator("HelpType", "Daily Activity")
                                                                                ));

                                            if (_locTicketHelpType != null)
                                            {
                                                _dailyActivity = _locTicketHelpType;
                                            }

                                            if (_locCallCenterXPO.HelpType == _dailyActivity)
                                            {
                                                XPCollection<CallCenterDailyActivity> _locCallCenterDailyActivitys = new XPCollection<CallCenterDailyActivity>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CallCenter", _locCallCenterXPO)
                                                                            ));
                                                if (_locCallCenterDailyActivitys != null && _locCallCenterDailyActivitys.Count() > 0)
                                                {
                                                    //Message Error
                                                    ErrorMessageShow("YOUR TICKET HAS BEEN SENT");
                                                }
                                                else
                                                {
                                                    CallCenterDailyActivity _saveCallCenterDailyActivity = new CallCenterDailyActivity(_currSession)
                                                    {
                                                        CallCenter = _locCallCenterXPO,
                                                        HelpType = _locCallCenterXPO.HelpType,
                                                        TopicType = _locCallCenterXPO.TopicType,
                                                        RequestCallCenter = _locCallCenterXPO.Employee,
                                                        PicCallCenter = _locCallCenterXPO.Employee,
                                                        StatusTicket = StatusTicket.Progress,
                                                        TicketSubject = _locCallCenterXPO.TicketSubject,
                                                        TicketDescription = _locCallCenterXPO.TicketDescription,
                                                    };
                                                    _saveCallCenterDailyActivity.Save();
                                                    _saveCallCenterDailyActivity.Session.CommitTransaction();

                                                    //update Status di Call Center 
                                                    _locCallCenterXPO.StatusTicket = StatusTicket.Closed;
                                                    _locCallCenterXPO.Delete();
                                                    _locCallCenterXPO.Session.CommitTransaction();
                                                    //end update status


                                                    //Message
                                                    SuccsessMessageShow("Send Ticket has successfully");
                                                }
                                            }
                                            else
                                            {
                                                CallCenterTaskBase _saveCallCenterTaskBase = new CallCenterTaskBase(_currSession)
                                                {
                                                    CallCenter = _locCallCenterXPO,
                                                    HelpType = _locCallCenterXPO.HelpType,
                                                    TopicType = _locCallCenterXPO.TopicType,
                                                    RequestCallCenter = _locCallCenterXPO.Employee,
                                                    StatusTicket = StatusTicket.Open,
                                                    TicketSubject = _locCallCenterXPO.TicketSubject,
                                                    TicketDescription = _locCallCenterXPO.TicketDescription,
                                                };
                                                _saveCallCenterTaskBase.Save();
                                                _saveCallCenterTaskBase.Session.CommitTransaction();

                                                //Message
                                                SuccsessMessageShow("Send Ticket has successfully");
                                            }
                                        }
                                        else
                                        {
                                            //Message Error
                                            ErrorMessageShow("Your Ticket is On Progress");
                                        }

                                    }
                                }
                            }
                            else
                            {
                                //Message Error
                                ErrorMessageShow("Ticket Not Available");
                            }

                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

                #endregion SEND
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CallCenter " + ex.ToString());
            }
        }

        #endregion SendCallCenterAction_Execute


        #region Global Method
        private void SuccsessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
